package pages.Asesoria;

import constants.Constants;
import driver.DriverContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.qameta.allure.Step;
import org.openqa.selenium.support.PageFactory;
import reporter.EstadoPrueba;
import reporter.PdfBciReports;
import util.AccionesGenericas;
import util.Gestures;

import java.util.List;

public class AsesorarPerfilarPregunta3 {

    private AppiumDriver driver;

    public AsesorarPerfilarPregunta3() {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver), this);
    }

    /*********************************************************************
     * OBJETOS
     **********************************************************************/

    //Título asesoría de inversiones
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/toolbar_title")
    @iOSFindBy(accessibility = "Asesoría de Inversiones")
    private MobileElement tituloAsesoria;

    //Botón cerrar
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/action_close")
    @iOSFindBy(accessibility = "close")
    private MobileElement btnCerrar;

    //Flecha atrás progreso
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/bt_progress")
    @iOSFindBy(accessibility = "UIView.ProgressBarPager.PreviousStepButton")
    private MobileElement flechaAtrasProgreso;

    //Barra progreso
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/horizontal_progress_bar")
    @iOSFindBy(accessibility = "UIView.ProgressBarPager.ProgressBarView")
    private MobileElement barraProgreso;

    //Stepper
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_stepper_indicator")
    @iOSFindBy(accessibility = "UIView.ProgressBarPager.StepIndicatorLabel")
    private MobileElement stepper;

    //Pregunta
    @iOSFindBy(accessibility = "¿Cuál es tu conocimiento del mundo financiero?")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_labelPregunta")
    private MobileElement pregunta;

    //Elige una opción
    @iOSFindBy(accessibility = "Elige una opción")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_labelSelectOption")
    private MobileElement labelEligeOpcion;
    /*********************************************************************
     *RADIO BUTTONS Y ALTERNATIVAS IOS
     **********************************************************************/

    //Radio button respuesta 1
    @iOSFindBy(accessibility = "AlternativeTableViewCell.127RadioButton")
    private MobileElement rdoRespuesta1;

    //Respuesta 1
    @iOSFindBy(accessibility = "Escenario hipotético A")
    private MobileElement respuesta1;

    //Radio button respuesta 2
    @iOSFindBy(accessibility = "AlternativeTableViewCell.128RadioButton")
    private MobileElement rdoRespuesta2;

    //Respuesta 2
    @iOSFindBy(accessibility = "Escenario hipotético B")
    private MobileElement respuesta2;

    //Radio button respuesta 3
    @iOSFindBy(accessibility = "AlternativeTableViewCell.129RadioButton")
    private MobileElement rdoRespuesta3;

    //Respuesta 3
    @iOSFindBy(accessibility = "Escenario hipotético C")
    private MobileElement respuesta3;

    /*********************************************************************
     *RADIO BUTTONS Y ALTERNATIVAS ANDROID
     **********************************************************************/

    //Radio buttons
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/radioButton_alternativa")
    private List<MobileElement> rdoRespuestas;

    //Respuestas
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_alternativa")
    private List<MobileElement> respuestas;

    //Imagenes
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/iv_alternativaImage")
    private List<MobileElement> imagenes;


    /*********************************************************************
     *METODOS
     **********************************************************************/

    @Step
    public void validarIngresoPantalla() {
        AccionesGenericas.ingresoPantalla(this.respuesta1, "Pregunta 5 Asesoría");
    }

    @Step
    public void validarElementosPreguntaRespuestas() {

        Gestures ges = new Gestures();

//        AccionesGenericas.validarElemento(this.pregunta, "En el supuesto que inviertes $10.000.000 y que el resultado de tu inversión al cabo de un año puede tener 3 resultados posibles, ¿con qué resultado de inversión te sentirías más cómodo?", 5);
        AccionesGenericas.validarElemento(this.labelEligeOpcion, "Elige una opción", 5);
        AccionesGenericas.validarElemento(this.stepper, "5 de 8", 5);
        if (DriverContext.getDriver() instanceof IOSDriver) {
//            AccionesGenericas.validarElemento(this.rdoRespuesta1, 5);
            AccionesGenericas.validarElemento(this.respuesta1, "Escenario hipotético A", 5);
//            AccionesGenericas.validarElemento(this.rdoRespuesta2, 5);
            AccionesGenericas.validarElemento(this.respuesta2, "Escenario hipotético B", 5);
            ges.scrollAbajo();
//            AccionesGenericas.validarElemento(this.rdoRespuesta3, 5);
            AccionesGenericas.validarElemento(this.respuesta3, "Escenario hipotético C", 5);
            ges.scrollArriba();
        } else {
            AccionesGenericas.validarElemento(this.rdoRespuestas.get(0), 5);
            AccionesGenericas.validarElemento(this.respuestas.get(0), "Escenario hipotético A", 5);
            AccionesGenericas.validarElemento(this.imagenes.get(0), 5);
            AccionesGenericas.validarElemento(this.rdoRespuestas.get(1), 5);
            AccionesGenericas.validarElemento(this.respuestas.get(1), "Escenario hipotético B", 5);
            AccionesGenericas.validarElemento(this.imagenes.get(1), 5);
            ges.scrollAbajo();
            AccionesGenericas.validarElemento(this.rdoRespuestas.get(1), 5);
            AccionesGenericas.validarElemento(this.respuestas.get(1), "Escenario hipotético C", 5);
            AccionesGenericas.validarElemento(this.imagenes.get(2), 5);
            ges.scrollArriba();
        }
    }

    @Step
    public void seleccionarRespuesta(String respuesta) {
        Gestures ges = new Gestures();
        if (DriverContext.getDriver() instanceof IOSDriver) {
            switch (respuesta) {
                case "1":
                    this.respuesta1.click();
                    break;
                case "2":
                    this.respuesta2.click();
                    break;
                case "3":
                    ges.scrollAbajo();
                    this.respuesta3.click();
                    break;
            }
        } else {
            if (respuesta.equals("3")) {
                ges.scrollAbajo();
            }
            PdfBciReports.addReport("Seleccionar Respuesta", "Se hace tap a la respuesta " + this.respuestas.get(Integer.parseInt(respuesta) - 1).getText(), EstadoPrueba.PASSED, false);
            this.respuestas.get(Integer.parseInt(respuesta) - 1).click();
        }
    }
}
