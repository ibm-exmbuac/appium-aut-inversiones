package pages.Asesoria;

import constants.Constants;
import driver.DriverContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.qameta.allure.Step;
import org.openqa.selenium.support.PageFactory;
import reporter.EstadoPrueba;
import reporter.PdfBciReports;
import util.AccionesGenericas;

import java.util.List;

public class AsesorarPerfilarPregunta4 {

    private AppiumDriver driver;

    public AsesorarPerfilarPregunta4() {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver), this);
    }

    /*********************************************************************
     * OBJETOS
     **********************************************************************/

    //Título asesoría de inversiones
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/toolbar_title")
    @iOSFindBy(accessibility = "Asesoría de Inversiones")
    private MobileElement tituloAsesoria;

    //Botón cerrar
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/action_close")
    @iOSFindBy(accessibility = "close")
    private MobileElement btnCerrar;

    //Flecha atrás progreso
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/bt_progress")
    @iOSFindBy(accessibility = "UIView.ProgressBarPager.PreviousStepButton")
    private MobileElement flechaAtrasProgreso;

    //Barra progreso
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/horizontal_progress_bar")
    @iOSFindBy(accessibility = "UIView.ProgressBarPager.ProgressBarView")
    private MobileElement barraProgreso;

    //Stepper
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_stepper_indicator")
    @iOSFindBy(accessibility = "UIView.ProgressBarPager.StepIndicatorLabel")
    private MobileElement stepper;

    //Pregunta
    @iOSFindBy(accessibility = "¿Cuál es tu conocimiento del mundo financiero?")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_labelPregunta")
    private MobileElement pregunta;

    //Elige una opción
    @iOSFindBy(accessibility = "Elige una opción")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_labelSelectOption")
    private MobileElement labelEligeOpcion;

    /*********************************************************************
     *RADIO BUTTONS Y ALTERNATIVAS IOS
     **********************************************************************/

    //Radio button respuesta 1
    @iOSFindBy(accessibility = "AlternativeTableViewCell.140RadioButton")
    private MobileElement rdoRespuesta1;

    //Respuesta 1
    @iOSFindBy(accessibility = "AsesoriaQuestionTableViewCell.140OptionLabel")
    private MobileElement respuesta1;

    //Radio button respuesta 2
    @iOSFindBy(accessibility = "AlternativeTableViewCell.141RadioButton")
    private MobileElement rdoRespuesta2;

    //Respuesta 2
    @iOSFindBy(accessibility = "AsesoriaQuestionTableViewCell.141OptionLabel")
    private MobileElement respuesta2;

    //Radio button respuesta 3
    @iOSFindBy(accessibility = "AlternativeTableViewCell.142RadioButton")
    private MobileElement rdoRespuesta3;

    //Respuesta 3
    @iOSFindBy(accessibility = "AsesoriaQuestionTableViewCell.142OptionLabel")
    private MobileElement respuesta3;

    //Radio button respuesta 4
    @iOSFindBy(accessibility = "AlternativeTableViewCell.143RadioButton")
    private MobileElement rdoRespuesta4;

    //Respuesta 4
    @iOSFindBy(accessibility = "AsesoriaQuestionTableViewCell.143OptionLabel")
    private MobileElement respuesta4;

    /*********************************************************************
     *RADIO BUTTONS Y ALTERNATIVAS ANDROID
     **********************************************************************/

    //Radio buttons
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/radioButton_alternativa")
    private List<MobileElement> rdoRespuestas;

    //Respuestas
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_alternativa")
    private List<MobileElement> respuestas;


    /*********************************************************************
     *METODOS
     **********************************************************************/

    @Step
    public void validarIngresoPantalla() {
        AccionesGenericas.ingresoPantalla(this.respuesta1, "Pregunta 6 Asesoría");
    }

    @Step
    public void validarElementosPreguntaRespuesta() {
//        AccionesGenericas.validarElemento(this.pregunta, "Si perdieras un 30% de tu inversión debido a una crisis económica mundial, ¿qué harías?", 5);
        AccionesGenericas.validarElemento(this.labelEligeOpcion, "Elige una opción", 5);
        AccionesGenericas.validarElemento(this.stepper, "6 de 8", 5);
        if (DriverContext.getDriver() instanceof IOSDriver) {
//            AccionesGenericas.validarElemento(this.rdoRespuesta1, 5);
//            AccionesGenericas.validarElemento(this.rdoRespuesta2, 5);
//            AccionesGenericas.validarElemento(this.rdoRespuesta3, 5);
//            AccionesGenericas.validarElemento(this.rdoRespuesta4, 5);
            AccionesGenericas.validarElemento(this.respuesta1, "Invertiría más.", 5);
            AccionesGenericas.validarElemento(this.respuesta2, "Me preocuparía, pero no harí\u00ADa ningún cambio.", 5);
            AccionesGenericas.validarElemento(this.respuesta3, "Vendería parte de lo que tengo e invertiría ese monto en un producto de menor riesgo.", 5);
            AccionesGenericas.validarElemento(this.respuesta4, "Vendería todo lo que tengo y lo invertiría en un producto de menor riesgo.", 5);
        } else {
            AccionesGenericas.validarElemento(this.rdoRespuestas.get(0), 5);
            AccionesGenericas.validarElemento(this.rdoRespuestas.get(1), 5);
            AccionesGenericas.validarElemento(this.rdoRespuestas.get(2), 5);
            AccionesGenericas.validarElemento(this.rdoRespuestas.get(3), 5);
            AccionesGenericas.validarElemento(this.respuestas.get(0), "Invertiría más.", 5);
            AccionesGenericas.validarElemento(this.respuestas.get(1), "Me preocuparía, pero no harí\u00ADa ningún cambio.", 5);
            AccionesGenericas.validarElemento(this.respuestas.get(2), "Vendería parte de lo que tengo e invertiría ese monto en un producto de menor riesgo.", 5);
            AccionesGenericas.validarElemento(this.respuestas.get(3), "Vendería todo lo que tengo y lo invertiría en un producto de menor riesgo.", 5);
        }
    }

    @Step
    public void seleccionarRespuesta(String respuesta) {
        if (DriverContext.getDriver() instanceof IOSDriver) {
            switch (respuesta) {
                case "1":
                    this.respuesta1.click();
                    break;
                case "2":
                    this.respuesta2.click();
                    break;
                case "3":
                    this.respuesta3.click();
                    break;
                case "4":
                    this.respuesta4.click();
                    break;
            }
        } else {
            PdfBciReports.addReport("Seleccionar Respuesta", "Se hace tap a la respuesta " + this.respuestas.get(Integer.parseInt(respuesta) - 1).getText(), EstadoPrueba.PASSED, false);
            this.respuestas.get(Integer.parseInt(respuesta) - 1).click();
        }
    }
}
