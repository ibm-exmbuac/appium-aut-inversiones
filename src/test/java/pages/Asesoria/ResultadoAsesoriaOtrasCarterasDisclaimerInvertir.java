package pages.Asesoria;

import constants.Constants;
import driver.DriverContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.qameta.allure.Step;
import org.openqa.selenium.support.PageFactory;
import reporter.EstadoPrueba;
import reporter.PdfBciReports;
import util.AccionesGenericas;
import util.Gestures;

public class ResultadoAsesoriaOtrasCarterasDisclaimerInvertir {

    private AppiumDriver driver;

    public ResultadoAsesoriaOtrasCarterasDisclaimerInvertir() {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver), this);
    }

    //Título otras alternativas de inversión
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_resultado_dap_title")
    @iOSFindBy(accessibility = "Otras alternativas de inversión")
    private MobileElement tituloOtrasAlts;

    //Título carteras autogestionadas
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_dap")
    @iOSFindBy(accessibility = "Carteras Autogestionadas")
    private MobileElement tituloCarteras;

    //Info carteras autogestionadas
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_dap_description")
    @iOSFindBy(accessibility = "Conoce y elige de acuerdo a tu perfil una de nuestras carteras de Fondos Mutuos Bci:")
    private MobileElement infoCarteras;

    //Flecha desplegar info carteras
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/iv_arrow")
    private MobileElement flechaDeplegarInfo;

    //Info 1
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_asesoria_cartera_autogestionada_ventaja_1")
    @iOSFindBy(accessibility = "Te permite invertir de manera diversificada y flexible")
    private MobileElement info1;

    //Info 2
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_asesoria_cartera_autogestionada_ventaja_2")
    @iOSFindBy(accessibility = "Puedes invertir desde $5.000")
    private MobileElement info2;

    //Botón ver carteras
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/btn_ver_carteras")
    @iOSFindBy(accessibility = "VER CARTERAS")
    private MobileElement btnVerCarteras;

    //Disclaimer
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_detalle_ffmm_disclaimer")
    @iOSFindBy(accessibility = "TextViewTableViewCell.descriptionTextView")
    private MobileElement disclaimer;

    //Botón ir a invertir
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/btn_detalle_ffmm_invertir")
    @iOSFindBy(accessibility = "IR A INVERTIR")
    private MobileElement btnIrAInvertir;

    //Botón volver a simular
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/btn_detalle_ffmm_volver_simular")
    @iOSFindBy(accessibility = "VOLVER A SIMULAR")
    private MobileElement btnVolverASimular;


    @Step
    public void validarElementos() {
        Gestures ges = new Gestures();
        ges.encontrarObjeto(infoCarteras, "Seccion carteras autogestionadas");
        AccionesGenericas.validarElemento(this.tituloOtrasAlts, "Otras alternativas de inversión", 5);
        AccionesGenericas.validarElemento(this.tituloCarteras, "Carteras Autogestionadas", 5);
        AccionesGenericas.validarElemento(this.infoCarteras, "Conoce y elige de acuerdo a tu perfil una de nuestras carteras de Fondos Mutuos Bci:", 5);
//        AccionesGenericas.validarElemento(this.flechaDeplegarInfo, 5);
//        this.flechaDeplegarInfo.click();
//        ges.encontrarObjeto(btnVerCarteras, "Seccion carteras autogestionadas");
//        AccionesGenericas.validarElemento(this.info1, "Te permite invertir de manera diversificada y flexible", 5);
//        AccionesGenericas.validarElemento(this.info2, "Puedes invertir desde $5.000", 5);
//        AccionesGenericas.validarElemento(this.btnVerCarteras, "VER CARTERAS", 5);
//        this.flechaDeplegarInfo.click();
        ges.encontrarObjeto(disclaimer, "Seccion disclaimer");
        AccionesGenericas.validarElemento(this.disclaimer, 5);
        ges.swipe(0.5, 0.7);
        AccionesGenericas.validarElemento(this.btnIrAInvertir, "IR A INVERTIR", 5);
//        AccionesGenericas.validarElemento(this.btnVolverASimular, "VOLVER A SIMULAR", 5);
    }

    @Step
    public void tapVerCarteras() {
        Gestures ges = new Gestures();
        ges.scrollAbajo();
        if (DriverContext.getDriver() instanceof IOSDriver) {

        } else{
            this.flechaDeplegarInfo.click();
        }
        this.btnVerCarteras.click();
        PdfBciReports.addReport("Tap Ver Carteras", "Se hace tap en botón ver carteras", EstadoPrueba.PASSED, false);
    }

    @Step
    public void tapIrAInvertir() {
        this.btnIrAInvertir.click();
        PdfBciReports.addReport("Tap Ir A Invertir", "Se hace tap a botón ir a invertir", EstadoPrueba.PASSED, false);
    }

    @Step
    public void tapVolverASimular() {
        this.btnVolverASimular.click();
        PdfBciReports.addReport("Tap Volver A Simular", "Se hace tap a botón volver a simular", EstadoPrueba.PASSED, false);
    }
}


