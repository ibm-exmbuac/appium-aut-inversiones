package pages.Asesoria;

import constants.Constants;
import driver.DriverContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import org.openqa.selenium.support.PageFactory;
import reporter.EstadoPrueba;
import reporter.PdfBciReports;
import util.AccionesGenericas;

import java.util.List;

public class AsesorarPregunta3 {

    private AppiumDriver driver;

    public AsesorarPregunta3() {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver), this);
    }


    /**********************
     * OBJETOS
     **********************/

    //Título asesoría de inversiones
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/toolbar_title")
    @iOSFindBy(accessibility = "Asesoría de Inversiones")
    private MobileElement tituloAsesoria;

    //Botón cerrar
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/action_close")
    @iOSFindBy(accessibility = "close")
    private MobileElement btnCerrar;

    //Flecha atrás progreso
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/bt_progress")
    @iOSFindBy(accessibility = "UIView.ProgressBarPager.PreviousStepButton")
    private MobileElement flechaAtrasProgreso;

    //Barra progreso
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/horizontal_progress_bar")
    @iOSFindBy(accessibility = "UIView.ProgressBarPager.ProgressBarView")
    private MobileElement barraProgreso;

    //Stepper
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_stepper_indicator")
    @iOSFindBy(accessibility = "UIView.ProgressBarPager.StepIndicatorLabel")
    private MobileElement stepper;

    //Pregunta
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_labelPregunta")
    @iOSFindBy(accessibility = "Si inviertes hoy, ¿Cuándo crees que utilizarás el dinero de tu inversión?")
    private MobileElement pregunta;

    //Elige una opción
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_labelSelectOption")
    @iOSFindBy(accessibility = "Elige una opción")
    private MobileElement eligeUnaOpcion;

    /*********************************************************************
     * RADIO BUTTONS Y ALTERNATIVAS IOS
     **********************************************************************/

    //Radio button respuesta 1
    @iOSFindBy(accessibility = "AsesoriaQuestionTableViewCell.148RadioButton")
    private MobileElement rdoRespuesta1;

    //Respuesta 1
    @iOSFindBy(accessibility = "AsesoriaQuestionTableViewCell.148OptionLabel")
    private MobileElement respuesta1;

    //Radio button respuesta 2
    @iOSFindBy(accessibility = "AsesoriaQuestionTableViewCell.149RadioButton")
    private MobileElement rdoRespuesta2;

    //Respuesta 2
    @iOSFindBy(accessibility = "AsesoriaQuestionTableViewCell.149OptionLabel")
    private MobileElement respuesta2;

    //Radio button respuesta 3
    @iOSFindBy(accessibility = "AsesoriaQuestionTableViewCell.150RadioButton")
    private MobileElement rdoRespuesta3;

    //Respuesta 3
    @iOSFindBy(accessibility = "AsesoriaQuestionTableViewCell.150OptionLabel")
    private MobileElement respuesta3;

    /*********************************************************************
     * RADIO BUTTONS Y ALTERNATIVAS ANDROID
     **********************************************************************/

    //Elige una opción
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/radioButton_alternativa")
    private List<MobileElement> rdoRespuestas;

    //Elige una opción
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_alternativa")
    private List<MobileElement> respuestas;


    /********************************
     *  MÉTODOS
     ********************************/

    public void validarIngresoPantalla() {
        AccionesGenericas.ingresoPantalla(this.pregunta, "Pregunta 3 Asesoría");
    }

    public void validaElementos() {
        AccionesGenericas.validarElemento(this.tituloAsesoria, "Asesoría de Inversiones", 5);
        AccionesGenericas.validarElemento(this.btnCerrar, 5);
        AccionesGenericas.validarElemento(this.flechaAtrasProgreso, 5);
        AccionesGenericas.validarElemento(this.barraProgreso, 5);
        AccionesGenericas.validarElemento(this.stepper, 5);
        AccionesGenericas.validarElemento(this.pregunta, "Si inviertes hoy, ¿Cuándo crees que utilizarás el dinero de tu inversión?", 5);
        AccionesGenericas.validarElemento(this.eligeUnaOpcion, "Elige una opción", 5);
        if (DriverContext.getDriver() instanceof IOSDriver) {
//            AccionesGenericas.validarElemento(this.rdoRespuesta1, 5);
            AccionesGenericas.validarElemento(this.respuesta1, "En menos de un año", 5);
//            AccionesGenericas.validarElemento(this.rdoRespuesta2, 5);
            AccionesGenericas.validarElemento(this.respuesta2, "Entre 1 y 2 años", 5);
//            AccionesGenericas.validarElemento(this.rdoRespuesta3, 5);
            AccionesGenericas.validarElemento(this.respuesta3, "Más de 2 años", 5);
        } else {
            AccionesGenericas.validarElemento(this.respuestas.get(0), 5);
            AccionesGenericas.validarElemento(this.respuestas.get(0), "En menos de un año", 5);
            AccionesGenericas.validarElemento(this.respuestas.get(1), 5);
            AccionesGenericas.validarElemento(this.respuestas.get(1), "Entre 1 y 2 años", 5);
            AccionesGenericas.validarElemento(this.respuestas.get(2), 5);
            AccionesGenericas.validarElemento(this.respuestas.get(2), "Más de 2 años", 5);
        }
    }

    public void seleccionarRespuesta(String respuesta) {
        if (DriverContext.getDriver() instanceof IOSDriver) {
            switch (respuesta) {
                case "1":
                    this.respuesta1.click();
                    PdfBciReports.addReport("Seleccionar Respuesta", "Se hace tap a la respuesta " + this.respuesta1.getText(), EstadoPrueba.PASSED, false);
                    break;
                case "2":
                    this.respuesta2.click();
                    PdfBciReports.addReport("Seleccionar Respuesta", "Se hace tap a la respuesta " + this.respuesta2.getText(), EstadoPrueba.PASSED, false);
                    break;
                case "3":
                    this.respuesta3.click();
                    PdfBciReports.addReport("Seleccionar Respuesta", "Se hace tap a la respuesta " + this.respuesta3.getText(), EstadoPrueba.PASSED, false);
                    break;
                default:
                    PdfBciReports.addMobilReportImage("Seleccionar Respuesta", "No se ha seleccionado ninguna respuesta", EstadoPrueba.FAILED, false);
            }
        } else {
            PdfBciReports.addReport("Seleccionar Respuesta", "Se hace tap a la respuesta " + this.respuestas.get(Integer.parseInt(respuesta) - 1).getText(), EstadoPrueba.PASSED, false);
            this.respuestas.get(Integer.parseInt(respuesta) - 1).click();
        }
    }
}