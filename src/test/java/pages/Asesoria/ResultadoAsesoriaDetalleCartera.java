package pages.Asesoria;

import constants.Constants;
import driver.DriverContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.qameta.allure.Step;
import org.openqa.selenium.support.PageFactory;
import util.AccionesGenericas;

public class ResultadoAsesoriaDetalleCartera {
    private AppiumDriver driver;

    public ResultadoAsesoriaDetalleCartera() {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver), this);
    }


    //Título resultado asesoría
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/toolbar_title")
    @iOSFindBy(accessibility = "Resultado de Asesoría")
    private MobileElement tituloResultadoAsesoria;

    //Botón cerrar
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/action_close")
    @iOSFindBy(accessibility = "close")
    private MobileElement btnCerrar;

    //Label tu perfil de inversionista
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_perfil_inversionista")
    @iOSFindBy(accessibility = "Tu perfil de inversionista:")
    private MobileElement tuPerfilDeInv;

    //Perfil
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_perfil")
    @iOSFindBy(xpath = "//XCUIElementTypeStaticText[contains(@name, \"PerfilClienteView.\")]")
    private MobileElement perfil;

    //Título FFMM
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_ffmm_nombre_label")
    @iOSFindBy(accessibility = "FondoMutuo.FondoMutuoTitleLbale")
    private MobileElement tituloFFMM;

    //Subtítulo FFMM
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_ffmm_nombre_value")
    @iOSFindBy(accessibility = "DetalleFondosMutuosViewController.TypeLabel")
    private MobileElement subtituloFFMM;

    //Info FFMM
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_ffmm_descripcion")
    @iOSFindBy(accessibility = "DetalleFondosMutuosViewController.DescriptionValue")
    private MobileElement infoFFMM;

    //Perfil de riesgo
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_ffmm_perfil")
    @iOSFindBy(accessibility = "DetalleFondosMutuosViewController.FondoPerfilRiesgoValue")
    private MobileElement perfilRiesgo;

    //Horizonte de inversion
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_ffmm_horizonte")
    @iOSFindBy(accessibility = "DetalleFondosMutuosViewController.HorozonteValue")
    private MobileElement horizonteInversion;

    //Serie
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_ffmm_serie")
    @iOSFindBy(accessibility = "DetalleFondosMutuosViewController.SerieValue")
    private MobileElement serie;

    //Monto mínimo de inversión
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_ffmm_monto_minimo")
    @iOSFindBy(accessibility = "DetalleFondosMutuosViewController.InversionMinimaValue")
    private MobileElement montoMinInv;

    @Step
    public void validarIngresoPantalla() {
        AccionesGenericas.ingresoPantalla(this.tituloResultadoAsesoria, "Resultado Asesoría");
    }

    @Step
    public void validarElementos() {
        AccionesGenericas.validarElemento(this.tituloResultadoAsesoria, 5);
        AccionesGenericas.validarElemento(this.btnCerrar, 5);
        AccionesGenericas.validarElemento(this.tuPerfilDeInv, "Tu perfil de inversionista:", 5);
        AccionesGenericas.validarElemento(this.perfil, 5);
        AccionesGenericas.validarElemento(this.tituloFFMM, 5);
        AccionesGenericas.validarElemento(this.subtituloFFMM, 5);
        AccionesGenericas.validarElemento(this.infoFFMM, 5);
        AccionesGenericas.validarElemento(this.perfilRiesgo, 5);
        AccionesGenericas.validarElemento(this.horizonteInversion, 5);
        AccionesGenericas.validarElemento(this.serie, 5);
        AccionesGenericas.validarElemento(this.montoMinInv, 5);
    }
}

