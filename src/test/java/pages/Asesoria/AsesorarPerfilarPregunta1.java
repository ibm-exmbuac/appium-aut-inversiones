package pages.Asesoria;

import constants.Constants;
import driver.DriverContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.qameta.allure.Step;
import org.openqa.selenium.support.PageFactory;
import reporter.EstadoPrueba;
import reporter.PdfBciReports;
import util.AccionesGenericas;

import java.util.List;

public class AsesorarPerfilarPregunta1 {

    private AppiumDriver driver;

    public AsesorarPerfilarPregunta1() {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver), this);
    }

    /*********************************************************************
     * OBJETOS
     **********************************************************************/

    //Título asesoría de inversiones
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/toolbar_title")
    @iOSFindBy(accessibility = "Asesoría de Inversiones")
    private MobileElement tituloAsesoria;

    //Botón cerrar
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/action_close")
    @iOSFindBy(accessibility = "close")
    private MobileElement btnCerrar;

    //Flecha atrás progreso
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/bt_progress")
    @iOSFindBy(accessibility = "UIView.ProgressBarPager.PreviousStepButton")
    private MobileElement flechaAtrasProgreso;

    //Barra progreso
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/horizontal_progress_bar")
    @iOSFindBy(accessibility = "UIView.ProgressBarPager.ProgressBarView")
    private MobileElement barraProgreso;

    //Stepper
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_stepper_indicator")
    @iOSFindBy(accessibility = "UIView.ProgressBarPager.StepIndicatorLabel")
    private MobileElement stepper;

    //Pregunta
    @iOSFindBy(accessibility = "¿Cuál es tu conocimiento del mundo financiero?")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_labelPregunta")
    private MobileElement pregunta;

    //Elige una opción
    @iOSFindBy(accessibility = "Elige una opción")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_labelSelectOption")
    private MobileElement labelEligeOpcion;
    /*********************************************************************
     *RADIO BUTTONS Y ALTERNATIVAS IOS
     **********************************************************************/

    //Radio button respuesta 1
    @iOSFindBy(accessibility = "AlternativeTableViewCell.127RadioButton")
    private MobileElement rdoRespuesta1;

    //Respuesta 1
    @iOSFindBy(accessibility = "AsesoriaQuestionTableViewCell.127OptionLabel")
    private MobileElement respuesta1;

    //Radio button respuesta 2
    @iOSFindBy(accessibility = "AlternativeTableViewCell.128RadioButton")
    private MobileElement rdoRespuesta2;

    //Respuesta 2
    @iOSFindBy(accessibility = "AsesoriaQuestionTableViewCell.128OptionLabel")
    private MobileElement respuesta2;

    //Radio button respuesta 3
    @iOSFindBy(accessibility = "AlternativeTableViewCell.129RadioButton")
    private MobileElement rdoRespuesta3;

    //Respuesta 3
    @iOSFindBy(accessibility = "AsesoriaQuestionTableViewCell.129OptionLabel")
    private MobileElement respuesta3;

    //Radio button respuesta 4
    @iOSFindBy(accessibility = "AlternativeTableViewCell.130RadioButton")
    private MobileElement rdoRespuesta4;

    //Respuesta 4
    @iOSFindBy(accessibility = "AsesoriaQuestionTableViewCell.130OptionLabel")
    private MobileElement respuesta4;

    /*********************************************************************
     *RADIO BUTTONS Y ALTERNATIVAS ANDROID
     **********************************************************************/

    //Radio buttons
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/radioButton_alternativa")
    private List<MobileElement> rdoRespuestas;

    //Respuestas
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_alternativa")
    private List<MobileElement> respuestas;


    /*********************************************************************
     *METODOS
     **********************************************************************/

    @Step
    public void validarIngresoPantalla() {
        AccionesGenericas.ingresoPantalla(tituloAsesoria, "Pregunta 2 Asesoría");
    }

    @Step
    public void validarElementosPreguntaRespuestas() {
        AccionesGenericas.validarElemento(pregunta, "¿Cuál es tu conocimiento del mundo financiero?", 5);
        AccionesGenericas.validarElemento(labelEligeOpcion, "Elige una opción", 5);
        AccionesGenericas.validarElemento(stepper, "2 de 8", 5);
        if (DriverContext.getDriver() instanceof IOSDriver) {
//            AccionesGenericas.validarElemento(rdoRespuesta1, 5);
//            AccionesGenericas.validarElemento(rdoRespuesta2, 5);
//            AccionesGenericas.validarElemento(rdoRespuesta3, 5);
//            AccionesGenericas.validarElemento(rdoRespuesta4, 5);
            AccionesGenericas.validarElemento(respuesta1, "No tengo conocimientos.", 5);
            AccionesGenericas.validarElemento(respuesta2, "Entiendo lo que es, pero no sigo el mundo financiero.", 5);
            AccionesGenericas.validarElemento(respuesta3, "Tengo conocimientos, pero no soy experto en finanzas.", 5);
            AccionesGenericas.validarElemento(respuesta4, "Tengo conocimientos y me considero experto en finanzas.", 5);
        } else {
            AccionesGenericas.validarElemento(respuestas.get(0), "No tengo conocimientos.", 5);
            AccionesGenericas.validarElemento(rdoRespuestas.get(0), 5);
            AccionesGenericas.validarElemento(respuestas.get(1), "Entiendo lo que es, pero no sigo el mundo financiero.", 5);
            AccionesGenericas.validarElemento(rdoRespuestas.get(1), 5);
            AccionesGenericas.validarElemento(respuestas.get(2), "Tengo conocimientos, pero no soy experto en finanzas.", 5);
            AccionesGenericas.validarElemento(rdoRespuestas.get(2), 5);
            AccionesGenericas.validarElemento(respuestas.get(3), "Tengo conocimientos y me considero experto en finanzas.", 5);
            AccionesGenericas.validarElemento(rdoRespuestas.get(3), 5);
        }
    }

    @Step
    public void seleccionarRespuesta(String respuesta) {
        if (DriverContext.getDriver() instanceof IOSDriver) {
            switch (respuesta) {
                case "1":
                    respuesta1.click();
                    break;
                case "2":
                    respuesta2.click();
                    break;
                case "3":
                    respuesta3.click();
                    break;
                case "4":
                    respuesta4.click();
            }
        } else {
            PdfBciReports.addReport("SeleccionarRespuesta", "Se hace tap a la respuesta " + this.respuestas.get(Integer.parseInt(respuesta) - 1).getText(), EstadoPrueba.PASSED, false);
            respuestas.get(Integer.parseInt(respuesta) - 1).click();
        }
    }
}
