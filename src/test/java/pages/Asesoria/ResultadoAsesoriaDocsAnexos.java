package pages.Asesoria;

import constants.Constants;
import driver.DriverContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.qameta.allure.Step;
import org.openqa.selenium.support.PageFactory;
import reporter.EstadoPrueba;
import reporter.PdfBciReports;
import util.AccionesGenericas;
import util.Gestures;

import java.util.List;

public class ResultadoAsesoriaDocsAnexos {

    private AppiumDriver driver;

    public ResultadoAsesoriaDocsAnexos() {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver), this);
    }

    //Botón documentos
    @AndroidFindBy(xpath = "//*[contains(@text,\"DOCUMENTOS\")]")
    @iOSFindBy(accessibility = "DOCUMENTOS")
    private MobileElement btnDocumentos;

    //Título documentos anexos
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_ffmm_documentos_label")
    @iOSFindBy(accessibility = "Documentos anexos")
    private MobileElement tituloDocAnexos;

    //Documento anexo 1
    @iOSFindBy(accessibility = "DetalleFondosMutuosPresenter.Document0")
    private MobileElement docAnexo1;

    //Documento anexo 2
    @iOSFindBy(accessibility = "DetalleFondosMutuosPresenter.Document1")
    private MobileElement docAnexo2;

    //Documento anexo 3
    @iOSFindBy(accessibility = "DetalleFondosMutuosPresenter.Document2")
    private MobileElement docAnexo3;

    //Bóton ver más / menos IOS
    @iOSFindBy(accessibility = "ActionTableViewCell.actionButton")
    private MobileElement btnVerMasMenos;

    //Botón ver más Android
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/btn_ver_mas")
    private MobileElement btnVerMas;

    //Botón ver menos Android
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/btn_ver_menos")
    private MobileElement btnVerMenos;

    //Documento anexo 4
    @iOSFindBy(accessibility = "DetalleFondosMutuosPresenter.Document3")
    private MobileElement docAnexo4;

    //Documento anexo 5
    @iOSFindBy(accessibility = "DetalleFondosMutuosPresenter.Document4")
    private MobileElement docAnexo5;

    //Documento anexo 6
    @iOSFindBy(accessibility = "DetalleFondosMutuosPresenter.Document5")
    private MobileElement docAnexo6;

    //Documento anexo 7
    @iOSFindBy(accessibility = "DetalleFondosMutuosPresenter.Document6")
    private MobileElement docAnexo7;

    //Documento anexo 8
    @iOSFindBy(accessibility = "DetalleFondosMutuosPresenter.Document7")
    private MobileElement docAnexo8;

    //Docs anexos ANDROID
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_ffmm_documentos")
    private List<MobileElement> docsAnexos;

    /*********************************************************************
     *METODOS
     **********************************************************************/

    @Step
    public void validarElementos() {
        Gestures ges = new Gestures();
        if (DriverContext.getDriver() instanceof IOSDriver) {

        } else {
            AccionesGenericas.validarElemento(this.btnDocumentos, "DOCUMENTOS", 5);
            AccionesGenericas.validarElemento(this.tituloDocAnexos, "Documentos anexos", 5);
            int ciclo = 1;
            for (int i = 0; i < this.docsAnexos.size(); i++) {

                boolean existe = utils.MetodosGenericos.visualizarObjeto(this.docsAnexos.get(i), 5);
                if (existe) {
                    PdfBciReports.addReport("Validar docs anexos", "Se encuentra el doc anexo" + this.docsAnexos.get(i).getText(), EstadoPrueba.PASSED, false);
                } else {
                    PdfBciReports.addReport("Validar docs anexos", "No se encuentra el doc anexo" + this.docsAnexos.get(i).getText(), EstadoPrueba.FAILED, false);
                }
            }
        }
    }

    @Step
    public void tapDocsAnexos() {
        this.btnDocumentos.click();
    }


}
