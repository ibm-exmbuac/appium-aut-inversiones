package pages.Asesoria;

import constants.Constants;
import driver.DriverContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.qameta.allure.Step;
import org.openqa.selenium.support.PageFactory;
import util.AccionesGenericas;

public class ResultadoAsesoriaValorCuota {

    private AppiumDriver driver;

    public ResultadoAsesoriaValorCuota() {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver), this);
    }

    //Botón valor cuota
    @AndroidFindBy(xpath = "//*[contains(@text,\"VALOR CUOTA\")]")
    @iOSFindBy(accessibility = "VALOR CUOTA")
    private MobileElement btnValorCuota;

    //Titulo evolución valor cuota
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_ffmm_rentabilidad_nominal_label")
    @iOSFindBy(accessibility = "Evolución Valor Cuota")
    private MobileElement tituloEvoValorCuota;

    //Botón valor cuota actual
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_ffmm_rentabilidad_valor_cuota_label")
    @iOSFindBy(accessibility = "Valor cuota actual")
    private MobileElement btnValorCuotaActual;

    //Value valor cuota actual
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_ffmm_rentabilidad_valor_cuota_value")
    @iOSFindBy(accessibility = "ValorCuotaViewController.ValorCuotaActualLabel")
    private MobileElement valueValorCuotaActual;

    //Label últimos 12 meses
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_ffmm_rentabilidad_ultimos_meses_label")
    @iOSFindBy(accessibility = "Últimos 12 meses")
    private MobileElement labelUltimos12Meses;

    //Gráfico
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/lineChart")
    @iOSFindBy(accessibility = "Line Chart. 1 dataset.")
    private MobileElement grafico;

    /************************************************
     * OBJETOS MODAL INFO
     ************************************************/

    //Título modal
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_title")
    @iOSFindBy(xpath = "(//XCUIElementTypeStaticText[@name=\"DetalleFondosMutuosViewController.BottomMessage.TitleLab\"])[2]")
    private MobileElement tituloModal;

    //Info modal
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_description")
    @iOSFindBy(xpath = "(//XCUIElementTypeStaticText[@name=\"DetalleFondosMutuosViewController.BottomMessage.MessageLabel\"])[2]")
    private MobileElement infoModal;

    //Botón entendido
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/cb_close")
    @iOSFindBy(xpath = "(//XCUIElementTypeButton[@name=\"DetalleFondosMutuosViewController.BottomMessage.FirstButton\"])[2]")
    private MobileElement btnEntendido;


    @Step
    public void validarElementos() {
        AccionesGenericas.validarElemento(this.btnValorCuota, "VALOR CUOTA", 5);
        AccionesGenericas.validarElemento(this.tituloEvoValorCuota, "Evolución Valor Cuota", 5);
//        AccionesGenericas.validarElemento(this.btnValorCuotaActual, "Valor Cuota", 5);
//        AccionesGenericas.validarElemento(this.valueValorCuotaActual, 5);
        AccionesGenericas.validarElemento(this.labelUltimos12Meses, "Últimos 12 meses", 5);
//        AccionesGenericas.validarElemento(this.grafico, 5);
    }

    @Step
    public void validarValorCuotaActual() {
        this.btnValorCuotaActual.click();
        AccionesGenericas.validarElemento(this.tituloModal, "Valor Cuota", 5);
        AccionesGenericas.validarElemento(this.infoModal, "Es el precio por cuota de un fondo, cuyo valor puede variar conforme a las ganancias o pérdidas de las inversiones del fondo y es informado diariamente.", 5);
        AccionesGenericas.validarElemento(this.btnEntendido, "ENTENDIDO", 5);
        this.btnEntendido.click();
    }

    @Step
    public void tapValorCuota() {
        this.btnValorCuota.click();
    }
}
