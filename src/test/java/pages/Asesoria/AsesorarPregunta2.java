package pages.Asesoria;

import constants.Constants;
import driver.DriverContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.qameta.allure.Step;
import org.openqa.selenium.support.PageFactory;
import reporter.EstadoPrueba;
import reporter.PdfBciReports;
import util.AccionesGenericas;

public class AsesorarPregunta2 {

    private AppiumDriver driver;

    public AsesorarPregunta2() {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver), this);
    }

    //Título asesoría de inversiones
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/toolbar_title")
    @iOSFindBy(accessibility = "Asesoría de Inversiones")
    private MobileElement tituloAsesoria;

    //Botón cerrar
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/action_close")
    @iOSFindBy(accessibility = "close")
    private MobileElement btnCerrar;

    //Flecha atrás progreso
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/bt_progress")
    @iOSFindBy(accessibility = "UIView.ProgressBarPager.PreviousStepButton")
    private MobileElement flechaAtrasProgreso;

    //Barra progreso
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/horizontal_progress_bar")
    @iOSFindBy(accessibility = "UIView.ProgressBarPager.ProgressBarView")
    private MobileElement barraProgreso;

    //Stepper
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_stepper_indicator")
    @iOSFindBy(accessibility = "UIView.ProgressBarPager.StepIndicatorLabel")
    private MobileElement stepper;

    //Pregunta
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_labelPregunta")
    @iOSFindBy(accessibility = "¿Cuánto dinero tienes pensado invertir?")
    private MobileElement pregunta;

    //Ingresa monto
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/ingrese_monto_label")
    @iOSFindBy(accessibility = "IngresoMonto.titleLabel")
    private MobileElement labelIngresaMonto;

    //Input monto
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/monto_input")
    @iOSFindBy(accessibility = "IngresoMonto.montoLabel")
    private MobileElement inputMonto;

    //Botón continuar
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/btn_continuar")
    @iOSFindBy(accessibility = "AsesoriaQuestionMontoInputTableViewCell.ContinuarButton")
    private MobileElement btnContinuar;


    @Step
    public void checkIngresoPantalla() {
        AccionesGenericas.ingresoPantalla(this.pregunta, "Pregunta Asesoría");
    }

    @Step
    public void validaElementos() {
        AccionesGenericas.validarElemento(this.tituloAsesoria, "Asesoría de Inversiones", 5);
        AccionesGenericas.validarElemento(this.tituloAsesoria, "Asesoría de Inversiones", 5);
        AccionesGenericas.validarElemento(this.btnCerrar, 5);
        AccionesGenericas.validarElemento(this.flechaAtrasProgreso, 5);
        AccionesGenericas.validarElemento(this.barraProgreso, 5);
        AccionesGenericas.validarElemento(this.stepper, 5);
        AccionesGenericas.validarElemento(this.pregunta, "¿Cuánto dinero tienes pensado invertir?", 5);
        AccionesGenericas.validarElemento(this.labelIngresaMonto, "Ingresa monto", 5);
        AccionesGenericas.validarElemento(this.barraProgreso, 5);
        AccionesGenericas.validarElemento(this.btnContinuar, "CONTINUAR", 5);

    }

    @Step
    public void ingresoMonto(String montoAsesoria) {
        this.inputMonto.setValue(montoAsesoria);
        PdfBciReports.addMobilReportImage("Ingresar Monto", "Se ingresa el monto: " + montoAsesoria, EstadoPrueba.PASSED, false);
    }

    @Step
    public void tapBotonContinuar() {
        if (this.btnContinuar.isEnabled()) {
            this.btnContinuar.click();
            PdfBciReports.addMobilReportImage("Tap Boton continuar", "Se hace tap a botón continuar", EstadoPrueba.PASSED, false);
        } else {
            PdfBciReports.addMobilReportImage("Tap Boton continuar", "Botón continuar no se encuentra habilitado", EstadoPrueba.FAILED, false);
        }
    }
}
