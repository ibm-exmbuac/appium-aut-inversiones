package pages.Asesoria;

import constants.Constants;
import driver.DriverContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.qameta.allure.Step;
import org.openqa.selenium.support.PageFactory;
import util.AccionesGenericas;
import util.Gestures;

public class ResultadoAsesoriaEstadisticaRentabilidad {

    private AppiumDriver driver;

    public ResultadoAsesoriaEstadisticaRentabilidad() {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver), this);
    }


    //Botón rentabilidad
    @AndroidFindBy(xpath = "//*[contains(@text,\"RENTABILIDAD\")]")
    @iOSFindBy(accessibility = "RENTABILIDAD")
    private MobileElement btnRentabilidad;

    //Título estadística de rentabilidad nominal
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_ffmm_rentabilidad_nominal_label")
    @iOSFindBy(accessibility = "Estadística de Rentabilidad Nominal")
    private MobileElement tituloEstadisticaRentabilidad;

    //Label rentabilidad mensual
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_ffmm_rentabilidad_mensual_label")
    @iOSFindBy(accessibility = "RentabilidadNominalViewController.RentabilidadMensualTitle")
    private MobileElement labelRentabilidadMensual;

    //Value rentabilidad mensual
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_ffmm_rentabilidad_mensual_value")
    @iOSFindBy(accessibility = "RentabilidadNominalViewController.RentabilidadMensualValue")
    private MobileElement valueRentabilidadMensual;

    //Label rentabilidad trimestral
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_ffmm_rentabilidad_trimestral_label")
    @iOSFindBy(accessibility = "RentabilidadNominalViewController.RentabilidadTrimestralTitle")
    private MobileElement labelRentabilidadTrimestral;

    //Value rentabilidad trimestral
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_ffmm_rentabilidad_trimestral_value")
    @iOSFindBy(accessibility = "RentabilidadNominalViewController.RentabilidadTrimestralValue")
    private MobileElement valueRentabilidadTrimestral;

    //Botón rentabilidad Anual
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_ffmm_rentabilidad_anual_label")
    @iOSFindBy(accessibility = "Rentabilidad Anual")
    private MobileElement btnRentabilidadAnual;

    //Value rentabilidad anual
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_ffmm_rentabilidad_anual_value")
    @iOSFindBy(xpath = "(//XCUIElementTypeStaticText[@name=\"RentabilidadNominalViewController.RentabilidadAnualValue\"])[1]\n")
    private MobileElement valueRentabilidadAnual;

    //Botón acumulado en el año
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_ffmm_rentabilidad_acumulado_anio_label")
    @iOSFindBy(accessibility = "RentabilidadNominalViewController.RentabilidadAcumuladoTitle")
    private MobileElement btnAcumuladoAnio;

    //Value acumulado en el año
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_ffmm_rentabilidad_acumulado_anio_value")
    @iOSFindBy(xpath = "(//XCUIElementTypeStaticText[@name=\"RentabilidadNominalViewController.RentabilidadAnualValue\"])[2]")
    private MobileElement valueAcumuladoAnio;

    /************************************************
     * OBJETOS MODAL INFO
     ************************************************/

    //Título modal
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_title")
    @iOSFindBy(xpath = "(//XCUIElementTypeStaticText[@name=\"DetalleFondosMutuosViewController.BottomMessage.TitleLab\"])[2]")
    private MobileElement tituloModal;

    //Info modal
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_description")
    @iOSFindBy(xpath = "(//XCUIElementTypeStaticText[@name=\"DetalleFondosMutuosViewController.BottomMessage.MessageLabel\"])[2]")
    private MobileElement infoModal;

    //Botón entendido
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/cb_close")
    @iOSFindBy(xpath = "(//XCUIElementTypeButton[@name=\"DetalleFondosMutuosViewController.BottomMessage.FirstButton\"])[2]")
    private MobileElement btnEntendido;


    @Step
    public void validarElementos() {
        Gestures gestures = new Gestures();
        gestures.encontrarObjeto(valueAcumuladoAnio, "Sección Rentabilidad");
        AccionesGenericas.validarElemento(this.btnRentabilidad, "RENTABILIDAD", 5);
        AccionesGenericas.validarElemento(this.tituloEstadisticaRentabilidad, "Estadística de Rentabilidad Nominal", 5);
        AccionesGenericas.validarElemento(this.labelRentabilidadMensual, "Rentabilidad Mensual", 5);
        AccionesGenericas.validarElemento(this.valueRentabilidadMensual, 5);
        AccionesGenericas.validarElemento(this.labelRentabilidadTrimestral, "Rentabilidad Trimestral", 5);
        AccionesGenericas.validarElemento(this.valueRentabilidadTrimestral, 5);
//        AccionesGenericas.validarElemento(this.btnRentabilidadAnual, "Rentabilidad Anual", 5);
//        AccionesGenericas.validarElemento(this.valueRentabilidadAnual, 5);
        AccionesGenericas.validarElemento(this.btnAcumuladoAnio, 5);
        AccionesGenericas.validarElemento(this.valueAcumuladoAnio, 5);
    }

    @Step
    public void validarRentabilidadAnual() {
        this.btnRentabilidadAnual.click();
        AccionesGenericas.validarElemento(this.tituloModal, "Rentabilidad Anual", 5);
        AccionesGenericas.validarElemento(this.infoModal, "Corresponde a la variación porcentual del valor cuota entre el 1 de enero y 31 de diciembre del año pasado.", 5);
        AccionesGenericas.validarElemento(this.btnEntendido, "ENTENDIDO", 5);
        this.btnEntendido.click();
    }

    @Step
    public void validarAcumuladoAnio() {
        this.btnAcumuladoAnio.click();
        AccionesGenericas.validarElemento(this.tituloModal, "Acumulado en el Año (YTD)", 5);
        AccionesGenericas.validarElemento(this.infoModal, "Corresponde a la variación porcentual del valor cuota entre el 1 de enero y el día de ayer.", 5);
        AccionesGenericas.validarElemento(this.btnEntendido, "ENTENDIDO", 5);
        this.btnEntendido.click();
    }
}
