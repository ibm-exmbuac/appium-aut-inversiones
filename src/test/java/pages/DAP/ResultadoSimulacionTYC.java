package pages.DAP;

import constants.Constants;
import driver.DriverContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.appium.java_client.touch.offset.PointOption;
import io.qameta.allure.Step;
import org.openqa.selenium.Point;
import org.openqa.selenium.support.PageFactory;
import util.AccionesGenericas;
import util.Gestures;

import static driver.DriverContext.getDriver;

public class ResultadoSimulacionTYC {

    private AppiumDriver driver;

    public ResultadoSimulacionTYC() {
        this.driver = getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver), this);
    }

    //Info terminos y condiciones
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_terms_and_conditions")
    @iOSFindBy(accessibility = "TextViewTableViewCell.descriptionTextView")
    private MobileElement infoTerminos;

    //Link términos y condiciones
    @iOSFindBy(accessibility = "términos y condiciones")
    private MobileElement linkTerminos;

    //Título términos y condiciones
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/toolbar_title")
    @iOSFindBy(accessibility = "Términos y condiciones")
    private MobileElement tituloTerminos;

    //Botón cerrar superior
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/iv_icon_close")
    @iOSFindBy(accessibility = "close")
    private MobileElement btnCerrarSuperior;

    //Términos y condiciones
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_message")
    @iOSFindBy(accessibility = "//XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell/XCUIElementTypeTextView")
    private MobileElement terminosCondiciones;

    //Botón cerrar
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/terminos_condiciones_button")
    @iOSFindBy(xpath = "CERRAR")
    private MobileElement btnCerrar;

    @Step
    private void tapTerminosYCondiciones() {
        Point point = this.infoTerminos.getLocation();
        System.out.println(point);
        int x = point.x + 40;
        int y = point.y + 126;
        TouchAction touchAction = new TouchAction(driver);
        touchAction.tap(PointOption.point(x, y)).perform();
    }

    @Step
    public void validaElementos() {
        Gestures gestures = new Gestures();
        gestures.encontrarObjeto(infoTerminos, "Sección términos y condiciones");
        AccionesGenericas.validarElemento(this.infoTerminos, "Al invertir declaras que has leído y aceptas los términos y condiciones de los Depósitos a Plazo.", 5);
    }

    @Step
    public void validaTerminosYCondiciones() {
        if (DriverContext.getDriver() instanceof IOSDriver) {
            //LOGICA IOS
        } else {
            tapTerminosYCondiciones();
            AccionesGenericas.validarElemento(this.tituloTerminos, "Términos y condiciones", 5);
            AccionesGenericas.validarElemento(this.btnCerrarSuperior, 5);
            AccionesGenericas.validarElemento(this.terminosCondiciones, 5);
            AccionesGenericas.validarElemento(this.btnCerrar, 5);
            this.btnCerrar.click();
        }

    }

}
