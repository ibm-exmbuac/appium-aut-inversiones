package pages.DAP;

import constants.Constants;
import driver.DriverContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.qameta.allure.Step;
import org.openqa.selenium.support.PageFactory;
import reporter.EstadoPrueba;
import reporter.PdfBciReports;
import util.AccionesGenericas;

public class ResultadoSimulacionDetalle {

    private AppiumDriver driver;

    public ResultadoSimulacionDetalle() {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver), this);
    }

    //Título resultado de simulación
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/toolbar_title")
    @iOSFindBy(accessibility = "Resultado de Simulación")
    private MobileElement tituloResultadoSimulacion;

    //Flecha atrás
    @AndroidFindBy(xpath = "//android.widget.RelativeLayout[@index='0']/android.view.ViewGroup[@index='1']/android.widget.ImageButton[@index='0']")
    @iOSFindBy(accessibility = "Button")
    private MobileElement flechaAtras;

    //Label ganancia
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_ganancia_label")
    @iOSFindBy(accessibility = "DAPResumenView.GananciaLabel")
    private MobileElement labelGanancia;

    //Value ganancia
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_ganancia_value")
    @iOSFindBy(accessibility = "DAPResumenView.GananciaValue")
    private MobileElement valueGanancia;

    //Label monto a invertir
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/label_monto_invertir")
    @iOSFindBy(accessibility = "DAPResumenView.MontoInvertirLabel")
    private MobileElement labelMontoInvertir;

    //Value monto a inverir
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/value_monto_invertir")
    @iOSFindBy(accessibility = "DAPResumenView.MontoInvertirValue")
    private MobileElement valueMontoInvertir;

    //Label monto final
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/label_monto_final")
    @iOSFindBy(accessibility = "DAPResumenView.MontoFinalLabel")
    private MobileElement labelMontoFinal;

    //Value monto final
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/value_monto_final")
    @iOSFindBy(accessibility = "DAPResumenView.MontoFinalValue")
    private MobileElement valueMontoFinal;

    //Info día no hábil
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_dia_habil")
    @iOSFindBy(accessibility = "InfoTableViewCell.MessageLabel")
    private MobileElement infoDiaNoHabil;

    //Label tasa mensual
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/label_tasa_mensual")
    @iOSFindBy(accessibility = "DAPResumenView.TitleTasaMensual")
    private MobileElement labelTasaMensual;

    //Value tasa mensual
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/value_tasa_mensual")
    @iOSFindBy(accessibility = "DAPResumenView.ValueTasaMensual")
    private MobileElement valueTasaMensual;

    //Label tasa periodo
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/label_tasa_periodo")
    @iOSFindBy(accessibility = "DAPResumenView.TitleTasaPeriodo")
    private MobileElement labelTasaPeriodo;

    //Value tasa periodo
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/value_tasa_periodo")
    @iOSFindBy(accessibility = "DAPResumenView.ValueTasaPeriodo")
    private MobileElement valueTasaPeriodo;

    //Label plazo
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/label_plazo")
    @iOSFindBy(accessibility = "DAPResumenView.TitlePlazo")
    private MobileElement labelPlazo;

    //Value plazo
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/value_plazo")
    @iOSFindBy(accessibility = "DAPResumenView.ValuePlazo")
    private MobileElement valuePlazo;

    //Label fecha de vencimiento
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/label_fecha_vencimiento")
    @iOSFindBy(accessibility = "DAPResumenView.TitleFechaVencimiento")
    private MobileElement labelFechaVencimiento;

    //Value fecha vencimieto
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/value_fecha_vencimiento")
    @iOSFindBy(accessibility = "DAPResumenView.ValueFechaVencimiento")
    private MobileElement valueFechaVencimiento;

    //Label tipo depósito
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/label_tipo_deposito")
    @iOSFindBy(accessibility = "DAPResumenView.TitleTipoDeposito")
    private MobileElement labelTipoDeposito;

    //Value tipo depósito
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/value_tipo_deposito")
    @iOSFindBy(accessibility = "DAPResumenView.ValueTipoDeposito")
    private MobileElement valueTipoDeposito;

    //Label moneda
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/label_moneda")
    @iOSFindBy(accessibility = "DAPResumenView.TitleMoneda")
    private MobileElement labelMoneda;

    //Value moneda
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/value_moneda")
    @iOSFindBy(accessibility = "DAPResumenView.ValueMoneda")
    private MobileElement valueMoneda;


    @Step
    public void validaIngresoPantalla() {
        AccionesGenericas.ingresoPantalla(this.labelGanancia, "Resultado Simulación DAP");
    }

    @Step
    public void validaElementos() {
        AccionesGenericas.validarElemento(this.tituloResultadoSimulacion, "Resultado de Simulación", 5);
        AccionesGenericas.validarElemento(this.flechaAtras, 5);
        AccionesGenericas.validarElemento(this.labelGanancia, "Ganancia", 5);
        AccionesGenericas.validarElemento(this.valueGanancia, 5);
        AccionesGenericas.validarElemento(this.labelMontoInvertir, "Monto a invertir", 5);
        AccionesGenericas.validarElemento(this.valueMontoInvertir, 5);
        AccionesGenericas.validarElemento(this.labelMontoFinal, "Monto final", 5);
        AccionesGenericas.validarElemento(this.valueMontoFinal, 5);
        AccionesGenericas.validarElemento(this.labelTasaMensual, "Tasa mensual", 5);
        AccionesGenericas.validarElemento(this.valueTasaMensual, 5);
        AccionesGenericas.validarElemento(this.labelTasaPeriodo, "Tasa periodo", 5);
        AccionesGenericas.validarElemento(this.valueTasaPeriodo, 5);
        AccionesGenericas.validarElemento(this.labelPlazo, "Plazo", 5);
        AccionesGenericas.validarElemento(this.valuePlazo, 5);
        AccionesGenericas.validarElemento(this.labelFechaVencimiento, "Fecha de vencimiento", 5);
        AccionesGenericas.validarElemento(this.valueFechaVencimiento, 5);
        AccionesGenericas.validarElemento(this.labelTipoDeposito, "Tipo de depósito", 5);
        AccionesGenericas.validarElemento(this.valueTipoDeposito, 5);
        AccionesGenericas.validarElemento(this.labelMoneda, "Moneda", 5);
        AccionesGenericas.validarElemento(this.valueMoneda, 5);

    }

    @Step
    public void validarAjusteDiaHabil() {
        if (this.infoDiaNoHabil.isDisplayed())
            PdfBciReports.addMobilReportImage("Ajuste día hábil", "Se hace ajuste a día hábil", EstadoPrueba.PASSED, false);
        else
            PdfBciReports.addMobilReportImage("Ajuste día hábil", "Nose hace el ajuste de día hábil", EstadoPrueba.FAILED, false);
    }

}

