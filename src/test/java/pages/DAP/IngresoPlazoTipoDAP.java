package pages.DAP;

import constants.Constants;
import driver.DriverContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.qameta.allure.Step;
import org.openqa.selenium.support.PageFactory;
import reporter.EstadoPrueba;
import reporter.PdfBciReports;
import util.AccionesGenericas;


public class IngresoPlazoTipoDAP {

    private AppiumDriver driver;

    public IngresoPlazoTipoDAP() {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver), this);
    }

    //Editar monto
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/editar_monto_button")
    @iOSFindBy(accessibility = "Editar monto")
    private MobileElement btnEditarMonto;

    //Input Plazo
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/plazo_input")
    @iOSFindBy(accessibility = "DAPPlazoYTipoCollectionViewCell.PlazoTextField")
    private MobileElement inputPlazo;

    //Título tipo DAP
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tipo_deposito_label")
    @iOSFindBy(accessibility = "Tipo de depósito")
    private MobileElement tituloTipoDAP;

    //Botón plazo renovable
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/bt_tipo_dap_opcion1")
    @iOSFindBy(accessibility = "TipoDeMonedaODepositoView.ButtonPlazoRenovable")
    private MobileElement btnPlazoRenovable;

    //Botón plazo fijo
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/bt_tipo_dap_opcion2")
    @iOSFindBy(accessibility = "TipoDeMonedaODepositoView.ButtonPlazoFijo")
    private MobileElement btnPlazoFijo;

    //Info plazos Android
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tipo_deposito_explanation_label")
    private MobileElement infoPlazos;

    //Info plazo renovable
    @iOSFindBy(accessibility = "Depósito a Plazo Renovable: Se renueva una vez cumplido el plazo y puedes recuperarlo hasta los tres días hábiles siguientes.")
    private MobileElement infoPLazoRenovable;

    //Info plazo fijo
    @iOSFindBy(accessibility = "Depósito a Plazo Fijo: Tu inversión estará vigente hasta el plazo acordado.")
    private MobileElement infoPlazoFijo;

    //Botón simular
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/simular_button")
    @iOSFindBy(accessibility = "DAPPlazoYTipoCollectionViewCell.SimularButton")
    private MobileElement btnSimular;


    @Step
    public void validaElementos() {
        AccionesGenericas.validarElemento(this.btnEditarMonto, 5);
        AccionesGenericas.validarElemento(this.inputPlazo, 5);
        AccionesGenericas.validarElemento(this.tituloTipoDAP, "Tipo de depósito", 5);
        AccionesGenericas.validarElemento(this.btnPlazoRenovable, "PLAZO RENOVABLE", 5);
        if (DriverContext.getDriver() instanceof IOSDriver) {
            AccionesGenericas.validarElemento(this.infoPLazoRenovable, "Depósito a Plazo Renovable: Se renueva una vez cumplido el plazo y puedes recuperarlo hasta los tres días hábiles siguientes.", 5);
        } else {
            AccionesGenericas.validarElemento(this.infoPlazos, "Depósito a Plazo Renovable: Se renueva una vez cumplido el plazo y puedes recuperarlo hasta los tres días hábiles siguientes.", 5);
        }
        AccionesGenericas.validarElemento(this.btnPlazoFijo, "PLAZO FIJO", 5);
        this.btnPlazoFijo.click();
        if (DriverContext.getDriver() instanceof IOSDriver) {
            AccionesGenericas.validarElemento(this.infoPlazoFijo, "Depósito a Plazo Fijo: Tu inversión estará vigente hasta el plazo acordado.", 5);
        } else {
            AccionesGenericas.validarElemento(this.infoPlazos, "Depósito a Plazo Fijo: Tu inversión estará vigente hasta el plazo acordado.", 5);
        }
        AccionesGenericas.validarElemento(this.btnSimular, "SIMULAR", 5);
    }

    @Step
    public void seleccionarTipoDAP(String tipoDAP) {
        if (tipoDAP.equals("Renovable")) {
            this.btnPlazoRenovable.click();
            PdfBciReports.addMobilReportImage("Seleccionar tipo DAP", "Se selecciona DAP Revovable", EstadoPrueba.PASSED, false);
        } else if (tipoDAP.equals("Fijo")) {
            this.btnPlazoFijo.click();
            PdfBciReports.addMobilReportImage("Seleccionar tipo DAP", "Se selecciona DAP Fijo", EstadoPrueba.PASSED, false);
        } else {
            PdfBciReports.addMobilReportImage("Seleccionar tipo DAP", "No se ingresa ningún tipo DAP por parámetro", EstadoPrueba.FAILED, false);
        }
    }

    @Step
    public void ingresoPlazo(String plazo) {
        this.inputPlazo.setValue(plazo);
        PdfBciReports.addMobilReportImage("Ingresar Plazo", "Se ingresa plazo: " + plazo + " dias", EstadoPrueba.PASSED, false);
    }

    @Step
    public void tapSimular() {
        if (this.btnSimular.isEnabled()) {
            this.btnSimular.click();
            PdfBciReports.addReport("Tap Simular", "Se hace tap en el botón simular", EstadoPrueba.PASSED, false);
        } else {
            PdfBciReports.addMobilReportImage("Tap Simular", "Botón simular no se encuentra habilitado", EstadoPrueba.FAILED, false);
        }
    }

    @Step
    public void ingresoPlazoDiaNoHabil() {
        String dia = Integer.toString(AccionesGenericas.generadorDiaNoHabil());
        this.inputPlazo.setValue(dia);
        PdfBciReports.addMobilReportImage("Ingreso dia no hábil", "Se ingresa plazo: " + dia + " dias", EstadoPrueba.PASSED, false);
    }
}
