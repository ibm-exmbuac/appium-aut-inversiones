package pages.DAP;

import constants.Constants;
import driver.DriverContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.qameta.allure.Step;
import org.openqa.selenium.support.PageFactory;
import reporter.EstadoPrueba;
import reporter.PdfBciReports;
import util.AccionesGenericas;
import util.Gestures;

public class ResultadoSimulacionComprobante {

    private AppiumDriver driver;

    public ResultadoSimulacionComprobante() {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver), this);
    }

    //Título envío de comprobante
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_envio_comprobante")
    @iOSFindBy(accessibility = "Envío de comprobante")
    private MobileElement tituloEnvioComprobante;

    //Input correo
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/email_input")
    @iOSFindBy(accessibility = "DAPResumenView.EmailInput")
    private MobileElement inputCorreo;

    //Info retiro dinero
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_retiro")
    @iOSFindBy(accessibility = "El retiro de tu dinero solo puede ser realizado desde nuestro sitio web o una sucursal")
    private MobileElement infoRetiroDinero;


    @Step
    public void validarElementos() {
        Gestures gestures = new Gestures();
        gestures.encontrarObjeto(infoRetiroDinero, "Sección envío comprobante");
        AccionesGenericas.validarElemento(this.tituloEnvioComprobante, "Envío de comprobante", 5);
        AccionesGenericas.validarElemento(this.inputCorreo, 5);
        AccionesGenericas.validarElemento(this.infoRetiroDinero, 5);
    }

    @Step
    public void validarCorreoVacio() {
        if (this.inputCorreo.getText().isEmpty())
            PdfBciReports.addMobilReportImage("Validar correo vacio", "Email vacío cargado por defecto", EstadoPrueba.PASSED, false);
        else
            PdfBciReports.addMobilReportImage("Validar correo vacio", "Email cargado por defecto", EstadoPrueba.FAILED, false);
    }

    public void validarCorreoCargado() {
        if (!this.inputCorreo.getText().isEmpty())
            PdfBciReports.addMobilReportImage("Validar correo cargado", "Email cargado por defecto", EstadoPrueba.PASSED, false);
        else
            PdfBciReports.addMobilReportImage("Validar correo cargado", "Email vacío cargado por defecto", EstadoPrueba.FAILED, false);
    }

    public void ingresarCorreo(String correo) {
        if (this.inputCorreo.getText().isEmpty()) {
            this.inputCorreo.setValue(correo);
            PdfBciReports.addMobilReportImage("Ingresar correo", "Se ingresa correo: " + correo, EstadoPrueba.PASSED, false);
            tituloEnvioComprobante.click();
        }
    }
}






