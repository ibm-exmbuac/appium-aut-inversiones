package pages.DAP;

import constants.Constants;
import driver.DriverContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.qameta.allure.Step;
import org.openqa.selenium.support.PageFactory;
import reporter.EstadoPrueba;
import reporter.PdfBciReports;
import util.AccionesGenericas;


public class IngresoMonedaMontoDAP {


    private AppiumDriver driver;

    public IngresoMonedaMontoDAP() {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver), this);
    }

    /******************************************************************************************************
     *  OBJETOS
     ******************************************************************************************************/

    //Título simula tu depósito a plazo
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/toolbar_title")
    @iOSFindBy(accessibility = "Simula tu Depósito a Plazo")
    private MobileElement tituloSimulaTuDAP;

    //Flecha atrás
    @AndroidFindBy(xpath = "//android.widget.RelativeLayout[@index = '0']/android.widget.LinearLayout[@index = '0']/android.view.ViewGroup[@index = '1']/android.widget.ImageButton[@index = '0']")
    @iOSFindBy(accessibility = "left arrow")
    private MobileElement flechaAtras;

    //Título tipo moneda
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tipo_moneda_label")
    @iOSFindBy(accessibility = "Tipo de moneda")
    private MobileElement tituloMoneda;

    //Botón pesos
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/bt_tipo_moneda_opcion1")
    @iOSFindBy(accessibility = "TipoDeMonedaODepositoView.ButtonPesos")
    private MobileElement btnPesos;

    //Botón UF
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/bt_tipo_moneda_opcion2")
    @iOSFindBy(xpath = "//XCUIElementTypeButton[@name=\"TipoDeMonedaODepositoView.ButtonUF\"]")
    private MobileElement btnUF;

    //Título cuánto quieres invertir
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/ingrese_monto_label")
    @iOSFindBy(accessibility = "IngresoMonto.titleLabel")
    private MobileElement tituloCuantoInv;

    //Input monto
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/monto_input")
    @iOSFindBy(accessibility = "IngresoMonto.montoLabel")
    private MobileElement inputMonto;

    //Mensaje monto
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/monto_label")
    @iOSFindBy(accessibility = "IngresoMonto.errorLabel")
    private MobileElement mensajeMonto;

    //Botón continuar
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/continue_view_monto")
    @iOSFindBy(accessibility = "DAPMonedaYMontoCollectionViewCell.ContinuarButton")
    private MobileElement btnContinuar;

    /********************************************
     *  OBJETOS CUADRO UF
     ********************************************/

    //Titulo tipo de moneda
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_title")
    @iOSFindBy(xpath = "(//XCUIElementTypeStaticText[@name=\"Tipo de moneda\"])[2]")
    private MobileElement tipoMonedaUF;

    //Info UF
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_description")
    @iOSFindBy(accessibility = "La opción de Depósito a Plazo en UF estará disponible próximamente.")
    private MobileElement infoUF;

    //Botón continuar
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/cb_close")
    @iOSFindBy(accessibility = "ACEPTAR")
    private MobileElement btnAceptar;


    @Step
    public void validaIngresoPantalla() {
        AccionesGenericas.ingresoPantalla(this.tituloSimulaTuDAP, "Ingreso Moneda Monto DAP");
    }

    @Step
    public void validaElementos() {
        AccionesGenericas.validarElemento(this.tituloSimulaTuDAP, "Simula tu Depósito a Plazo", 5);
        AccionesGenericas.validarElemento(this.flechaAtras, 5);
        AccionesGenericas.validarElemento(this.tituloMoneda, "Tipo de moneda", 5);
        AccionesGenericas.validarElemento(this.btnPesos, "PESOS", 5);
        AccionesGenericas.validarElemento(this.btnUF, "U.F.", 5);
        AccionesGenericas.validarElemento(this.tituloCuantoInv, "¿Cuánto quieres invertir?", 5);
        AccionesGenericas.validarElemento(this.inputMonto, 5);
        AccionesGenericas.validarElemento(this.mensajeMonto, "Invierte desde $5.000", 5);
        AccionesGenericas.validarElemento(this.btnContinuar, "CONTINUAR", 5);
    }

    @Step
    public void validarCuadroUF() {
        this.btnUF.click();
        AccionesGenericas.validarElemento(this.tipoMonedaUF, "Tipo de moneda", 5);
        AccionesGenericas.validarElemento(this.infoUF, "La opción de Depósito a Plazo en UF estará disponible próximamente.", 5);
        AccionesGenericas.validarElemento(this.btnAceptar, "ACEPTAR", 5);
    }

    public void ingresoMonto(String monto) {
        this.inputMonto.setValue(monto);
        PdfBciReports.addMobilReportImage("Ingresa Monto", "Se ingresa el monto: " + monto, EstadoPrueba.PASSED, false);
    }

    public void tapContinuar() {
        this.btnContinuar.click();
        PdfBciReports.addReport("Tap Continuar", "Se hace tap en botón continuar", EstadoPrueba.PASSED, false);
    }

}