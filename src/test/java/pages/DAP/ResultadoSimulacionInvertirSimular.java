package pages.DAP;

import constants.Constants;
import driver.DriverContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.qameta.allure.Step;
import org.openqa.selenium.support.PageFactory;
import reporter.EstadoPrueba;
import reporter.PdfBciReports;
import util.AccionesGenericas;
import util.Gestures;

public class ResultadoSimulacionInvertirSimular {

    private AppiumDriver driver;

    public ResultadoSimulacionInvertirSimular() {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver), this);
    }

    //Botón invertir
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/btn_invertir")
    @iOSFindBy(accessibility = "DAPResumenHomePresenter.InvertirButton")
    private MobileElement btnInvertir;

    //Botón volver a simular
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/btn_volver_simular")
    @iOSFindBy(accessibility = "DAPResumenHomePresenter.VolverAInvertirButton")
    private MobileElement btnVolverSimular;

    //Título tipo moneda
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tipo_moneda_label")
    @iOSFindBy(accessibility = "Tipo de moneda")
    private MobileElement tituloMoneda;

    /***********************************************
     *  OBJETOS MODAL VAS A SALIR DE ESTA SIMULACIÓN
     ***********************************************/

    //Icono
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/iv_error_icon")
    @iOSFindBy(xpath = "(//XCUIElementTypeImage[@name=\"CalculadoraRentabilidadViewController.BottomMessage.HeaderImageView\"])[1]")
    private MobileElement icono;

    //Título "Vas a salir de esta simulación"
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_error_message")
    @iOSFindBy(xpath = "(//XCUIElementTypeStaticText[@name=\"CalculadoraRentabilidadViewController.BottomMessage.TitleLab\"])[1]")
    private MobileElement tituloModal;

    //Deseas continuar
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_error_message_description")
    @iOSFindBy(xpath = "(//XCUIElementTypeStaticText[@name=\"CalculadoraRentabilidadViewController.BottomMessage.MessageLabel\"])[1]")
    private MobileElement labelDeseasContinuar;

    //Botón volver a simular
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/positive_button")
    @iOSFindBy(xpath = "(//XCUIElementTypeButton[@name=\"CalculadoraRentabilidadViewController.BottomMessage.FirstButton\"])[1]")
    private MobileElement btnVolverSimularModal;

    //Botón cancelar
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/negative_button")
    @iOSFindBy(id = "CalculadoraRentabilidadViewController.BottomMessage.SecondaryButton")
    private MobileElement btnCancelarModal;

    @Step
    public void validarElementos() {
        Gestures gestures = new Gestures();
        gestures.encontrarObjeto(btnVolverSimular, "Sección botones");
        AccionesGenericas.validarElemento(this.btnInvertir, "INVERTIR", 5);
        AccionesGenericas.validarElemento(this.btnVolverSimular, "VOLVER A SIMULAR", 5);
    }

    @Step
    public void tapInvertir() {
        this.btnInvertir.click();
        PdfBciReports.addReport("Tap botón invertir", "Se hace tap a botón Invertir", EstadoPrueba.PASSED, false);
    }

    @Step
    public void tapVolverASimular() {
        this.btnVolverSimular.click();
        PdfBciReports.addReport("Tap botón volver a simular", "Se hace tap a botón Volver a Simular", EstadoPrueba.PASSED, false);
    }

    @Step
    public void validarElementosVolverASimular() {
        AccionesGenericas.validarElemento(this.icono, 5);
        AccionesGenericas.validarElemento(this.tituloModal, "Vas a salir de esta simulación", 5);
        AccionesGenericas.validarElemento(this.labelDeseasContinuar, "Los datos ingresados no serán guardados.", 5);
        AccionesGenericas.validarElemento(this.btnVolverSimularModal, "VOLVER A SIMULAR", 5);
        AccionesGenericas.validarElemento(this.btnCancelarModal, "CONTINUAR", 5);
    }

    @Step
    public void tapVolverASimularModal() {
        this.btnVolverSimularModal.click();
        boolean existe = AccionesGenericas.existeElemento(tituloMoneda, 30);
        if (existe) {
            PdfBciReports.addMobilReportImage("Tap volver a simular modal", "Se vuelve a la primera pantalla de simulación DAP", EstadoPrueba.PASSED, false);
        } else {
            PdfBciReports.addMobilReportImage("Tap volver a simular modal", "No se vuelve a la primera pantalla de simulación DAP", EstadoPrueba.FAILED, false);
        }
    }

    @Step
    public void tapContinuarSimulacionModal() {
        this.btnCancelarModal.click();
        boolean existe = AccionesGenericas.existeElemento(btnInvertir, 30);
        if (existe) {
            PdfBciReports.addMobilReportImage("Tap Continuar Simulació Modal", "Se vuelve al resultado simulación DAP", EstadoPrueba.PASSED, false);
        } else {
            PdfBciReports.addMobilReportImage("Tap Continuar Simulació Modal", "No se vuelve al resultado simulación DAP", EstadoPrueba.FAILED, false);
        }
    }
}
