package pages.DAP;

import constants.Constants;
import driver.DriverContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.qameta.allure.Step;
import org.openqa.selenium.support.PageFactory;
import util.AccionesGenericas;
import util.Gestures;

import java.awt.geom.GeneralPath;

public class ResultadoSimulacionCuentas {

    private AppiumDriver driver;

    public ResultadoSimulacionCuentas() {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver), this);
    }

    //Título cuentas de cobro y abono
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_cuentas")
    @iOSFindBy(accessibility = "Cuentas de cobro y abono")
    private MobileElement tituloCuentasCobroAbono;

    //Cobrar a:
    @AndroidFindBy(xpath = "//android.view.ViewGroup[@index='0']/android.widget.LinearLayout[@index='21']/android.widget.TextView[@text='Cobrar a:']")
    @iOSFindBy(accessibility = "Cobrar a:")
    private MobileElement cobrarA;

    //Value cuenta cobro
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_cuenta_cobro")
    @iOSFindBy(accessibility = "DAPResumenView.CobroLabelValue")
    private MobileElement valueCuentaCobro;

    //Abonar a:
    @AndroidFindBy(xpath = "//android.view.ViewGroup[@index='0']/android.widget.LinearLayout[@index='22']/android.widget.TextView[@index='0']")
    @iOSFindBy(accessibility = "Abonar a:")
    private MobileElement abonarA;

    //Value cuenta abono
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_cuenta_abono")
    @iOSFindBy(accessibility = "DAPResumenView.AbonoLabelValue")
    private MobileElement valueCuentaAbono;

    @Step
    public void validarElementos() {
        Gestures gestures = new Gestures();
        gestures.encontrarObjeto(valueCuentaAbono, "Sección de cuentas");
        AccionesGenericas.validarElemento(this.tituloCuentasCobroAbono, "Cuentas de cobro y abono", 5);
//       AccionesGenericas.validarElemento(this.cobrarA, "Cobrar a:", 5);
        utils.MetodosGenericos.visualizarObjeto(this.valueCuentaCobro, 5);
//        AccionesGenericas.validarElemento(this.abonarA, "Abonar a:", 5);
        AccionesGenericas.validarElemento(this.valueCuentaAbono, 5);
    }
}
