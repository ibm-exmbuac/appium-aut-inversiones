package pages.DAP;

import driver.DriverContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.qameta.allure.Step;
import org.openqa.selenium.support.PageFactory;
import reporter.EstadoPrueba;
import reporter.PdfBciReports;
import util.Gestures;

public class CampaniaDAP {

    public AppiumDriver driver;

    public CampaniaDAP() {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver), this);
    }

    //Texto invierte en DAP
    @AndroidFindBy(xpath = "//*[contains(@text,\"Invierte en Depósitos a Plazo\n" +
            "Tómalo ahora en 3 simples pasos\")]")
    @iOSFindBy(accessibility = "Depósitos a Plazo")
    private MobileElement textoDAP;

    //Boton Simular
    @AndroidFindBy(xpath = "//*[contains(@text,\"Simular\")]")
    @iOSFindBy(accessibility = "Simular")
    private MobileElement btnSimular;


    @Step
    public void validarDespliegueCampania() {
        Gestures gestures = new Gestures();
        gestures.encontrarObjeto(textoDAP, "Campaña DAP");
    }

    @Step
    public void tapBtnSimular() {
        btnSimular.click();
        PdfBciReports.addReport("Tap botón simular", "Se hace tal al botón simukar", EstadoPrueba.PASSED, false);
    }

}
