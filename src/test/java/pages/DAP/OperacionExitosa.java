package pages.DAP;

import constants.Constants;
import driver.DriverContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.qameta.allure.Step;
import org.openqa.selenium.support.PageFactory;
import reporter.EstadoPrueba;
import reporter.PdfBciReports;
import util.AccionesGenericas;
import util.Gestures;

public class OperacionExitosa {

    private AppiumDriver driver;

    public OperacionExitosa() {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver), this);
    }

    //Título operación exitosa
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_operacion_title")
    @iOSFindBy(accessibility = "Operación exitosa")
    private MobileElement tituloOperacionExitosa;

    //Label monto invertido
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_monto_invertido_label")
    @iOSFindBy(accessibility = "DAPResultadoViewController.MontiInvertirLabel")
    private MobileElement labelMontoInvertido;

    //Value monto invertido
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/value_monto_invertido")
    @iOSFindBy(accessibility = "DAPResultadoViewController.MontiInvertirValue")
    private MobileElement valueMontoInvertido;

    //Label número de comprobante
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/label_numero_comprobante")
    @iOSFindBy(accessibility = "DAPResultadoViewController.TitleNumeroComprobante")
    private MobileElement labelNroComprobante;

    //Value número de comprobante
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/value_numero_comprobante")
    @iOSFindBy(accessibility = "DAPResultadoViewController.ValueNumeroComprobante")
    private MobileElement valueNroComprobante;

    //Label fecha de operación
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/label_fecha_operacion")
    @iOSFindBy(accessibility = "DAPResultadoViewController.TitleFechaOperacion")
    private MobileElement labelFechaOperacion;

    //Value fecha de operación
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/value_fecha_operacion")
    @iOSFindBy(accessibility = "DAPResultadoViewController.ValueFechaOperacion")
    private MobileElement valueFechaOperacion;

    //Label número depósito
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/label_numero_deposito")
    @iOSFindBy(accessibility = "DAPResultadoViewController.TitleNumeroDeposito")
    private MobileElement labelNumeroDeposito;

    //Value número depósito
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/value_numero_deposito")
    @iOSFindBy(accessibility = "DAPResultadoViewController.ValueNumeroDeposito")
    private MobileElement valueNumeroDeposito;

    //Label tipo depósito
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/label_tipo_deposito")
    @iOSFindBy(accessibility = "DAPResultadoViewController.TitleTipoDeposito")
    private MobileElement labelTipoDeposito;

    //Value tipo depósito
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/value_tipo_deposito")
    @iOSFindBy(accessibility = "DAPResultadoViewController.ValueTipoDeposito")
    private MobileElement valueTipoDeposito;

    //Label moneda
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/label_moneda")
    @iOSFindBy(accessibility = "DAPResultadoViewController.TitleMoneda")
    private MobileElement labelMoneda;

    //Value moneda
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/value_moneda")
    @iOSFindBy(accessibility = "DAPResultadoViewController.ValueMoneda")
    private MobileElement valueMoneda;

    //Label cuenta de cargo
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/label_cuenta_cargo")
    @iOSFindBy(accessibility = "DAPResultadoViewController.TitleCuentaCargo")
    private MobileElement labelCuentaCargo;

    //Value cuenta cargo
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/value_cuenta_cargo")
    @iOSFindBy(accessibility = "DAPResultadoViewController.ValueCuentaCargo")
    private MobileElement valueCuentaCargo;

    //Label cuenta abono
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/label_cuenta_abono")
    @iOSFindBy(accessibility = "DAPResultadoViewController.TitleCuentaAbono")
    private MobileElement labelCuentaAbono;

    //Value cuenta abono
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/value_cuenta_abono")
    @iOSFindBy(accessibility = "DAPResultadoViewController.ValueCuentaAbono")
    private MobileElement valueCuentaAbono;

    //Label plazo
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/label_plazo")
    @iOSFindBy(accessibility = "DAPResultadoViewController.TitlePlazo")
    private MobileElement labelPlazo;

    //Value plazo
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/value_plazo")
    @iOSFindBy(accessibility = "DAPResultadoViewController.ValuePlazo")
    private MobileElement valuePlazo;

    //Label monto a obtener
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/label_monto_obtener")
    @iOSFindBy(accessibility = "DAPResultadoViewController.TitleMonto")
    private MobileElement labelMontoObtener;

    //Value monto a obtener
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/value_monto_obtener")
    @iOSFindBy(accessibility = "DAPResultadoViewController.ValueMonto")
    private MobileElement valueMontoObtener;

    //Label ganancia
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/label_ganancia")
    @iOSFindBy(accessibility = "DAPResultadoViewController.TitleGanancia")
    private MobileElement labelGanancia;

    //Value ganancia
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/value_ganancia")
    @iOSFindBy(accessibility = "DAPResultadoViewController.ValueGanancia")
    private MobileElement valueGanancia;

    //Label tasa interés
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/label_tasa_interes")
    @iOSFindBy(accessibility = "DAPResultadoViewController.TitleTasaInteres")
    private MobileElement labelTasaInteres;

    //Value tasa interés
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/value_tasa_interes")
    @iOSFindBy(accessibility = "DAPResultadoViewController.ValueTasaInteres")
    private MobileElement valueTasaInteres;

    //Label vencimiento
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/label_vencimiento")
    @iOSFindBy(accessibility = "DAPResultadoViewController.TitleVencimiento")
    private MobileElement labelVencimiento;

    //Value vencimiento
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/value_vencimiento")
    @iOSFindBy(accessibility = "DAPResultadoViewController.ValueVencimiento")
    private MobileElement valueVencimiento;

    //Botón ver más / menos IOS
    @iOSFindBy(accessibility = "ActionTableViewCell.actionButton")
    private MobileElement btnVerMasMenos;

    //Botón ver más Android
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/btn_ver_mas")
    private MobileElement btnVerMas;

    //Botón ver menos Android
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/btn_ver_menos")
    private MobileElement btnVerMenos;

    //Sello BCI
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/iv_sello")
    @iOSFindBy(xpath = "//XCUIElementTypeImage[@name=\"sello\"]")
    private MobileElement sello;

    //Info envío comprobante
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/email_label")
    @iOSFindBy(accessibility = "DAPResultadoHomePresenter.DAPSuccessLabel")
    private MobileElement infoEnvioComprobante;

    //BOTÓN FINALIZAR
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/btn_finalizar")
    @iOSFindBy(accessibility = "ButtonTableViewCell.CustomTextLabel")
    private MobileElement btnFinalizar;

    @Step
    public void validarIngresoPantalla() {
        AccionesGenericas.ingresoPantalla(this.tituloOperacionExitosa, "Operación Exitosa DAP");
    }

    @Step
    public void validarElementos() {
        Gestures ges = new Gestures();

        AccionesGenericas.validarElemento(this.tituloOperacionExitosa, "Operación exitosa", 5);
        AccionesGenericas.validarElemento(this.labelMontoInvertido, "Monto invertido", 5);
        AccionesGenericas.validarElemento(this.valueMontoInvertido, 5);
        AccionesGenericas.validarElemento(this.labelNroComprobante, "Nº de comprobante", 5);
        AccionesGenericas.validarElemento(this.valueNroComprobante, 5);
        AccionesGenericas.validarElemento(this.labelFechaOperacion, "Fecha de operación", 5);
        AccionesGenericas.validarElemento(this.valueFechaOperacion, 5);
        AccionesGenericas.validarElemento(this.labelNumeroDeposito, "Nº de depósito", 5);
        AccionesGenericas.validarElemento(this.valueNumeroDeposito, 5);
        if (DriverContext.getDriver() instanceof IOSDriver) {
            AccionesGenericas.validarElemento(this.btnVerMasMenos, "Ver más", 5);
            this.btnVerMasMenos.click();
        } else {
            AccionesGenericas.validarElemento(this.btnVerMas, "Ver más", 5);
            this.btnVerMas.click();
        }
        AccionesGenericas.validarElemento(this.labelTipoDeposito, "Tipo de depósito", 5);
        AccionesGenericas.validarElemento(this.valueTipoDeposito, 5);
        AccionesGenericas.validarElemento(this.labelMoneda, "Moneda", 5);
        AccionesGenericas.validarElemento(this.valueMoneda, 5);
        AccionesGenericas.validarElemento(this.labelCuentaCargo, "Cuenta de cargo", 5);
        AccionesGenericas.validarElemento(this.valueCuentaCargo, 5);
        AccionesGenericas.validarElemento(this.labelCuentaAbono, "Cuenta de abono", 5);
        AccionesGenericas.validarElemento(this.valueCuentaAbono, 5);
        AccionesGenericas.validarElemento(this.labelPlazo, "Plazo", 5);
        AccionesGenericas.validarElemento(this.valuePlazo, 5);
        ges.scrollAbajo();
        AccionesGenericas.validarElemento(this.labelMontoObtener, "Monto a obtener", 5);
        AccionesGenericas.validarElemento(this.valueMontoObtener, 5);
        AccionesGenericas.validarElemento(this.labelGanancia, "Ganancia", 5);
        AccionesGenericas.validarElemento(this.valueGanancia, 5);
        AccionesGenericas.validarElemento(this.labelTasaInteres, "Tasa interés", 5);
        AccionesGenericas.validarElemento(this.valueTasaInteres, 5);
        AccionesGenericas.validarElemento(this.labelVencimiento, "Vencimiento", 5);
        AccionesGenericas.validarElemento(this.valueVencimiento, 5);
        if (DriverContext.getDriver() instanceof IOSDriver) {
            AccionesGenericas.validarElemento(this.btnVerMasMenos, "Ver menos", 5);
        } else {
            AccionesGenericas.validarElemento(this.btnVerMenos, "Ver menos", 5);
        }
//        AccionesGenericas.validarElemento(this.sello, 10);
        AccionesGenericas.validarElemento(this.infoEnvioComprobante, 5);
        AccionesGenericas.validarElemento(this.btnFinalizar, "FINALIZAR", 5);
        if (DriverContext.getDriver() instanceof IOSDriver) {
            this.btnVerMasMenos.click();
        } else {
            this.btnVerMenos.click();
        }
    }

    @Step
    public void validarTipoDeposito(String deposito) {
        Gestures ges = new Gestures();
        if (DriverContext.getDriver() instanceof IOSDriver) {
            this.btnVerMasMenos.click();
        } else {
            this.btnVerMas.click();
        }
        if (this.valueTipoDeposito.getText().equals(deposito)) {
            PdfBciReports.addMobilReportImage("Validar tipo depósito", "Tipo de depósito (" + deposito + ") esperado correcto", EstadoPrueba.PASSED, false);
        } else {
            PdfBciReports.addMobilReportImage("Validar tipo depósito", "Tipo de depósito (" + deposito + ") esperado incorrecto", EstadoPrueba.FAILED, false);
        }
        ges.scrollAbajo();
        if (DriverContext.getDriver() instanceof IOSDriver) {
            this.btnVerMasMenos.click();
        } else {
            this.btnVerMenos.click();
        }
    }

    @Step
    public void tapFinalizar() {
        this.btnFinalizar.click();
        PdfBciReports.addReport("Tap Finalizar", "Se hace tap al botón finalizar", EstadoPrueba.PASSED, false);
    }
}