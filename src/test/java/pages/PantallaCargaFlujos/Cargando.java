package pages.PantallaCargaFlujos;

import constants.Constants;
import driver.DriverContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.qameta.allure.Step;
import org.openqa.selenium.support.PageFactory;
import util.AccionesGenericas;

public class Cargando {
    private AppiumDriver driver;

    public Cargando() {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver), this);
    }

    //Logo BCI
    @iOSFindBy(xpath = "//XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/v_animation")
    private MobileElement logoBCI;

    //Cargando
    @iOSFindBy(accessibility = "Cargando")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_message")
    private MobileElement cargando;

    //Espera un momento
    @iOSFindBy(accessibility = "Espera un momento")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_wait_message")
    private MobileElement esperaUnMomento;

    @Step
    public void validarPantallaCarga() {
        AccionesGenericas.validarElemento(this.logoBCI, 10);
        AccionesGenericas.validarElemento(this.cargando, "Cargando", 5);
        AccionesGenericas.validarElemento(this.esperaUnMomento, "Espera un momento", 5);
    }
}
