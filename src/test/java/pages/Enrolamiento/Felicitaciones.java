package pages.Enrolamiento;

import constants.ConstantesInversiones;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.qameta.allure.Step;
import reporter.EstadoPrueba;
import reporter.PdfBciReports;
import util.AccionesGenericas;

public class Felicitaciones {

    /*********************************************************************
     * OBJETOS
     **********************************************************************/

    //Título felicitaciones
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID+":id/tv_finalizar_enrolamiento_felicitaciones")
    private MobileElement tituloFelicitaciones;

    //Ya estás listo para invertir en FFMM
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID+":id/tv_finalizar_enrolamiento_title")
    private MobileElement tituloYaEstasListo;

    //Info 1
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID+":id/tv_finalizar_enrolamiento_doc")
    private MobileElement info1;

    //Título felicitaciones
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID+":id/tv_finalizar_enrolamiento_contacto")
    private MobileElement info2;

    //Título felicitaciones
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID+":id/btn_finalizar_enrolamiento")
    private MobileElement btnFinalizar;


    @Step
    public void validarIngresoPantalla(){
        AccionesGenericas.ingresoPantalla(tituloFelicitaciones, "Pantalla Felicitaciones");
    }

    @Step
    public void validarElementos(){
        AccionesGenericas.validarElemento(tituloFelicitaciones, "¡Felicitaciones!", 5);
        AccionesGenericas.validarElemento(tituloYaEstasListo, "Ya estás listo para invertir en Fondos Mutuos", 5);
        AccionesGenericas.validarElemento(info1, "La documentación asociada a nuestros productos fue enviada a tu correo electrónico", 5);
        AccionesGenericas.validarElemento(info2, "Si tienes dudas contáctanos desde celulares al (2) 25404999 o desde teléfonos fijos al 800 200 006", 5);
        AccionesGenericas.validarElemento(btnFinalizar, "FINALIZAR", 5);
    }

    @Step
    public void tapBtnFinalizar(){
        btnFinalizar.click();
        PdfBciReports.addReport("Tap botón finalizar", "Se hace tap al botón finalizar", EstadoPrueba.PASSED, false);
    }

}
