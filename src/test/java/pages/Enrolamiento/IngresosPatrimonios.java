package pages.Enrolamiento;

import constants.ConstantesInversiones;
import driver.DriverContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import reporter.EstadoPrueba;
import reporter.PdfBciReports;
import util.AccionesGenericas;
import util.Gestures;

import java.util.List;

public class IngresosPatrimonios {

    private AppiumDriver driver;

    public IngresosPatrimonios() {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver), this);
    }

    /***************************************************************************************************
     *                                          OBJETOS                                                *
     ***************************************************************************************************/

    //Título pantalla
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/toolbar_title")
    @iOSFindBy(accessibility = "Enrolarme para invertir")
    private MobileElement tituloPagina;

    //Botón cerrar
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/action_close")
    @iOSFindBy(accessibility = "close")
    private MobileElement btnCerrar;

    //Flecha atrás progreso
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/bt_progress_back")
    @iOSFindBy(accessibility = "UIView.ProgressBarPager.PreviousStepButton")
    private MobileElement flechaAtrasProgreso;

    //Barra progreso
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/horizontal_progress_bar")
    @iOSFindBy(accessibility = "UIView.ProgressBarPager.ProgressBarView")
    private MobileElement barraProgreso;

    //Stepper
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/tv_stepper_indicator")
    @iOSFindBy(accessibility = "UIView.ProgressBarPager.StepIndicatorLabel")
    private MobileElement stepper;

    //Titulo Mis datos laborales
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/tv_title")
    @iOSFindBy(accessibility = "Mis ingresos, patrimonios y adicionales")
    private MobileElement tituloIngresosPatrimonios;

    /***************************************************************************************************/

    //Título radio button
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/tv_group_title")
    @iOSFindBy(accessibility = "OptionsTableViewCell.TitleLabel.OrigenFondosIniciales")
    private MobileElement tituloRdos;

    //Radio button venta de activos
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/rb_ingresos_patrimonio_origen_fondo_venta")
    @iOSFindBy(accessibility = "OptionsTableViewCell.OptionLabelOption0")
    private MobileElement rdoVentaActivos;

    //Radio button vencimiento otras inversiones
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/rb_ingresos_patrimonio_origen_fondo_vencimiento")
    @iOSFindBy(accessibility = "OptionsTableViewCell.OptionLabelOption1")
    private MobileElement rdoVencOtrasInv;

    //Radio button herencia
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/rb_ingresos_patrimonio_origen_fondo_herencia")
    @iOSFindBy(accessibility = "OptionsTableViewCell.OptionLabelOption2")
    private MobileElement rdoHerencia;

    //Radio button otro
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/rb_ingresos_patrimonio_origen_fondo_otro")
    @iOSFindBy(accessibility = "OptionsTableViewCell.OptionLabelOption3")
    private MobileElement rdoOtro;

    /***************************************************************************************************/

    //Contenedor origen fondos
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/selector_region")
    private MobileElement contPaisOrigenFF;

    //Selector origen fondos
    @iOSFindBy(accessibility = "PickerSelectorTableViewCell.TextField.PaisOrigenFondos")
    private MobileElement selectorOrigenFondos;

    /***************************************************************************************************/

    //Contenedor ingresos mensuales
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/selector_ingresos_mensuales")
    private MobileElement contIngresosMensuales;

    //Selector ingresos mensuales
    @iOSFindBy(accessibility = "PickerSelectorTableViewCell.TextField.IngresosMensuales")
    private MobileElement selectorIngresosMensuales;

    /***************************************************************************************************/

    //Contenedor ingresos ocasionales
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/selector_ingresos_ocasionales")
    private MobileElement contIngresosOcasionales;

    //Selector ingresos ocasionales
    @iOSFindBy(accessibility = "PickerSelectorTableViewCell.TextField.IngresosOcasionales")
    private MobileElement selectorIngresosOcasionales;

    /***************************************************************************************************/

    //Contenedor patrimonio financiero
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/selector_patrimonio_financiero")
    private MobileElement contPatrimonioFinanciero;

    //Selector patrimonio financiero
    @iOSFindBy(accessibility = "PickerSelectorTableViewCell.TextField.PatromonioFinanciero")
    private MobileElement selectorPatrimonioFinanciero;
    /***************************************************************************************************/

    //Contenedor patrimonio no financiero
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/selector_patrimonio_no_financiero")
    private MobileElement contPatrimonioNoFinanciero;

    //Selector patrimonio no financiero
    @iOSFindBy(accessibility = "PickerSelectorTableViewCell.TextField.PatrimonioNoFinanciero")
    private MobileElement selectorPatrimonioNoFinanciero;

    /*******************************************************************************************/
    /**
     * Elementos Selectores
     **/

    //Titulo selector
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/tv_title")
    private MobileElement tituloSelector;

    //Elementos selector
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/tv_element")
    private List<MobileElement> elementosSelector;

    //Pickerwheel
    @iOSFindBy(xpath = "//XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypePicker/XCUIElementTypePickerWheel")
    private MobileElement pickerWheel;

    //Botón continuar selector
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/btn_wheel_continuar")
    @iOSFindBy(xpath = "(//XCUIElementTypeButton[@name=\"CONTINUAR\"])[2]")
    private MobileElement btnContinuarSelector;

    /*******************************************************************************************/

    //Botón continuar
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/btn_continuar")
    @iOSFindBy(accessibility = "CONTINUAR")
    private MobileElement btnContinuar;


    //***************************************************************************************************
    // *                                        MÉTODOS                                                 *
    // **************************************************************************************************

    @Step
    public void validarIngresoPantalla() {
        AccionesGenericas.ingresoPantalla(tituloPagina, "Ingresos, Patrimonios y adicionales");
    }

    //***************************************************************************************************

    @Step
    public void validarElemtosSuperiores() {
        AccionesGenericas.validarElemento(tituloPagina, "Enrolarme para invertir", 5);
        AccionesGenericas.validarElemento(btnCerrar, 5);
        AccionesGenericas.validarElemento(flechaAtrasProgreso, 5);
        AccionesGenericas.validarElemento(barraProgreso, 5);
        AccionesGenericas.validarElemento(stepper, "4 de 5", 5);
       // AccionesGenericas.validarElemento(tituloIngresosPatrimonios, "Mis ingresos, patrimonios y adicionales", 5);
    }

    //***************************************************************************************************

    @Step
    public void validarOrigenFondos() {
        AccionesGenericas.validarElemento(tituloRdos, "Origen de fondos iniciales a invertir", 5);
        AccionesGenericas.validarElemento(rdoVentaActivos, "Venta de activos (Propiedades, Vehículos)", 5);
        AccionesGenericas.validarElemento(rdoVencOtrasInv, "Vencimiento otras inversiones financieras", 5);
        AccionesGenericas.validarElemento(rdoHerencia, "Herencia", 5);
        AccionesGenericas.validarElemento(rdoOtro, "Otro", 5);
    }

    //***************************************************************************************************

    @Step
    public void seleccionarOrigenFondos(String origenFondo) {
        switch (origenFondo) {
            case "Venta de activos":
                rdoVentaActivos.click();
                PdfBciReports.addMobilReportImage("Seleccionar Origen Fondos", "Se selecciona origen de fondos: " + origenFondo, EstadoPrueba.PASSED, false);
                break;
            case "Vencimiento otras inversiones financieras":
                rdoVencOtrasInv.click();
                PdfBciReports.addMobilReportImage("Seleccionar Origen Fondos", "Se selecciona origen de fondos: " + origenFondo, EstadoPrueba.PASSED, false);
                break;
            case "Herencia":
                rdoHerencia.click();
                PdfBciReports.addMobilReportImage("Seleccionar Origen Fondos", "Se selecciona origen de fondos: " + origenFondo, EstadoPrueba.PASSED, false);
                break;
            case "Otro":
                rdoOtro.click();
                PdfBciReports.addMobilReportImage("Seleccionar Origen Fondos", "Se selecciona origen de fondos: " + origenFondo, EstadoPrueba.PASSED, false);
                break;
            default:
                PdfBciReports.addMobilReportImage("Seleccionar Origen Fondos", "No se seleccionó ningún origen de fondos", EstadoPrueba.FAILED, true);
        }
    }

    //***************************************************************************************************

    @Step
    public void validarSelectorPaisOrigenFF() {
        if (DriverContext.getDriver() instanceof IOSDriver){
            AccionesGenericas.validarElemento(selectorOrigenFondos, 5);
            if(selectorOrigenFondos.getText().isEmpty()){
                PdfBciReports.addMobilReportImage("Validar Selector Pais Origen Fondos", "No se encuentra país de origen de fondos por defecto", EstadoPrueba.PASSED, false);
            }else{
                PdfBciReports.addMobilReportImage("Validar Selector Pais Origen Fondos", "País de origen de fondos cargado por defecto: " + selectorOrigenFondos.getText(), EstadoPrueba.PASSED, false);
            }
        }else {
            List<MobileElement> elementos = contPaisOrigenFF.findElements(By.className("android.widget.TextView"));
            if (elementos.size() == 1) {
                AccionesGenericas.validarElemento(elementos.get(0), "País de origen de los fondos", 5);
                PdfBciReports.addMobilReportImage("Validar Selector Pais Origen Fondos", "No se encuentra país de origen de fondos por defecto", EstadoPrueba.PASSED, false);
            } else {
                AccionesGenericas.validarElemento(elementos.get(0), "País de origen de los fondos", 5);
                AccionesGenericas.validarElemento(elementos.get(1), 5);
                PdfBciReports.addMobilReportImage("Validar Selector Pais Origen Fondos", "País de origen de fondos cargado por defecto: " + elementos.get(1).getText(), EstadoPrueba.PASSED, false);
            }
        }
    }

    //***************************************************************************************************

    @Step
    public void seleccionarPaisOrigenFF(String paisOrigen) {
        if (DriverContext.getDriver() instanceof IOSDriver) {
            selectorOrigenFondos.click();
            pickerWheel.sendKeys(paisOrigen);
            btnContinuarSelector.click();
            PdfBciReports.addMobilReportImage("Seleccionar Pais Origen Fondos", "Se selecciona país de origen de fondos: " + paisOrigen, EstadoPrueba.PASSED, false);
        }else {
            Gestures gestures = new Gestures();
            List<MobileElement> elementos = contPaisOrigenFF.findElements(By.className("android.widget.TextView"));
            boolean seleccionado = false;
            if (elementos.size() == 1) {
                elementos.get(0).click();
            } else {
                elementos.get(1).click();
            }

            //Para los primeros 4 elementos del pickerwheel //
            loop:
            for (int i = 0; i < 4; i++) {
                if (elementosSelector.get(i).getText().equals(paisOrigen)) {
                    seleccionado = true;
                    btnContinuarSelector.click();
                    elementos = contPaisOrigenFF.findElements(By.className("android.widget.TextView"));
                    if (elementos.get(1).getText().equals(paisOrigen)) {
                        PdfBciReports.addMobilReportImage("Seleccionar Pais Origen Fondos", "Se selecciona país de origen de fondos: " + paisOrigen, EstadoPrueba.PASSED, false);
                    } else {
                        PdfBciReports.addMobilReportImage("Seleccionar Pais Origen Fondos", "No se selecciona país de origen de fondos esperado: " + paisOrigen, EstadoPrueba.FAILED, true);
                    }
                    break loop;
                }
                gestures.scrollSelectorElementos(elementosSelector.get(i));
            }

            //Para el resto de elementos del pickerwheel
            if (!seleccionado) {
                while (!elementosSelector.get(3).getText().equals(paisOrigen)) {
                    gestures.scrollSelectorElementos(elementosSelector.get(3));
                }
                btnContinuarSelector.click();
                elementos = contPaisOrigenFF.findElements(By.className("android.widget.TextView"));
                if (elementos.get(1).getText().equals(paisOrigen)) {
                    PdfBciReports.addMobilReportImage("Seleccionar Pais Origen Fondos", "Se selecciona país de origen de fondos: " + paisOrigen, EstadoPrueba.PASSED, false);
                } else {
                    PdfBciReports.addMobilReportImage("Seleccionar Pais Origen Fondos", "No se selecciona país de origen de fondos esperado: " + paisOrigen, EstadoPrueba.FAILED, true);
                }
            }
        }
    }

    //***************************************************************************************************

    @Step
    public void validarSelectorIngresosMensuales() {
        Gestures gestures = new Gestures();
        if (DriverContext.getDriver() instanceof IOSDriver) {
            gestures.encontrarObjeto(selectorIngresosMensuales, "Selector Ingresos mensuales");
            AccionesGenericas.validarElemento(selectorIngresosMensuales, 5);
            if(selectorIngresosMensuales.getText().isEmpty()){
                PdfBciReports.addMobilReportImage("Validar Selector Ingresos Mensuales", "No se encuentra ingresos mensuales cargado por defecto", EstadoPrueba.PASSED, false);
            }else{
                PdfBciReports.addMobilReportImage("Validar Selector Ingresos Mensuales", "Ingresos mensuales cargado por defecto: " + selectorIngresosMensuales.getText(), EstadoPrueba.PASSED, false);
            }
        }else {
            gestures.encontrarObjeto(contIngresosMensuales, "Selector Ingresos mensuales");
            List<MobileElement> elementos = contIngresosMensuales.findElements(By.className("android.widget.TextView"));
            if (elementos.size() == 1) {
                AccionesGenericas.validarElemento(elementos.get(0), "Ingresos mensuales", 5);
                PdfBciReports.addMobilReportImage("Validar Selector Ingresos Mensuales", "No se encuentra ingresos mensuales cargado por defecto", EstadoPrueba.PASSED, false);
            } else {
                AccionesGenericas.validarElemento(elementos.get(0), "Ingresos mensuales", 5);
                AccionesGenericas.validarElemento(elementos.get(1), 5);
                PdfBciReports.addMobilReportImage("Validar Selector Ingresos Mensuales", "Ingresos mensuales cargado por defecto: " + elementos.get(1).getText(), EstadoPrueba.PASSED, false);
            }
        }
    }

    //***************************************************************************************************

    @Step
    public void seleccionarIngresosMensuales(String ingresos) {
        if (DriverContext.getDriver() instanceof IOSDriver) {
            selectorIngresosMensuales.click();
            pickerWheel.sendKeys(ingresos);
            btnContinuarSelector.click();
            PdfBciReports.addMobilReportImage("Seleccionar Ingresos Mensuales", "Se selecciona ingresos: " + ingresos, EstadoPrueba.PASSED, false);
        }else {
            List<MobileElement> elementos = contIngresosMensuales.findElements(By.className("android.widget.TextView"));
            if (elementos.size() == 1) {
                elementos.get(0).click();
            } else {
                elementos.get(1).click();
            }
            switch (ingresos) {
                case "De 0 a $1.600.000":
                    elementosSelector.get(0).click();
                    PdfBciReports.addMobilReportImage("Seleccionar Ingresos Mensuales", "Se selecciona ingresos: " + ingresos, EstadoPrueba.PASSED, false);
                    break;
                case "De $1.600.001 a $4.160.000":
                    elementosSelector.get(1).click();
                    PdfBciReports.addMobilReportImage("Seleccionar Ingresos Mensuales", "Se selecciona ingresos: " + ingresos, EstadoPrueba.PASSED, false);
                    break;
                case "De $4.160.001 a $7.500.000":
                    elementosSelector.get(2).click();
                    PdfBciReports.addMobilReportImage("Seleccionar Ingresos Mensuales", "Se selecciona ingresos: " + ingresos, EstadoPrueba.PASSED, false);
                    break;
                case "Más de $7.500.000":
                    elementosSelector.get(3).click();
                    PdfBciReports.addMobilReportImage("Seleccionar Ingresos Mensuales", "Se selecciona ingresos: " + ingresos, EstadoPrueba.PASSED, false);
                    break;
                default:
                    PdfBciReports.addMobilReportImage("Seleccionar Ingresos Mensuales", "No se ha seleccionado ningún ingreso mensual", EstadoPrueba.FAILED, true);
                    break;
            }
        }
    }

    //***************************************************************************************************

    @Step
    public void validarSelectorIngresosOcasionales() {
        Gestures gestures = new Gestures();
        if (DriverContext.getDriver() instanceof IOSDriver) {
            gestures.encontrarObjeto(selectorIngresosOcasionales, "Selector Ingresos Ocasionales");
            AccionesGenericas.validarElemento(selectorIngresosOcasionales, 5);
            if(selectorIngresosOcasionales.getText().isEmpty()){
                PdfBciReports.addMobilReportImage("Validar Selector Ingresos Ocasionales", "No se encuentra ingresos ocasionales cargado por defecto", EstadoPrueba.PASSED, false);
            }else{
                PdfBciReports.addMobilReportImage("Validar Selector Ingresos Ocasionales", "Ingresos ocasionales cargado por defecto: " + selectorIngresosOcasionales.getText(), EstadoPrueba.PASSED, false);
            }
        }else {
            gestures.encontrarObjeto(contIngresosOcasionales, "Selecctor Ingresos ocasionales");
            List<MobileElement> elementos = contIngresosOcasionales.findElements(By.className("android.widget.TextView"));
            if (elementos.size() == 1) {
                AccionesGenericas.validarElemento(elementos.get(0), "Ingresos ocasionales", 5);
                PdfBciReports.addMobilReportImage("Validar Selector Ingresos Ocasionales", "No se encuentra ingresos ocasionales cargado por defecto", EstadoPrueba.PASSED, false);
            } else {
                AccionesGenericas.validarElemento(elementos.get(0), "Ingresos ocasionales", 5);
                AccionesGenericas.validarElemento(elementos.get(1), 5);
                PdfBciReports.addMobilReportImage("Validar Selector Ingresos Ocasionales", "Ingresos ocasionales cargado por defecto: " + elementos.get(1).getText(), EstadoPrueba.PASSED, false);
            }
        }
    }

    //***************************************************************************************************

    @Step
    public void seleccionarIngresosOcasionales(String ingresos) {
        if (DriverContext.getDriver() instanceof IOSDriver) {
            selectorIngresosOcasionales.click();
            pickerWheel.sendKeys(ingresos);
            btnContinuarSelector.click();
            PdfBciReports.addMobilReportImage("Seleccionar Ingresos Mensuales", "Se selecciona ingresos: " + ingresos, EstadoPrueba.PASSED, false);
        }else {
            List<MobileElement> elementos = contIngresosOcasionales.findElements(By.className("android.widget.TextView"));
            if (elementos.size() == 1) {
                elementos.get(0).click();
            } else {
                elementos.get(1).click();
            }
            switch (ingresos) {
                case "De 0 a $1.600.000":
                    elementosSelector.get(0).click();
                    PdfBciReports.addMobilReportImage("Seleccionar Ingresos Mensuales", "Se selecciona ingresos: " + ingresos, EstadoPrueba.PASSED, false);
                    break;
                case "De $1.600.001 a $4.160.000":
                    elementosSelector.get(1).click();
                    PdfBciReports.addMobilReportImage("Seleccionar Ingresos Mensuales", "Se selecciona ingresos: " + ingresos, EstadoPrueba.PASSED, false);
                    break;
                case "De $4.160.001 a $7.500.000":
                    elementosSelector.get(2).click();
                    PdfBciReports.addMobilReportImage("Seleccionar Ingresos Mensuales", "Se selecciona ingresos: " + ingresos, EstadoPrueba.PASSED, false);
                    break;
                case "Más de $7.500.000":
                    elementosSelector.get(3).click();
                    PdfBciReports.addMobilReportImage("Seleccionar Ingresos Mensuales", "Se selecciona ingresos: " + ingresos, EstadoPrueba.PASSED, false);
                    break;
                default:
                    PdfBciReports.addMobilReportImage("Seleccionar Ingresos Mensuales", "No se ha seleccionado ningún ingreso mensual", EstadoPrueba.FAILED, true);
                    break;
            }
        }
    }

    //***************************************************************************************************

    @Step
    public void validarSelectorPatriminioFinanciero() {
        Gestures gestures = new Gestures();
        if (DriverContext.getDriver() instanceof IOSDriver) {
            gestures.encontrarObjeto(selectorPatrimonioFinanciero, "Selector Patrimonio Financiero");
            AccionesGenericas.validarElemento(selectorPatrimonioFinanciero, 5);
            if(selectorPatrimonioFinanciero.getText().isEmpty()){
                PdfBciReports.addMobilReportImage("Validar Selector Patrimonio Financiero", "No se encuentra patrimonio financiero cargado por defecto", EstadoPrueba.PASSED, false);
            }else{
                PdfBciReports.addMobilReportImage("Validar Selector Patrimonio Financiero", "Patrimonio financiero cargado por defecto: " + selectorPatrimonioFinanciero.getText(), EstadoPrueba.PASSED, false);
            }
        }else {
            gestures.encontrarObjeto(contPatrimonioFinanciero, "Selector patrimonio financiero");
            List<MobileElement> elementos = contPatrimonioFinanciero.findElements(By.className("android.widget.TextView"));
            if (elementos.size() == 1) {
                AccionesGenericas.validarElemento(elementos.get(0), "Patrimonio financiero", 5);
                PdfBciReports.addMobilReportImage("Validar Selector Patrimonio Financiero", "No se encuentra patrimonio financiero cargado por defecto", EstadoPrueba.PASSED, false);
            } else {
                AccionesGenericas.validarElemento(elementos.get(0), "Patrimonio financiero", 5);
                AccionesGenericas.validarElemento(elementos.get(1), 5);
                PdfBciReports.addMobilReportImage("Validar Selector Patrimonio Financiero", "Patrimonio financiero cargado por defecto: " + elementos.get(1).getText(), EstadoPrueba.PASSED, false);
            }
        }
    }

    //***************************************************************************************************

    @Step
    public void seleccionarPatrimonioFinanciero(String patrimonio) {
        if (DriverContext.getDriver() instanceof IOSDriver) {
            selectorPatrimonioFinanciero.click();
            pickerWheel.sendKeys(patrimonio);
            btnContinuarSelector.click();
            PdfBciReports.addMobilReportImage("Seleccionar patrimonio financiero", "Se selecciona patrimonio: " + patrimonio, EstadoPrueba.PASSED, false);
        }else {
            List<MobileElement> elementos = contPatrimonioFinanciero.findElements(By.className("android.widget.TextView"));
            if (elementos.size() == 1) {
                elementos.get(0).click();
            } else {
                elementos.get(1).click();
            }
            switch (patrimonio) {
                case "Menos de 30 millones":
                    elementosSelector.get(0).click();
                    PdfBciReports.addMobilReportImage("Seleccionar patrimonio financiero", "Se selecciona patrimonio: " + patrimonio, EstadoPrueba.PASSED, false);
                    break;
                case "Entre 30 y 125 millones":
                    elementosSelector.get(1).click();
                    PdfBciReports.addMobilReportImage("Seleccionar patrimonio financiero", "Se selecciona patrimonio: " + patrimonio, EstadoPrueba.PASSED, false);
                    break;
                case "Entre 125 y 300 millones":
                    elementosSelector.get(2).click();
                    PdfBciReports.addMobilReportImage("Seleccionar patrimonio financiero", "Se selecciona patrimonio: " + patrimonio, EstadoPrueba.PASSED, false);
                    break;
                case "Entre 300 y 500 millones":
                    elementosSelector.get(3).click();
                    PdfBciReports.addMobilReportImage("Seleccionar patrimonio financiero", "Se selecciona patrimonio: " + patrimonio, EstadoPrueba.PASSED, false);
                    break;
                case "Más de 500 millones":
                    elementosSelector.get(4).click();
                    PdfBciReports.addMobilReportImage("Seleccionar patrimonio financiero", "Se selecciona patrimonio: " + patrimonio, EstadoPrueba.PASSED, false);
                    break;
                default:
                    PdfBciReports.addMobilReportImage("Seleccionar patrimonio financiero", "No se selcciona ningún rango de patrimonio financiero", EstadoPrueba.FAILED, true);
                    break;
            }
        }
    }

    //***************************************************************************************************

    @Step
    public void validarSelectorPatriminioNoFinanciero() {
        Gestures gestures = new Gestures();
        if (DriverContext.getDriver() instanceof IOSDriver) {
            gestures.encontrarObjeto(selectorPatrimonioNoFinanciero, "Selector patrimonio no financiero");
            AccionesGenericas.validarElemento(selectorPatrimonioNoFinanciero, 5);
            if(selectorPatrimonioNoFinanciero.getText().isEmpty()){
                PdfBciReports.addMobilReportImage("Validar Selector Patrimonio No Financiero", "No se encuentra patrimonio no financiero cargado por defecto", EstadoPrueba.PASSED, false);
            }else{
                PdfBciReports.addMobilReportImage("Validar Selector Patrimonio No Financiero", "Patrimonio no financiero cargado por defecto: " + selectorPatrimonioNoFinanciero, EstadoPrueba.PASSED, false);
            }
        }else {
            gestures.encontrarObjeto(contPatrimonioNoFinanciero, "selector patrimonio no financiero");
            List<MobileElement> elementos = contPatrimonioNoFinanciero.findElements(By.className("android.widget.TextView"));
            if (elementos.size() == 1) {
                AccionesGenericas.validarElemento(elementos.get(0), "Patrimonio no financiero", 5);
                PdfBciReports.addMobilReportImage("Validar Selector Patrimonio No Financiero", "No se encuentra patrimonio no financiero cargado por defecto", EstadoPrueba.PASSED, false);
            } else {
                AccionesGenericas.validarElemento(elementos.get(0), "Patrimonio no financiero", 5);
                AccionesGenericas.validarElemento(elementos.get(1), 5);
                PdfBciReports.addMobilReportImage("Validar Selector Patrimonio No Financiero", "Patrimonio no financiero cargado por defecto: " + elementos.get(1).getText(), EstadoPrueba.PASSED, false);
            }
        }
    }

    //***************************************************************************************************

    @Step
    public void seleccionarPatrimonioNoFinanciero(String patrimonio) {
        if (DriverContext.getDriver() instanceof IOSDriver) {
            selectorPatrimonioNoFinanciero.click();
            pickerWheel.sendKeys(patrimonio);
            btnContinuarSelector.click();
            PdfBciReports.addMobilReportImage("Seleccionar Patrimonio no Financiero", "Se selecciona patrimonio: " + patrimonio, EstadoPrueba.PASSED, false);
        }else {
            List<MobileElement> elementos = contPatrimonioNoFinanciero.findElements(By.className("android.widget.TextView"));
            if (elementos.size() == 1) {
                elementos.get(0).click();
            } else {
                elementos.get(1).click();
            }
            switch (patrimonio) {
                case "Menos de 30 millones":
                    elementosSelector.get(0).click();
                    PdfBciReports.addMobilReportImage("Seleccionar Patrimonio no Financiero", "Se selecciona patrimonio: " + patrimonio, EstadoPrueba.PASSED, false);
                    break;
                case "Entre 30 y 125 millones":
                    elementosSelector.get(1).click();
                    PdfBciReports.addMobilReportImage("Seleccionar Patrimonio no Financiero", "Se selecciona patrimonio: " + patrimonio, EstadoPrueba.PASSED, false);
                    break;
                case "Entre 125 y 300 millones":
                    elementosSelector.get(2).click();
                    PdfBciReports.addMobilReportImage("Seleccionar Patrimonio no Financiero", "Se selecciona patrimonio: " + patrimonio, EstadoPrueba.PASSED, false);
                    break;
                case "Entre 300 y 500 millones":
                    elementosSelector.get(3).click();
                    PdfBciReports.addMobilReportImage("Seleccionar Patrimonio no Financiero", "Se selecciona patrimonio: " + patrimonio, EstadoPrueba.PASSED, false);
                    break;
                case "Más de 500 millones":
                    elementosSelector.get(4).click();
                    PdfBciReports.addMobilReportImage("Seleccionar Patrimonio no Financiero", "Se selecciona patrimonio: " + patrimonio, EstadoPrueba.PASSED, false);
                    break;
                default:
                    PdfBciReports.addMobilReportImage("Seleccionar Patrimonio no Financiero", "No se ha seleccionado ningún rango de patrionio no financiero", EstadoPrueba.FAILED, true);
                    break;
            }
        }
    }

    //***************************************************************************************************

    @Step
    public void validarBotonContinuar() {
        Gestures gestures = new Gestures();
        gestures.encontrarObjeto(btnContinuar, "Botón CONTINUAR");
        AccionesGenericas.validarElemento(btnContinuar, "CONTINUAR", 5);
    }

    //***************************************************************************************************

    @Step
    public void tapBotonContinuar() {
        if (btnContinuar.isEnabled()) {
            btnContinuar.click();
            PdfBciReports.addReport("Tap botón continuar", "Se hace tap al botón continuar", EstadoPrueba.PASSED, false);
        } else {
            PdfBciReports.addMobilReportImage("Tap botón continuar", "El botón continuar no se encuentra habilitado", EstadoPrueba.PASSED, false);
        }
    }

    //***************************************************************************************************

    @Step
    public void tapBotonCerrar() {
        btnCerrar.click();
        PdfBciReports.addReport("Tap botón cerrar", "Se hace tap al botón cerrar", EstadoPrueba.PASSED, false);
    }

    //***************************************************************************************************

    @Step
    public void tapFlechaVolverAtras() {
        flechaAtrasProgreso.click();
        PdfBciReports.addReport("Tap botón volver atrás", "Se hace tap a la flecha volver atrás", EstadoPrueba.PASSED, false);
    }


}
