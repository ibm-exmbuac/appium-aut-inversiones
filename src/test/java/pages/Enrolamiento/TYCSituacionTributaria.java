package pages.Enrolamiento;

import constants.ConstantesInversiones;
import driver.DriverContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.qameta.allure.Step;
import org.openqa.selenium.support.PageFactory;
import reporter.EstadoPrueba;
import reporter.PdfBciReports;
import util.AccionesGenericas;

public class TYCSituacionTributaria {

    private AppiumDriver driver;

    public TYCSituacionTributaria() {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver), this);
    }

    /***************************************************************************************************
     *                                          OBJETOS                                                *
     ***************************************************************************************************/

    //Terminos y condiciones
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/cbTerminosCondiciones")
    private MobileElement terminosYCondiciones;

    /*******************************************************************************************/

    //Título terminos y condiciones
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/toolbar_title")
    private MobileElement tituloTYC;

    //Botón X cerrar
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/iv_icon_close")
    private MobileElement btnXCerrar;

    //Terminos y condiciones info
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/tv_message")
    private MobileElement infoTYC;

    //Terminos y condiciones
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/terminos_condiciones_button")
    private MobileElement btnCerrar;


    //***************************************************************************************************
    // *                                        MÉTODOS                                                 *
    // **************************************************************************************************

    @Step
    public void validarElementos() {
        AccionesGenericas.validarElemento(tituloTYC, "Se entiende como US Person:", 5);
        AccionesGenericas.validarElemento(btnXCerrar, 5);
        AccionesGenericas.validarElemento(infoTYC, "- Cualquier persona natural que sea residente de los Estados Unidos de América\n" +
                " - Cualquier sociedad, corporación u otra entidad organizada o constituida bajo las leyes de los Estados Unidos de América o que tenga su establecimiento principal en los Estados Unidos de América.\n" +
                " - Cualquier Estado o trust (fideicomiso), cuyas utilidades están sujetas al pago de impuestos en los Estados Unidos de América, independiente de su fuente.\n" +
                " - Cualquier entidad organizada principalmente para la inversión pasiva, como un commodity pool, sociedad de inversiones u otro tipo similar (distinto de un plan de pensiones para trabajadores, ejecutivos y gerentes de una entidad organizada y con su establecimiento principal de negocios fuera de los Estados Unidos de América):\n" +
                " - En que los U.S. Person tengan una participación agregada igual o superior a un 10% del total de la entidad o que tenga como objeto principal facilitar la realización de inversiones por un U.S. Person en un commodity pool, respecto del cual su administrador u operador esté exento de cumplir ciertos requisitos de la Parte 4 de las regulaciones de la Comisión en razón de que sus partícipes no son U.S. Person.", 5);
        AccionesGenericas.validarElemento(btnCerrar, "CERRAR", 5);
    }

    @Step
    public void tapBotonCerrar() {
        btnCerrar.click();
        PdfBciReports.addMobilReportImage("tapBotonCerrar", "Se hace tap al botón 'Cerrar'", EstadoPrueba.PASSED, false);
    }
}
