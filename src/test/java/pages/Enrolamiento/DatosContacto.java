package pages.Enrolamiento;

import constants.ConstantesInversiones;
import driver.DriverContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import reporter.EstadoPrueba;
import reporter.PdfBciReports;
import util.AccionesGenericas;
import util.Gestures;

import java.util.List;

public class DatosContacto {

    private AppiumDriver driver;

    public DatosContacto() {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver), this);
    }

    /***************************************************************************************************
     *                                          OBJETOS                                                *
     ***************************************************************************************************/

    //Título pantalla
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/toolbar_title")
    @iOSFindBy(accessibility = "Enrolarme para invertir")
    private MobileElement tituloPagina;

    //Botón cerrar
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/action_close")
    @iOSFindBy(accessibility = "close")
    private MobileElement btnCerrar;

    //Flecha atrás progreso
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/bt_progress_back")
    @iOSFindBy(accessibility = "UIView.ProgressBarPager.PreviousStepButton")
    private MobileElement flechaAtrasProgreso;

    //Barra progreso
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/horizontal_progress_bar")
    @iOSFindBy(accessibility = "UIView.ProgressBarPager.ProgressBarView")
    private MobileElement barraProgreso;

    //Stepper
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/tv_stepper_indicator")
    @iOSFindBy(accessibility = "UIView.ProgressBarPager.StepIndicatorLabel")
    private MobileElement stepper;

    //Titulo Mis datos de contacto
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/tv_title")
    @iOSFindBy(accessibility = "Mis datos de contacto")
    private MobileElement tituloDatosContacto;

    /***************************************************************************************************/

    //Contenedor dirección principal
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/etb_direccion_municipal")
    private MobileElement contDireccion;

    //Input Dirección principal
    @iOSFindBy(accessibility = "TextFieldTableViewCell.TextField.Direccion")
    private MobileElement inputDireccionPrincipal;

    /***************************************************************************************************/

    //Contenedor región
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/selector_region")
    private MobileElement contRegion;

    //Selector región
    @iOSFindBy(accessibility = "PickerSelectorTableViewCell.TextField.Region")
    private MobileElement selectorRegion;

    /***************************************************************************************************/

    //Contenedor comuna
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/selector_comuna")
    private MobileElement contComuna;

    //Selector comuna
    @iOSFindBy(accessibility = "PickerSelectorTableViewCell.TextField.Comuna")
    private MobileElement selectorComuna;

    /***************************************************************************************************/

    //Contenedor telefono
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/etb_telefono")
    private MobileElement contTelefono;

    //Input teléfono
    @iOSFindBy(accessibility = "TextFieldTableViewCell.TextField.Telefono")
    private MobileElement inputTelefono;

    /***************************************************************************************************/

    //Contenedor email
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/etb_correo")
    private MobileElement contEmail;

    //Input email
    @iOSFindBy(accessibility = "TextFieldTableViewCell.TextField.Correo")
    private MobileElement inputEmail;

    /***************************************************************************************************/

    //Disclaimer
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/tv_disclaimer")
    @iOSFindBy(accessibility = "InfoTableViewCell.MessageLabel")
    private MobileElement disclaimer;

    //Botón continuar
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/btn_continuar")
    @iOSFindBy(accessibility = "CONTINUAR")
    private MobileElement btnContinuar;

    /*******************************************************************************************/
    /**
     * Elementos Selectores
     **/

    //Titulo selector
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/tv_title")
    private MobileElement tituloSelector;

    //Elementos selector
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/tv_element")
    private List<MobileElement> elementosSelector;

    //Pickerwheel
    @iOSFindBy(xpath = "//XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypePicker/XCUIElementTypePickerWheel")
    private MobileElement pickerWheel;

    //Botón continuar selector
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/btn_wheel_continuar")
    @iOSFindBy(xpath = "(//XCUIElementTypeButton[@name=\"CONTINUAR\"])[2]")
    private MobileElement btnContinuarSelector;


    //***************************************************************************************************
    // *                                        MÉTODOS                                                 *
    // **************************************************************************************************

    @Step
    public void validarIngresoPantalla() {
        AccionesGenericas.ingresoPantalla(tituloDatosContacto, "Datos de Contacto");
    }

    // **************************************************************************************************

    @Step
    public void validarElementosSuperiores() {
        AccionesGenericas.validarElemento(tituloPagina, "Enrolarme para invertir", 5);
        AccionesGenericas.validarElemento(btnCerrar, 5);
        AccionesGenericas.validarElemento(flechaAtrasProgreso, 5);
        AccionesGenericas.validarElemento(barraProgreso, 5);
        AccionesGenericas.validarElemento(stepper, "2 de 5", 5);
        AccionesGenericas.validarElemento(tituloDatosContacto, "Mis datos de contacto", 5);
    }

    // **************************************************************************************************

    @Step
    public void validarInputDireccion() {
        if (DriverContext.getDriver() instanceof IOSDriver){
            AccionesGenericas.validarElemento(inputDireccionPrincipal, 5);
            if (inputDireccionPrincipal.getText().isEmpty()){
                PdfBciReports.addMobilReportImage("Validar input dirección", "No se encuentra dirección precargada por defecto", EstadoPrueba.PASSED, false);
            }else{
                PdfBciReports.addMobilReportImage("Validar input dirección", "Dirección precargada por defecto: " + inputDireccionPrincipal.getText(), EstadoPrueba.PASSED, false);

            }
        }else{
            MobileElement labelDireccion = contDireccion.findElement(By.className("TextInputLayout"));
            MobileElement contInput = labelDireccion.findElement(By.className("android.widget.FrameLayout"));
            MobileElement inputDireccion = contInput.findElement(By.className("android.widget.EditText"));
            AccionesGenericas.validarElemento(labelDireccion, "Dirección principal", 5);
            AccionesGenericas.validarElemento(inputDireccion, 5);
            if (!inputDireccion.getText().isEmpty()) {
                PdfBciReports.addMobilReportImage("Validar input dirección", "Dirección precargada por defecto: " + inputDireccion.getText(), EstadoPrueba.PASSED, false);
            } else {
                PdfBciReports.addMobilReportImage("Validar input dirección", "No se encuentra dirección precargada por defecto", EstadoPrueba.PASSED, false);
            }
        }

    }

    // **************************************************************************************************

    @Step
    public void validarIngresoCaracteresDireccion() {
        MobileElement labelDireccion = contDireccion.findElement(By.className("TextInputLayout"));
        MobileElement contInput = labelDireccion.findElement(By.className("android.widget.FrameLayout"));
        MobileElement inputDireccion = contInput.findElement(By.className("android.widget.EditText"));
        inputDireccion.setValue("12345123451234512345123451234512345123451");
        MobileElement mensajeValidacion = contDireccion.findElement(By.className("android.widget.TextView"));
        if (AccionesGenericas.existeElemento(mensajeValidacion, 5)) {
            AccionesGenericas.validarElemento(mensajeValidacion, "", 5);
            PdfBciReports.addMobilReportImage("Validar ingreso caracteres dirección", "Se muestra mensaje de alerta", EstadoPrueba.PASSED, false);
        } else {
            PdfBciReports.addMobilReportImage("Validar ingreso caracteres dirección", "No se muestra mensaje de alerta", EstadoPrueba.FAILED, false);
            PdfBciReports.closePDF();
        }
    }

    // **************************************************************************************************

    @Step
    public void ingresarDireccion(String direccion) {
        if (DriverContext.getDriver() instanceof IOSDriver){
            inputDireccionPrincipal.setValue(direccion);
            PdfBciReports.addMobilReportImage("Ingresar dirección", "Se ingresa la dirección: " + direccion, EstadoPrueba.PASSED, false);
        }else{
            MobileElement labelDireccion = contDireccion.findElement(By.className("TextInputLayout"));
            MobileElement contInput = labelDireccion.findElement(By.className("android.widget.FrameLayout"));
            MobileElement inputDireccion = contInput.findElement(By.className("android.widget.EditText"));
            inputDireccion.setValue(direccion);
            PdfBciReports.addMobilReportImage("Ingresar dirección", "Se ingresa la dirección: " + direccion, EstadoPrueba.PASSED, false);
        }
    }

    // **************************************************************************************************

    @Step
    public void validarSelectorRegion() {
        if (DriverContext.getDriver() instanceof IOSDriver) {
            AccionesGenericas.validarElemento(selectorRegion, 5);
            if(selectorRegion.getText().isEmpty()){
                PdfBciReports.addMobilReportImage("Validar selector región", "No se encuentra región cargada por defecto", EstadoPrueba.PASSED, false);
            }else{
                PdfBciReports.addMobilReportImage("Validar selector región", "Región cargada por defecto: " + selectorRegion.getText(), EstadoPrueba.PASSED, false);
            }
        }else{
            List<MobileElement> elementos = contRegion.findElements(By.className("android.widget.TextView"));
            if (elementos.size() == 1) {
                AccionesGenericas.validarElemento(elementos.get(0), "Región", 5);
                PdfBciReports.addMobilReportImage("Validar selector región", "No se encuentra región cargada por defecto", EstadoPrueba.PASSED, false);
            } else {
                AccionesGenericas.validarElemento(elementos.get(0), "Región", 5);
                AccionesGenericas.validarElemento(elementos.get(1), 5);
                PdfBciReports.addMobilReportImage("Validar selector región", "Región cargada por defecto: " + elementos.get(1).getText(), EstadoPrueba.PASSED, false);
            }
        }

    }

    // **************************************************************************************************

    @Step
    public void seleccionarRegion(String region) {
        if (DriverContext.getDriver() instanceof IOSDriver) {
            selectorRegion.click();
            pickerWheel.sendKeys(region);
            btnContinuarSelector.click();
            if(selectorRegion.getText().equals(region)){
                PdfBciReports.addMobilReportImage("Seleccionar región", "Se selecciona region: " + region, EstadoPrueba.PASSED, false);
            }else{
                PdfBciReports.addMobilReportImage("Seleccionar región", "No se selecciona la region esperada: " + region, EstadoPrueba.FAILED, true);
            }
        }else {
            Gestures gestures = new Gestures();
            List<MobileElement> elementos = contRegion.findElements(By.className("android.widget.TextView"));
            boolean seleccionado = false;
            if (elementos.size() == 1) {
                elementos.get(0).click();
            } else {
                elementos.get(1).click();
            }

            //Para los primeros 4 elementos del pickerwheel //
            loop:
            for (int i = 0; i < 4; i++) {
                if (elementosSelector.get(i).getText().equals(region)) {
                    seleccionado = true;
                    btnContinuarSelector.click();
                    elementos = contRegion.findElements(By.className("android.widget.TextView"));
                    if (elementos.get(1).getText().equals(region)) {
                        PdfBciReports.addMobilReportImage("Seleccionar región", "Se selecciona region: " + region, EstadoPrueba.PASSED, false);
                    } else {
                        PdfBciReports.addMobilReportImage("Seleccionar región", "No se selecciona la region esperada: " + region, EstadoPrueba.FAILED, false);
                        PdfBciReports.closePDF();
                    }
                    break loop;
                }
                gestures.scrollSelectorElementos(elementosSelector.get(i));
            }

            //Para el resto de elementos del pickerwheel
            if (!seleccionado) {
                while (!elementosSelector.get(3).getText().equals(region)) {
                    gestures.scrollSelectorElementos(elementosSelector.get(3));
                }
                btnContinuarSelector.click();
                elementos = contRegion.findElements(By.className("android.widget.TextView"));
                if (elementos.get(1).getText().equals(region)) {
                    PdfBciReports.addMobilReportImage("Seleccionar región", "Se selecciona region: " + region, EstadoPrueba.PASSED, false);
                } else {
                    PdfBciReports.addMobilReportImage("Seleccionar región", "No se selecciona la region esperada: " + region, EstadoPrueba.FAILED, false);
                    PdfBciReports.closePDF();
                }
            }
        }
    }

    // **************************************************************************************************

    @Step
    public void validarSelectorComuna() {
        if (DriverContext.getDriver() instanceof IOSDriver) {
            AccionesGenericas.validarElemento(selectorComuna, 5);
            if (selectorComuna.getText().isEmpty()) {
                PdfBciReports.addMobilReportImage("Validar selector comuna", "No se encuentra Comuna cargada por defecto", EstadoPrueba.PASSED, false);
            }else{
                PdfBciReports.addMobilReportImage("Validar selector comuna", "Comuna cargada por defecto: " + selectorComuna.getText(), EstadoPrueba.PASSED, false);
            }
        }else {
            List<MobileElement> elementos = contComuna.findElements(By.className("android.widget.TextView"));
            if (elementos.size() == 1) {
                AccionesGenericas.validarElemento(elementos.get(0), "Comuna", 5);
                PdfBciReports.addMobilReportImage("Validar selector comuna", "No se encuentra Comuna cargada por defecto", EstadoPrueba.PASSED, false);
            } else {
                AccionesGenericas.validarElemento(elementos.get(0), "Comuna", 5);
                AccionesGenericas.validarElemento(elementos.get(1), 5);
                PdfBciReports.addMobilReportImage("Validar selector comuna", "Comuna cargada por defecto: " + elementos.get(1).getText(), EstadoPrueba.PASSED, false);
            }
        }
    }

    // **************************************************************************************************

    @Step
    public void seleccionarComuna(String comuna) {
        if (DriverContext.getDriver() instanceof IOSDriver) {
            selectorComuna.click();
            pickerWheel.sendKeys(comuna);
            btnContinuarSelector.click();
            if(selectorComuna.getText().equals(comuna)) {
                PdfBciReports.addMobilReportImage("Seleccionar comuna", "Se selecciona comuna: " + comuna, EstadoPrueba.PASSED, false);
            }else {
                PdfBciReports.addMobilReportImage("Seleccionar Comuna", "No se selecciona la comuna esperada: " + comuna, EstadoPrueba.FAILED, true);
            }
        }else {
            Gestures gestures = new Gestures();
            List<MobileElement> elementos = contComuna.findElements(By.className("android.widget.TextView"));
            boolean seleccionado = false;
            if (elementos.size() == 1) {
                elementos.get(0).click();
            } else {
                elementos.get(1).click();
            }

            //Para los primeros 4 elementos del pickerwheel //
            loop:
            for (int i = 0; i < 4; i++) {
                if (elementosSelector.get(i).getText().equals(comuna)) {
                    seleccionado = true;
                    btnContinuarSelector.click();
                    elementos = contComuna.findElements(By.className("android.widget.TextView"));
                    if (elementos.get(1).getText().equals(comuna)) {
                        PdfBciReports.addMobilReportImage("Seleccionar comuna", "Se selecciona comuna: " + comuna, EstadoPrueba.PASSED, false);
                    } else {
                        PdfBciReports.addMobilReportImage("Seleccionar Comuna", "No se selecciona la comuna esperada: " + comuna, EstadoPrueba.FAILED, false);
                        PdfBciReports.closePDF();
                    }
                    break loop;
                }
                gestures.scrollSelectorElementos(elementosSelector.get(i));
            }

            //Para el resto de elementos del pickerwheel
            if (!seleccionado) {
                while (!elementosSelector.get(3).getText().equals(comuna)) {
                    gestures.scrollSelectorElementos(elementosSelector.get(3));
                }
                btnContinuarSelector.click();
                elementos = contComuna.findElements(By.className("android.widget.TextView"));
                if (elementos.get(1).getText().equals(comuna)) {
                    PdfBciReports.addMobilReportImage("Seleccionar comuna", "Se selecciona comuna: " + comuna, EstadoPrueba.PASSED, false);
                } else {
                    PdfBciReports.addMobilReportImage("Seleccionar Comuna", "No se selecciona la comuna esperada: " + comuna, EstadoPrueba.FAILED, false);
                    PdfBciReports.closePDF();
                }
            }
        }
    }

    // **************************************************************************************************

    @Step
    public void validarInputTelefono() {
        if (DriverContext.getDriver() instanceof IOSDriver) {
            Gestures gestures = new Gestures();
            gestures.encontrarObjeto(btnContinuar, "Sección input teléfono");
            AccionesGenericas.validarElemento(inputTelefono, 5);
            if(inputTelefono.getText().isEmpty()){
                PdfBciReports.addMobilReportImage("Validar input teléfono", "No se encuentra teléfono precargado por defecto", EstadoPrueba.PASSED, false);
            }else{
                PdfBciReports.addMobilReportImage("Validar input telefono", "Teléfono precargado por defecto: " + inputTelefono.getText(), EstadoPrueba.PASSED, false);
            }
        }else {
            MobileElement labelTelefono = contTelefono.findElement(By.className("TextInputLayout"));
            MobileElement contInput = labelTelefono.findElement(By.className("android.widget.FrameLayout"));
            MobileElement inputTelefono = contInput.findElement(By.className("android.widget.EditText"));
            AccionesGenericas.validarElemento(labelTelefono, "Número de teléfono móvil", 5);
            AccionesGenericas.validarElemento(inputTelefono, 5);
            if (!inputTelefono.getText().isEmpty()) {
                PdfBciReports.addMobilReportImage("Validar input telefono", "Teléfono precargado por defecto: " + inputTelefono.getText(), EstadoPrueba.PASSED, false);
            } else {
                PdfBciReports.addMobilReportImage("Validar input teléfono", "No se encuentra teléfono precargado por defecto", EstadoPrueba.PASSED, false);
            }
        }
    }

    // **************************************************************************************************

    @Step
    public void validarIngresoCaracteresTelefono() {
        MobileElement labelTelefono = contTelefono.findElement(By.className("TextInputLayout"));
        MobileElement contInput = labelTelefono.findElement(By.className("android.widget.FrameLayout"));
        MobileElement inputTelefono = contInput.findElement(By.className("android.widget.EditText"));
        inputTelefono.clear();
        inputTelefono.setValue("9123456789");
        MobileElement mensajeValidacion = contTelefono.findElement(By.className("android.widget.TextView"));
        if (AccionesGenericas.existeElemento(mensajeValidacion, 5)) {
            AccionesGenericas.validarElemento(mensajeValidacion, "No puedes ingresar más de 9 números", 5);
            PdfBciReports.addMobilReportImage("Validar ingreso caracteres dirección", "Se muestra mensaje de alerta", EstadoPrueba.PASSED, false);
        } else {
            PdfBciReports.addMobilReportImage("Validar ingreso caracteres dirección", "No se muestra mensaje de alerta", EstadoPrueba.FAILED, false);
            PdfBciReports.closePDF();
        }
    }

    // **************************************************************************************************

    @Step
    public void ingresarTelefono(String telefono) {
        if (DriverContext.getDriver() instanceof IOSDriver) {
            inputTelefono.setValue(telefono);
            PdfBciReports.addMobilReportImage("Ingresar teléfono", "Se ingresa el teléfono: " + telefono, EstadoPrueba.PASSED, false);
        }else {
            MobileElement labelTelefono = contTelefono.findElement(By.className("TextInputLayout"));
            MobileElement contInput = labelTelefono.findElement(By.className("android.widget.FrameLayout"));
            MobileElement inputTelefono = contInput.findElement(By.className("android.widget.EditText"));
            inputTelefono.setValue(telefono);
            PdfBciReports.addMobilReportImage("Ingresar teléfono", "Se ingresa el teléfono: " + telefono, EstadoPrueba.PASSED, false);
        }
    }

    // **************************************************************************************************

    @Step
    public void validarInputEmail() {
        if (DriverContext.getDriver() instanceof IOSDriver) {
            //Gestures gestures = new Gestures();
            //gestures.encontrarObjeto(btnContinuar, "Sección correo electrónico");
            AccionesGenericas.validarElemento(inputEmail, 5);
            if(inputEmail.getText().isEmpty()){
                PdfBciReports.addMobilReportImage("Validar input email", "No se encuentra email precargado por defecto", EstadoPrueba.PASSED, false);

            }else{
                PdfBciReports.addMobilReportImage("Validar input email", "Email precargado por defecto: " + inputEmail.getText(), EstadoPrueba.PASSED, false);
            }
        }else {
            Gestures gestures = new Gestures();
            gestures.encontrarObjeto(contEmail, "Sección correo electrónico");
            MobileElement labelEmail = contEmail.findElement(By.className("TextInputLayout"));
            MobileElement contInput = labelEmail.findElement(By.className("android.widget.FrameLayout"));
            MobileElement inputEmail = contInput.findElement(By.className("android.widget.EditText"));
            AccionesGenericas.validarElemento(labelEmail, "Correo electrónico", 5);
            AccionesGenericas.validarElemento(inputEmail, 5);
            if (!inputEmail.getText().isEmpty()) {
                PdfBciReports.addMobilReportImage("Validar input email", "Email precargado por defecto: " + inputEmail.getText(), EstadoPrueba.PASSED, false);
            } else {
                PdfBciReports.addMobilReportImage("Validar input email", "No se encuentra email precargado por defecto", EstadoPrueba.PASSED, false);
            }
        }
    }

    // **************************************************************************************************

    @Step
    public void ingresarEmail(String email) {
        if (DriverContext.getDriver() instanceof IOSDriver) {
            inputEmail.setValue(email);
            PdfBciReports.addMobilReportImage("Ingresar email", "Se ingresa el teléfono: " + email, EstadoPrueba.PASSED, false);
            disclaimer.click();
        }else {
            MobileElement labelEmail = contEmail.findElement(By.className("TextInputLayout"));
            MobileElement contInput = labelEmail.findElement(By.className("android.widget.FrameLayout"));
            MobileElement inputEmail = contInput.findElement(By.className("android.widget.EditText"));
            inputEmail.setValue(email);
            PdfBciReports.addMobilReportImage("Ingresar email", "Se ingresa el teléfono: " + email, EstadoPrueba.PASSED, false);
        }
    }

    // **************************************************************************************************

    @Step
    public void validarElementosInferiores() {
        Gestures gestures = new Gestures();
        gestures.encontrarObjeto(btnContinuar, "Sección elementos inferiores de la pantalla");
        AccionesGenericas.validarElemento(disclaimer, "Toda la documentación asociada a nuestros productos serán enviadas a tu correo electrónico.", 5);
        AccionesGenericas.validarElemento(btnContinuar, "CONTINUAR", 5);
    }

    // **************************************************************************************************

    @Step
    public void tapBotonContinuar() {
        if (btnContinuar.isEnabled()) {
            btnContinuar.click();
            PdfBciReports.addMobilReportImage("Tap botón continuar", "Se hace tap al botón continuar", EstadoPrueba.PASSED, false);
        } else {
            PdfBciReports.addMobilReportImage("Tap botón continuar", "El botón continuar no se encuentra habilitado", EstadoPrueba.FAILED, false);
            PdfBciReports.closePDF();
        }
    }

    // **************************************************************************************************

    @Step
    public void tapBotonCerrar() {
        btnCerrar.click();
        PdfBciReports.addReport("Tap botón cerrar", "Se hace tap al botón cerrar", EstadoPrueba.PASSED, false);
    }

    // **************************************************************************************************

    @Step
    public void tapFlechaAtras() {
        flechaAtrasProgreso.click();
        PdfBciReports.addReport("Tap flecha atrás", "Se hace tap a la flecha volver atrás", EstadoPrueba.PASSED, false);

    }
}
