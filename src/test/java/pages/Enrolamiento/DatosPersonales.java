package pages.Enrolamiento;

import constants.ConstantesInversiones;
import driver.DriverContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import reporter.EstadoPrueba;
import reporter.PdfBciReports;
import util.AccionesGenericas;
import util.Gestures;

import java.util.List;

public class DatosPersonales {

    private AppiumDriver driver;

    public DatosPersonales() {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver), this);
    }

    /***************************************************************************************************
     *                                          OBJETOS                                                *
     ***************************************************************************************************/

    //Título pantalla
    @iOSFindBy(accessibility = "Enrolarme para invertir")
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/toolbar_title")
    private MobileElement tituloPagina;

    //Botón cerrar
    @iOSFindBy(accessibility = "close")
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/action_close")
    private MobileElement btnCerrar;

    //Flecha atrás progreso
    @iOSFindBy(accessibility = "UIView.ProgressBarPager.PreviousStepButton")
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/bt_progress_back")
    private MobileElement flechaAtrasProgreso;

    //Barra progreso
    @iOSFindBy(accessibility = "UIView.ProgressBarPager.ProgressBarView")
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/horizontal_progress_bar")
    private MobileElement barraProgreso;

    //Stepper
    @iOSFindBy(accessibility = "UIView.ProgressBarPager.StepIndicatorLabel")
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/tv_stepper_indicator")
    private MobileElement stepper;

    //Texto info
    @iOSFindBy(accessibility = "DescriptionTableViewCell.descriptionLabel")
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/tv_description")
    private MobileElement info;

    //Titulo datos personales
    @iOSFindBy(accessibility = "Mis datos personales")
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/tv_title")
    private MobileElement tituloDatosPersonales;

    //Label nombre
    @iOSFindBy(accessibility = "EnrolamientoFormularioPresenter.NombreTitle")
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/tv_nombre_label")
    private MobileElement labelNombre;

    //Value nombre
    @iOSFindBy(accessibility = "EnrolamientoFormularioPresenter.NombreValue")
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/tv_nombre_value")
    private MobileElement valueNombre;

    /*******************************************************************************************/

    //Cuadro de Nacionalidad
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/selector_nacionalidad")
    private MobileElement contenedorNacionalidad;

    //Selector Nacionalidad
    @iOSFindBy(accessibility = "PickerSelectorTableViewCell.TextField.Nacionalidad")
    private MobileElement selectorNacionalidad;

    /*******************************************************************************************/

    //Titulo radio buttons
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/tv_group_title")
    private List<MobileElement> titulosRdos;

    //Título radio button país residencia
    @iOSFindBy(accessibility = "TwoChoiceTableViewCell.TitleLabel.PaisResidencia")
    private MobileElement tituloRdoPaisResidencia;

    //Título radio button doble nacionalidad
    @iOSFindBy(accessibility = "TwoChoiceTableViewCell.TitleLabel.DobleNacionalidad")
    private MobileElement tituloRdoDobleNacionalidad;

    /*******************************************************************************************/

    //Radio button Label Chile
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/rb_datos_personales_pais_chile")
    @iOSFindBy(accessibility = "TwoChoiceTableViewCell.LabelA.PaisResidencia")
    private MobileElement rdoLabelChile;

    //Radio button Label Otro país
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/rb_datos_personales_pais_otro_pais")
    @iOSFindBy(accessibility = "TwoChoiceTableViewCell.LabelB.PaisResidencia")
    private MobileElement rdoLabelOtroPais;

    /*******************************************************************************************/

    //Cuadro de pais residencia
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/selector_paises")
    private MobileElement contenedorPaisResidencia;

    //Selector pais residencia
    @iOSFindBy(accessibility = "PickerSelectorTableViewCell.TextField.PaisResidencia")
    private MobileElement selectorPaisResidencia;

    /*******************************************************************************************/

    //Radio button Si
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/rb_datos_personales_doble_nacionalidad_si")
    @iOSFindBy(accessibility = "TwoChoiceTableViewCell.LabelA.DobleNacionalidad")
    private MobileElement rdoSi;

    //Radio button No
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/rb_datos_personales_doble_nacionalidad_no")
    @iOSFindBy(accessibility = "TwoChoiceTableViewCell.LabelB.DobleNacionalidad")
    private MobileElement rdoNo;

    /*******************************************************************************************/

    //Cuadro de doble nacionalidad
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/selector_paises_doble_nacionalidadd")
    private MobileElement contenedorDobleNac;

    //Selector doble nacionalidad
    @iOSFindBy(accessibility = "PickerSelectorTableViewCell.TextField.DobleNacionalidad")
    private MobileElement selectorDobleNacionalidad;

    /*******************************************************************************************/

    //Cuadro ID doble nacionalidad
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/til_edit_text")
    private MobileElement contenedorIDDobleNac;

    //Input doble nacionalidad
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/et_edit_text")
    @iOSFindBy(accessibility = "TextFieldTableViewCell.TextField.IDDobleNacionalidad")
    private MobileElement inputDobleNac;

    /*******************************************************************************************/

    //Cuadro estado civil
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/selector_estados_civiles")
    private MobileElement contenedorEstadoCivil;

    //Selector estado civil
    @iOSFindBy(accessibility = "PickerSelectorTableViewCell.TextField.EstadoCivil")
    private MobileElement selectorEstadoCivil;

    /*******************************************************************************************/
    /**
     * Elementos Selectores
     **/

    //Titulo selector Android
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/tv_title")
    private MobileElement tituloSelector;

    //Titulo selector Estado Civil iOS
    @iOSFindBy(accessibility = "Selecciona tu Estado Civil")
    private MobileElement tituloSelctEstadoCivil;

    //Elementos selector
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/tv_element")
    private List<MobileElement> elementosSelector;

    //Pickerwheel
    @iOSFindBy(xpath = "//XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypePicker/XCUIElementTypePickerWheel")
    private MobileElement pickerWheel;

    //Botón continuar selector
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/btn_wheel_continuar")
    private MobileElement btnContinuarSelector;

    /*******************************************************************************************/

    //Botón continuar
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/btn_continuar")
    @iOSFindBy(accessibility = "CONTINUAR")
    private MobileElement btnContinuar;



    //***************************************************************************************************
    // *                                        MÉTODOS                                                 *
    // **************************************************************************************************
    @Step
    public void validarIngresoPantalla() {
        AccionesGenericas.ingresoPantalla(tituloDatosPersonales, "Mis Datos Personales");
    }

    @Step
    public void validarElementosSuperiores() {
        AccionesGenericas.validarElemento(tituloPagina, "Enrolarme para invertir", 5);
        AccionesGenericas.validarElemento(btnCerrar, 5);
        AccionesGenericas.validarElemento(flechaAtrasProgreso, 5);
        AccionesGenericas.validarElemento(barraProgreso, 5);
        AccionesGenericas.validarElemento(stepper, "1 de 5", 5);
        AccionesGenericas.validarElemento(info, "Para continuar con tu inversión es necesario que completes tus datos y revises la documentación asociada. De esta manera podrás comenzar a invertir 100% online.", 5);
        AccionesGenericas.validarElemento(tituloDatosPersonales, "Mis datos personales", 5);
        AccionesGenericas.validarElemento(labelNombre, "Nombre", 5);
        AccionesGenericas.validarElemento(valueNombre, 5);
    }

    //***************************************************************************************************

    @Step
    public void validarSelectorNacionalidad() {
        if (DriverContext.getDriver() instanceof IOSDriver) {
            AccionesGenericas.validarElemento(selectorNacionalidad, 5);
            if (selectorNacionalidad.getText().isEmpty()) {
                PdfBciReports.addMobilReportImage("Validar Selector Nacionalidad", "Cliente sin Nacionalidad precargada", EstadoPrueba.PASSED, false);
            } else {
                PdfBciReports.addMobilReportImage("Validar Selector Nacionalidad", "Cliente con Nacionalidad precargada " + selectorNacionalidad.getText(), EstadoPrueba.PASSED, false);
            }
        } else {
            List<MobileElement> elementos = contenedorNacionalidad.findElements(By.className("android.widget.TextView"));
            if (elementos.size() == 1) {
                AccionesGenericas.validarElemento(elementos.get(0), 5);
                PdfBciReports.addMobilReportImage("Validar Selector Nacionalidad", "Cliente sin Nacionalidad precargada", EstadoPrueba.PASSED, false);
            } else {
                AccionesGenericas.validarElemento(elementos.get(0), "Nacionalidad", 5);
                AccionesGenericas.validarElemento(elementos.get(1), 5);
                PdfBciReports.addMobilReportImage("Validar Selector Nacionalidad", "Cliente con Nacionalidad precargada " + elementos.get(1).getText(), EstadoPrueba.PASSED, false);
            }
        }
    }

    //***************************************************************************************************

    @Step
    public void seleccionarNacionalidad(String nacionalidad) {
        if (DriverContext.getDriver() instanceof IOSDriver) {
            selectorNacionalidad.click();
            pickerWheel.sendKeys(nacionalidad);
            btnContinuarSelector.click();
            if (selectorNacionalidad.getText().equals(nacionalidad)) {
                PdfBciReports.addMobilReportImage("Seleccionar Nacionalidad", "Se selecciona la nacionalidad " + nacionalidad, EstadoPrueba.PASSED, false);
            } else {
                PdfBciReports.addMobilReportImage("Seleccionar Nacionalidad", "No se selecciona la nacionalidad esperada " + nacionalidad, EstadoPrueba.FAILED, true);
            }
        } else {
            Gestures gestures = new Gestures();
            List<MobileElement> elementos = contenedorNacionalidad.findElements(By.className("android.widget.TextView"));
            boolean seleccionado = false;
            if (elementos.size() == 1) {
                elementos.get(0).click();
            } else {
                elementos.get(1).click();
            }

            //Para los primeros 4 elementos del pickerwheel //
            loop:
            for (int i = 0; i < 4; i++) {
                if (elementosSelector.get(i).getText().equals(nacionalidad)) {
                    seleccionado = true;
                    btnContinuarSelector.click();
                    elementos = contenedorNacionalidad.findElements(By.className("android.widget.TextView"));
                    if (elementos.get(1).getText().equals(nacionalidad)) {
                        PdfBciReports.addMobilReportImage("Seleccionar Nacionalidad", "Se selecciona la nacionalidad " + nacionalidad, EstadoPrueba.PASSED, false);
                    } else {
                        PdfBciReports.addMobilReportImage("Seleccionar Nacionalidad", "No se selecciona la nacionalidad esperada " + nacionalidad, EstadoPrueba.FAILED, true);
                    }
                    break loop;
                }
                gestures.scrollSelectorElementos(elementosSelector.get(i));
            }

            //Para el resto de elementos del pickerwheel
            if (!seleccionado) {
                while (!elementosSelector.get(3).getText().equals(nacionalidad)) {
                    gestures.scrollSelectorElementos(elementosSelector.get(3));
                }
                btnContinuarSelector.click();
                elementos = contenedorNacionalidad.findElements(By.className("android.widget.TextView"));
                if (elementos.get(1).getText().equals(nacionalidad)) {
                    PdfBciReports.addMobilReportImage("Seleccionar Nacionalidad", "Se selecciona la nacionalidad " + nacionalidad, EstadoPrueba.PASSED, false);
                } else {
                    PdfBciReports.addMobilReportImage("Seleccionar Nacionalidad", "No se selecciona la nacionalidad esperada " + nacionalidad, EstadoPrueba.FAILED, true);
                }
            }
        }
    }

    //***************************************************************************************************

    @Step
    public void validarRdoPaisResidencia() {
        if (DriverContext.getDriver() instanceof IOSDriver) {
            AccionesGenericas.validarElemento(tituloRdoPaisResidencia, "País de residencia", 5);
        } else {
            AccionesGenericas.validarElemento(titulosRdos.get(0), "País de residencia", 5);
        }
        AccionesGenericas.validarElemento(rdoLabelChile, "Chile", 5);
        AccionesGenericas.validarElemento(rdoLabelOtroPais, "Otro país", 5);
    }

    //***************************************************************************************************

    @Step
    public void seleccionarRdoPaisResidencia(String rdoResidencia) {
        if (rdoResidencia.equals("Chile")) {
            rdoLabelChile.click();
            PdfBciReports.addMobilReportImage("Seleccionar Radio Button Pais Residencia", "Se hace tap a 'Chile' en pais de residencia", EstadoPrueba.PASSED, false);
        } else if (rdoResidencia.equals("OtroPais")) {
            rdoLabelOtroPais.click();
            PdfBciReports.addMobilReportImage("Seleccionar Radio Button Pais Residencia", "Se hace tap a 'Otro país' en pais de residencia", EstadoPrueba.PASSED, false);
        } else {
            PdfBciReports.addMobilReportImage("Seleccionar Radio Button Pais Residencia", "No se selecciona ninguna opción de país de residencia", EstadoPrueba.FAILED, false);
            PdfBciReports.closePDF();
        }
    }

    //***************************************************************************************************

    @Step
    public void validarSelectorPaisResidencia() {
        Gestures gestures = new Gestures();
        if (DriverContext.getDriver() instanceof IOSDriver) {
            gestures.encontrarObjeto(selectorPaisResidencia, "Selector país residencia");
            AccionesGenericas.validarElemento(selectorPaisResidencia, 5);
            if(selectorPaisResidencia.getText().isEmpty()){
                PdfBciReports.addMobilReportImage("Validar Selector País Residencia", "Cliente sin país de residencia precargado", EstadoPrueba.PASSED, false);
            }else{
                PdfBciReports.addMobilReportImage("Validar Selector País Residencia", "Cliente con país de residencia precargado " + selectorPaisResidencia.getText(), EstadoPrueba.PASSED, false);

            }
        }else {
            if (rdoLabelOtroPais.getAttribute("checked").equals("true")) {
                gestures.encontrarObjeto(contenedorPaisResidencia, "Sección Selector País Residencia");
                List<MobileElement> elementos = contenedorPaisResidencia.findElements(By.className("android.widget.TextView"));
                if (elementos.size() == 1) {
                    AccionesGenericas.validarElemento(elementos.get(0), 5);
                    PdfBciReports.addMobilReportImage("Validar Selector País Residencia", "Cliente sin país de residencia precargado", EstadoPrueba.PASSED, false);
                } else {
                    AccionesGenericas.validarElemento(elementos.get(0), 5);
                    AccionesGenericas.validarElemento(elementos.get(1), 5);
                    PdfBciReports.addMobilReportImage("Validar Selector País Residencia", "Cliente con país de residencia precargado " + elementos.get(1).getText(), EstadoPrueba.PASSED, false);
                }
            } else {
                PdfBciReports.addMobilReportImage("Validar Selector País Residencia", "Cliente con país de residencia Chile", EstadoPrueba.PASSED, false);
            }
        }

    }

    //***************************************************************************************************

    @Step
    public void seleccionarPaisResidencia(String paisResidencia){
        if (DriverContext.getDriver() instanceof IOSDriver) {
            Gestures gestures = new Gestures();
            gestures.encontrarObjeto(selectorPaisResidencia, "Selector país residencia");
            selectorPaisResidencia.click();
            pickerWheel.sendKeys(paisResidencia);
            btnContinuarSelector.click();
            if (selectorNacionalidad.getText().equals(paisResidencia)) {
                PdfBciReports.addMobilReportImage("Seleccionar Pais Residencia", "Se selecciona pais de residencia: " + paisResidencia, EstadoPrueba.PASSED, false);
            } else {
                PdfBciReports.addMobilReportImage("Seleccionar Pais Residencia", "No se selecciona el país de residencia esperado: " + paisResidencia, EstadoPrueba.FAILED, true);
            }
        }else {
            Gestures gestures = new Gestures();
            List<MobileElement> elementos = contenedorPaisResidencia.findElements(By.className("android.widget.TextView"));
            boolean seleccionado = false;
            if (elementos.size() == 1) {
                elementos.get(0).click();
            } else {
                elementos.get(1).click();
            }

            //Para los primeros 4 elementos del pickerwheel //
            loop:
            for (int i = 0; i < 4; i++) {
                if (elementosSelector.get(i).getText().equals(paisResidencia)) {
                    seleccionado = true;
                    btnContinuarSelector.click();
                    elementos = contenedorPaisResidencia.findElements(By.className("android.widget.TextView"));
                    if (elementos.get(1).getText().equals(paisResidencia)) {
                        PdfBciReports.addMobilReportImage("Seleccionar Pais Residencia", "Se selecciona pais de residencia: " + paisResidencia, EstadoPrueba.PASSED, false);
                    } else {
                        PdfBciReports.addMobilReportImage("Seleccionar Pais Residencia", "No se selecciona el país de residencia esperado: " + paisResidencia, EstadoPrueba.FAILED, true);
                        PdfBciReports.closePDF();
                    }
                    break loop;
                }
                gestures.scrollSelectorElementos(elementosSelector.get(i));
            }

            //Para el resto de elementos del pickerwheel
            if (!seleccionado) {
                while (!elementosSelector.get(3).getText().equals(paisResidencia)) {
                    gestures.scrollSelectorElementos(elementosSelector.get(3));
                }
                btnContinuarSelector.click();
                elementos = contenedorPaisResidencia.findElements(By.className("android.widget.TextView"));
                if (elementos.get(1).getText().equals(paisResidencia)) {
                    PdfBciReports.addMobilReportImage("Seleccionar Pais Residencia", "Se selecciona pais de residencia: " + paisResidencia, EstadoPrueba.PASSED, false);
                } else {
                    PdfBciReports.addMobilReportImage("Seleccionar Pais Residencia", "No se selecciona el país de residencia esperado: " + paisResidencia, EstadoPrueba.FAILED, true);
                }
            }
        }

    }

    //***************************************************************************************************

    @Step
    public void validarRdoDobleNacionalidad() {
        Gestures gestures = new Gestures();
        gestures.encontrarObjeto(rdoNo, "Seccion radio buttons doble nacionalidad");
        if (DriverContext.getDriver() instanceof IOSDriver) {
            AccionesGenericas.validarElemento(tituloRdoDobleNacionalidad, "¿Tienes doble nacionalidad?", 5);
        }else {
            if (titulosRdos.size() == 1) {
                AccionesGenericas.validarElemento(titulosRdos.get(0), "¿Tienes doble nacionalidad?", 5);
            } else {
                AccionesGenericas.validarElemento(titulosRdos.get(1), "¿Tienes doble nacionalidad?", 5);
            }
        }
        AccionesGenericas.validarElemento(rdoSi, "Sí", 5);
        AccionesGenericas.validarElemento(rdoNo, "No", 5);
    }

    //***************************************************************************************************

    @Step
    public void seleccionarRdoDobleNacionalidad(String dobleNac) {
        if (dobleNac.equals("Si")) {
            rdoSi.click();
            PdfBciReports.addMobilReportImage("Seleccionar radio button doble nacionalidad", "Se hace tap a 'Sí' en doble nacionalidad", EstadoPrueba.PASSED, false);
        } else if (dobleNac.equals("No")) {
            rdoNo.click();
            PdfBciReports.addMobilReportImage("Seleccionar radio button doble nacionalidad", "Se hace tap a 'No' en doble nacionalidad", EstadoPrueba.PASSED, false);
        } else {
            PdfBciReports.addMobilReportImage("Seleccionar radio button doble nacionalidad", "No se selcciona ninguna opción de residencia", EstadoPrueba.FAILED, false);
            PdfBciReports.closePDF();
        }
    }

    //***************************************************************************************************

    @Step
    public void validarSelectorDobleNacionalidad() {
        Gestures gestures = new Gestures();
        if (DriverContext.getDriver() instanceof IOSDriver) {
            gestures.encontrarObjeto(selectorDobleNacionalidad, "Selector doble nacionalidad");
            AccionesGenericas.validarElemento(selectorDobleNacionalidad, 5);
            if(selectorDobleNacionalidad.getText().isEmpty()){
                PdfBciReports.addMobilReportImage("Validar selector doble nacionalidad", "Cliente sin doble nacionalidad precargada", EstadoPrueba.PASSED, false);
            }else{
                PdfBciReports.addMobilReportImage("Validar selector doble nacionalidad", "Cliente con doble nacionalidad precargada " + selectorDobleNacionalidad.getText(), EstadoPrueba.PASSED, false);
            }
        }else {
            if (rdoSi.getAttribute("checked").equals("true")) {
                gestures.encontrarObjeto(contenedorDobleNac, "Sección selector Doble Nacionalidad");
                List<MobileElement> elementos = contenedorDobleNac.findElements(By.className("android.widget.TextView"));
                if (elementos.size() == 1) {
                    AccionesGenericas.validarElemento(elementos.get(0), 5);
                    PdfBciReports.addMobilReportImage("Validar selector doble nacionalidad", "Cliente sin doble nacionalidad precargada", EstadoPrueba.PASSED, false);
                } else {
                    AccionesGenericas.validarElemento(elementos.get(0), 5);
                    AccionesGenericas.validarElemento(elementos.get(1), 5);
                    PdfBciReports.addMobilReportImage("Validar selector doble nacionalidad", "Cliente con doble nacionalidad precargada " + elementos.get(1).getText(), EstadoPrueba.PASSED, false);
                }
            } else {
                PdfBciReports.addMobilReportImage("Validar selector doble nacionalidad", "Cliente sin doble nacionalidad", EstadoPrueba.PASSED, false);
            }
        }
    }

    //***************************************************************************************************

    @Step
    public void seleccionarDobleNacionalidad(String dobleNacionalidad) {
        if (DriverContext.getDriver() instanceof IOSDriver) {
            Gestures gestures = new Gestures();
            gestures.encontrarObjeto(selectorDobleNacionalidad, "Selector doble nacionalidad");
            selectorDobleNacionalidad.click();
            pickerWheel.sendKeys(dobleNacionalidad);
            btnContinuarSelector.click();
            if (selectorDobleNacionalidad.getText().equals(dobleNacionalidad)) {
                PdfBciReports.addMobilReportImage("Seleccionar Doble Nacionalidad", "Se selecciona doble nacionalidad: " + dobleNacionalidad, EstadoPrueba.PASSED, false);
            } else {
                PdfBciReports.addMobilReportImage("Seleccionar Doble Nacionalidad", "No se selecciona doble nacionalidad esperada: " + dobleNacionalidad, EstadoPrueba.FAILED, true);
            }
        } else {
            Gestures gestures = new Gestures();
            List<MobileElement> elementos = contenedorDobleNac.findElements(By.className("android.widget.TextView"));
            boolean seleccionado = false;
            if (elementos.size() == 1) {
                elementos.get(0).click();
            } else {
                elementos.get(1).click();
            }

            //Para los primeros 4 elementos del pickerwheel //
            loop:
            for (int i = 0; i < 4; i++) {
                if (elementosSelector.get(i).getText().equals(dobleNacionalidad)) {
                    seleccionado = true;
                    btnContinuarSelector.click();
                    elementos = contenedorDobleNac.findElements(By.className("android.widget.TextView"));
                    if (elementos.get(1).getText().equals(dobleNacionalidad)) {
                        PdfBciReports.addMobilReportImage("Seleccionar doble nacionalidad", "Se selecciona doble nacionalidad: " + dobleNacionalidad, EstadoPrueba.PASSED, false);
                    } else {
                        PdfBciReports.addMobilReportImage("Seleccionar doble nacionalidad", "No se selecciona la doble nacionalidad esperada: " + dobleNacionalidad, EstadoPrueba.FAILED, false);
                        PdfBciReports.closePDF();
                    }
                    break loop;
                }
                gestures.scrollSelectorElementos(elementosSelector.get(i));
            }

            //Para el resto de elementos del pickerwheel
            if (!seleccionado) {
                while (!elementosSelector.get(3).getText().equals(dobleNacionalidad)) {
                    gestures.scrollSelectorElementos(elementosSelector.get(3));
                }
                btnContinuarSelector.click();
                elementos = contenedorDobleNac.findElements(By.className("android.widget.TextView"));
                if (elementos.get(1).getText().equals(dobleNacionalidad)) {
                    PdfBciReports.addMobilReportImage("Seleccionar doble nacionalidad", "Se selecciona doble nacionalidad: " + dobleNacionalidad, EstadoPrueba.PASSED, false);
                } else {
                    PdfBciReports.addMobilReportImage("Seleccionar doble nacionalidad", "No se selecciona la doble nacionalidad esperada: " + dobleNacionalidad, EstadoPrueba.FAILED, false);
                }
            }
        }
    }

    //***************************************************************************************************

    @Step
    public void validarInputIDDobleNac() {
        if (rdoSi.getAttribute("checked").equals("true")) {
            Gestures gestures = new Gestures();
            gestures.encontrarObjeto(contenedorIDDobleNac, "Sección input ID doble nacionalidad");
            AccionesGenericas.validarElemento(contenedorIDDobleNac, "ID de tu doble nacionalidad", 5);
            AccionesGenericas.validarElemento(inputDobleNac, 5);
            if (!inputDobleNac.getText().isEmpty()) {
                PdfBciReports.addMobilReportImage("Validar input ID doble nacionalidad", "Cliente con ID doble nacionalidad precargado: " + inputDobleNac.getText(), EstadoPrueba.PASSED, false);
            }
        }
    }

    //***************************************************************************************************

    @Step
    public void ingresarIDDobleNac(String dobleNac) {
        inputDobleNac.setValue(dobleNac);
        PdfBciReports.addMobilReportImage("Ingresar ID doble nacionalidad", "Se ingresa ID doble nacionalidad: " + dobleNac, EstadoPrueba.PASSED, false);
    }

    //***************************************************************************************************

    @Step
    public void validarSelectorEstCivil() {
        Gestures gestures = new Gestures();
        if (DriverContext.getDriver() instanceof IOSDriver) {
            gestures.encontrarObjeto(selectorEstadoCivil,"Sección selector estado civil");
            AccionesGenericas.validarElemento(selectorEstadoCivil, 5);
        }else {
            gestures.encontrarObjeto(contenedorEstadoCivil, "Sección selector estado civil");
            List<MobileElement> elementos = contenedorEstadoCivil.findElements(By.className("android.widget.TextView"));
            if (elementos.size() == 1) {
                AccionesGenericas.validarElemento(elementos.get(0), "Selecciona tu Estado Civil", 5);
                PdfBciReports.addMobilReportImage("Validar selector estado civil", "Cliente sin estado civil precargado", EstadoPrueba.PASSED, false);
            } else {
                AccionesGenericas.validarElemento(elementos.get(0), "Selecciona tu Estado Civil", 5);
                AccionesGenericas.validarElemento(elementos.get(1), 5);
                PdfBciReports.addMobilReportImage("Validar selector estado civil", "Cliente con estado civil precargado: " + elementos.get(1).getText(), EstadoPrueba.PASSED, false);
            }
        }
    }

    //***************************************************************************************************

    @Step
    public void validarElementosSelectorEstCivil() {
        if (DriverContext.getDriver() instanceof IOSDriver){
            selectorEstadoCivil.click();
            AccionesGenericas.validarElemento(tituloSelctEstadoCivil, 10);
            PdfBciReports.addMobilReportImage("Validar Selector Estado Civil", "Se despliega selector de estado civil", EstadoPrueba.PASSED, false);
        }else{
            String[] estados = {"Casado(a)", "Conviviente Civil", "Separado(a)", "Soltero(a)", "Viudo(a)"};
            List<MobileElement> elementos = contenedorEstadoCivil.findElements(By.className("android.widget.TextView"));
            if (elementos.size() == 1) {
                elementos.get(0).click();
            } else {
                elementos.get(1).click();
            }
            if (AccionesGenericas.existeElemento(tituloSelector, 5)) {
                PdfBciReports.addMobilReportImage("Validar elementos selector estado civil", "Se ingresa al selecctor de estado civil correctamente", EstadoPrueba.PASSED, false);
            } else {
                PdfBciReports.addMobilReportImage("Validar elementos selector estado civil", "Error al ingresar al selector de estado civil", EstadoPrueba.FAILED, false);
                PdfBciReports.closePDF();
            }
            for (int i = 0; i < estados.length; i++) {
                AccionesGenericas.validarElemento(elementosSelector.get(i), estados[i], 5);
            }
        }

    }

    //***************************************************************************************************

    @Step
    public void seleccionarEstadoCivil(String estadoCivil){
        if (DriverContext.getDriver() instanceof IOSDriver) {
            selectorEstadoCivil.click();
            pickerWheel.sendKeys(estadoCivil);
            btnContinuarSelector.click();
            if(selectorEstadoCivil.getText().equals(estadoCivil)){
                PdfBciReports.addMobilReportImage("Seleccionar estado civil", "Se selecciona estado civil: "+ estadoCivil, EstadoPrueba.PASSED, false);
            }else{
                PdfBciReports.addMobilReportImage("Seleccionar estado civil", "No se selecciona el estado civil esperado: "+ estadoCivil, EstadoPrueba.FAILED, true);
            }
        }else {
            List<MobileElement> elementos = contenedorEstadoCivil.findElements(By.className("android.widget.TextView"));
            if (elementos.size() == 1) {
                elementos.get(0).click();
            } else {
                elementos.get(1).click();
            }
            switch (estadoCivil) {
                case "Casado(a)":
                    elementosSelector.get(0).click();
                    PdfBciReports.addMobilReportImage("Seleccionar estado civil", "Se selecciona estado civil: Casado(a)", EstadoPrueba.PASSED, false);
                    break;
                case "Conviviente Civil":
                    elementosSelector.get(1).click();
                    PdfBciReports.addMobilReportImage("Seleccionar estado civil", "Se selecciona estado civil: Conviviente Civil", EstadoPrueba.PASSED, false);
                    break;
                case "Separado(a)":
                    elementosSelector.get(2).click();
                    PdfBciReports.addMobilReportImage("Seleccionar estado civil", "Se selecciona estado civil: Separado(a)", EstadoPrueba.PASSED, false);
                    break;
                case "Soltero(a)":
                    elementosSelector.get(3).click();
                    PdfBciReports.addMobilReportImage("Seleccionar estado civil", "Se selecciona estado civil: Soltero(a)", EstadoPrueba.PASSED, false);
                    break;
                case "Viudo(a)":
                    elementosSelector.get(4).click();
                    PdfBciReports.addMobilReportImage("Seleccionar estado civil", "Se selecciona estado civil: Viudo(a)", EstadoPrueba.PASSED, false);
                    break;
                default:
                    PdfBciReports.addMobilReportImage("Seleccionar estado civil", "No se ha seleccionado ningún estado civil", EstadoPrueba.FAILED, false);
                    PdfBciReports.closePDF();
                    break;
            }
        }
    }

    //***************************************************************************************************

    @Step
    public void validarBotonContinuar() {
        Gestures gestures = new Gestures();
        gestures.encontrarObjeto(btnContinuar, "Sección botón continuar");
        AccionesGenericas.validarElemento(btnContinuar, "CONTINUAR", 5);
    }

    //***************************************************************************************************

    @Step
    public void tapBotonContinuar() {
        if (btnContinuar.isEnabled()) {
            btnContinuar.click();
            PdfBciReports.addReport("Tap botón continuar", "Se hace tap a botón continuar", EstadoPrueba.PASSED, false);
        } else {
            PdfBciReports.addMobilReportImage("Tap botón continuar", "El botón continuar se encuentra deshabilitado", EstadoPrueba.FAILED, false);
            PdfBciReports.closePDF();
        }
    }

    //***************************************************************************************************

    @Step
    public void tapBotonCerrar() {
        btnCerrar.click();
        PdfBciReports.addMobilReportImage("Tap botón cerrar X", "Se hace tap a la X", EstadoPrueba.PASSED, false);
    }

    //***************************************************************************************************
}
