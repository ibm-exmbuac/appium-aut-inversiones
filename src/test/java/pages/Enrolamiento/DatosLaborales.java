package pages.Enrolamiento;

import constants.ConstantesInversiones;
import driver.DriverContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import reporter.EstadoPrueba;
import reporter.PdfBciReports;
import util.AccionesGenericas;
import util.Gestures;

import java.util.List;

public class DatosLaborales {

    private AppiumDriver driver;

    public DatosLaborales() {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver), this);
    }

    /***************************************************************************************************
     *                                          OBJETOS                                                *
     ***************************************************************************************************/

    //Título pantalla
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/toolbar_title")
    @iOSFindBy(accessibility = "Enrolarme para invertir")
    private MobileElement tituloPagina;

    //Botón cerrar
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/action_close")
    @iOSFindBy(accessibility = "close")
    private MobileElement btnCerrar;

    //Flecha atrás progreso
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/bt_progress_back")
    @iOSFindBy(accessibility = "UIView.ProgressBarPager.PreviousStepButton")
    private MobileElement flechaAtrasProgreso;

    //Barra progreso
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/horizontal_progress_bar")
    @iOSFindBy(accessibility = "UIView.ProgressBarPager.ProgressBarView")
    private MobileElement barraProgreso;

    //Stepper
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/tv_stepper_indicator")
    @iOSFindBy(accessibility = "UIView.ProgressBarPager.StepIndicatorLabel")
    private MobileElement stepper;

    //Titulo Mis datos laborales
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/tv_title")
    @iOSFindBy(accessibility = "Mis datos laborales")
    private MobileElement tituloDatosLaborales;

    /***************************************************************************************************/

    //Contenedor actividad
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/selector_actividad")
    private MobileElement contActividad;

    //Titulo Mis datos laborales
    @iOSFindBy(accessibility = "PickerSelectorTableViewCell.TextField.ActividadEconomica")
    private MobileElement selectorActividad;

    /***************************************************************************************************/

    //Contenedor rut empresa
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/etb_rut_empresa")
    private MobileElement contRutEmpresa;

    //Input rut empresa
    @iOSFindBy(accessibility = "TextFieldTableViewCell.TextField.RUT")
    private MobileElement inputRutEmpresa;

    /***************************************************************************************************/

    //Contenedor razon social
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/etb_razon_social")
    private MobileElement contRazonSoc;

    //Input rut empresa
    @iOSFindBy(accessibility = "TextFieldTableViewCell.TextField.RazonSocial")
    private MobileElement inputRazonSocial;

    /***************************************************************************************************/

    //Contenedor actividad económica SII
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/selector_actividad_economica")
    private MobileElement contActEconomica;

    //Selector actividad economica
    @iOSFindBy(accessibility = "PickerSelectorTableViewCell.TextField.ActividadSII")
    private MobileElement selectorActividadEconomica;

    /*******************************************************************************************/
    /**
     * Elementos Selectores
     **/

    //Titulo selector
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/tv_title")
    private MobileElement tituloSelector;

    //Pickerwheel
    @iOSFindBy(xpath = "//XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypePicker/XCUIElementTypePickerWheel")
    private MobileElement pickerWheel;

    //Boton Continuar Pickerwheel
    @iOSFindBy(xpath = "(//XCUIElementTypeButton[@name=\"CONTINUAR\"])[2]")
    private MobileElement btnContinuarSelector;

    //Elementos selector
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/tv_element")
    private List<MobileElement> elementosSelector;

    /*******************************************************************************************/

    //Botón continuar
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/btn_continuar")
    @iOSFindBy(accessibility = "CONTINUAR")
    private MobileElement btnContinuar;


    //***************************************************************************************************
    // *                                        MÉTODOS                                                 *
    // **************************************************************************************************

    @Step
    public void validarIngresoPantalla() {
        AccionesGenericas.ingresoPantalla(tituloDatosLaborales, "Datos Laboales");
    }
    //***************************************************************************************************


    @Step
    public void validarElementosSuperiores() {
        AccionesGenericas.validarElemento(tituloPagina, "Enrolarme para invertir", 5);
        AccionesGenericas.validarElemento(btnCerrar, 5);
        AccionesGenericas.validarElemento(flechaAtrasProgreso, 5);
        AccionesGenericas.validarElemento(barraProgreso, 5);
        AccionesGenericas.validarElemento(stepper, "3 de 5", 5);
        AccionesGenericas.validarElemento(tituloDatosLaborales, "Mis datos laborales", 5);
    }

    //***************************************************************************************************

    @Step
    public void validarSelectorActividad() {
        if (DriverContext.getDriver() instanceof IOSDriver){
            AccionesGenericas.validarElemento(selectorActividad, 5);
            if(selectorActividad.getText().isEmpty()){
                PdfBciReports.addMobilReportImage("Validar Selector Actividad", "No se encuentra actividada cargada por defecto", EstadoPrueba.PASSED, false);
            }else{
                PdfBciReports.addMobilReportImage("Validar Selector Actividad", "Actividad cargada por defecto: " + selectorActividad.getText(), EstadoPrueba.PASSED, false);
            }
        }else {
            List<MobileElement> elementos = contActividad.findElements(By.className("android.widget.TextView"));
            if (elementos.size() == 1) {
                AccionesGenericas.validarElemento(elementos.get(0), "Selecciona una actividad", 5);
                PdfBciReports.addMobilReportImage("Validar Selector Actividad", "No se encuentra actividada cargada por defecto", EstadoPrueba.PASSED, false);
            } else {
                AccionesGenericas.validarElemento(elementos.get(0), "Selecciona una actividad", 5);
                AccionesGenericas.validarElemento(elementos.get(1), 5);
                PdfBciReports.addMobilReportImage("Validar Selector Actividad", "Actividad cargada por defecto: " + elementos.get(1).getText(), EstadoPrueba.PASSED, false);
            }
        }
    }

    //***************************************************************************************************

    private void validarInputRutEmpresa() {
        if (DriverContext.getDriver() instanceof IOSDriver) {
            AccionesGenericas.validarElemento(inputRutEmpresa, 5);
            if(inputRutEmpresa.getText().isEmpty()){
                PdfBciReports.addMobilReportImage("Validar Input Rut Empresa", "No se encuentra rut empresa precargado por defecto", EstadoPrueba.PASSED, false);
            }else{
                PdfBciReports.addMobilReportImage("Validar Input Rut Empresa", "Rut empresa precargado por defecto: " + inputRutEmpresa.getText(), EstadoPrueba.PASSED, false);
            }
        }else {
            MobileElement labelRutEmp = contRutEmpresa.findElement(By.className("TextInputLayout"));
            MobileElement contInput = labelRutEmp.findElement(By.className("android.widget.FrameLayout"));
            MobileElement inputRutEmp = contInput.findElement(By.className("android.widget.EditText"));
            AccionesGenericas.validarElemento(labelRutEmp, "RUT de la empresa", 5);
            AccionesGenericas.validarElemento(inputRutEmp, 5);
            if (!inputRutEmp.getText().isEmpty()) {
                PdfBciReports.addMobilReportImage("Validar Input Rut Empresa", "Rut empresa precargado por defecto: " + inputRutEmp.getText(), EstadoPrueba.PASSED, false);
            } else {
                PdfBciReports.addMobilReportImage("Validar Input Rut Empresa", "No se encuentra rut empresa precargado por defecto", EstadoPrueba.PASSED, false);
            }
        }
    }

    private void validarInputRazonSocial() {
        if (DriverContext.getDriver() instanceof IOSDriver) {
            AccionesGenericas.validarElemento(inputRazonSocial, 5);
            if(inputRazonSocial.getText().isEmpty()){
                PdfBciReports.addMobilReportImage("Validar Input Razón Social", "No se encuentra razón social precargada por defecto", EstadoPrueba.PASSED, false);
            }else{
                PdfBciReports.addMobilReportImage("Validar Input Razón Social", "Razón social precargada por defecto: " + inputRazonSocial.getText(), EstadoPrueba.PASSED, false);
            }
        }else {
            MobileElement labelRazonSoc = contRazonSoc.findElement(By.className("TextInputLayout"));
            MobileElement contInput = labelRazonSoc.findElement(By.className("android.widget.FrameLayout"));
            MobileElement inputRazonSoc = contInput.findElement(By.className("android.widget.EditText"));
            AccionesGenericas.validarElemento(labelRazonSoc, "Razón social de la empresa", 5);
            AccionesGenericas.validarElemento(inputRazonSoc, 5);
            if (!inputRazonSoc.getText().isEmpty()) {
                PdfBciReports.addMobilReportImage("Validar Input Razón Social", "Razón social precargada por defecto: " + inputRazonSoc.getText(), EstadoPrueba.PASSED, false);
            } else {
                PdfBciReports.addMobilReportImage("Validar Input Razón Social", "No se encuentra razón social precargada por defecto", EstadoPrueba.PASSED, false);
            }
        }
    }

    //***************************************************************************************************

    private void validarSelectorActEconomica() {
        if (DriverContext.getDriver() instanceof IOSDriver) {
            AccionesGenericas.validarElemento(selectorActividadEconomica, 5);
            if(selectorActividad.getText().isEmpty()){
                PdfBciReports.addMobilReportImage("Validar Selector Actividad Económica", "No se encuentra actividad económica cargada por defecto", EstadoPrueba.PASSED, false);
            }else{
                PdfBciReports.addMobilReportImage("Validar Selector Actividad Económica", "Actividad económica cargada por defecto: " + selectorActividadEconomica.getText(), EstadoPrueba.PASSED, false);
            }
        }else {
            List<MobileElement> elementos = contActEconomica.findElements(By.className("android.widget.TextView"));
            if (elementos.size() == 1) {
                AccionesGenericas.validarElemento(elementos.get(0), "Actividad Económica SII", 5);
                PdfBciReports.addMobilReportImage("Validar Selector Actividad Económica", "No se encuentra actividad económica cargada por defecto", EstadoPrueba.PASSED, false);
            } else {
                AccionesGenericas.validarElemento(elementos.get(0), "Actividad Económica SII", 5);
                AccionesGenericas.validarElemento(elementos.get(1), 5);
                PdfBciReports.addMobilReportImage("Validar Selector Actividad Económica", "Actividad económica cargada por defecto: " + elementos.get(1).getText(), EstadoPrueba.PASSED, false);
            }
        }
    }

    //***************************************************************************************************

    @Step
    public void validarOpcionesActividades() {
        if (DriverContext.getDriver() instanceof IOSDriver) {
            switch (selectorActividad.getText()) {
                case "Empleado(a)":
                case "Empresario(a)":
                    validarInputRutEmpresa();
                    validarInputRazonSocial();
                    break;
                case "Independiente":
                case "Rentista":
                    validarSelectorActEconomica();
                    break;
            }
        }else{
            List<MobileElement> elementos = contActividad.findElements(By.className("android.widget.TextView"));
            if (elementos.size() == 1) {
                PdfBciReports.addMobilReportImage("Validar opciones actividades", "No hay ninguna actividad seleccionada, o se ha seleccionado la opción de Jubilado(a)", EstadoPrueba.PASSED, false);
            } else {
                switch (elementos.get(1).getText()) {
                    case "Empleado(a)":
                    case "Empresario(a)":
                        validarInputRutEmpresa();
                        validarInputRazonSocial();
                        break;
                    case "Independiente":
                    case "Rentista":
                        validarSelectorActEconomica();
                        break;
                }
            }
        }
    }

    //***************************************************************************************************

    @Step
    public void seleccionarActividad(String actividad) {
        if (DriverContext.getDriver() instanceof IOSDriver) {
            selectorActividad.click();
            pickerWheel.sendKeys(actividad);
            btnContinuarSelector.click();
            PdfBciReports.addMobilReportImage("Seleccionar Actividad", "Se se lecciona la actividad " + actividad, EstadoPrueba.PASSED, false);
        } else {
            List<MobileElement> elementos = contActividad.findElements(By.className("android.widget.TextView"));
            if (elementos.size() == 1) {
                elementos.get(0).click();
            } else {
                elementos.get(1).click();
            }
            switch (actividad) {
                case "Empleado(a)":
                    elementosSelector.get(0).click();
                    PdfBciReports.addMobilReportImage("Seleccionar Actividad", "Se se lecciona la actividad " + actividad, EstadoPrueba.PASSED, false);
                    break;
                case "Empresario(a)":
                    elementosSelector.get(1).click();
                    PdfBciReports.addMobilReportImage("Seleccionar Actividad", "Se se lecciona la actividad " + actividad, EstadoPrueba.PASSED, false);
                    break;
                case "Independiente":
                    elementosSelector.get(2).click();
                    PdfBciReports.addMobilReportImage("Seleccionar Actividad", "Se se lecciona la actividad " + actividad, EstadoPrueba.PASSED, false);
                    break;
                case "Rentista":
                    elementosSelector.get(3).click();
                    PdfBciReports.addMobilReportImage("Seleccionar Actividad", "Se se lecciona la actividad " + actividad, EstadoPrueba.PASSED, false);
                    break;
                case "Jubilado(a)":
                    elementosSelector.get(4).click();
                    PdfBciReports.addMobilReportImage("Seleccionar Actividad", "Se se lecciona la actividad " + actividad, EstadoPrueba.PASSED, false);
                    break;
                default:
                    PdfBciReports.addMobilReportImage("Seleccionar Actividad", "Se se lecciona la actividad " + actividad, EstadoPrueba.PASSED, false);
                    break;
            }
        }
    }

    //***************************************************************************************************

    @Step
    public void ingresarRutEmpresa(String rut) {
        if (DriverContext.getDriver() instanceof IOSDriver) {
            inputRutEmpresa.setValue(rut);
            PdfBciReports.addMobilReportImage("Ingresar Rut Empresa", "Se ingresa el rut " + rut, EstadoPrueba.PASSED, false);
        }else {
            MobileElement labelRutEmp = contRutEmpresa.findElement(By.className("TextInputLayout"));
            MobileElement contInput = labelRutEmp.findElement(By.className("android.widget.FrameLayout"));
            MobileElement inputRutEmp = contInput.findElement(By.className("android.widget.EditText"));
            inputRutEmp.setValue(rut);
            PdfBciReports.addMobilReportImage("Ingresar Rut Empresa", "Se ingresa el rut " + rut, EstadoPrueba.PASSED, false);
        }
    }

    //***************************************************************************************************

    @Step
    public void ingresarRazonSocial(String razonSoc) {
        if (DriverContext.getDriver() instanceof IOSDriver) {
            inputRazonSocial.setValue(razonSoc);
            PdfBciReports.addMobilReportImage("Ingresar Razón Social", "Se ingresa razón social " + razonSoc, EstadoPrueba.PASSED, false);
        }else {
            MobileElement labelRazonSoc = contRazonSoc.findElement(By.className("TextInputLayout"));
            MobileElement contInput = labelRazonSoc.findElement(By.className("android.widget.FrameLayout"));
            MobileElement inputRazonSoc = contInput.findElement(By.className("android.widget.EditText"));
            inputRazonSoc.setValue(razonSoc);
            PdfBciReports.addMobilReportImage("Ingresar Razón Social", "Se ingresa razón social " + razonSoc, EstadoPrueba.PASSED, false);
        }
    }

    //***************************************************************************************************

    @Step
    public void seleccionarActividadEconomica(String actEconomica) {
        if (DriverContext.getDriver() instanceof IOSDriver) {
            selectorActividadEconomica.click();
            pickerWheel.sendKeys(actEconomica);
            btnContinuarSelector.click();
            PdfBciReports.addMobilReportImage("Seleccionar Actividad Económica", "Se selecciona la actividad " + actEconomica, EstadoPrueba.PASSED, false);
        }else {
            Gestures gestures = new Gestures();
            List<MobileElement> elementos = contActEconomica.findElements(By.className("android.widget.TextView"));
            if (elementos.size() == 1) {
                elementos.get(0).click();
            } else {
                elementos.get(1).click();
            }
            boolean seleccionado = false;

            loop:
            while (!seleccionado) {
                for (MobileElement actividad : elementosSelector) {
                    if (actividad.getText().equals(actEconomica)) {
                        actividad.click();
                        PdfBciReports.addMobilReportImage("Seleccionar Actividad Económica", "Se selecciona la actividad " + actEconomica, EstadoPrueba.PASSED, false);
                        seleccionado = true;
                        break loop;
                    }
                }
                gestures.swipe(0.9, 0.6);
            }
        }
    }

    //***************************************************************************************************

    @Step
    public void validarBotonContinuar() {
        AccionesGenericas.validarElemento(btnContinuar, "CONTINUAR", 5);
    }

    //***************************************************************************************************

    @Step
    public void tapBotonContinuar() {
        if (btnContinuar.isEnabled()) {
            btnContinuar.click();
            PdfBciReports.addReport("Tap botón continuar", "Se hace tap al botón continuar", EstadoPrueba.PASSED, false);
        } else {
            PdfBciReports.addMobilReportImage("Tap botón continuar", "El botón continuar no se encuentra habilitado", EstadoPrueba.FAILED, false);
        }
    }

    //***************************************************************************************************

    @Step
    public void tapBotonCerrar() {
        btnCerrar.click();
        PdfBciReports.addReport("Tap botón cerrar", "Se hace tap al botón cerrar", EstadoPrueba.PASSED, false);
    }

    //***************************************************************************************************

    @Step
    public void tapFlechaVolverAtras() {
        flechaAtrasProgreso.click();
        PdfBciReports.addReport("Tap flecha atrás", "Se hace tap a la flecha volver atrás", EstadoPrueba.PASSED, false);
    }
}
