package pages.Enrolamiento;

import constants.ConstantesInversiones;
import driver.DriverContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.appium.java_client.touch.offset.PointOption;
import io.qameta.allure.Step;
import org.openqa.selenium.Point;
import org.openqa.selenium.support.PageFactory;
import reporter.EstadoPrueba;
import reporter.PdfBciReports;
import util.AccionesGenericas;
import util.Gestures;

import java.util.List;

public class SituacionTributaria {

    private AppiumDriver driver;

    public SituacionTributaria() {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver), this);
    }

    /***************************************************************************************************
     *                                          OBJETOS                                                *
     ***************************************************************************************************/

    //Título pantalla
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/toolbar_title")
    @iOSFindBy(accessibility = "Enrolarme para invertir")
    private MobileElement tituloPagina;

    //Botón cerrar
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/action_close")
    @iOSFindBy(accessibility = "close")
    private MobileElement btnCerrar;

    //Flecha atrás progreso
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/bt_progress_back")
    @iOSFindBy(accessibility = "UIView.ProgressBarPager.PreviousStepButton")
    private MobileElement flechaAtrasProgreso;

    //Barra progreso
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/horizontal_progress_bar")
    @iOSFindBy(accessibility = "UIView.ProgressBarPager.ProgressBarView")
    private MobileElement barraProgreso;

    //Stepper
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/tv_stepper_indicator")
    @iOSFindBy(accessibility = "UIView.ProgressBarPager.StepIndicatorLabel")
    private MobileElement stepper;

    //Titulo Situación tributaria
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/tv_title")
    @iOSFindBy(accessibility = "Antecedentes adicionales")
    private MobileElement tituloSituacionTributaria;

    /*******************************************************************************************/

    //Títulos radio buttons
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/tv_group_title")
    private List<MobileElement> titulosRdos;

    //Titulo radiobutton US Person
    @iOSFindBy(accessibility = "TwoChoiceTableViewCell.TitleLabel.USPerson")
    private MobileElement titulordoUSPerson;

    //Titulo radiobutton fondos US Person
    @iOSFindBy(accessibility = "TwoChoiceTableViewCell.TitleLabel.OrigenFondos")
    private MobileElement titulordoFondosUSPerson;

    //Titulo radiobutton PEP
    @iOSFindBy(accessibility = "TwoChoiceTableViewCell.TitleLabel.PEP")
    private MobileElement titulordoPEP;

    /*******************************************************************************************/

    //Radio button si US Person
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/rb_situacion_tributaria_nacion_usa_si")
    @iOSFindBy(accessibility = "TwoChoiceTableViewCell.LabelA.USPerson")
    private MobileElement rdoSiUSPerson;

    //Radio button no US Person
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/rb_situacion_tributaria_nacion_usa_no")
    @iOSFindBy(accessibility = "TwoChoiceTableViewCell.LabelB.USPerson")
    private MobileElement rdoNoUSPerson;

    /*******************************************************************************************/

    //Radio button si fondos US Person
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/rb_situacion_tributaria_fondos_us_person_si")
    @iOSFindBy(accessibility = "TwoChoiceTableViewCell.LabelA.OrigenFondos")
    private MobileElement rdoSiFondosUSPerson;

    //Radio button no fondos US Person
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/rb_situacion_tributaria_fondos_us_person_no")
    @iOSFindBy(accessibility = "TwoChoiceTableViewCell.LabelB.OrigenFondos")
    private MobileElement rdoNoFondosUSPerson;

    /*******************************************************************************************/

    //Radio button si PEP
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/rb_situacion_tributaria_vinculos_pep_si")
    @iOSFindBy(accessibility = "TwoChoiceTableViewCell.LabelA.PEP")
    private MobileElement rdoSiPEP;

    //Radio button no fondos US Person
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/rb_situacion_tributaria_vinculos_pep_no")
    @iOSFindBy(accessibility = "TwoChoiceTableViewCell.LabelB.PEP")
    private MobileElement rdoNoPEP;

    /*******************************************************************************************/

    //Switch pago impuestos otros paises
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/scImpuestosOtrosPaises")
    @iOSFindBy(xpath = "//XCUIElementTypeSwitch[@name=\"¿Pagas impuestos en otros países diferentes a Chile?, Al mantener esta opción desactivada, declaras que debes pagar impuestos en Chile y no tienes obligaciones tributarias en ninguna otra jurisdicción.\"]")
    private MobileElement switchImpuestos;

    //Info pago impuestos otros paises
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/tvImpuestosOtrosPaisesBajada")
    @iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Al mantener esta opción desactivada, declaras que debes pagar impuestos en Chile y no tienes obligaciones tributarias en ninguna otra jurisdicción.\"]")
    private MobileElement infoImpuestos;

    /*******************************************************************************************/

    //Check TYC
    @iOSFindBy(accessibility = "CheckBoxWithDescriptionTableViewCell.CheckBox.TerminosYCondicionesEnrolamiento")
    private MobileElement checkTYC;

    //Terminos y condiciones
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/cbTerminosCondiciones")
    @iOSFindBy(accessibility = "CheckBoxWithDescriptionTableViewCell.DescriptionTextView.TerminosYCondicionesEnrolamiento")
    private MobileElement terminosYCondiciones;

    /*******************************************************************************************/

    //Botón continuar
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/btn_continuar")
    @iOSFindBy(accessibility = "CONTINUAR")
    private MobileElement btnContinuar;

    /*******************************************************************************************/
    //MODAL PEP / US PERSON

    //Título modal
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/tv_title")
    @iOSFindBy(xpath = "(//XCUIElementTypeStaticText[@name=\"EnrolamientoViewController.BottomMessage.TitleLab\"])[1]")
    private MobileElement tituloModal;

    //Info modal
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/tv_description")
    @iOSFindBy(xpath = "(//XCUIElementTypeStaticText[@name=\"EnrolamientoViewController.BottomMessage.MessageLabel\"])[1]")
    private MobileElement infoModal;

    //Boton si modal
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/first_button")
    @iOSFindBy(xpath = "(//XCUIElementTypeButton[@name=\"EnrolamientoViewController.BottomMessage.FirstButton\"])[2]")
    private MobileElement btnSiModal;

    //Boton no modal
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/second_button")
    @iOSFindBy(accessibility = "EnrolamientoViewController.BottomMessage.SecondaryButton")
    private MobileElement btnNoModal;

    //Boton entendido
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/cb_close")
    @iOSFindBy(xpath = "(//XCUIElementTypeButton[@name=\"EnrolamientoViewController.BottomMessage.FirstButton\"])[1]")
    private MobileElement btnEntendidoModal;


    //***************************************************************************************************
    // *                                        MÉTODOS                                                 *
    // **************************************************************************************************

    @Step
    public void validarIngresoPantalla() {
        AccionesGenericas.ingresoPantalla(tituloPagina, "Situación Tributaria");
    }

    // **************************************************************************************************

    @Step
    public void validarElementosSuperiores() {
        AccionesGenericas.validarElemento(tituloPagina, "Enrolarme para invertir", 5);
        AccionesGenericas.validarElemento(btnCerrar, 5);
        AccionesGenericas.validarElemento(flechaAtrasProgreso, 5);
        AccionesGenericas.validarElemento(barraProgreso, 5);
        AccionesGenericas.validarElemento(stepper, "5 de 5", 5);
        AccionesGenericas.validarElemento(tituloSituacionTributaria, "Antecedentes adicionales", 5);
    }

    @Step
    public void validarRdosUSPerson() {
        if (DriverContext.getDriver() instanceof IOSDriver){
            AccionesGenericas.validarElemento(titulordoUSPerson, "¿Eres nacido, ciudadano o residente de Estados Unidos de América?", 5);
        }else {
            AccionesGenericas.validarElemento(titulosRdos.get(0), "¿Eres nacido, ciudadano o residente de Estados Unidos de América?", 5);
        }
        AccionesGenericas.validarElemento(rdoSiUSPerson, "Sí", 5);
        AccionesGenericas.validarElemento(rdoNoUSPerson, "No", 5);
    }

    @Step
    public void validarPrecargaRdosUSPerson() {
        if (rdoSiUSPerson.getAttribute("checked").equals("true") || rdoNoUSPerson.getAttribute("checked").equals("true")) {
            PdfBciReports.addMobilReportImage("Validar precarga radio buttons US Person", "Viene precargado US Person", EstadoPrueba.FAILED, false);
            PdfBciReports.closePDF();
        } else {
            PdfBciReports.addMobilReportImage("Validar precarga radio buttons US Person", "No vienen datos precargados", EstadoPrueba.PASSED, false);

        }
    }

    @Step
    public void seleccionarUSPerson(String usPerson) {
        switch (usPerson) {
            case "Si":
                rdoSiUSPerson.click();
                PdfBciReports.addMobilReportImage("Seleccionar US Person", "Se selecciona 'Si' en pregunta US Person", EstadoPrueba.PASSED, false);
                break;
            case "No":
                if (DriverContext.getDriver() instanceof IOSDriver){
                    Point point = rdoNoUSPerson.getLocation();
                    int x = point.x + 10;
                    int y = point.y + 10;
                    TouchAction touchAction = new TouchAction(driver);
                    touchAction.tap(PointOption.point(x, y)).perform();
                }else {
                    rdoNoUSPerson.click();
                }
                PdfBciReports.addMobilReportImage("Seleccionar US Person", "Se selecciona 'No' en pregunta US Person", EstadoPrueba.PASSED, false);
                break;
            default:
                PdfBciReports.addMobilReportImage("Seleccionar US Person", "No se selecciona ninguna opción en pregunta US Person", EstadoPrueba.FAILED, true);
                break;
        }
    }

    @Step
    public void validarRdosFondosUSPerson() {
        Gestures gestures = new Gestures();
        gestures.encontrarObjeto(rdoNoFondosUSPerson, "Sección Radio Buttons Fondos US Person");
        if (DriverContext.getDriver() instanceof IOSDriver) {
            AccionesGenericas.validarElemento(titulordoFondosUSPerson, "¿Los fondos a invertir fueron proveídos por US Person (entidades o personas)?", 5);
        }else {
            AccionesGenericas.validarElemento(titulosRdos.get(1), "¿Los fondos a invertir fueron proveídos por US Person (entidades o personas)?", 5);
        }
        AccionesGenericas.validarElemento(rdoSiFondosUSPerson, "Sí", 5);
        AccionesGenericas.validarElemento(rdoNoFondosUSPerson, "No", 5);
    }

    @Step
    public void validarPrecargaRdosFondosUSPerson() {
        if (rdoSiFondosUSPerson.getAttribute("checked").equals("true") || rdoNoFondosUSPerson.getAttribute("checked").equals("true")) {
            PdfBciReports.addMobilReportImage("Validar precarga radio buttons Fondos US Person", "Viene precargado Fondos US Person", EstadoPrueba.FAILED, false);
            PdfBciReports.closePDF();
        } else {
            PdfBciReports.addMobilReportImage("Validar precarga radio buttons Fondos US Person", "No vienen datos precargados", EstadoPrueba.PASSED, false);

        }
    }

    @Step
    public void seleccionarFondosUSPerson(String fondosUSPerson) {
        switch (fondosUSPerson) {
            case "Si":
                rdoSiFondosUSPerson.click();
                PdfBciReports.addMobilReportImage("Seleccionar Fondos US Person", "Se selecciona 'Si' en pregunta fondos US Person", EstadoPrueba.PASSED, false);
                break;
            case "No":
                if (DriverContext.getDriver() instanceof IOSDriver) {
                    Point point = rdoNoFondosUSPerson.getLocation();
                    int x = point.x + 10;
                    int y = point.y + 10;
                    TouchAction touchAction = new TouchAction(driver);
                    touchAction.tap(PointOption.point(x, y)).perform();
                }else {
                    rdoNoFondosUSPerson.click();
                }
                PdfBciReports.addMobilReportImage("Seleccionar Fondos US Person", "Se selecciona 'No' en pregunta fondos US Person", EstadoPrueba.PASSED, false);
                break;
            default:
                PdfBciReports.addMobilReportImage("Seleccionar Fondos US Person", "No se selecciona ninguna opción en pregunta fondos US Person", EstadoPrueba.FAILED, true);
                break;
        }
    }

    @Step
    public void validarRdosPEP() {
        Gestures gestures = new Gestures();
        gestures.encontrarObjeto(rdoNoPEP, "Sección Radio Buttons PEP");
        if (DriverContext.getDriver() instanceof IOSDriver){
            AccionesGenericas.validarElemento(titulordoPEP, "¿Eres o tienes vínculos con personas políticamente expuestas? (PEP)", 5);
        }else {
            AccionesGenericas.validarElemento(titulosRdos.get(1), "¿Eres o tienes vínculos con personas políticamente expuestas? (PEP)", 5);
        }
        AccionesGenericas.validarElemento(rdoSiPEP, "Sí", 5);
        AccionesGenericas.validarElemento(rdoNoPEP, "No", 5);
    }

    @Step
    public void validarPrecargaRdosPEP() {
        if (rdoSiPEP.getAttribute("checked").equals("true") || rdoNoPEP.getAttribute("checked").equals("true")) {
            PdfBciReports.addMobilReportImage("Validar precarga radio buttons PEP", "Viene precargado PEP", EstadoPrueba.FAILED, false);
            PdfBciReports.closePDF();
        } else {
            PdfBciReports.addMobilReportImage("Validar precarga radio buttons PEP", "No vienen datos precargados", EstadoPrueba.PASSED, false);

        }
    }

    @Step
    public void seleccionarPEP(String pep) {
        switch (pep) {
            case "Si":
                rdoSiPEP.click();
                PdfBciReports.addMobilReportImage("Seleccionar PEP", "Se selecciona 'Si' en pregunta PEP", EstadoPrueba.PASSED, false);
                break;
            case "No":
                if (DriverContext.getDriver() instanceof IOSDriver) {
                    Point point = rdoNoPEP.getLocation();
                    int x = point.x + 10;
                    int y = point.y + 10;
                    TouchAction touchAction = new TouchAction(driver);
                    touchAction.tap(PointOption.point(x, y)).perform();
                }else {
                    rdoNoPEP.click();
                }
                PdfBciReports.addMobilReportImage("Seleccionar PEP", "Se selecciona 'No' en pregunta PEP", EstadoPrueba.PASSED, false);
                break;
            default:
                PdfBciReports.addMobilReportImage("Seleccionar PEP", "No se selecciona ninguna opción en pregunta PEP", EstadoPrueba.PASSED, false);
                break;
        }
    }

    @Step
    public void validarSwitchImpuestos() {
        Gestures gestures = new Gestures();
        gestures.encontrarObjeto(infoImpuestos, "Sección Switch Impuestos Otros Paises");
        if (DriverContext.getDriver() instanceof IOSDriver){
            AccionesGenericas.validarElemento(switchImpuestos, 5);
            AccionesGenericas.validarElemento(infoImpuestos, 5);
        }else {
            AccionesGenericas.validarElemento(switchImpuestos, "¿Pagas impuestos en otros países diferentes a Chile? DESACTIVADO", 5);
            AccionesGenericas.validarElemento(infoImpuestos, "Al mantener esta opción desactivada, declaras que debes pagar impuestos en Chile y no tienes obligaciones tributarias en ninguna otra jurisdicción.", 5);
        }
    }

    @Step
    public void tapSwitchImpuestos() {
        switchImpuestos.click();
        PdfBciReports.addMobilReportImage("Tap Switch Impuestos", "Se hace tap a switch pago impuestos otros paises", EstadoPrueba.PASSED, false);
    }

    @Step
    public void validarTYC() {
        Gestures gestures = new Gestures();
        gestures.encontrarObjeto(terminosYCondiciones, "Sección Términos y Condiciones");
        AccionesGenericas.validarElemento(terminosYCondiciones, "Declaro haber leído y aceptado los términos y condiciones.", 5);
    }

    @Step
    public void checkTYC() {
        if (DriverContext.getDriver() instanceof IOSDriver){
            checkTYC.click();
        }else {
            Point point = terminosYCondiciones.getLocation();
            int x = point.x + 40;
            int y = point.y + 40;
            TouchAction touchAction = new TouchAction(driver);
            touchAction.tap(PointOption.point(x, y)).perform();
            PdfBciReports.addMobilReportImage("Check Terminos y condiciones", "Se hace tap al check términos y condiciones", EstadoPrueba.PASSED, false);
        }
    }

    @Step
    public void tapTYC() {
        Point point = terminosYCondiciones.getLocation();
        int x = point.x + 1020;
        int y = point.y + 40;
        TouchAction touchAction = new TouchAction(driver);
        touchAction.tap(PointOption.point(x, y)).perform();
        PdfBciReports.addReport("Tap Terminos y condiciones", "Se hace tap al link términos y condiciones", EstadoPrueba.PASSED, false);
    }

    @Step
    public void validarBtnContinuar() {
        Gestures gestures = new Gestures();
        gestures.encontrarObjeto(btnContinuar, "Botón continuar");
        AccionesGenericas.validarElemento(btnContinuar, "CONTINUAR", 5);
    }

    @Step
    public void tapBtnContinuar() {
        btnContinuar.click();
        PdfBciReports.addReport("Tap botón continuar", "Se hace tap al botón continuar", EstadoPrueba.PASSED, false);
    }

    @Step
    public void validarSwitchHabilitado() {
        if (switchImpuestos.getAttribute("checked").equals("true")) {
            PdfBciReports.addMobilReportImage("Validar switch habilidatado", "El switch se encuentra habilitado", EstadoPrueba.PASSED, false);
        } else {
            PdfBciReports.addMobilReportImage("Validar switch habilidatado", "El switch se encuentra deshabilitado", EstadoPrueba.FAILED, false);
            PdfBciReports.closePDF();
        }
    }

    @Step
    public void validarBtnContinuarDeshabilitado() {
        if (btnContinuar.isEnabled()) {
            PdfBciReports.addMobilReportImage("Validar botón continuar deshabilitado", "El botón continuar se encuentra habilitado", EstadoPrueba.FAILED, false);
            PdfBciReports.closePDF();
        } else {
            PdfBciReports.addMobilReportImage("Validar botón continuar deshabilitado", "El botón continuar se encuentra deshabilitado", EstadoPrueba.PASSED, false);
        }
    }

    @Step
    public void tapBotonVolverAtras() {
        flechaAtrasProgreso.click();
        PdfBciReports.addReport("Tap botón volver atrás", "Se hace tap a flecha volver atrás", EstadoPrueba.PASSED, false);
    }

    @Step
    public void tapBotonCerrar() {
        btnCerrar.click();
        PdfBciReports.addReport("Tap botón cerrar", "Se hace tap a botón cerrar", EstadoPrueba.PASSED, false);
    }

    @Step
    public void validarModalUSPerson() {
        AccionesGenericas.validarElemento(tituloModal, "Cliente US Person", 5);
        AccionesGenericas.validarElemento(infoModal, 5);
        AccionesGenericas.validarElemento(btnSiModal, "SÍ, SOY US PERSON", 5);
        AccionesGenericas.validarElemento(btnNoModal, "NO, NO SOY US PERSON", 5);
    }

    @Step
    public void validarModalPEP() {
        AccionesGenericas.validarElemento(tituloModal, "Persona políticamente expuesta (PEP)", 5);
        AccionesGenericas.validarElemento(infoModal, 5);
        AccionesGenericas.validarElemento(btnSiModal, "SÍ, SOY PEP", 5);
        AccionesGenericas.validarElemento(btnNoModal, "NO, NO SOY PEP", 5);
    }

    @Step
    public void validarModalImpuestos() {
        AccionesGenericas.validarElemento(tituloModal, "Pago de impuestos en otros países", 5);
        AccionesGenericas.validarElemento(infoModal, "La opción de ingresar el (los) país (es) donde mantienes obligaciones tributarias, estará pronto disponible desde la APP. Ingresa a Bci.cl y en Mi Primera Inversión Online podrás continuar tu proceso de enrolamiento para invertir.", 5);
        AccionesGenericas.validarElemento(btnEntendidoModal, "ENTENDIDO", 5);
    }

    @Step
    public void tapBotonSiModal() {
        btnSiModal.click();
        PdfBciReports.addMobilReportImage("tapBotonSiModal", "Se hace tap a boton 'Si' del modal", EstadoPrueba.PASSED, false);
    }

    @Step
    public void tapBotonNoModal() {
        btnNoModal.click();
        PdfBciReports.addMobilReportImage("tapBotonNoModal", "Se hace tap a boton 'No' del modal", EstadoPrueba.PASSED, false);
    }

    @Step
    public void tapBotonEntendidoModal() {
        btnEntendidoModal.click();
        PdfBciReports.addMobilReportImage("tapBotonEntendidoModal", "Se hace tap a boton 'Entendido' del modal", EstadoPrueba.PASSED, false);
    }


}
