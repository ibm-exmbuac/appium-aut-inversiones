package pages.Enrolamiento;

import constants.ConstantesInversiones;
import driver.DriverContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.appium.java_client.touch.offset.PointOption;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.Point;
import org.openqa.selenium.support.PageFactory;
import reporter.EstadoPrueba;
import reporter.PdfBciReports;
import util.AccionesGenericas;
import util.Gestures;

import java.util.List;

public class Contratos {

    private AppiumDriver driver;

    public Contratos() {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver), this);
    }

    /*************************************************************************************************
     *                                          OBJETOS                                                *
     ***************************************************************************************************/

    //Título pantalla
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/toolbar_title")
    @iOSFindBy(accessibility = "Enrolarme para invertir")
    private MobileElement tituloPagina;

    //Botón cerrar
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/action_close")
    @iOSFindBy(accessibility = "close")
    private MobileElement btnCerrar;

    //Título Revisa tus Contratos
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/tv_title")
    @iOSFindBy(accessibility = "Revisa tus Contratos")
    private MobileElement tituloRevisaContratos;

    //Info contratos
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/tv_description")
    @iOSFindBy(accessibility = "DescriptionTableViewCell.descriptionLabel")
    private MobileElement infoContratos;

    /***************************************************************************************************/

    //Contratos
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/tv_ffmm_documentos")
    private List<MobileElement> contratos;

    //Contrato 1
    @iOSFindBy(accessibility = "EnrolamientoContratosPresenter.DocumentFATCA")
    private MobileElement contratos1;

    //Contrato 2
    @iOSFindBy(accessibility = "EnrolamientoContratosPresenter.DocumentVOLCKER")
    private MobileElement contratos2;

    //Contrato 3
    @iOSFindBy(accessibility = "EnrolamientoContratosPresenter.DocumentCGF")
    private MobileElement contratos3;

    //Contrato 4
    @iOSFindBy(accessibility = "EnrolamientoContratosPresenter.DocumentFICHA")
    private MobileElement contratos4;

    /***************************************************************************************************/

    //Botón Permitir cuadro nativo
    @AndroidFindBy(id = ConstantesInversiones.packageInstaller+":id/permission_allow_button")
    private MobileElement btnPermitir;

    //Toolbar vista PDF
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/toolbar_pdf_viewer")
    private MobileElement toolbarPDF;

    //PDF
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/image_pdf")
    private MobileElement pdf;

    //Botón crear PDF
    @iOSFindBy(accessibility = "Create PDF")
    private MobileElement btnCrearPDF;

    //Botón Done
    @iOSFindBy(accessibility = "QLOverlayDoneButtonAccessibilityIdentifier")
    private MobileElement btnDone;

    //Botón Delete PDF
    @iOSFindBy(accessibility = "Delete PDF")
    private MobileElement btnDeletePDF;


    /***************************************************************************************************/

    //Términos y condiciones
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/cbTerminosCondiciones")
    @iOSFindBy(xpath = "(//XCUIElementTypeTextView[@name=\"CheckBoxWithDescriptionTableViewCell.DescriptionTextView.DeclaracionFirmaContrato\"])[1]")
    private MobileElement terminosYCondiciones;

    //Check TYC iOS
    @iOSFindBy(accessibility = "CheckBoxWithDescriptionTableViewCell.CheckBox.DeclaracionFirmaContrato")
    private MobileElement checkTYC;

    //Botón continuar
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/btn_continuar")
    @iOSFindBy(accessibility = "CONTINUAR")
    private MobileElement btnContinuar;

    //***************************************************************************************************
    // *                                        MÉTODOS                                                 *
    // **************************************************************************************************

    @Step
    public void validarIngresoPantalla() {
        Gestures gestures = new Gestures();
        gestures.swipe(0.4, 0.9);
        AccionesGenericas.ingresoPantalla(tituloPagina, "Pantalla Contratos");
    }

    // **************************************************************************************************

    @Step
    public void validarElementosSuperiores() {
        AccionesGenericas.validarElemento(tituloPagina, "Enrolarme para invertir", 5);
        AccionesGenericas.validarElemento(btnCerrar, 5);
        AccionesGenericas.validarElemento(tituloRevisaContratos, "Revisa tus contratos", 5);
        AccionesGenericas.validarElemento(infoContratos, "Los contratos que te presentamos a continuación son indispensables para continuar tu proceso para invertir en Fondos Mutuos por lo que es importantes que leas, aceptes y firmes los contratos con BciPass.", 5);
    }

    @Step
    public void validarListadoContratos() {
        String[] arregloNombresContratos = new String[4];
        arregloNombresContratos[0] = "Declaración de Clasificación y Consentimiento";
        arregloNombresContratos[1] = "Carta de Representación de Inversionista";
        arregloNombresContratos[2] = "Contrato General de Fondos Bci Asset Management Administradora General de Fondos S.A.";
        arregloNombresContratos[3] = "Ficha Cliente Inversionista Personas Naturales";
        if (DriverContext.getDriver() instanceof IOSDriver){
            AccionesGenericas.validarElemento(contratos1, arregloNombresContratos[0], 5);
            AccionesGenericas.validarElemento(contratos2, arregloNombresContratos[1], 5);
            AccionesGenericas.validarElemento(contratos3, arregloNombresContratos[2], 5);
            AccionesGenericas.validarElemento(contratos4, arregloNombresContratos[3], 5);

        }else {
            for (int i = 0; i < contratos.size(); i++) {
                AccionesGenericas.validarElemento(contratos.get(i), arregloNombresContratos[i], 5);
            }
        }
    }

    @Step
    public void validarListadoPDFs() throws InterruptedException {
        if (DriverContext.getDriver() instanceof IOSDriver){
            for(int i=0; i<4; i++){
                switch (i){
                    case 0:
                        contratos1.click();
                        AccionesGenericas.esperar(2);
                        btnCrearPDF.click();
                        validarPantallaPdf("");
                        break;
                    case 1:
                        contratos2.click();
                        AccionesGenericas.esperar(1);
                        btnCrearPDF.click();
                        validarPantallaPdf("");
                        break;
                    case 2:
                        contratos3.click();
                        AccionesGenericas.esperar(1);
                        btnCrearPDF.click();
                        validarPantallaPdf("");
                        break;
                    case 3:
                        contratos4.click();
                        AccionesGenericas.esperar(1);
                        btnCrearPDF.click();
                        validarPantallaPdf("");
                        break;
                }
            }
        }else {
            String[] tituloContratos = new String[4];
            tituloContratos[0] = "Declaración de Clasificación y Consentimiento";
            tituloContratos[1] = "Carta de Representación de Inversionista";
            tituloContratos[2] = "Contrato General de Fondos Bci Asset Management Administradora General de Fondos S.A.";
            tituloContratos[3] = "Ficha Cliente Inversionista Personas Naturales";
            for (int i = 0; i < contratos.size(); i++) {
                contratos.get(i).click();
                if (AccionesGenericas.existeElemento(btnPermitir, 3)) {
                    btnPermitir.click();
                }
                validarPantallaPdf(tituloContratos[i]);
            }
        }
    }

    private void validarPantallaPdf(String tituloContrato) throws InterruptedException {
        if (DriverContext.getDriver() instanceof IOSDriver){
            AccionesGenericas.ingresoPantalla(btnDone, "Pantalla PDF");
            btnDone.click();
            btnDeletePDF.click();
            AccionesGenericas.esperar(2);
        }else {
            AccionesGenericas.ingresoPantalla(toolbarPDF, "Pantalla PDF");
            MobileElement flechaAtras = toolbarPDF.findElement(By.className("android.widget.ImageButton"));
            MobileElement titulo = toolbarPDF.findElement(By.className("android.widget.TextView"));
            AccionesGenericas.validarElemento(flechaAtras, 5);
            AccionesGenericas.validarElemento(titulo, tituloContrato, 5);
            AccionesGenericas.validarElemento(pdf, 5);
            flechaAtras.click();
        }
    }

    @Step
    public void validarTYC() {
        Gestures gestures = new Gestures();
        gestures.encontrarObjeto(terminosYCondiciones, "Términos y Condiciones");
        AccionesGenericas.validarElemento(terminosYCondiciones, "Declaro haber leído los documentos y acepto los términos y condiciones de éstos, de esta manera autorizar al Banco de Crédito e Inversiones a realizar el proceso de firma digital de los siguientes contratos para invertir en Fondos Mutuos.", 5);
    }

    @Step
    public void checkTYC() {
        if (DriverContext.getDriver() instanceof IOSDriver) {
            checkTYC.click();
        }else {
            Point point = terminosYCondiciones.getLocation();
            int x = point.x + 40;
            int y = point.y + 40;
            TouchAction touchAction = new TouchAction(driver);
            touchAction.tap(PointOption.point(x, y)).perform();
            PdfBciReports.addMobilReportImage("Check terminos y condiciones", "Se hace tap al check de TYC", EstadoPrueba.PASSED, false);
        }
    }

    @Step
    public void validarBtnContinuar() {
        Gestures gestures = new Gestures();
        gestures.encontrarObjeto(btnContinuar, "Botón continuar");
        AccionesGenericas.validarElemento(btnContinuar, "CONTINUAR", 5);
    }

    @Step
    public void tapBotonContinuar() {
        btnContinuar.click();
        PdfBciReports.addMobilReportImage("Tap Boton Continuar", "Se hace tap al botón continuar", EstadoPrueba.PASSED, false);
    }

    @Step
    public void tapBotonCerrar() {
        btnCerrar.click();
        PdfBciReports.addReport("Tap botón cerrar", "Se hace tap al botón cerrar", EstadoPrueba.PASSED, false);
    }
}
