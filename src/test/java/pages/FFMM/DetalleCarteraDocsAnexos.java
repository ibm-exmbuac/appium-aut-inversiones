package pages.FFMM;

import constants.Constants;
import driver.DriverContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.qameta.allure.Step;
import org.openqa.selenium.support.PageFactory;
import reporter.EstadoPrueba;
import reporter.PdfBciReports;
import util.AccionesGenericas;
import util.Gestures;

import java.util.List;

public class DetalleCarteraDocsAnexos {
    private AppiumDriver driver;

    public DetalleCarteraDocsAnexos() {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver), this);
    }

    /*********************************************************************
     * OBJETOS
     **********************************************************************/

    //Título documentos anexos
    @iOSFindBy(accessibility = "Documentos anexos")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_ffmm_documentos_label")
    private MobileElement tituloDocAnexos;

    //Documento anexo 1
    @iOSFindBy(accessibility = "DetalleFondosMutuosPresenter.Document0")
    private MobileElement docAnexo1;

    //Documento anexo 2
    @iOSFindBy(accessibility = "DetalleFondosMutuosPresenter.Document1")
    private MobileElement docAnexo2;

    //Documento anexo 3
    @iOSFindBy(accessibility = "DetalleFondosMutuosPresenter.Document2")
    private MobileElement docAnexo3;

    //Bóton ver más / menos IOS
    @iOSFindBy(accessibility = "ActionTableViewCell.actionButton")
    private MobileElement btnVerMasMenos;

    //Bóton ver más Android
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/btn_ver_mas")
    private MobileElement btnVerMas;

    //Bóton ver menos Android
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/btn_ver_menos")
    private MobileElement btnVerMenos;

    //Documento anexo 4
    @iOSFindBy(accessibility = "DetalleFondosMutuosPresenter.Document3")
    private MobileElement docAnexo4;

    //Documento anexo 5
    @iOSFindBy(accessibility = "DetalleFondosMutuosPresenter.Document4")
    private MobileElement docAnexo5;

    //Documento anexo 6
    @iOSFindBy(accessibility = "DetalleFondosMutuosPresenter.Document5")
    private MobileElement docAnexo6;

    //Documento anexo 7
    @iOSFindBy(accessibility = "DetalleFondosMutuosPresenter.Document6")
    private MobileElement docAnexo7;

    //Documento anexo 8
    @iOSFindBy(accessibility = "DetalleFondosMutuosPresenter.Document7")
    private MobileElement docAnexo8;


    /*********************************************************************
     *DOCS ANEXOS ANDROID
     **********************************************************************/

    //Documentos anexos
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_ffmm_documentos")
    private List<MobileElement> docAnexos;


    /*********************************************************************
     *METODOS
     **********************************************************************/

    @Step
    public void validarElementos() {
        Gestures gestures = new Gestures();
        if (DriverContext.getDriver() instanceof IOSDriver) {
            gestures.encontrarObjeto(tituloDocAnexos, btnVerMasMenos, "Sección Documentos Anexos");
            PdfBciReports.addMobilReportImage("Documentos Anexos", "Se visualiza sección Documentos Anexos", EstadoPrueba.PASSED, false);
            AccionesGenericas.validarElemento(this.tituloDocAnexos, 5);
            AccionesGenericas.validarElemento(this.docAnexo1, 5);
            AccionesGenericas.validarElemento(this.docAnexo2, 5);
            AccionesGenericas.validarElemento(this.docAnexo3, 5);
            AccionesGenericas.validarElemento(this.btnVerMasMenos, 5);
            this.btnVerMasMenos.click();
            gestures.scrollAbajoCorto();
            AccionesGenericas.validarElemento(this.docAnexo4, 5);
            AccionesGenericas.validarElemento(this.docAnexo5, 5);
            AccionesGenericas.validarElemento(this.docAnexo6, 5);
            AccionesGenericas.validarElemento(this.docAnexo7, 5);
            gestures.scrollAbajoCorto();
            AccionesGenericas.validarElemento(this.docAnexo8, 5);
            this.btnVerMasMenos.click();
        } else {
            gestures.encontrarObjeto(tituloDocAnexos, btnVerMas, "Sección Documentos Anexos");
            PdfBciReports.addMobilReportImage("Documentos Anexos", "Se visualiza sección Documentos Anexos", EstadoPrueba.PASSED, false);
            AccionesGenericas.validarElemento(this.tituloDocAnexos, 5);
            int ciclo = 1;
            for (int i = 0; i < this.docAnexos.size(); i++) {

                boolean existe = AccionesGenericas.existeElemento(this.docAnexos.get(i), 5);
                if (existe) {
                    PdfBciReports.addReport("Validar doc Anexos", "Se encuentra el doc anexo" + this.docAnexos.get(i).getText(), EstadoPrueba.PASSED, false);
                } else {
                    PdfBciReports.addReport("Validar doc Anexos", "No se encuentra el doc anexo" + this.docAnexos.get(i).getText(), EstadoPrueba.FAILED, false);
                }

                if (this.docAnexos.size() == i + 1 && ciclo == 1) {
                    gestures.scrollAbajo();
                    this.btnVerMas.click();
                    ciclo++;
                }
            }
            gestures.scrollAbajo();
            this.btnVerMenos.click();
        }
    }


}
