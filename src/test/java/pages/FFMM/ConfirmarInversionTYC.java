package pages.FFMM;

import constants.Constants;
import driver.DriverContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.appium.java_client.touch.offset.PointOption;
import io.qameta.allure.Step;
import org.openqa.selenium.Point;
import org.openqa.selenium.support.PageFactory;
import reporter.EstadoPrueba;
import reporter.PdfBciReports;
import util.AccionesGenericas;
import util.Gestures;

import static driver.DriverContext.getDriver;

public class ConfirmarInversionTYC {

    private AppiumDriver driver;

    public ConfirmarInversionTYC() {
        this.driver = getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver), this);
    }

    /****************************
     *CUADRO TERMINOS IOS
     ****************************/
    //Info términos y condiciones
    @iOSFindBy(accessibility = "Declaro haber leído y aceptado el reglamento interno y los términos y condiciones de este Fondo Mutuo.")
    private MobileElement terminosYCondiciones;

    //Botón términos y condiciones
    @iOSFindBy(accessibility = "términos y condiciones")
    private MobileElement btnTerminosYCondiciones;

    //Checkbox términos y condiciones
    @iOSFindBy(accessibility = "ConfirmacionDatosFFMMViewController.CheckBox")
    private MobileElement checkTerminosYCondiciones;

    /****************************
     *CUADRO TERMINOS ANDROID
     ****************************/
    //Términos y Condiciones Android
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/cb_confirmar_ffmm_terminos_condiciones")
    private MobileElement terminosYCondicionesAndroid;

    /****************************/

    //Título términos y condiciones
    @iOSFindBy(accessibility = "Términos y condiciones")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/toolbar_title")
    private MobileElement tituloTerminosYCondiciones;

    //Botón cerrar superior
    @iOSFindBy(accessibility = "close")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/iv_icon_close")
    private MobileElement btnCerrarSuperior;

    //Términos y condiciones
    @iOSFindBy(xpath = "//XCUIElementTypeCell/XCUIElementTypeTextView")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_message")
    private MobileElement terminosYCondicionesInfo;

    //Botón cerrar
    @iOSFindBy(accessibility = "CERRAR")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/terminos_condiciones_button")
    private MobileElement btnCerrar;

    //Botón invertir
    @iOSFindBy(accessibility = "INVERTIR")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/btn_confirmar_ffmm_invertir")
    private MobileElement btnInvertir;

    //Texto app BCI
    @iOSFindBy(accessibility = "En la App Bci no se encuentra habilitada la opción de acogerse a algunos de los beneficios tributarios contemplados en los reglamentos internos de los fondos. Para acogerse a algún beneficio tributario debe realizar su inversión presencialmente.")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_ffmm_disclaimer")
    private MobileElement textoAppBci;

    /****************************
     *CGF
     ****************************/

    //Titulo CGF
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_ffmm_cgf_title")
    private MobileElement tituloCGF;

    //Info CGF
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_ffmm_cgf_message")
    private MobileElement infoCGF;

    //Doc CGF
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_ffmm_documentos")
    private MobileElement docCGF;


    @Step
    private void checkTYC() {
        Point point = this.terminosYCondicionesAndroid.getLocation();
        int x = point.x + 40;
        int y = point.y + 30;
        TouchAction touchAction = new TouchAction(driver);
        touchAction.tap(PointOption.point(x, y)).perform();
    }

    @Step
    public void validaElementos() {
        Gestures gestures = new Gestures();
        gestures.encontrarObjeto(btnInvertir, "Seccion TYC");
        if (DriverContext.getDriver() instanceof IOSDriver) {
            AccionesGenericas.validarElemento(this.terminosYCondiciones, 5);
            AccionesGenericas.validarElemento(this.btnTerminosYCondiciones, "términos y condiciones", 5);
            AccionesGenericas.validarElemento(this.checkTerminosYCondiciones, 5);
        }else{
            AccionesGenericas.validarElemento(terminosYCondicionesAndroid, 5);
            AccionesGenericas.validarElemento(this.textoAppBci, "En la App Bci no se encuentra habilitada la opción de acogerse a alguno de los beneficios tributarios contemplados en los reglamentos internos de los fondos. Para acogerse a algún beneficio tributario debe realizar su inversión presencialmente.", 5);

        }
    }

    @Step
    public void validaTerminosYCondiciones() {
        this.btnTerminosYCondiciones.click();
        AccionesGenericas.ingresoPantalla(this.tituloTerminosYCondiciones, "Titulo Términos y Condiciones");
        utils.MetodosGenericos.visualizarObjeto(this.btnCerrarSuperior, 5);
        utils.Utils.reporteValidacionTextos(this.terminosYCondicionesInfo, "Declaro conocer y tener a la vista el folleto informativo del fondo, su reglamento interno y la copia de sus últimas carteras de inversiones y estados financieros remitidos a la Comisión de Mercado Financiero con sus notas.");
        utils.Utils.reporteValidacionTextos(this.btnCerrar, "CERRAR");
        this.btnCerrar.click();
    }

    @Step
    public void checkCGF() {
        if (DriverContext.getDriver() instanceof IOSDriver) {
            this.checkTerminosYCondiciones.click();
            PdfBciReports.addMobilReportImage("Check CGF", "Se hace check a términos y condiciones", EstadoPrueba.PASSED, false);
        } else {
            checkTYC();
            PdfBciReports.addMobilReportImage("Check CGF", "Se hace check a términos y condiciones", EstadoPrueba.PASSED, false);
        }
    }

    @Step
    public void validarCGF() {
        AccionesGenericas.validarElemento(this.tituloCGF, "Contrato General de Fondos", 5);
        AccionesGenericas.validarElemento(this.infoCGF, "Previo a realizar la inversión debes leer y aceptar el Contrato General de Fondos", 5);
        AccionesGenericas.validarElemento(this.docCGF, "Contrato General de Fondos", 5);

    }

    @Step
    public void tapInvertir() {
        this.btnInvertir.click();
        PdfBciReports.addReport("Tap Invertir", "Se hace tap al botón invertir", EstadoPrueba.PASSED, false);
    }
}
