package pages.FFMM;

import constants.Constants;
import driver.DriverContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.qameta.allure.Step;
import org.openqa.selenium.support.PageFactory;
import reporter.EstadoPrueba;
import reporter.PdfBciReports;
import util.AccionesGenericas;
import util.Gestures;

import java.util.List;

public class ConfirmarInversionDocsAnexos {

    private AppiumDriver driver;

    public ConfirmarInversionDocsAnexos() {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver), this);
    }


    /*********************************************************************
     * OBJETOS
     **********************************************************************/

    //Título documentos anexos
    @iOSFindBy(accessibility = "Documentos anexos")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_ffmm_documentos_label")
    private MobileElement tituloDocAnexos;

    //Documento anexo 1
    @iOSFindBy(accessibility = "ConfirmacionDatosFFMMPresenter.Document0")
    @AndroidFindBy(xpath = "//android.view.ViewGroup[@index='0']/android.widget.LinearLayout[@index='15']/android.widget.TextView[@index='0']")
    private MobileElement docAnexo1;

    //Documento anexo 2
    @iOSFindBy(accessibility = "ConfirmacionDatosFFMMPresenter.Document1")
    @AndroidFindBy(xpath = "//android.view.ViewGroup[@index='0']/android.widget.LinearLayout[@index='15']/android.widget.TextView[@index='2']")
    private MobileElement docAnexo2;

    //Documento anexo 3
    @iOSFindBy(accessibility = "ConfirmacionDatosFFMMPresenter.Document2")
    @AndroidFindBy(xpath = "//android.view.ViewGroup[@index='0']/android.widget.LinearLayout[@index='15']/android.widget.TextView[@index='4']")
    private MobileElement docAnexo3;

    //Bóton ver más / menos IOS
    @iOSFindBy(accessibility = "ActionTableViewCell.actionButton")
    private MobileElement btnVerMasMenos;

    //Bóton ver más Android
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/btn_ver_mas")
    private MobileElement btnVerMas;

    //Bóton ver menos Android
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/btn_ver_menos")
    private MobileElement btnVerMenos;

    //Documento anexo 4
    @iOSFindBy(accessibility = "ConfirmacionDatosFFMMPresenter.Document3")
    @AndroidFindBy(xpath = "//android.view.ViewGroup[@index='0']/android.widget.LinearLayout[@index='16']/android.widget.TextView[@index='1']")
    private MobileElement docAnexo4;

    //Documento anexo 5
    @iOSFindBy(accessibility = "ConfirmacionDatosFFMMPresenter.Document4")
    @AndroidFindBy(xpath = "//android.view.ViewGroup[@index='0']/android.widget.LinearLayout[@index='16']/android.widget.TextView[@index='3']")
    private MobileElement docAnexo5;

    //Documento anexo 6
    @iOSFindBy(accessibility = "ConfirmacionDatosFFMMPresenter.Document5")
    @AndroidFindBy(xpath = "//android.view.ViewGroup[@index='0']/android.widget.LinearLayout[@index='16']/android.widget.TextView[@index='5']")
    private MobileElement docAnexo6;

    //Documento anexo 7
    @iOSFindBy(accessibility = "ConfirmacionDatosFFMMPresenter.Document6")
    @AndroidFindBy(xpath = "//android.view.ViewGroup[@index='0']/android.widget.LinearLayout[@index='16']/android.widget.TextView[@index='7']")
    private MobileElement docAnexo7;

    //Documento anexo 8
    @iOSFindBy(accessibility = "ConfirmacionDatosFFMMPresenter.Document7")
    @AndroidFindBy(xpath = "//android.view.ViewGroup[@index='0']/android.widget.LinearLayout[@index='16']/android.widget.TextView[@index='9']")
    private MobileElement docAnexo8;

    /*********************************************************************
     *DOCS ANEXOS ANDROID
     **********************************************************************/

    //Documentos anexos
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_ffmm_documentos")
    private List<MobileElement> docAnexos;


    /*********************************************************************
     *METODOS
     **********************************************************************/

    @Step
    public void validarElementos() {
        Gestures ges = new Gestures();
        utils.MetodosGenericos.visualizarObjeto(this.btnVerMasMenos, 5);
        if (DriverContext.getDriver() instanceof IOSDriver) {
            ges.encontrarObjeto(btnVerMasMenos, "Seccion Docs Anexos");
            AccionesGenericas.validarElemento(this.docAnexo1, 5);
            AccionesGenericas.validarElemento(this.docAnexo2, 5);
            AccionesGenericas.validarElemento(this.docAnexo3, 5);
            AccionesGenericas.validarElemento(this.btnVerMasMenos, 5);
        } else {
            ges.encontrarObjeto(btnVerMas, "Docs Anexos");
            for (int i = 0; i < this.docAnexos.size(); i++) {
                boolean existe = utils.MetodosGenericos.visualizarObjeto(this.docAnexos.get(i), 5);
                if (existe) {
                    PdfBciReports.addReport("Validar docs Anexos", "Se encuentra el doc anexo" + this.docAnexos.get(i).getText(), EstadoPrueba.PASSED, false);
                } else {
                    PdfBciReports.addMobilReportImage("Validar docs Anexos", "No se encuentra el doc anexo" + this.docAnexos.get(i).getText(), EstadoPrueba.FAILED, false);
                }
            }
        }
    }

}
