package pages.FFMM;

import constants.Constants;
import driver.DriverContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.qameta.allure.Step;
import org.openqa.selenium.support.PageFactory;
import reporter.EstadoPrueba;
import reporter.PdfBciReports;
import util.AccionesGenericas;

public class ConfirmarInversionDetalle {

    private AppiumDriver driver;

    public ConfirmarInversionDetalle() {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver), this);
    }

    //Título confirmar la inversión
    @iOSFindBy(accessibility = "Confirmar la Inversión")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/toolbar_title")
    private MobileElement tituloConfirmarInv;

    //Flecha atrás
    @iOSFindBy(accessibility = "left arrow")
    @AndroidFindBy(xpath = "//android.widget.LinearLayout[@index='0']/android.view.ViewGroup[@index='1']/android.widget.ImageButton[@index='0']")
    private MobileElement flechaAtras;

    //Label monto a invertir
    @iOSFindBy(accessibility = "ConfirmacionDatosFFMMViewController.MontoInvertidoTitle")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_monto_invertir_label")
    private MobileElement labelMontoAInvertir;

    //Value monto a invertir
    @iOSFindBy(accessibility = "ConfirmacionDatosFFMMViewController.MontoInvertidoLabel")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_monto_invertir_value")
    private MobileElement valueMontoAInvertir;

    //Info fuera horario
    @iOSFindBy(accessibility = "InfoTableViewCell.MessageLabel")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_horario")
    private MobileElement infoFueraHorario;

    //Label FFMM
    @iOSFindBy(accessibility = "ConfirmacionDatosFFMMPresenter.TitleFondoMutuo")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_ffmm_nombre_fondo_label")
    private MobileElement labelFFMM;

    //Value FFMM
    @iOSFindBy(accessibility = "ConfirmacionDatosFFMMPresenter.ValueFondoMutuo")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_ffmm_nombre_fondo_value")
    private MobileElement valueFFMM;

    //Label cuenta de cargo
    @iOSFindBy(accessibility = "ConfirmacionDatosFFMMPresenter.TitleCuentaOrigen")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_ffmm_cuenta_origen_label")
    private MobileElement labelCuentaCargo;

    //Value cuenta de cargo
    @iOSFindBy(accessibility = "ConfirmacionDatosFFMMPresenter.ValueCuentaOrigen")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_ffmm_cuenta_origen_value")
    private MobileElement valueCuentaCargo;

    //Label cuenta de FFMM
    @iOSFindBy(accessibility = "ConfirmacionDatosFFMMPresenter.TitleNombreFondoInversion")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_ffmm_cuenta_inversion_label")
    private MobileElement labelCuentaFFMM;

    //Value cuenta de FFMM
    @iOSFindBy(accessibility = "ConfirmacionDatosFFMMPresenter.ValueNombreFondoInversion")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_ffmm_cuenta_inversion_value")
    private MobileElement valueCuentaFFMM;


    @Step
    public void validarIngresoPantalla() {
        AccionesGenericas.ingresoPantalla(this.tituloConfirmarInv, "Confirmar Inversión FFMM");
    }

    @Step
    public void validarElementos() {
        AccionesGenericas.validarElemento(this.tituloConfirmarInv, "Confirmar la Inversión", 5);
        AccionesGenericas.validarElemento(this.flechaAtras, 5);
        AccionesGenericas.validarElemento(this.labelMontoAInvertir, "Monto a invertir", 5);
        AccionesGenericas.validarElemento(this.valueMontoAInvertir, 5);
        AccionesGenericas.validarElemento(this.labelFFMM, "Fondo Mutuo", 5);
        AccionesGenericas.validarElemento(this.valueFFMM, 5);
        AccionesGenericas.validarElemento(this.labelCuentaCargo, "Cuenta de cargo", 5);
        AccionesGenericas.validarElemento(this.valueCuentaCargo, 5);
        AccionesGenericas.validarElemento(this.labelCuentaFFMM, "Cuenta de Fondo Mutuo", 5);
        AccionesGenericas.validarElemento(this.valueCuentaFFMM, 5);
    }

    @Step
    public void validarMensajeFueraHorario() {
        boolean existe = utils.MetodosGenericos.visualizarObjeto(this.infoFueraHorario, 5);
        if (existe) {
            PdfBciReports.addMobilReportImage("Validar Fuera de Horario", "Se despliega mensaje inversión fuera de horario", EstadoPrueba.PASSED, false);
        } else {
            PdfBciReports.addMobilReportImage("Validar Fuera de Horario", "No se despliega mensaje inversión fuera de horario", EstadoPrueba.FAILED, false);
            PdfBciReports.closePDF();
        }
    }
}
