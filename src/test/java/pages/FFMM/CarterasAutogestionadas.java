package pages.FFMM;

import constants.Constants;
import driver.DriverContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.qameta.allure.Step;
import org.openqa.selenium.support.PageFactory;
import reporter.EstadoPrueba;
import reporter.PdfBciReports;
import util.AccionesGenericas;
import util.Gestures;

import java.util.List;

public class CarterasAutogestionadas {

    private AppiumDriver driver;

    public CarterasAutogestionadas() {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver), this);
    }

    /***************************************************************************************************
     *                                          OBJETOS                                                *
     ***************************************************************************************************/

    //Título
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/toolbar_title")
    @iOSFindBy(accessibility = "Fondos Mutuos")
    private MobileElement tituloFFMM;

    //Flecha atrás
    @AndroidFindBy(xpath = "//android.widget.LinearLayout[@index = '0']/android.view.ViewGroup[@index = '1']/android.widget.ImageButton[@index = '0']")
    @iOSFindBy(accessibility = "left arrow")
    private MobileElement flechaAtras;

    //Botón cerrar
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/action_close")
    private MobileElement btnCerrar;

    //Tu perfil de inversionista
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_perfil_inversionista")
    @iOSFindBy(accessibility = "Tu perfil de inversionista:")
    private MobileElement tuPerfilInv;

    //Perfil inversionista
    //@iOSFindBy(accessibility = "")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_perfil")
    private MobileElement perfil;

    //Título invierte en carteras autogestionadas
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_description_title")
    @iOSFindBy(accessibility = "CarterasDestacadasViewController.CarterasAutogestionadasTitle")
    private MobileElement tituloInvierteCarteras;

    //Info carteras
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_description")
    @iOSFindBy(accessibility = "CarterasDestacadasVieController.DescriptionLabel")
    private MobileElement infoCarteras;

    //Descripción 1
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_estrategia_1")
    @iOSFindBy(accessibility = "CarterasDestacadasViewController.Item1Label")
    private MobileElement descripcion1;

    //Descripcion 2
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_estrategia_2")
    @iOSFindBy(accessibility = "CarterasDestacadasViewController.Item2Label")
    private MobileElement descripcion2;

    //Título puedes conocer y elegir...
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_carteras_destacadas_list_title")
    @iOSFindBy(accessibility = "CarterasDestacadasViewController.CallToActionLabel")
    private MobileElement tituloPuedesConocer;

    /***************************************************************************************************/

    //Otras alternativas
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_carteras_alternativas_list_title")
    @iOSFindBy(accessibility = "Otras alternativas")
    private MobileElement tituloOtrasAlt;

    //Info otras alternativas
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_carteras_alternativas_list_bajada")
    @iOSFindBy(accessibility = "Si lo anterior no es lo que necesitas, tenemos otras opciones de Fondos Mutuos:")
    private MobileElement infoOtrasAlt;

    /**********************************************
     *  OBJETOS CARTERAS IOS
     **********************************************/

    /**
     * Gestión ahorro corto plazo
     */
    //Nombre cartera
    @iOSFindBy(accessibility = "CarteraDestacadaTableViewCell.Gestión Ahorro Corto Plazo.CarteraNameLabel")
    private MobileElement nombreCartera1;

    /**
     * Gestión global dinámica ahorro
     */
    //Nombre cartera
    @iOSFindBy(accessibility = "CarteraDestacadaTableViewCell.Gestión Global Dinámica Ahorro.CarteraNameLabel")
    private MobileElement nombreCartera2;

    /**
     * Gestión global dinámica 20
     */
    //Nombre
    @iOSFindBy(accessibility = "CarteraDestacadaTableViewCell.Gestión Global Dinámica 20.CarteraNameLabel")
    private MobileElement nombreCartera3;


    /**
     * Gestión global dinámica 50
     */
    //Nombre
    @iOSFindBy(accessibility = "CarteraDestacadaTableViewCell.Gestión Global Dinámica 50.CarteraNameLabel")
    private MobileElement nombreCartera4;

    /**
     * Gestión global dinámica 80 Agresivo
     */
    //Nombre
    @iOSFindBy(xpath = "(//XCUIElementTypeStaticText[@name=\"CarteraDestacadaTableViewCell.Gestión Global Dinámica 80.CarteraNameLabel\"])[1]")
    private MobileElement nombreCartera5;

    /**
     * Gestión global dinámica 80 Muy Agresivo
     */
    //Nombre
    @iOSFindBy(xpath = "(//XCUIElementTypeStaticText[@name=\"CarteraDestacadaTableViewCell.Gestión Global Dinámica 80.CarteraNameLabel\"])[1]")
    private MobileElement nombreCartera6;

    /**********************************************
     *  OBJETOS CARTERAS ANDROID
     **********************************************/

    //Nombre
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_nombre_cartera_bci_value")
    private List<MobileElement> nombresCarteras;

    //Perfil
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_perfil")
    private List<MobileElement> perfilesCarteras;

    //Título
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/dashboard_toolbar_logo")
    @iOSFindBy(accessibility = "Fondos Mutuos")
    private MobileElement logoBCIInicio;


    /******************************************************************************************************
     *  MÉTODOS
     ******************************************************************************************************/

    @Step
    public void validarIngresoPantalla() {
        AccionesGenericas.ingresoPantalla(this.tituloFFMM, "Pantalla Carteras Autogestionadas");
    }

    @Step
    public void validarElementos(String flujo) {
        AccionesGenericas.validarElemento(this.tituloFFMM, "Fondos Mutuos", 5);
        if (flujo.equals("Campania")) {
//            AccionesGenericas.validarElemento(this.btnCerrar, 5);
        }
        if (flujo.equals("Dashboard")) {
            utils.MetodosGenericos.visualizarObjeto(this.flechaAtras, 5);
        }
        AccionesGenericas.validarElemento(this.tituloInvierteCarteras, "Invierte en Carteras Autogestionadas", 5);
        AccionesGenericas.validarElemento(this.infoCarteras, "Son estrategias dinámicas de inversión que se van adaptando al mercado. Son gestionadas por especialistas, para que inviertas a través de fondos mutuos y en activos en Chile y el extranjero, según tu perfil de riesgo y horizonte de inversión.", 5);
        AccionesGenericas.validarElemento(this.descripcion1, "Son gestionadas por un equipo especializado con amplia trayectoria.", 5);
        AccionesGenericas.validarElemento(this.tituloPuedesConocer, 5);
    }

    @Step
    public void validarCarteras() {
        if (DriverContext.getDriver() instanceof IOSDriver) {
            validarCarterasIOS(this.nombreCartera1);
            validarCarterasIOS(this.nombreCartera2);
            validarCarterasIOS(this.nombreCartera3);
            validarCarterasIOS(this.nombreCartera4);
            validarCarterasIOS(this.nombreCartera5);
            validarCarterasIOS(this.nombreCartera6);
        } else {
            validarCarterasAndroid();
        }
    }


    @Step
    public void seleccionarCartera(String cartera) {
        Gestures ges = new Gestures();
        if (DriverContext.getDriver() instanceof IOSDriver) {
            switch (cartera) {
                case "Gestión Ahorro Corto Plazo":
                    seleccionarCarteraIOS(this.nombreCartera1);
                    break;
                case "Gestión Global Dinámica Ahorro":
                    seleccionarCarteraIOS(this.nombreCartera2);
                    break;
                case "Gestión Global Dinámica 20":
                    seleccionarCarteraIOS(this.nombreCartera3);
                    break;
                case "Gestión Global Dinámica 50":
                    seleccionarCarteraIOS(this.nombreCartera4);
                    break;
                case "Gestión Global Dinámica 80 Agresivo":
                    seleccionarCarteraIOS(this.nombreCartera5);
                    break;
                case "Gestión Global Dinámica 80 Muy Agresivo":
                    seleccionarCarteraIOS(this.nombreCartera6);
                    break;
            }
        } else {
            boolean salir = false;
            ges.swipe(0.3, 0.9);
            ges.swipe(0.3, 0.9);

            for (int i = 0; i < this.nombresCarteras.size(); i++) {
                if (this.nombresCarteras.get(i).getText().equals(cartera)) {
                    this.nombresCarteras.get(i).click();
                    PdfBciReports.addReport("Seleccionar cartera", "Se selecciona la cartera " + cartera, EstadoPrueba.PASSED, false);
                    break;
                }
                if (this.nombresCarteras.size() == i + 1) {
                    ges.swipe(0.9, 0.3);
                    ges.swipe(0.9, 0.3);
                    for (i = 0; i < nombresCarteras.size(); i++) {
                        if (this.nombresCarteras.get(i).getText().equals(cartera)) {
                            this.nombresCarteras.get(i).click();
                            PdfBciReports.addReport("Seleccionar cartera", "Se selecciona la cartera " + cartera, EstadoPrueba.PASSED, false);
                            salir = true;
                            break;
                        }
                    }
                }
                if (salir) {
                    break;
                }
            }
        }
    }

    @Step
    public void cerrarCarteras() {
        this.btnCerrar.click();
        PdfBciReports.addReport("Cerrar Carteras", "Se hace tap al botón cerrar", EstadoPrueba.PASSED, false);
        boolean existe = AccionesGenericas.existeElemento(this.logoBCIInicio, 20);
        if (existe) {
            PdfBciReports.addMobilReportImage("Cerrar Carteras", "Se redirige a la sección de cuentas", EstadoPrueba.PASSED, false);
        } else {
            PdfBciReports.addMobilReportImage("Cerrar Carteras", "No se redirige a la sección de cuentas", EstadoPrueba.FAILED, false);
        }
    }


    private void validarCarterasAndroid() {
        String[] arrayCarteras = new String[6];
        arrayCarteras[0] = "Gestión Ahorro Corto Plazo";
        arrayCarteras[1] = "Gestión Global Dinámica Ahorro";
        arrayCarteras[2] = "Gestión Global Dinámica 20";
        arrayCarteras[3] = "Gestión Global Dinámica 50";
        arrayCarteras[4] = "Gestión Global Dinámica 80";
        arrayCarteras[5] = "Gestión Global Dinámica 80";

        Gestures gestures = new Gestures();
        gestures.swipe(0.5, 0.4);

        int ciclo = 1;
        while (ciclo < 3) {
            for (int x = 0; x < nombresCarteras.size(); x++) {
                loop:
                for (int i = 0; i <= 5; i++) {
                    if (nombresCarteras.get(x).getText().equals("Gestión Global Dinámica 80")) {
                        if (perfilesCarteras.get(x).getText().equals("Agresivo")) {
                            PdfBciReports.addMobilReportImage("Validar Cartera", "Se valida corretamente la cartera: " + arrayCarteras[4] + " con perfil: " + perfilesCarteras.get(x).getText(), EstadoPrueba.PASSED, false);
                            arrayCarteras[4] = "0";
                            break loop;
                        } else if (perfilesCarteras.get(x).getText().equals("Muy Agresivo")) {
                            PdfBciReports.addMobilReportImage("Validar Cartera", "Se valida corretamente la cartera" + arrayCarteras[5] + " con perfil: " + perfilesCarteras.get(x).getText(), EstadoPrueba.PASSED, false);
                            arrayCarteras[5] = "0";
                            break loop;
                        }
                    } else {
                        if (nombresCarteras.get(x).getText().equals(arrayCarteras[i])) {
                            PdfBciReports.addMobilReportImage("Validar Cartera", "Se valida corretamente la cartera" + arrayCarteras[i] + " con perfil: " + perfilesCarteras.get(x).getText(), EstadoPrueba.PASSED, false);
                            arrayCarteras[i] = "0";
                            break loop;
                        }
                    }
                }
            }
            ciclo = ciclo + 1;
            gestures.swipe(0.9, 0.1);
            gestures.swipe(0.9, 0.1);
            gestures.swipe(0.45, 0.5);
        }

        for (int i = 0; i < arrayCarteras.length; i++) {
            if (!arrayCarteras[i].equals("0")) {
                PdfBciReports.addReport("Cartera no encontrada", "No se ha encontrado la cartera: " + arrayCarteras[i], EstadoPrueba.FAILED, false);
            }
        }
    }

    private void validarCarterasIOS(MobileElement cartera) {
        Gestures ges = new Gestures();
        boolean existe = false;
        int intento = 0;
        do {
            intento++;
            ges.scrollAbajoCorto();
            existe = utils.MetodosGenericos.visualizarObjeto(cartera, 2);
            if (intento == 5) {
                PdfBciReports.addReport("Validar carteras", "No se encuentra la cartera" + cartera.getText(), EstadoPrueba.FAILED, false);
            }
        } while (existe == false);
        PdfBciReports.addReport("Validar carteras", "Se encuentra la cartera" + cartera.getText(), EstadoPrueba.PASSED, false);
        ges.scrollArriba();
        ges.scrollArriba();
    }

    private void seleccionarCarteraIOS(MobileElement cartera) {
        Gestures ges = new Gestures();
        boolean existe = false;
        int intento = 0;
        do {
            intento++;
            ges.scrollAbajoCorto();
            existe = utils.MetodosGenericos.visualizarObjeto(cartera, 2);
            if (intento == 5) {
                PdfBciReports.addReport("Seleccionar Cartera", "No se encuentra la cartera" + cartera.getText(), EstadoPrueba.FAILED, false);
            }
        } while (!existe);
        PdfBciReports.addReport("Seleccionar Cartera", "Se selecciona la cartera: " + cartera.getText(), EstadoPrueba.PASSED, false);
        cartera.click();
    }

}

