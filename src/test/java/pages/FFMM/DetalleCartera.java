package pages.FFMM;

import constants.Constants;
import driver.DriverContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.qameta.allure.Step;
import org.openqa.selenium.support.PageFactory;
import reporter.EstadoPrueba;
import reporter.PdfBciReports;
import util.AccionesGenericas;

public class DetalleCartera {
    private AppiumDriver driver;

    public DetalleCartera() {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver), this);
    }

    /*********************************************************************
     * OBJETOS
     **********************************************************************/

    //Título carteras autogestionadas
    @iOSFindBy(accessibility = "Cartera Autogestionada")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/toolbar_title")
    private MobileElement tituloCarteraAutogestionada;

    //Flecha atrás
    @iOSFindBy(accessibility = "Button")
    @AndroidFindBy(xpath = "//android.widget.LinearLayout[@index='1']/android.view.ViewGroup[@index='1']/android.widget.ImageButton[@index='0']")
    private MobileElement flechaAtras;

    //Botón cerrar
    @iOSFindBy(accessibility = "close")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/action_close")
    private MobileElement btnCerrar;

    //Tu perfil de inversionista
    @iOSFindBy(accessibility = "Tu perfil de inversionista:")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_perfil_inversionista")
    private MobileElement tuPerfilDeInv;

    //Perfil
    @iOSFindBy(xpath = "//XCUIElementTypeStaticText[contains(@name, \"PerfilClienteView.\")]")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_perfil")
    private MobileElement perfil;

    //Título FFMM
    @iOSFindBy(accessibility = "FondoMutuo.FondoMutuoTitleLbale")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_ffmm_nombre_label")
    private MobileElement tituloFFMM;

    //Subtítulo FFMM
    @iOSFindBy(accessibility = "DetalleFondosMutuosViewController.TypeLabel")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_ffmm_nombre_value")
    private MobileElement subtituloFFMM;

    //Descripción FFMM
    @iOSFindBy(accessibility = "DetalleFondosMutuosViewController.DescriptionValue")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_ffmm_descripcion")
    private MobileElement descripcionFFMM;

    //Perfil de riesgo
    @iOSFindBy(accessibility = "DetalleFondosMutuosViewController.FondoPerfilRiesgoValue")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_ffmm_perfil")
    private MobileElement perfilRiesgo;

    //Horizonte de inversión
    @iOSFindBy(accessibility = "DetalleFondosMutuosViewController.HorozonteValue")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_ffmm_horizonte")
    private MobileElement horizonteInversion;

    //Serie
    @iOSFindBy(accessibility = "DetalleFondosMutuosViewController.SerieValue")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_ffmm_serie")
    private MobileElement serie;

    //Monto mínimo de inversión
    @iOSFindBy(accessibility = "DetalleFondosMutuosViewController.InversionMinimaValue")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_ffmm_monto_minimo")
    private MobileElement montoMinInversion;


    //Título
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/dashboard_toolbar_logo")
    @iOSFindBy(accessibility = "Fondos Mutuos")
    private MobileElement logoBCIInicio;


    /*********************************************************************
     *METODOS
     **********************************************************************/

    @Step
    public void validarIngresoPantalla() {
        AccionesGenericas.ingresoPantalla(this.tituloCarteraAutogestionada, "Cartera Autogestionada");
    }

    @Step
    public void validarElementosPerfil() {
        AccionesGenericas.validarElemento(this.tuPerfilDeInv, "Tu perfil de inversionista:", 5);
        AccionesGenericas.validarElemento(this.perfil, 5);
    }

    @Step
    public void validarElementosCartera() {
        AccionesGenericas.validarElemento(this.tituloFFMM, 5);
        AccionesGenericas.validarElemento(this.subtituloFFMM, 5);
        AccionesGenericas.validarElemento(this.descripcionFFMM, 5);
        AccionesGenericas.validarElemento(this.perfilRiesgo, 5);
        AccionesGenericas.validarElemento(this.horizonteInversion, 5);
        AccionesGenericas.validarElemento(this.serie, 5);
        AccionesGenericas.validarElemento(this.montoMinInversion, 5);
    }

    @Step
    public void tapFlechaAtras() {
        this.flechaAtras.click();
        PdfBciReports.addReport("Tap flecha atrás", "Se hace tap a flecha volver atrás", EstadoPrueba.PASSED, false);
    }

    @Step
    public void cerrarDetalleFFFMM() {
        this.btnCerrar.click();
        PdfBciReports.addReport("Cerra Detalle FFMM", "Se hace tap a botón cerrar", EstadoPrueba.PASSED, false);
        boolean existe = AccionesGenericas.existeElemento(this.logoBCIInicio, 20);
        if (existe) {
            PdfBciReports.addMobilReportImage("Cerrar Detalle FFMM", "Se redirige a la sección de Cuentas", EstadoPrueba.PASSED, false);
        } else {
            PdfBciReports.addMobilReportImage("Cerrar Detalle FFMM", "No se redirige a la sección de Cuentas", EstadoPrueba.FAILED, false);
        }
    }
}
