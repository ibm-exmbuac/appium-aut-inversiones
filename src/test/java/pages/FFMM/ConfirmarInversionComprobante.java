package pages.FFMM;

import constants.Constants;
import driver.DriverContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.qameta.allure.Step;
import org.openqa.selenium.support.PageFactory;
import reporter.EstadoPrueba;
import reporter.PdfBciReports;
import util.AccionesGenericas;
import util.Gestures;

public class ConfirmarInversionComprobante {

    private AppiumDriver driver;

    public ConfirmarInversionComprobante() {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver), this);
    }

    //Título te enviaremos el comprobate a
    @iOSFindBy(accessibility = "Te enviaremos el comprobante a:")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_envio_comprobante")
    private MobileElement tituloTeEnviaremosComprobante;

    //Input correo
    @iOSFindBy(accessibility = "ConfirmacionDatosFFMMPresenter.EmailInput")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/email_input")
    private MobileElement inputCorreo;

    //Label detalle correo
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_validation_label")
    private MobileElement infoCorreo;


    @Step
    public void validarElementos() {
        Gestures gestures = new Gestures();
        gestures.encontrarObjeto(inputCorreo, "Sección Envío Comprobante");
        AccionesGenericas.validarElemento(this.tituloTeEnviaremosComprobante, "Te enviaremos el comprobante a:", 5);
        AccionesGenericas.validarElemento(this.inputCorreo, 5);
    }

    @Step
    public void ingresarCorreo(String correo) {
        if (this.inputCorreo.getText().isEmpty()) {
            this.inputCorreo.setValue(correo);
            PdfBciReports.addMobilReportImage("Ingresar correo", "Se ingresa correo: " + correo, EstadoPrueba.PASSED, false);
        } else {
            PdfBciReports.addMobilReportImage("Ingresar correo", "Correo precargado por defecto", EstadoPrueba.PASSED, false);
        }
    }
}
