package pages.FFMM;

import constants.Constants;
import driver.DriverContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.qameta.allure.Step;
import org.openqa.selenium.support.PageFactory;
import reporter.EstadoPrueba;
import reporter.PdfBciReports;
import util.AccionesGenericas;
import util.Gestures;

public class DetalleCarteraValorCuota {
    private AppiumDriver driver;

    public DetalleCarteraValorCuota() {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver), this);
    }

    /*********************************************************************
     * OBJETOS
     **********************************************************************/

    //Label valor cuota
    @iOSFindBy(accessibility = "DetalleFondosMutuosPresenter.ValorCuotaButtonTitle")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_ffmm_rentabilidad_valor_cuota_label")
    private MobileElement labelValorCuota;

    //Value valor cuota
    @iOSFindBy(accessibility = "DetalleFondosMutuosPresenter.ValorCuotaValue")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_ffmm_rentabilidad_valor_cuota_value")
    private MobileElement valueValorCuota;

    /************************************************
     * OBJETOS MODAL INFO
     ************************************************/

    //Título modal
    @iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"DetalleFondosMutuosViewController.BottomMessage.TitleLab\"]")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_title")
    private MobileElement tituloModal;

    //Info modal
    @iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"DetalleFondosMutuosViewController.BottomMessage.MessageLabel\"]")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_description")
    private MobileElement infoModal;

    //Botón entendido
    @iOSFindBy(xpath = "(//XCUIElementTypeButton[@name=\"DetalleFondosMutuosViewController.BottomMessage.FirstButton\"])[2]")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/cb_close")
    private MobileElement btnEntendido;


    /*********************************************************************
     *METODOS
     **********************************************************************/

    @Step
    public void validarElementos() {
        Gestures gestures = new Gestures();
        gestures.encontrarObjeto(labelValorCuota, "Sección ValorCuota");
        PdfBciReports.addMobilReportImage("Valor Cuota", "Se visualiza sección Valor Cuota", EstadoPrueba.PASSED, false);
        AccionesGenericas.validarElemento(this.labelValorCuota, "Valor Cuota", 5);
        AccionesGenericas.validarElemento(this.valueValorCuota, 5);
    }

    @Step
    public void validarValorCuota() throws InterruptedException {
        this.labelValorCuota.click();
        PdfBciReports.addMobilReportImage("Modal Valor Cuota", "Se visualiza Modal Valor cuota", EstadoPrueba.PASSED, false);
        AccionesGenericas.validarElemento(this.tituloModal, "Valor Cuota", 5);
        AccionesGenericas.validarElemento(this.infoModal, "Es el precio por cuota de un fondo, cuyo valor puede variar conforme a las ganancias o pérdidas de las inversiones del fondo y es informado diariamente.", 5);
        AccionesGenericas.validarElemento(this.btnEntendido, "ENTENDIDO", 5);
        this.btnEntendido.click();
        AccionesGenericas.esperar(2);
    }

}
