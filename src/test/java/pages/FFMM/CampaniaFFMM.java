package pages.FFMM;

import driver.DriverContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.qameta.allure.Step;
import org.openqa.selenium.support.PageFactory;
import reporter.EstadoPrueba;
import reporter.PdfBciReports;
import util.AccionesGenericas;
import util.Gestures;

public class CampaniaFFMM {

    private AppiumDriver driver;

    public CampaniaFFMM() {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver), this);
    }

    /******************************************************************************************************
     *  OBJETOS
     ******************************************************************************************************/

    //Texto campaña
    @iOSFindBy(accessibility = "Invierte según tu estilo")
    @AndroidFindBy(xpath = "//android.view.View[@index='0']/android.view.View[@index='2']/android.view.View[@index='1']")
    private MobileElement textoCampania;

    //Botón simular campaña
    @iOSFindBy(accessibility = "Simular prueba")
    @AndroidFindBy(xpath = "//android.view.View[@index='0']/android.view.View[@index='2']/android.view.View[@index='0']")
    private MobileElement btnCampania;


    @Step
    public void validarDespliegueCampania() {
        Gestures ges = new Gestures();
        boolean existe = AccionesGenericas.existeElemento(this.textoCampania, 60);
        ges.scrollAbajoCorto();
        if (existe && this.btnCampania.getText().equals("Simular prueba")) {
            PdfBciReports.addMobilReportImage("Validar despliegue campaña FFMM", "Se despliega campaña FFMM", EstadoPrueba.PASSED, false);
        } else {
            PdfBciReports.addMobilReportImage("Validar despliega campaña FFMM", "No se despliega la campaña FFMM", EstadoPrueba.FAILED, false);
        }
    }

    @Step
    public void tapBtnSimular() {
        this.btnCampania.click();
        PdfBciReports.addMobilReportImage("Tap boton simular", "Se hace tap al botón simular de la campaña", EstadoPrueba.PASSED, false);
    }
}
