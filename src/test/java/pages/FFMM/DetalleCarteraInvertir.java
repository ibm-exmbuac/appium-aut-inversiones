package pages.FFMM;

import constants.Constants;
import driver.DriverContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.qameta.allure.Step;
import org.openqa.selenium.support.PageFactory;
import reporter.EstadoPrueba;
import reporter.PdfBciReports;
import util.AccionesGenericas;
import util.Gestures;

public class DetalleCarteraInvertir {

    private AppiumDriver driver;

    public DetalleCarteraInvertir() {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver), this);
    }

    //Info disclaimer
    @iOSFindBy(accessibility = "TextViewTableViewCell.descriptionTextView")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_detalle_ffmm_disclaimer")
    private MobileElement infoDisclaimer;

    //Botón ir a invertir
    @iOSFindBy(accessibility = "IR A INVERTIR")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/btn_detalle_ffmm_invertir")
    private MobileElement btnIrAInvertir;

    /*********************************************************************
     *      FALTA OBJETOS MODAL RIEGO SUPERIOR
     **********************************************************************/


    @Step
    public void validarElementos() {
        Gestures ges = new Gestures();
        ges.encontrarObjeto(btnIrAInvertir, "Sección inferior detalle cartera");
        PdfBciReports.addMobilReportImage("Sección inferior", "Se visualiza sección inferor con info y botón invertir", EstadoPrueba.PASSED, false);
        AccionesGenericas.validarElemento(this.infoDisclaimer, 10);
        AccionesGenericas.validarElemento(this.btnIrAInvertir, "IR A INVERTIR", 5);
    }

    @Step
    public void tapIrAInvertir() {
        this.btnIrAInvertir.click();
        PdfBciReports.addReport("Tap Ir A Invertir", "Se hace tap al botón invertir", EstadoPrueba.PASSED, false);
    }

}
