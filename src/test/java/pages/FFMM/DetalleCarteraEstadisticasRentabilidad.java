package pages.FFMM;

import constants.Constants;
import driver.DriverContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.qameta.allure.Step;
import org.openqa.selenium.support.PageFactory;
import reporter.EstadoPrueba;
import reporter.PdfBciReports;
import util.AccionesGenericas;
import util.Gestures;


public class DetalleCarteraEstadisticasRentabilidad {

    private AppiumDriver driver;

    public DetalleCarteraEstadisticasRentabilidad() {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver), this);
    }

    /*********************************************************************
     * OBJETOS
     **********************************************************************/

    //Título estadísca de rentabilidad nominal
    @iOSFindBy(accessibility = "Estadística de Rentabilidad Nominal")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_ffmm_rentabilidad_nominal_label")
    private MobileElement estadisticaRentabilidadNominal;

    //Label mensual
    @iOSFindBy(accessibility = "DetalleFondosMutuosPresenter.RentabilidadMensualTitle")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_ffmm_rentabilidad_mensual_label")
    private MobileElement labelMensual;

    //Value mensual
    @iOSFindBy(accessibility = "DetalleFondosMutuosPresenter.RentabilidadMensualValue")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_ffmm_rentabilidad_mensual_value")
    private MobileElement valueMensual;

    //Label trimestral
    @iOSFindBy(accessibility = "DetalleFondosMutuosPresenter.RentabilidadTrimestralTitle")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_ffmm_rentabilidad_trimestral_label")
    private MobileElement labelTrimestral;

    //Value trimestral
    @iOSFindBy(accessibility = "DetalleFondosMutuosPresenter.RentabilidadTrimestralValue")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_ffmm_rentabilidad_trimestral_value")
    private MobileElement valueTrimestral;

    //Label retabilidad anual
    @iOSFindBy(accessibility = "DetalleFondosMutuosPresenter.RentabilidadAnualTitle")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_ffmm_rentabilidad_anual_label")
    private MobileElement labelRentabilidadAnual;

    //Value rentabilidad anual
    @iOSFindBy(accessibility = "DetalleFondosMutuosPresenter.RentabilidadAnualValue")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_ffmm_rentabilidad_anual_value")
    private MobileElement valueRentabilidadAnual;

    //Label acumulado en el anio
    @iOSFindBy(accessibility = "DetalleFondosMutuosPresenter.AcumuladoAnualButtonTitle")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_ffmm_rentabilidad_acumulado_anio_label")
    private MobileElement labelAcumuladoAnio;

    //Value acumulado en el anio
    @iOSFindBy(accessibility = "DetalleFondosMutuosPresenter.AcumuladoAnualValue")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_ffmm_rentabilidad_acumulado_anio_value")
    private MobileElement valueAcumuadoAnio;

    /************************************************
     * OBJETOS MODAL INFO
     ************************************************/

    //Título modal
    @iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"DetalleFondosMutuosViewController.BottomMessage.TitleLab\"]")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_title")
    private MobileElement tituloModal;

    //Info modal
    @iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"DetalleFondosMutuosViewController.BottomMessage.MessageLabel\"]")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_description")
    private MobileElement infoModal;

    //Botón entendido
    @iOSFindBy(xpath = "(//XCUIElementTypeButton[@name=\"DetalleFondosMutuosViewController.BottomMessage.FirstButton\"])[2]")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/cb_close")
    private MobileElement btnEntendido;


    /*********************************************************************
     *METODOS
     **********************************************************************/

    @Step
    public void validarElementos() {
        Gestures gestures = new Gestures();
        gestures.encontrarObjeto(labelAcumuladoAnio, "Sección Estadística Rentabilidad Nominal");
        PdfBciReports.addMobilReportImage("Estadistica Rentabilidad", "Se visuliza sección Estadistica Rentabilidad", EstadoPrueba.PASSED, false);
        AccionesGenericas.validarElemento(this.estadisticaRentabilidadNominal, "Estadística de Rentabilidad Nominal", 5);
        AccionesGenericas.validarElemento(this.labelMensual, "Rentabilidad Mensual", 5);
        AccionesGenericas.validarElemento(this.valueMensual, 5);
        AccionesGenericas.validarElemento(this.labelTrimestral, "Rentabilidad Trimestral", 5);
        AccionesGenericas.validarElemento(this.valueTrimestral, 5);
        AccionesGenericas.validarElemento(this.labelRentabilidadAnual, "Rentabilidad Anual", 5);
        AccionesGenericas.validarElemento(this.valueRentabilidadAnual, 5);
        AccionesGenericas.validarElemento(this.labelAcumuladoAnio, "Acumulado en el Año (YTD)", 5);
        AccionesGenericas.validarElemento(this.valueAcumuadoAnio, 5);
    }

    @Step
    public void validarRentabilidadAnual() {
        this.labelRentabilidadAnual.click();
        PdfBciReports.addMobilReportImage("Modal Rentabilidad Anual", "Se visualiza modal de Rentabilidad anual", EstadoPrueba.PASSED, false);
        AccionesGenericas.validarElemento(this.tituloModal, "Rentabilidad Anual", 5);
        AccionesGenericas.validarElemento(this.infoModal, "Corresponde a la variación porcentual del valor cuota entre el 1 de enero y 31 de diciembre del año pasado.", 5);
        AccionesGenericas.validarElemento(this.btnEntendido, "ENTENDIDO", 5);
        this.btnEntendido.click();
    }

    @Step
    public void validarAcumuladoAnio() throws InterruptedException {
        this.labelAcumuladoAnio.click();
        PdfBciReports.addMobilReportImage("Modal Acumlado Año", "Se visualiza modal Acumulado Año", EstadoPrueba.PASSED, false);
        AccionesGenericas.validarElemento(this.tituloModal, "Acumulado en el Año (YTD)", 5);
        AccionesGenericas.validarElemento(this.infoModal, "Corresponde a la variación porcentual del valor cuota entre el 1 de enero y el día de ayer.", 5);
        AccionesGenericas.validarElemento(this.btnEntendido, "ENTENDIDO", 5);
        this.btnEntendido.click();
        AccionesGenericas.esperar(3);
    }
}

