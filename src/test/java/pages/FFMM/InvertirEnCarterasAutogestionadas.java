package pages.FFMM;

import constants.Constants;
import driver.DriverContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.qameta.allure.Step;
import org.openqa.selenium.support.PageFactory;
import reporter.EstadoPrueba;
import reporter.PdfBciReports;
import util.AccionesGenericas;

public class InvertirEnCarterasAutogestionadas {

    private AppiumDriver driver;

    public InvertirEnCarterasAutogestionadas() {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver), this);
    }

    /*********************************************************************
     * OBJETOS
     **********************************************************************/

    //Título invertir en carteras autogestionadas
    @iOSFindBy(accessibility = "IngresoDatosFFMMViewController.IngresoDatosFFMMTitleLabel")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/toolbar_title")
    private MobileElement tituloInvCarteras;

    //Subtítulo nombre FFMM
    @iOSFindBy(accessibility = "IngresoDatosFFMMViewController.NombreFFMMLabel")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/toolbar_subtitle")
    private MobileElement subtituloNombreFFMM;

    //Flecha atrás
    @iOSFindBy(accessibility = "left arrow")
    @AndroidFindBy(xpath = "//android.widget.LinearLayout[@index='0']/android.view.ViewGroup[@index='1']/android.widget.ImageButton[@index='0']")
    private MobileElement flechaAtras;

    //Boton cerrar
    @iOSFindBy(accessibility = "close")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/action_close")
    private MobileElement cerrar;

    //Título cuánto quieres invertir
    @iOSFindBy(accessibility = "IngresoDatosFFMMViewController.TitleLabel")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/ingrese_monto_label")
    private MobileElement tituloCuantoInv;

    //Input monto
    @iOSFindBy(accessibility = "IngresoDatosFFMMViewController.MontoLabel")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/monto_input")
    private MobileElement inputMonto;

    //Info monto
    @iOSFindBy(accessibility = "IngresoDatosFFMMViewController.ErrorLabel")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/monto_label")
    private MobileElement infoMonto;

    //Titulo selecciona una cuenta de cargo
    @iOSFindBy(accessibility = "Selecciona una cuenta de cargo")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_cuenta_origen_label")
    private MobileElement tituloCuentaCargo;

    //Cargo a:
    @iOSFindBy(accessibility = "Cargo a:")
    @AndroidFindBy(xpath = "//android.view.ViewGroup[@index='0']/android.widget.LinearLayout[@index='6']/android.widget.TextView[@index='0']")
    private MobileElement cargoA;

    //Value cuenta cargo
    @iOSFindBy(accessibility = "IngresoDatosFFMMPresenter.CuentaCobroLabel")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_cuenta_origen")
    private MobileElement valueCuentaCargo;

    //Botón cambiar cuenta cargo
    @iOSFindBy(accessibility = "IngresoDatosFFMMPresenter.CuentaCobroLabel")
    //@AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID+"")
    private MobileElement btnCambiarCtaCargo;

    //Título selecciona una cuenta para ingresar tu inversión
    @iOSFindBy(accessibility = "Selecciona una cuenta para ingresar tu inversión")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_cuenta_abono_label")
    private MobileElement tituloCuentaAbono;

    //Ingresar en:
    @iOSFindBy(accessibility = "Ingresar en:")
    @AndroidFindBy(xpath = "//android.view.ViewGroup[@index='0']/android.widget.LinearLayout[@index='9']/android.widget.TextView[@index='0']")
    private MobileElement ingresarEn;

    //Value cuenta abono
    @iOSFindBy(accessibility = "IngresoDatosFFMMPresenter.CuentaInversionLabel")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_cuenta_abono")
    private MobileElement valueCuentaAbono;

    //Botón cambiar cuenta abono
    @iOSFindBy(accessibility = "IngresoDatosFFMMPresenter.CambiarCuentaAbonoButton")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/abono_change")
    private MobileElement btnCambiarCtaAbono;

    //Botón continuar
    @iOSFindBy(accessibility = "IngresoDatosFFMMViewController.ContinuarButton")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/btn_invertir_ffmm_continuar")
    private MobileElement btnContinuar;


    /*********************************************************************
     *METODOS
     **********************************************************************/

    @Step
    public void validarIngresoPantalla() {
        AccionesGenericas.ingresoPantalla(this.tituloInvCarteras, "Invertir en Carteras Autogestionadas");
    }

    @Step
    public void validarElementos() {
        AccionesGenericas.validarElemento(this.tituloInvCarteras, "Invertir en Cartera Autogestionada", 5);
        AccionesGenericas.validarElemento(this.subtituloNombreFFMM, 5);
        AccionesGenericas.validarElemento(this.flechaAtras, 5);
        AccionesGenericas.validarElemento(this.cerrar, 5);
        AccionesGenericas.validarElemento(this.tituloCuantoInv, "¿Cuánto quieres invertir?", 5);
        AccionesGenericas.validarElemento(this.inputMonto, 5);
        AccionesGenericas.validarElemento(this.infoMonto, 5);
        AccionesGenericas.validarElemento(this.tituloCuentaCargo, "Selecciona una cuenta de cargo", 5);
        AccionesGenericas.validarElemento(this.cargoA, "Cargo a:", 5);
        AccionesGenericas.validarElemento(this.valueCuentaCargo, 5);
        AccionesGenericas.validarElemento(this.tituloCuentaAbono, "Selecciona una cuenta para ingresar tu inversión", 5);
        AccionesGenericas.validarElemento(this.ingresarEn, "Ingresar en:", 5);
        AccionesGenericas.validarElemento(this.valueCuentaAbono, 5);
        AccionesGenericas.validarElemento(this.btnCambiarCtaAbono, "Cambiar", 5);
        AccionesGenericas.validarElemento(this.btnContinuar, "CONTINUAR", 5);
    }

    @Step
    public void ingresarMonto(String monto) {
        this.inputMonto.setValue(monto);
        PdfBciReports.addMobilReportImage("Ingresa monto", "Se ingresa monto: " + monto, EstadoPrueba.PASSED, false);
        tituloInvCarteras.click();
    }

    @Step
    public void tapContinuar() {
        this.btnContinuar.click();
        PdfBciReports.addReport("Tap botón continuar", "Se hace tap al botón continuar", EstadoPrueba.PASSED, false);
    }

    @Step
    public void tapBotonCerrar() {
        this.cerrar.click();
        PdfBciReports.addMobilReportImage("Tap botón X Cerrar", "Se hace tap al botón 'X' para cerrar la pantalla", EstadoPrueba.PASSED, false);
    }
}
