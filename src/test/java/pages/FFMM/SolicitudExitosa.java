package pages.FFMM;

import constants.Constants;
import driver.DriverContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.qameta.allure.Step;
import org.openqa.selenium.support.PageFactory;
import reporter.EstadoPrueba;
import reporter.PdfBciReports;
import util.AccionesGenericas;
import util.Gestures;

import static driver.DriverContext.getDriver;

public class SolicitudExitosa {

    private AppiumDriver driver;

    public SolicitudExitosa() {
        this.driver = getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver), this);
    }


    /*********************************************************************
     * OBJETOS
     **********************************************************************/

    //Titulo solicitud exitosa
    @iOSFindBy(accessibility = "Solicitud exitosa")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_operacion_title")
    private MobileElement tituloSolicitudExitosa;

    //Botón cerrar
    @iOSFindBy(accessibility = "close")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/action_close")
    private MobileElement btnCerrar;

    //Check monto invertido
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/iv_check")
    private MobileElement checkMontoInvertido;

    //Label monto invertido
    @iOSFindBy(accessibility = "Monto invertido")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_monto_invertido_label")
    private MobileElement labelMontoInvertido;

    //Value monto invertido
    @iOSFindBy(accessibility = "ComprobanteFFMMViewController.MontoInvertidoLabel")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/value_monto_invertido")
    private MobileElement valueMontoInvertido;

    //Botón ver pdf
    @iOSFindBy(accessibility = "ComprobanteFFMMViewController.GuardarButton")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/btn_guardar_comprobante")
    private MobileElement btnPDF;

    //Info inversión
    @iOSFindBy(accessibility = "InfoTableViewCell.MessageLabel")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_comprobante_label")
    private MobileElement infoInversion;

    //Label FFMM
    @iOSFindBy(accessibility = "ComprobanteFFMMPresenter.TitleFondoMutuo")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_ffmm_nombre_fondo_label")
    private MobileElement labelFFMM;

    //Value FFMM
    @iOSFindBy(accessibility = "ComprobanteFFMMPresenter.ValueFondoMutuo")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_ffmm_nombre_fondo_value")
    private MobileElement valueFFMM;

    //Label cuenta de cargo
    @iOSFindBy(accessibility = "ComprobanteFFMMPresenter.TitleCuentaOrigen")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_ffmm_cuenta_origen_label")
    private MobileElement labelCuentaCargo;

    //Value cuenta de cargo
    @iOSFindBy(accessibility = "ComprobanteFFMMPresenter.ValueCuentaOrigen")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_ffmm_cuenta_origen_value")
    private MobileElement valueCuentaCargo;

    //Label cuenta de FFMM
    @iOSFindBy(accessibility = "ComprobanteFFMMPresenter.TitleCuentaInversion")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_ffmm_cuenta_inversion_label")
    private MobileElement labelCuentaFFMM;

    //Value cuenta de FFMM
    @iOSFindBy(accessibility = "ComprobanteFFMMPresenter.ValueCuentaInversion")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_ffmm_cuenta_inversion_value")
    private MobileElement valueCuentaFFMM;

    //Botón ver más / menos IOS
    @iOSFindBy(accessibility = "ActionTableViewCell.actionButton")
    private MobileElement btnVerMasMenos;

    //Botón ver más Android
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/btn_ver_mas")
    private MobileElement btnVerMas;

    //Label fecha de inversión
    @iOSFindBy(accessibility = "ComprobanteFFMMPresenter.TitleFechaInversion")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/label_fecha_inversion")
    private MobileElement labelFechaInversion;

    //Value fecha de inversión
    @iOSFindBy(accessibility = "ComprobanteFFMMPresenter.ValueFechaInversion")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/value_fecha_inversion")
    private MobileElement valueFechaInversion;

    //Label fecha de cargo
    @iOSFindBy(accessibility = "ComprobanteFFMMPresenter.TitleFechaCargo")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/label_fecha_cargo")
    private MobileElement labelFechaCargo;

    //Value fecha de cargo
    @iOSFindBy(accessibility = "ComprobanteFFMMPresenter.ValueFechaCargo")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/value_fecha_cargo")
    private MobileElement valueFechaCargo;


    //Sello BCI
    @iOSFindBy(accessibility = "sello")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/iv_sello")
    private MobileElement sello;

    //Info mail comprobante
    @iOSFindBy(accessibility = "ComprobanteFFMMPresenter.FFMMSuccessLabel")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/email_label")
    private MobileElement infoMailComprobante;

    //Botón finalizar
    @iOSFindBy(accessibility = "ButtonTableViewCell.CustomTextLabel")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/btn_finalizar")
    private MobileElement btnFinalizar;

    /*********************************************************************
     *FUERA DE HORARIO
     **********************************************************************/

    //Label número de operación
    @iOSFindBy(accessibility = "ComprobanteFFMMPresenter.TitleNumeroOperacion")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/label_numero_operacion")
    private MobileElement labelNroOperacion;

    //Value número de operación
    @iOSFindBy(accessibility = "ComprobanteFFMMPresenter.ValueNumeroOperacion")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/value_numero_operacion")
    private MobileElement valueNroOperacion;

    /*********************************************************************
     *DENTRO DE HORARIO
     **********************************************************************/

    //Label hora
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/label_hora")
    private MobileElement labelHora;

    //Value hora
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/value_hora")
    private MobileElement valueHora;

    //Label nro folio
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/label_numero_folio")
    private MobileElement labelNroFolio;

    //Value nro folio
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/value_numero_folio")
    private MobileElement valueNroFolio;


    /*********************************************************************
     *METODOS
     **********************************************************************/

    @Step
    public void validarIngresoPantalla() {
        AccionesGenericas.ingresoPantalla(this.tituloSolicitudExitosa, "Solicitud Exitosa FFMM");
    }

    @Step
    public void validarElementos(String casuistica) {
        Gestures ges = new Gestures();
        AccionesGenericas.validarElemento(this.tituloSolicitudExitosa, "Solicitud exitosa", 5);
        AccionesGenericas.validarElemento(this.btnCerrar, 5);
        AccionesGenericas.validarElemento(this.labelMontoInvertido, "Monto invertido", 5);
        AccionesGenericas.validarElemento(this.valueMontoInvertido, 5);
        AccionesGenericas.validarElemento(this.btnPDF, "VER PDF", 5);
        AccionesGenericas.validarElemento(this.infoInversion, 5);
        AccionesGenericas.validarElemento(this.labelFFMM, "Fondo Mutuo", 5);
        AccionesGenericas.validarElemento(this.valueFFMM, 5);
        AccionesGenericas.validarElemento(this.labelCuentaCargo, "Cuenta de cargo", 5);
        AccionesGenericas.validarElemento(this.valueCuentaCargo, 5);
        AccionesGenericas.validarElemento(this.labelCuentaFFMM, "Cuenta de Fondo Mutuo", 5);
        AccionesGenericas.validarElemento(this.valueCuentaFFMM, 5);

        if (DriverContext.getDriver() instanceof IOSDriver) {
            AccionesGenericas.validarElemento(this.btnVerMasMenos, "Ver más", 5);
            btnVerMasMenos.click();
        } else {
            AccionesGenericas.validarElemento(this.btnVerMas, "Ver más", 5);
            btnVerMas.click();
        }
        ges.scrollAbajo();

        AccionesGenericas.validarElemento(this.labelFechaInversion, "Fecha de inversión", 5);
        AccionesGenericas.validarElemento(this.valueFechaInversion, 5);
        AccionesGenericas.validarElemento(this.labelFechaCargo, "Fecha de cargo", 5);
        AccionesGenericas.validarElemento(this.labelFechaCargo, 5);

        if (casuistica.equals("DentroHorario")) {
            AccionesGenericas.validarElemento(this.labelHora, "Hora", 5);
            AccionesGenericas.validarElemento(this.valueHora, 5);
            AccionesGenericas.validarElemento(this.labelNroFolio, "Nº de folio", 5);
            AccionesGenericas.validarElemento(this.valueNroFolio, 5);
            PdfBciReports.addMobilReportImage("Inversión dentro de horario", "La inversión se realiza dentro del horario hábil", EstadoPrueba.PASSED, false);
        }
        if (casuistica.equals("FueraHorario")) {
            AccionesGenericas.validarElemento(this.labelNroOperacion, "Nº de operación", 5);
            AccionesGenericas.validarElemento(this.labelFechaCargo, 5);
            PdfBciReports.addMobilReportImage("Inversión fuera de horario", "La inversión se realiza fuera del horario hábil", EstadoPrueba.PASSED, false);
        }

//        AccionesGenericas.validarElemento(this.sello, 5);
        AccionesGenericas.validarElemento(this.infoMailComprobante, 5);
        AccionesGenericas.validarElemento(this.btnFinalizar, "FINALIZAR", 5);
    }

    @Step
    public void tapFinalizar() {
        this.btnFinalizar.click();
        PdfBciReports.addReport("Tap Finalizar", "Se hace tap en el botón finalizar", EstadoPrueba.PASSED, false);
    }
}
