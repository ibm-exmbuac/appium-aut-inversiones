package pages.Modales;

import constants.Constants;
import driver.DriverContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.qameta.allure.Step;
import org.openqa.selenium.support.PageFactory;
import reporter.EstadoPrueba;
import reporter.PdfBciReports;
import util.AccionesGenericas;

public class ModalNoEnrolado {

    private AppiumDriver driver;

    public ModalNoEnrolado() {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver), this);
    }

    //Botón ir a invertir
    @iOSFindBy(accessibility = "IR A INVERTIR")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/btn_detalle_ffmm_invertir")
    private MobileElement btnIrAInvertir;

    //Icono
    @iOSFindBy(xpath = "(//XCUIElementTypeImage[@name=\"DetalleFondosMutuosViewController.BottomMessage.HeaderImageView\"])[1]")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/iv_icon")
    private MobileElement icono;

    //Título "Nos faltan algunos datos"
    @iOSFindBy(xpath = "(//XCUIElementTypeStaticText[@name=\"DetalleFondosMutuosViewController.BottomMessage.TitleLab\"])[2]")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_title")
    private MobileElement tituloModal;

    //Info no enrolado
    @iOSFindBy(xpath = "(//XCUIElementTypeStaticText[@name=\"DetalleFondosMutuosViewController.BottomMessage.MessageLabel\"])[2]")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_description")
    private MobileElement infoNoEnrolado;

    //Botón más información
    @iOSFindBy(xpath = "(//XCUIElementTypeButton[@name=\"DetalleFondosMutuosViewController.BottomMessage.FirstButton\"])[2]")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/cb_action1")
    private MobileElement btnMasInformacion;

    //Botón en otro momento
    @iOSFindBy(accessibility = "DetalleFondosMutuosViewController.BottomMessage.SecondaryButton")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/cb_action2")
    private MobileElement btnEnOtroMomento;

    @Step
    public void validarDespliegueModal() {
        boolean modalRiesgoSuperior = AccionesGenericas.existeElemento(this.tituloModal, 30);
        if (modalRiesgoSuperior) {
            PdfBciReports.addMobilReportImage("Modal cliente no enrolado", "Modal no enrolado se despliega correctamente", EstadoPrueba.PASSED, false);
        } else {
            PdfBciReports.addMobilReportImage("Modal cliente no enrolado", "Modal no enrolado NO se despliega correctamente", EstadoPrueba.FAILED, false);
        }
    }

    @Step
    public void validarElementos() {
        if (DriverContext.getDriver() instanceof IOSDriver){
            AccionesGenericas.validarElemento(this.tituloModal, "Nos faltan algunos datos.", 5);
            AccionesGenericas.validarElemento(this.infoNoEnrolado, "Lamentablemente no te encuentras habilitado para realizar tu primera inversión. Para comenzar a invertir, inicia sesión en Bci.cl y completa tus datos en el link \"Mi primera inversión\" de la sección Inversiones. Te recomendamos realizar esta acción en la versión escritorio de tu teléfono o desde tu computador.", 5);
            AccionesGenericas.validarElemento(this.btnMasInformacion, "MÁS INFORMACIÓN", 5);
            AccionesGenericas.validarElemento(this.btnEnOtroMomento, "EN OTRO MOMENTO", 5);
        }else {
            AccionesGenericas.validarElemento(this.icono, 60);
            AccionesGenericas.validarElemento(this.tituloModal, "Nos faltan algunos datos.", 5);
            AccionesGenericas.validarElemento(this.infoNoEnrolado, "Lamentablemente no te encuentras habilitado para realizar tu primera inversión. Para comenzar a invertir, inicia sesión en Bci.cl y completa tus datos en el link \"Mi primera inversión\" de la sección Inversiones. Te recomendamos realizar esta acción en la versión escritorio de tu teléfono o desde tu computador.", 5);
            AccionesGenericas.validarElemento(this.btnMasInformacion, "MÁS INFORMACIÓN", 5);
            AccionesGenericas.validarElemento(this.btnEnOtroMomento, "ENTENDIDO", 5);
        }
    }

    @Step
    public void tapInvertir() {
        this.btnIrAInvertir.click();
        PdfBciReports.addReport("Tap botón Invertir", "Se hace tap al botón invertir", EstadoPrueba.PASSED, false);
    }

    @Step
    public void tapEnOtroMomento() {
        this.btnEnOtroMomento.click();
        PdfBciReports.addReport("Tap botón en otro momento", "Se hace al botón en otro momento", EstadoPrueba.PASSED, false);
    }
}
