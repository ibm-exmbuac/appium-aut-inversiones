package pages.Modales;

import constants.Constants;
import driver.DriverContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.qameta.allure.Step;
import org.openqa.selenium.support.PageFactory;
import reporter.EstadoPrueba;
import reporter.PdfBciReports;
import util.AccionesGenericas;

public class ModalSinSaldo {
    private AppiumDriver driver;

    public ModalSinSaldo() {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver), this);
    }

    //Botón ir a invertir
    @iOSFindBy(accessibility = "IR A INVERTIR")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/btn_detalle_ffmm_invertir")
    private MobileElement btnIrAInvertir;

    //Icono
    @iOSFindBy(xpath = "(//XCUIElementTypeImage[@name=\"DetalleFondosMutuosViewController.BottomMessage.HeaderImageView\"])[1]")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/iv_icon")
    private MobileElement icono;

    //Título modal
    @iOSFindBy(xpath = "(//XCUIElementTypeStaticText[@name=\"DetalleFondosMutuosViewController.BottomMessage.TitleLab\"])[2]")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_title")
    private MobileElement tituloModal;

    //Info saldo
    @iOSFindBy(xpath = "(//XCUIElementTypeStaticText[@name=\"DetalleFondosMutuosViewController.BottomMessage.MessageLabel\"])[2]")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_description")
    private MobileElement infoSaldo;

    //Botón entendido
    @iOSFindBy(xpath = "(//XCUIElementTypeButton[@name=\"DetalleFondosMutuosViewController.BottomMessage.FirstButton\"])[2]")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/cb_close")
    private MobileElement btnEntendido;

    @Step
    public void validarDespliegueModal() {
        boolean modalRiesgoSuperior = utils.MetodosGenericos.visualizarObjeto(this.tituloModal, 30);
        if (modalRiesgoSuperior) {
            PdfBciReports.addMobilReportImage("Modal Sin Saldo", "Modal sin saldo se despliega correctamente", EstadoPrueba.PASSED, false);
        } else {
            PdfBciReports.addMobilReportImage("Modal Sin Saldo", "Modal sin saldo NO se despliega correctamente", EstadoPrueba.FAILED, false);
        }
    }

    @Step
    public void validarElementos() {
        if (DriverContext.getDriver() instanceof IOSDriver){
            AccionesGenericas.validarElemento(this.tituloModal, "Saldo insuficiente", 5);
            AccionesGenericas.validarElemento(this.infoSaldo, "El saldo disponible en tu cuenta no es suficiente para realizar esta inversión.", 5);
            AccionesGenericas.validarElemento(this.btnEntendido, "ENTENDIDO", 5);
        }else{
            AccionesGenericas.validarElemento(this.icono, 5);
            AccionesGenericas.validarElemento(this.tituloModal, "Saldo insuficiente", 5);
            AccionesGenericas.validarElemento(this.infoSaldo, "El saldo disponible en tu cuenta no es suficiente para realizar esta inversión.", 5);
            AccionesGenericas.validarElemento(this.btnEntendido, "ENTENDIDO", 5);
        }
    }

    @Step
    public void tapInvertir() {
        this.btnIrAInvertir.click();
        PdfBciReports.addReport("Tap Invertir", "Se hace tap al botón Invertir", EstadoPrueba.PASSED, false);
    }


}
