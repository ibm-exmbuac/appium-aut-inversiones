package pages.Modales;

import constants.Constants;
import driver.DriverContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.qameta.allure.Step;
import org.openqa.selenium.support.PageFactory;
import reporter.EstadoPrueba;
import reporter.PdfBciReports;
import util.AccionesGenericas;

public class DAPFueraHorario {

    private AppiumDriver driver;

    public DAPFueraHorario() {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver), this);
    }

    //Imagen
    @iOSFindBy(accessibility = "icon-solicitud-fallida-horario")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/v_icon")
    private MobileElement imagen;

    //Título y si vuelves más tarde
    @iOSFindBy(accessibility = "FullScreenMessageError.title")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_error_title")
    private MobileElement tituloSiVuelvesMasTarde;

    //Info fuera horario
    @iOSFindBy(accessibility = "FullScreenMessageError.message")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_error_message")
    private MobileElement info;

    //Botón entendido
    @iOSFindBy(accessibility = "ENTENDIDO")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/bt_main_action")
    private MobileElement btnEntendido;

    @Step
    public void validarIngresoPantalla() {
        AccionesGenericas.ingresoPantalla(this.tituloSiVuelvesMasTarde, "Modal DAP Fuera Horario");
    }

    @Step
    public void validarElementos() {
        AccionesGenericas.validarElemento(this.imagen, 30);
        AccionesGenericas.validarElemento(this.tituloSiVuelvesMasTarde, "¿Y si vuelves más tarde?", 5);
        AccionesGenericas.validarElemento(this.info, "Intenta tomar tu Depósito a Plazo más tarde, ya que entre las 18:00 y las 19:00 horas estamos en mantención de nuestros sistemas.", 5);
        AccionesGenericas.validarElemento(this.btnEntendido, "VOLVER A INVERSIONES", 5);
    }

    @Step
    public void tapBotonEntendido() {
        btnEntendido.click();
        PdfBciReports.addReport("Tap botón Entendido", "Se hace tap al botón entendido", EstadoPrueba.PASSED, false);
    }


}
