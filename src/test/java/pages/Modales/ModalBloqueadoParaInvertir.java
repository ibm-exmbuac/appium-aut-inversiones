package pages.Modales;

import constants.Constants;
import driver.DriverContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.qameta.allure.Step;
import org.openqa.selenium.support.PageFactory;
import reporter.EstadoPrueba;
import reporter.PdfBciReports;
import util.AccionesGenericas;

public class ModalBloqueadoParaInvertir {

    private AppiumDriver driver;

    public ModalBloqueadoParaInvertir() {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver), this);
    }

    //Botón ir a invertir
    @iOSFindBy(accessibility = "IR A INVERTIR")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/btn_detalle_ffmm_invertir")
    private MobileElement btnIrAInvertir;

    //Icono
    @iOSFindBy(xpath = "(//XCUIElementTypeImage[@name=\"DetalleFondosMutuosViewController.BottomMessage.HeaderImageView\"])[1]")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/iv_icon")
    private MobileElement icono;

    //Titulo "Tenemos un problame con tu solicitud de inversión"
    @iOSFindBy(xpath = "(//XCUIElementTypeStaticText[@name=\"DetalleFondosMutuosViewController.BottomMessage.TitleLab\"])[2]")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_title")
    private MobileElement tituloModal;

    //Info bloqueado
    @iOSFindBy(xpath = "(//XCUIElementTypeStaticText[@name=\"DetalleFondosMutuosViewController.BottomMessage.MessageLabel\"])[2]")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_description")
    private MobileElement infoBloqueado;

    //Botón llamar
    @iOSFindBy(xpath = "(//XCUIElementTypeButton[@name=\"DetalleFondosMutuosViewController.BottomMessage.FirstButton\"])[2]")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/cb_action1")
    private MobileElement btnLlamar;

    //Botón cerrar
    @iOSFindBy(accessibility = "DetalleFondosMutuosViewController.BottomMessage.SecondaryButton")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/cb_action2")
    private MobileElement btnCerrar;


    @Step
    public void validarDespliegueModal() {
        boolean modalRiesgoSuperior = AccionesGenericas.existeElemento(this.tituloModal, 30);
        if (modalRiesgoSuperior) {
            PdfBciReports.addMobilReportImage("Modal Bloqueado Para Invertir", "Modal bloqueado para invertir se depliega correctamente", EstadoPrueba.PASSED, false);
        } else {
            PdfBciReports.addMobilReportImage("Modal Bloqueado Para Invertir", "Modal bloqueado para invertir NO se depliega correctamente", EstadoPrueba.FAILED, false);
        }
    }

    @Step
    public void validarElementos() {
        if (DriverContext.getDriver() instanceof IOSDriver) {
            AccionesGenericas.validarElemento(this.tituloModal, "Tenemos un problema con tu solicitud de Inversión", 5);
            AccionesGenericas.validarElemento(this.infoBloqueado, "Acércate a una sucursal o llámanos desde teléfonos fijos al 800 200 006 y desde celulares al (2) 2540 4999. Horarios de atención: Lunes a jueves de 09:00 a 18:00 hrs. y viernes de 09:00 a 15:30 hrs.", 5);
            AccionesGenericas.validarElemento(this.btnLlamar, "LLAMAR", 5);
            AccionesGenericas.validarElemento(this.btnCerrar, "CERRAR", 5);
        }else{
            AccionesGenericas.validarElemento(this.icono, 5);
            AccionesGenericas.validarElemento(this.tituloModal, "Tenemos un problema con tu solicitud de Inversión", 5);
            AccionesGenericas.validarElemento(this.infoBloqueado, "Acércate a una sucursal o llámanos desde teléfonos fijos al 800 200 006 y desde celulares al (2) 2540 4999. Horarios de atención: Lunes a jueves de 09:00 a 18:00 hrs. y viernes de 09:00 a 15:30 hrs.", 5);
            AccionesGenericas.validarElemento(this.btnLlamar, "LLAMAR", 5);
            AccionesGenericas.validarElemento(this.btnCerrar, "CERRAR", 5);
        }
    }

    @Step
    public void tapInvertir() {
        this.btnIrAInvertir.click();
        PdfBciReports.addReport("Tap invertir", "Se hace tap en el botón invertir", EstadoPrueba.PASSED, false);
    }


}
