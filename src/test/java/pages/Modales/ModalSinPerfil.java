package pages.Modales;

import constants.Constants;
import driver.DriverContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.qameta.allure.Step;
import io.qameta.allure.model.Status;
import org.openqa.selenium.support.PageFactory;
import reporter.EstadoPrueba;
import reporter.PdfBciReports;
import reporter.Reports;
import util.AccionesGenericas;

public class ModalSinPerfil {

    private AppiumDriver driver;

    public ModalSinPerfil() {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver), this);
    }

    /*********************************************************************
     * OBJETOS MODAL SIN PERFIL
     **********************************************************************/
    //Botón ir a invertir
    @iOSFindBy(accessibility = "IR A INVERTIR")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/btn_detalle_ffmm_invertir")
    private MobileElement btnIrAInvertir;

    //Título modal
    @iOSFindBy(xpath = "(//XCUIElementTypeStaticText[@name=\"DetalleFondosMutuosViewController.BottomMessage.TitleLab\"])[2]")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_title")
    private MobileElement tituloModal;

    //Info perfil
    @iOSFindBy(xpath = "(//XCUIElementTypeStaticText[@name=\"DetalleFondosMutuosViewController.BottomMessage.MessageLabel\"])[1]")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_description")
    private MobileElement infoPerfil;

    //Botón conocer mi perfil
    @iOSFindBy(xpath = "(//XCUIElementTypeButton[@name=\"DetalleFondosMutuosViewController.BottomMessage.FirstButton\"])[1]")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/cb_action1")
    private MobileElement btnConocerPerfil;

    //Botón en otro momento
    // Cambio de id nuevo:DetalleFondosMutuosViewController.BottomMessage.SecondaryButton
    // Cambio de id antiguo:DetalleFondosMutuosViewController.BottomMessage.SecondButton
    @iOSFindBy(accessibility = "DetalleFondosMutuosViewController.BottomMessage.SecondaryButton")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/cb_action2")
    private MobileElement btnEnOtroMomento;


    /*********************************************************************
     * METODOS
     **********************************************************************/

    @Step
    public void validarDespliegueModal() {
        boolean modalRiesgoSuperior = AccionesGenericas.existeElemento(this.tituloModal, 30);
        if (modalRiesgoSuperior) {
            PdfBciReports.addMobilReportImage("Modal Sin Perfil", "Modal sin perfil se despliega correctamente", EstadoPrueba.PASSED, false);
        } else {
            PdfBciReports.addMobilReportImage("Modal Sin Perfil", "Modal sin perfil NO se despliega correctamente", EstadoPrueba.FAILED, false);
        }
    }

    @Step
    public void validarElementosModal() {
        AccionesGenericas.validarElemento(this.tituloModal, "Debes conocer tu perfil de inversionista para invertir.", 5);
        AccionesGenericas.validarElemento(this.infoPerfil, "El perfil de inversionista describe cómo eres a la hora de invertir y te ubica en una de las 5 categorías: Muy Conservador, Conservador, Balanceado, Agresivo o Muy Agresivo.\n" +
                "\n" +
                "Dependiendo de tu nivel de tolerancia al riesgo, es decir, cuánto riesgo estás dispuesto a asumir en una inversión te podemos ofrecer diversas alternativas de inversión.", 5);
        AccionesGenericas.validarElemento(this.btnConocerPerfil, "CONOCER MI PERFIL", 5);
        AccionesGenericas.validarElemento(this.btnEnOtroMomento, "EN OTRO MOMENTO", 5);
    }

    @Step
    public void tapBotonInvertir() {
        PdfBciReports.addReport("Tap botón Invertir", "Se hace tap al botón invertir", EstadoPrueba.PASSED, false);
        this.btnIrAInvertir.click();
    }

    @Step
    public void tapBotonEnOtroMomento() {
        this.btnEnOtroMomento.click();
        PdfBciReports.addReport("Tap botón En Otro Momento", "Se hae tap al botón en otro momento", EstadoPrueba.PASSED, false);
    }

    @Step
    public void tapBotonConocerMiPerfil() {
        this.btnConocerPerfil.click();
        PdfBciReports.addReport("Tap botón conocer mi perfil", "Se hace tap al botón conocer mi perfil", EstadoPrueba.PASSED, false);
        Reports.addStep("Se hace tap en botón ConocerMiPerfil", false, Status.PASSED);
    }

}
