package pages.Modales;

import constants.Constants;
import driver.DriverContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.appium.java_client.touch.offset.PointOption;
import io.qameta.allure.Step;
import org.openqa.selenium.Point;
import org.openqa.selenium.support.PageFactory;
import reporter.EstadoPrueba;
import reporter.PdfBciReports;
import util.AccionesGenericas;

public class ModalRiesgoSuperior {

    private AppiumDriver driver;

    public ModalRiesgoSuperior() {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver), this);
    }

    /*********************************************************************
     * OBJETOS MODAL RIESGO SUPERIOR EN CARTERA AUTOGESTIONADA
     **********************************************************************/

    //Botón ir a invertir
    @iOSFindBy(accessibility = "IR A INVERTIR")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/btn_detalle_ffmm_invertir")
    private MobileElement btnIrAInvertir;

    //Titulo Modal
    @iOSFindBy(xpath = "(//XCUIElementTypeStaticText[@name=\"DetalleFondosMutuosViewController.BottomMessage.TitleLab\"])[2]")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_title")
    private MobileElement tituloModal;

    //Texto "Quieres invertir en un fondo de un perfil..."
    @iOSFindBy(xpath = "(//XCUIElementTypeStaticText[@name=\"DetalleFondosMutuosViewController.BottomMessage.MessageLabel\"])[2]")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_description")
    private MobileElement textoInfoPerfil;

    //Texto "Acepto la declaración"
    @iOSFindBy(accessibility = "DetalleFondosMutuosViewController.DescriptionLabel")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/cb_confirmar_ffmm_terminos_condiciones")
    private MobileElement textoDeclaracionResultado;

    //Link declaración resultado perfil de riego
    @iOSFindBy(accessibility = "Declaración Resultado Perfil de Riesgo")
    private MobileElement linkDeclaracionResultado;

    //Check declaracion resultado
    @iOSFindBy(accessibility = "checkBoxUnchecked")
    private MobileElement chkDeclaracionResultado;

    //Botón Acepto
    @iOSFindBy(xpath = "(//XCUIElementTypeButton[@name=\"DetalleFondosMutuosViewController.BottomMessage.FirstButton\"])[2]")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/cb_action1")
    private MobileElement btnAcepto;

    //Botón No Acepto
    @iOSFindBy(accessibility = "DetalleFondosMutuosViewController.BottomMessage.SecondaryButton")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/cb_action2")
    private MobileElement btnNoAcepto;

    /*********************************************************************
     * OBJETOS DE DECLARACIÓN RESULTADO PERFIL DE RIESGO
     **********************************************************************/

    //Título declaración resultado
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/toolbar_title")
    @iOSFindBy(accessibility = "Declaración Resultado Perfil de Riesgo")
    private MobileElement tituloDeclaracionResultado;

    //Botón cerrar superior
    @iOSFindBy(accessibility = "close")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/iv_icon_close")
    private MobileElement btnCerrarSuperior;

    //Contenido declaración resultado
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_message")
    @iOSFindBy(xpath = "//XCUIElementTypeApplication[@name=\"Bci-qa\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell/XCUIElementTypeTextView")
    private MobileElement infoDeclaracion;

    //Botón cerrar
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/terminos_condiciones_button")
    @iOSFindBy(accessibility = "CERRAR")
    private MobileElement btnCerrar;

    /*********************************************************************
     * METODOS
     **********************************************************************/

    @Step
    public void validarDespliegueModal() {
        boolean modalRiesgoSuperior = AccionesGenericas.existeElemento(this.tituloModal, 30);
        if (modalRiesgoSuperior) {
            PdfBciReports.addMobilReportImage("Modal Riesgo Superior", "Modal riesgo superior se despliega correctamente", EstadoPrueba.PASSED, false);
        } else {
            PdfBciReports.addMobilReportImage("Modal Riesgo Superior", "Modal riesgo superior NO se despliega correctamente", EstadoPrueba.FAILED, false);
        }
    }

    @Step
    public void validarElementosModal() {
        AccionesGenericas.validarElemento(this.tituloModal, "Este Fondo Mutuo es de un riesgo superior a tu actual perfil de inversionista", 5);
        AccionesGenericas.validarElemento(this.textoInfoPerfil, 5);
        AccionesGenericas.validarElemento(this.textoDeclaracionResultado, "Acepto la Declaración Resultado Perfil de Riesgo de la inversión y manifiesto mi conformidad de invertir en un perfil de riesgo superior.", 5);
        if (DriverContext.getDriver() instanceof IOSDriver) {
            AccionesGenericas.validarElemento(this.linkDeclaracionResultado, "Declaración Resultado Perfil de Riesgo", 5);
//            AccionesGenericas.validarElemento(this.chkDeclaracionResultado, 5);
        }
        AccionesGenericas.validarElemento(this.btnAcepto, "ACEPTO", 5);
        AccionesGenericas.validarElemento(this.btnNoAcepto, "NO ACEPTO", 5);
    }

    @Step
    public void tapTerminosYCondiciones() {
        if (DriverContext.getDriver() instanceof IOSDriver){
            linkDeclaracionResultado.click();
        }else{
            Point point = this.textoDeclaracionResultado.getLocation();
            System.out.println(point);
            int x = point.x + 500;
            int y = point.y + 13;
            TouchAction touchAction = new TouchAction(driver);
            touchAction.tap(PointOption.point(x, y)).perform();
        }
    }

    public void tapBotonInvertir() {
        this.btnIrAInvertir.click();
    }

    public void validarElementosDeclaracion() {
        AccionesGenericas.validarElemento(this.tituloDeclaracionResultado, "Declaración Resultado Perfil de Riesgo", 5);
        AccionesGenericas.validarElemento(this.btnCerrarSuperior, 5);
        AccionesGenericas.validarElemento(this.infoDeclaracion, 5);
        AccionesGenericas.validarElemento(this.btnCerrar, "CERRAR", 5);
    }

    public void tapCerrarTermYCond() {
        PdfBciReports.addReport("Cerrar TYC", "Se hace tap para cerrar Términos y condiciones", EstadoPrueba.PASSED, false);
        this.btnCerrar.click();
    }
}
