package pages.Modales;

import constants.ConstantesInversiones;
import driver.DriverContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.qameta.allure.Step;
import org.openqa.selenium.support.PageFactory;
import reporter.EstadoPrueba;
import reporter.PdfBciReports;
import util.AccionesGenericas;

public class ModalSalirEnrolamiento {

    private AppiumDriver driver;

    public ModalSalirEnrolamiento() {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver), this);
    }

    /***************************************************************************************************
     *                                          OBJETOS                                                *
     ***************************************************************************************************/

    //Icono
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID+":id/imageView")
    private MobileElement icono;

    //Título
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID+":id/tv_title")
    @iOSFindBy(xpath = "(//XCUIElementTypeStaticText[@name=\"EnrolamientoViewController.BottomMessage.TitleLab\"])[1]")
    private MobileElement titulo;

    //Info
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID+":id/tv_description")
    @iOSFindBy(xpath = "(//XCUIElementTypeStaticText[@name=\"EnrolamientoViewController.BottomMessage.MessageLabel\"])[1]")
    private MobileElement info;

    //Botón continuar
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID+":id/first_button")
    @iOSFindBy(xpath = "(//XCUIElementTypeButton[@name=\"EnrolamientoViewController.BottomMessage.FirstButton\"])[1]")
    private MobileElement btnContinuar;

    //Botón salir
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID+":id/second_button")
    @iOSFindBy(accessibility = "EnrolamientoViewController.BottomMessage.SecondaryButton")
    private MobileElement btnSalir;

    /***************************************************************************************************/

    //Título enrolamiento
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/toolbar_title")
    @iOSFindBy(accessibility = "Enrolarme para invertir")
    private MobileElement tituloEnrolamiento;


    //***************************************************************************************************
    // *                                        MÉTODOS                                                 *
    // **************************************************************************************************

    @Step
    public void validarIngresoModal() {
        AccionesGenericas.ingresoPantalla(titulo, "Modal Salir Flujo Enrolamiento");
    }

    //***************************************************************************************************

    @Step
    public void validarElementosModal() {
        if (DriverContext.getDriver() instanceof IOSDriver){
            AccionesGenericas.validarElemento(titulo, "¿Estás seguro de salir?", 5);
            AccionesGenericas.validarElemento(info, 5);
            AccionesGenericas.validarElemento(btnContinuar, "CONTINUAR", 5);
            AccionesGenericas.validarElemento(btnSalir, "SALIR", 5);
        }else{
            AccionesGenericas.validarElemento(icono, 5);
            AccionesGenericas.validarElemento(titulo, "¿Estás seguro de salir?", 5);
            AccionesGenericas.validarElemento(info, 5);
            AccionesGenericas.validarElemento(btnContinuar, "CONTINUAR", 5);
            AccionesGenericas.validarElemento(btnSalir, "SALIR", 5);
        }

    }

    //***************************************************************************************************

    @Step
    public void tapBotonContinuar(String pantalla) {
        btnContinuar.click();
        PdfBciReports.addReport("Tap botón continuar", "Se hace tap al botón continuar", EstadoPrueba.PASSED, false);
        if (AccionesGenericas.existeElemento(tituloEnrolamiento, 10)) {
            PdfBciReports.addMobilReportImage("Tap botón continuar", "Se vuelve correctamente a la pantalla de enrolamiento: " + pantalla, EstadoPrueba.PASSED, false);
        } else {
            PdfBciReports.addMobilReportImage("Tap botón continuar", "No se vuelve a la pantalla de " + pantalla + " del flujo de enrolamiento", EstadoPrueba.FAILED, false);
            PdfBciReports.closePDF();
        }
    }

    //***************************************************************************************************

    @Step
    public void tapBotonSalir() {
        btnSalir.click();
        PdfBciReports.addReport("Tap botón salir", "Se hace tap al botón salir", EstadoPrueba.PASSED, false);
    }
}
