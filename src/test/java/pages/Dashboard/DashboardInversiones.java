package pages.Dashboard;

import constants.ConstantesInversiones;
import driver.DriverContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.qameta.allure.Step;
import org.openqa.selenium.support.PageFactory;
import reporter.EstadoPrueba;
import reporter.PdfBciReports;
import util.AccionesGenericas;
import util.Gestures;

public class DashboardInversiones {

    private AppiumDriver driver;

    public DashboardInversiones() {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver), this);
    }

    /***************************************************************************************************
     *                                          OBJETOS                                                *
     ***************************************************************************************************/

    //Título que quieres hacer
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/tv_alternativa_title")
    @iOSFindBy(accessibility = "DashboardInversionesViewController.Section1TitleLabel")
    private MobileElement tituloQueQuieresHacer;

    //Botón DAP
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/tv_dashboard_dap")
    @iOSFindBy(accessibility = "OpcionInversionTableViewCell.Depósitos a PlazoLabel")
    private MobileElement btnDAP;

    //Botón FFMM
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/tv_dashboard_ffmm")
    @iOSFindBy(accessibility = "OpcionInversionTableViewCell.Fondos MutuosLabel")
    private MobileElement btnFFMM;

    /***************************************************************************************************
     * ELEMENTOS ASESORÍA */


    //Título asesoría
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/tv_asesoria_title")
    @iOSFindBy(accessibility = "Te asesoramos para invertir")
    private MobileElement tituloAsesoria;

    //Info asesoría
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/tv_asesoria_description")
    @iOSFindBy(accessibility = "¿No sabes cómo? Cuéntanos de ti y buscaremos la mejor opción de acuerdo a tu perfil y objetivo de inversión.")
    private MobileElement infoAsesesoria;

    //Botón asesoría
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/btn_asesoria")
    @iOSFindBy(accessibility = "COMENZAR")
    private MobileElement btnAsesoria;

    /***************************************************************************************************
     * ELEMENTOS CLIENTE NO PERFILADO */

    //Titulo cliente no perfilado
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/tv_no_perfilado_title")
    @iOSFindBy(accessibility = "DashboardInversionesViewController.Section0TitleLabel")
    private MobileElement tituloConoceTuPerfil;

    //Descripción perfilamiento sin perfil
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/tv_no_perfilado_description")
    @iOSFindBy(accessibility = "UserWithoutPerfilClienteTableViewCell.ConocerMiPerfilDescriptionLabel")
    private MobileElement infoPerfilamiento;

    //Botón perfilar
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/btn_perfilar")
    @iOSFindBy(accessibility = "UserWithoutPerfilClienteTableViewCell.ConocerMiPerfilButton")
    private MobileElement btnPerfilar;

    /***************************************************************************************************
     * ELEMENTOS CLIENTE PERFILADO */

    //Titulo "Tu perfil de inversionista"
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/tv_perfil_title")
    @iOSFindBy(accessibility = "DashboardInversionesViewController.Section0TitleLabel")
    private MobileElement tituloTuPerfilInv;

    //Perfil
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/tv_perfil")
    @iOSFindBy(xpath = "//XCUIElementTypeStaticText[contains(@name, \"PerfilClienteView.\")]")
    private MobileElement perfil;

    //Botón volver a perfilarme
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/tv_volver_perfilar")
    @iOSFindBy(accessibility = "Volver a perfilarme")
    private MobileElement btnVolverPerfilar;

    /***************************************************************************************************
     * ELEMENTOS ENROLAMIENTO */

    //Título enrolamiento
    @iOSFindBy(accessibility = "Invierte 100% online en Fondos Mutuos")
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID+":id/tv_enrolamient_title")
    private MobileElement tituloEnrolamiento;

    //Link enrolamiento
    @iOSFindBy(accessibility = "Completa tus datos aquí")
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID+":id/tv_enrolamient_description")
    private MobileElement linkEnrolamiento;

    //Imagen enrolamiento
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID + ":id/iv_enrolamiento")
    private MobileElement imgEnrolamiento;

    /***************************************************************************************************
     * ELEMENTOS RESUMEN INVERSIONES */

    //Titulo saldo acutal carteras
    @iOSFindBy(accessibility = "")
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID+":id/tvSummaryTitle")
    private MobileElement tituloSaldoActualCarteras;

    //Value saldo actual carteras
    @iOSFindBy(accessibility = "")
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID+":id/tvSaldoActualValue")
    private MobileElement valueSaldoActualCarteras;

    //Titulo detalle último año
    @iOSFindBy(accessibility = "")
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID+":id/tvSummaryDetalle")
    private MobileElement tituloDetalleUltimoAnio;

    //Label saldo inicial
    @iOSFindBy(accessibility = "")
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID+":id/tvSaldoInicialTitle")
    private MobileElement labelSaldoInicial;

    //Value saldo inicial
    @iOSFindBy(accessibility = "")
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID+":id/tvSaldoInicialValue")
    private MobileElement valueSaldoInicial;

    //Label aportes
    @iOSFindBy(accessibility = "")
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID+":id/tvAportesTitle")
    private MobileElement labelAportes;

    //Value aportes
    @iOSFindBy(accessibility = "")
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID+":id/tvAportesValue")
    private MobileElement valueAportes;

    //Label rescates
    @iOSFindBy(accessibility = "")
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID+":id/tvRescatesTitle")
    private MobileElement labelRescates;

    //Value rescates
    @iOSFindBy(accessibility = "")
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID+":id/tvRescatesValue")
    private MobileElement valueRescates;

    //Botón sobre tus saldos
    @iOSFindBy(accessibility = "")
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID+":id/btnVer")
    private MobileElement btnSobretusSaldos;

    //Info saldos
    @iOSFindBy(accessibility = "")
    @AndroidFindBy(id = ConstantesInversiones.BCI_APP_PACKAGE_ID+":id/tvSobreSaldo")
    private MobileElement infoSaldos;


    //***************************************************************************************************
    // *                                        MÉTODOS                                                 *
    // **************************************************************************************************

    @Step
    public void validarIngresoPantalla() {
        AccionesGenericas.ingresoPantalla(this.tituloQueQuieresHacer, "Dashboard Inversiones");
    }

    //***************************************************************************************************

    @Step
    public void validarElementosEnrolamiento(){
        AccionesGenericas.validarElemento(tituloEnrolamiento, "Invierte 100% online en Fondos Mutuos", 30);
        AccionesGenericas.validarElemento(linkEnrolamiento, "Completa tus datos aquí", 5);
        if (DriverContext.getDriver() instanceof AndroidDriver) {
            AccionesGenericas.validarElemento(imgEnrolamiento, 5);
        }
    }

    //***************************************************************************************************

    public void validarQueNoSeMuestreAccesoAEnrolamiento() {
        boolean existe = AccionesGenericas.existeElemento(tituloEnrolamiento, 15);
        if (!existe) {
            PdfBciReports.addMobilReportImage("Validar que no se muestre acceso a enrolamiento", "No se muestra acceso al enrolamiento", EstadoPrueba.PASSED, false);
        } else {
            PdfBciReports.addMobilReportImage("Validar que no se muestre acceso a enrolamiento", "Se muestra el acceso al enrolamiento", EstadoPrueba.FAILED, true);
        }
    }

    //***************************************************************************************************

    @Step
    public void validarElementosPerfilamiento(String casuistica) {
        Gestures gestures = new Gestures();
        switch (casuistica) {
            case "ConPerfil":
                gestures.encontrarObjeto(tituloTuPerfilInv, "Sección perfil de inversionista");
                AccionesGenericas.validarElemento(this.tituloTuPerfilInv, "Tu perfil de inversionista", 5);
                AccionesGenericas.validarElemento(this.perfil, 5);
                AccionesGenericas.validarElemento(this.btnVolverPerfilar, "Volver a perfilarme", 5);
                break;
            case "SinPerfil":
                gestures.encontrarObjeto(tituloConoceTuPerfil, "Sección perfil de inversionista");
                AccionesGenericas.validarElemento(this.tituloConoceTuPerfil, "Conoce tu perfil de inversionista", 5);
                AccionesGenericas.validarElemento(this.infoPerfilamiento, "Responde unas simples preguntas y accede a diversos productos de inversión.", 5);
                AccionesGenericas.validarElemento(this.btnPerfilar, "CONOCER MI PERFIL", 5);
                break;
            default:
                PdfBciReports.addReport("Validar elementos perfilamiento", "La opción ingresada para la casuistica de validación de elementos de perfil en dashboard es inválida", EstadoPrueba.PASSED, false);
                break;
        }
    }

    //***************************************************************************************************

    @Step
    public void validarElementosAlternativasInversion() {
        AccionesGenericas.validarElemento(this.tituloQueQuieresHacer, "¿Qué quieres hacer?", 5);
        AccionesGenericas.validarElemento(this.btnFFMM, "Invertir en Fondos Mutuos", 5);
        AccionesGenericas.validarElemento(this.btnDAP, "Invertir en Depósitos a Plazo", 5);
    }

    //***************************************************************************************************

    @Step
    public void validarElementosAsesoria() {
        Gestures ges = new Gestures();
        ges.encontrarObjeto(tituloAsesoria, btnAsesoria, "Sección Asesoría");
        AccionesGenericas.validarElemento(this.tituloAsesoria, "Te asesoramos para invertir", 5);
        AccionesGenericas.validarElemento(this.infoAsesesoria, "¿No sabes cómo? Cuéntanos de ti y buscaremos la mejor opción de acuerdo a tu perfil y objetivo de inversión.", 5);
        AccionesGenericas.validarElemento(this.btnAsesoria, "COMENZAR", 5);
        ges.scrollArriba();
    }

    //***************************************************************************************************

    @Step
    public void validarElementosResumenInversiones() {
        Gestures ges = new Gestures();
        AccionesGenericas.validarElemento(tituloSaldoActualCarteras, "Saldo actual de tus carteras", 5);
        AccionesGenericas.validarElemento(valueSaldoActualCarteras, 5);
        AccionesGenericas.validarElemento(tituloDetalleUltimoAnio, "Detalle del último año", 5);
        AccionesGenericas.validarElemento(labelSaldoInicial, "Saldo inicial", 5);
        AccionesGenericas.validarElemento(valueSaldoInicial, 5);
        AccionesGenericas.validarElemento(labelAportes, "Aportes", 5);
        AccionesGenericas.validarElemento(valueAportes, 5);
        AccionesGenericas.validarElemento(labelRescates, "Rescates", 5);
        AccionesGenericas.validarElemento(valueRescates, 5);
        AccionesGenericas.validarElemento(btnSobretusSaldos, "Sobre tus saldos", 5);
        btnSobretusSaldos.click();
        ges.encontrarObjeto(btnSobretusSaldos, "Sección Disclaimer Saldos");
        AccionesGenericas.validarElemento(infoSaldos, 5);
        AccionesGenericas.validarElemento(btnSobretusSaldos, "Entendido", 5);
        btnSobretusSaldos.click();
    }

    //***************************************************************************************************

    @Step
    public void validarSaldoActualEnCero(){
        String saldo = valueSaldoActualCarteras.getText();
        if(saldo.equals("0")){
            PdfBciReports.addMobilReportImage("Valiar Saldo actual en 0", "Se muestra saldo actual en 0", EstadoPrueba.PASSED, false);
        }else{
            PdfBciReports.addMobilReportImage("Valiar Saldo actual en 0", "No se muestra saldo actual en 0", EstadoPrueba.FAILED, true);

        }
    }

    //***************************************************************************************************

    @Step
    public void tapEnrolamiento() {
        linkEnrolamiento.click();
        PdfBciReports.addReport("Tap Enrolamiento", "Se hace tap en link Enrolamiento", EstadoPrueba.PASSED, false);
    }

    //***************************************************************************************************

    @Step
    public void tapPerfilamiento() {
        this.btnPerfilar.click();
        PdfBciReports.addReport("tapPerfilamiento", "Se hace tap a botón Perfilamiento", EstadoPrueba.PASSED, false);
    }

    //***************************************************************************************************

    @Step
    public void tapVolverAPerfilar() {
        this.btnVolverPerfilar.click();
        PdfBciReports.addReport("Tap boton volver a perfilar", "Se hace tap al botón volver a perfilar", EstadoPrueba.PASSED, false);
    }

    //***************************************************************************************************

    @Step
    public void tapFFMM() {
        this.btnFFMM.click();
        PdfBciReports.addReport("Tap botón FFMM", "Se hace tap al botón de FFMM", EstadoPrueba.PASSED, false);
    }

    //***************************************************************************************************

    @Step
    public void tapDAP() {
        this.btnDAP.click();
        PdfBciReports.addReport("Tap botón DAP", "Se hace tap al botón de DAP", EstadoPrueba.PASSED, false);
    }

    //***************************************************************************************************

    @Step
    public void tapAsesoria() {
        Gestures ges = new Gestures();
        ges.encontrarObjeto(tituloAsesoria, btnAsesoria, "Sección Asesoría");
        this.btnAsesoria.click();
        PdfBciReports.addReport("Tap botón Comenzar Asesoría", "Se hace tap al botón Comenzar", EstadoPrueba.PASSED, false);
    }
}
