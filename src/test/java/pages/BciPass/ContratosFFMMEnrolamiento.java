package pages.BciPass;

import driver.DriverContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.qameta.allure.Step;
import org.openqa.selenium.support.PageFactory;
import reporter.EstadoPrueba;
import reporter.PdfBciReports;
import util.AccionesGenericas;

import java.util.List;

public class ContratosFFMMEnrolamiento {

    private AppiumDriver driver;

    public ContratosFFMMEnrolamiento() {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver), this);
    }

    /*********************************************************************
     * OBJETOS
     **********************************************************************/

    //Título Autorización contratos
    @AndroidFindBy(id = "cl.bci.pass:id/msg_actionbar")
    @iOSFindBy(xpath = "//XCUIElementTypeOther[@name=\"Autorización contratos\"]")
    private MobileElement tituloAutorizacion;

    ///////////////*****************
    //Label sección central
    @AndroidFindBy(id = "cl.bci.pass:id/trx1_key")
    private List<MobileElement> listLabels;

    //Value sección central
    @AndroidFindBy(id = "cl.bci.pass:id/trx1_value")
    private List<MobileElement> listValues;

    ///////////////*****************
    //Titulo contratos FFMM
    @iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Contratos Fondos Mutuos\"]")
    private MobileElement tituloContratosFFMMIOS;

    //Label Fecha
    @iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Fecha:\"]")
    private MobileElement labelFechaIOS;

    ///////////////*****************

    //Input password
    @AndroidFindBy(id = "cl.bci.pass:id/pinTxt")
    @iOSFindBy(xpath = "//XCUIElementTypeApplication[@name=\"BciPass\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther")
    private MobileElement inputPass;

    //Botón autorizar
    @AndroidFindBy(id = "cl.bci.pass:id/accept")
    @iOSFindBy(xpath = "//XCUIElementTypeButton[@name=\"AUTORIZAR\"]")
    private MobileElement btnAutorizar;

    //Botón rechazar
    @AndroidFindBy(id = "cl.bci.pass:id/cancel")
    @iOSFindBy(xpath = "//XCUIElementTypeButton[@name=\"RECHAZAR\"]")
    private MobileElement btnRechazar;

    /*********************************************************************
     * Modal Rechazar transacción
     **********************************************************************/

    //Botón rechazar
    @AndroidFindBy(id = "cl.bci.pass:id/title_footer")
    @iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Rechazar transacción\"]")
    private MobileElement tituloModal;

    //Botón rechazar
    @AndroidFindBy(id = "cl.bci.pass:id/subtitle_footer")
    @iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"¿Estás seguro que deseas rechazar esta transacción?\"]")
    private MobileElement detalleModal;

    //Botón rechazar
    @AndroidFindBy(id = "cl.bci.pass:id/btn_accept")
    @iOSFindBy(xpath = "//XCUIElementTypeButton[@name=\"SI\"]")
    private MobileElement btnSi;

    //Botón rechazar
    @AndroidFindBy(id = "cl.bci.pass:id/btn_cancel")
    @iOSFindBy(xpath = "//XCUIElementTypeButton[@name=\"NO\"]")
    private MobileElement btnNo;



    //***************************************************************************************************
    // *                                        MÉTODOS                                                 *
    // **************************************************************************************************

    @Step
    public void validarIngresoPantalla(){
        AccionesGenericas.ingresoPantalla(tituloAutorizacion, "Autorización Contratos");
    }

    // **************************************************************************************************

    @Step
    public void validarElemetos(){
        if (DriverContext.getDriver() instanceof IOSDriver){
            AccionesGenericas.validarElemento(tituloAutorizacion, 5);
        }else {
            AccionesGenericas.validarElemento(tituloAutorizacion, "Autorización contratos", 60);
            AccionesGenericas.validarElemento(listLabels.get(0), "Contratos Fondos Mutuos", 5);
            AccionesGenericas.validarElemento(listLabels.get(1), "Fecha:", 5);
            AccionesGenericas.validarElemento(listValues.get(1), 5);
            AccionesGenericas.validarElemento(inputPass, 5);
            AccionesGenericas.validarElemento(btnAutorizar, "AUTORIZAR", 5);
            AccionesGenericas.validarElemento(btnRechazar, "RECHAZAR", 5);
        }
    }

    @Step
    public void ingresarPass(String pass){
        inputPass.setValue(pass);
        PdfBciReports.addMobilReportImage("Ingresar Pass", "Se ingresa la pass: "+pass, EstadoPrueba.PASSED, false);
    }

    @Step
    public void tapBtnAutotizar(){
        btnAutorizar.click();
        PdfBciReports.addReport("Tap botón autorizar", "Se hace tap al botón autorizar", EstadoPrueba.PASSED, false);
    }

    @Step
    public void tapBtnRechazar(){
        btnRechazar.click();
        PdfBciReports.addReport("Tap botón rechazar", "Se hace tap al botón rechazar", EstadoPrueba.PASSED, false);
    }

    @Step
    public void validarModal(){
        AccionesGenericas.validarElemento(tituloModal, 5);
        AccionesGenericas.validarElemento(detalleModal, 5);
        AccionesGenericas.validarElemento(btnSi, 5);
        AccionesGenericas.validarElemento(btnNo, 5);
        PdfBciReports.addMobilReportImage("Validar modal", "Modal rechazar transacción se despliega correctamente", EstadoPrueba.PASSED, false);
    }

    @Step
    public void tapSiModal(){
        btnSi.click();
        PdfBciReports.addReport("Tap Si Modal", "Se hace tap a botón si del modal", EstadoPrueba.PASSED, false);
    }

    @Step
    public void tapNoModal(){
        btnNo.click();
        PdfBciReports.addReport("Tap No Modal", "Se hace tap a botón no del modal", EstadoPrueba.PASSED, false);
    }
}
