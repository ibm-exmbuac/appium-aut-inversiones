package pages.CalculadoraRentabilidad;

import driver.DriverContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.qameta.allure.Step;
import org.openqa.selenium.support.PageFactory;
import reporter.EstadoPrueba;
import reporter.PdfBciReports;
import util.AccionesGenericas;

public class CalculadoraRentabilidad {

    private AppiumDriver driver;

    public CalculadoraRentabilidad() {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver), this);
    }

    //Botón calculadora rentabilidad histórica
    @iOSFindBy(accessibility = "Revisa la rentabilidad histórica")
    private MobileElement btnCalculadoraRentabilidad;

    //Título revisa la rentabilidad histórica
    @iOSFindBy(accessibility = "Revisa la rentabilidad histórica")
    private MobileElement tituloCalculadoraRentabilidad;

    //Botón cerrar
    @iOSFindBy(accessibility = "close")
    private MobileElement btnCerrar;

    //Título si hubieses invertido
    @iOSFindBy(accessibility = "CalculadoraRentabilidadTableViewCell.TitleLabel")
    private MobileElement tituloSiHubiesesInv;

    //Input monto
    @iOSFindBy(accessibility = "CalculadoraRentabilidadTableViewCell.MontoLabel")
    private MobileElement inputMonto;

    //Label Hace
    @iOSFindBy(accessibility = "Hace")
    private MobileElement labelHace;

    //Input días
    @iOSFindBy(accessibility = "CalculadoraRentabilidadTableViewCell.PlazoTextField")
    private MobileElement inputDias;

    //Label días
    @iOSFindBy(accessibility = "días")
    private MobileElement labelDias;

    //Label hoy tendrías
    @iOSFindBy(accessibility = "Hoy tendrías")
    private MobileElement labelHoyTedrias;

    //Info rentabilidades
    @iOSFindBy(accessibility = "Las rentabilidades obtenidas en el pasado por los fondos administrador por esta sociedad, no garantizan que se repitan en el futuro.")
    private MobileElement infoRentabilidades;

    //Botón calcular
    @iOSFindBy(accessibility = "CALCULAR")
    private MobileElement btnCalcular;

    //Resultado
    @iOSFindBy(accessibility = "CalculadoraRentanilidadTableViewCell.RentabilidadResultLabel")
    private MobileElement resultado;

    @Step
    public void validarIngresoPantalla() {
        AccionesGenericas.ingresoPantalla(this.tituloSiHubiesesInv, "Calculadola Rentabilidad");
    }

    @Step
    public void validarElementos() {
        AccionesGenericas.validarElemento(this.tituloCalculadoraRentabilidad, "Revisa la rentabilidad histórica", 5);
        AccionesGenericas.validarElemento(this.btnCerrar, 5);
        AccionesGenericas.validarElemento(this.tituloSiHubiesesInv, "Si hubieses invertido", 5);
        AccionesGenericas.validarElemento(this.inputMonto, 5);
        AccionesGenericas.validarElemento(this.labelHace, "Hace", 5);
        AccionesGenericas.validarElemento(this.inputDias, 5);
        AccionesGenericas.validarElemento(this.labelDias, "días", 5);
        AccionesGenericas.validarElemento(this.labelHoyTedrias, "Hoy tendrías", 5);
        AccionesGenericas.validarElemento(this.infoRentabilidades, "Las rentabilidades obtenidas en el pasado por los fondos administrador por esta sociedad, no garantizan que se repitan en el futuro.", 5);
        AccionesGenericas.validarElemento(this.btnCalcular, "CALCULAR", 5);
    }

    @Step
    public void ingresarACalculadora() {
        this.btnCalculadoraRentabilidad.click();
        PdfBciReports.addReport("Tap calcular rentabilidad", "Se hace tap a botón de calculadora de rentabilidades", EstadoPrueba.PASSED, false);
    }

    @Step
    public void ingresarMonto(String monto) {
        this.inputMonto.setValue(monto);
        PdfBciReports.addReport("Ingresar Monto", "Se ingresa monto " + monto, EstadoPrueba.PASSED, false);
    }

    @Step
    public void ingresarDias(String dias) {
        this.inputDias.setValue(dias);
        PdfBciReports.addReport("Ingresa dias", "Se ingresa dias " + dias, EstadoPrueba.PASSED, false);
    }

    @Step
    public void calcularRentabilidad() {
        if (btnCalcular.isEnabled()) {
            this.btnCalcular.click();
            boolean existe = AccionesGenericas.existeElemento(resultado, 60);
            if (existe) {
                PdfBciReports.addMobilReportImage("Calcular rentabilidad", "Se hace el cálculo de la rentabilidad", EstadoPrueba.PASSED, false);
            } else {
                PdfBciReports.addMobilReportImage("Calcular rentabilidad", "No se hace el cálculo de la rentabilidad", EstadoPrueba.FAILED, false);
            }
        } else {
            PdfBciReports.addMobilReportImage("Calcular rentabilidad", "Botón calcular no se encuentra habilitado", EstadoPrueba.FAILED, false);
        }
    }
}
