package pages.Perfilamiento;

import constants.Constants;
import driver.DriverContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.qameta.allure.Step;
import org.openqa.selenium.support.PageFactory;
import reporter.EstadoPrueba;
import reporter.PdfBciReports;
import util.AccionesGenericas;

public class ResultadoPerfilamiento {

    private AppiumDriver driver;

    public ResultadoPerfilamiento() {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver), this);
    }

    /*********************************************************************
     * OBJETOS
     **********************************************************************/

    //Título Resultado
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/toolbar_title")
    @iOSFindBy(accessibility = "Resultado")
    private MobileElement resultado;

    //Tu perfil de inversionista es
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_perfil_label")
    @iOSFindBy(accessibility = "Tu perfil inversionista es")
    private MobileElement labelTuPerfilInv;

    //Perfil
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_perfil")
    @iOSFindBy(xpath = "//XCUIElementTypeStaticText[contains(@name, \"PerfilClienteView.\")]")
    private MobileElement perfil;

    //Info Perfil
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_perfilamiento_message")
    @iOSFindBy(accessibility = "ResultadoPerfilamientoCell")
    private MobileElement infoPerfil;

    //Te recomendamos invertir
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_perfilamiento_recomendaciones_title")
    @iOSFindBy(accessibility = "Te recomendamos invertir en:")
    private MobileElement recomendamosInvertir;

    //Info volver a perfilar
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_perfilar")
    @iOSFindBy(accessibility = "ResultadoPerfilamientoCell.VolverAResponderLabel")
    private MobileElement infoVolverPerfilar;

    //Botón comenzar a invertir
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/btn_detalle_ffmm_perfilamiento_invertir")
    @iOSFindBy(accessibility = "COMENZAR A INVERTIR")
    private MobileElement btnComenzarInvertir;

    //Botón finalizar
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/btn_detalle_ffmm_perfilamiento_finalizar")
    @iOSFindBy(accessibility = "FINALIZAR")
    private MobileElement btnFinalizar;

    /*********************************************************************
     *RECOMENDACIONES IOS
     **********************************************************************/

    //Recomendacion 1
    @iOSFindBy(accessibility = "ResultadoPerfilamientoCell")
    private MobileElement recomendacion1;

    //Recomendacion 2
    @iOSFindBy(accessibility = "ResultadoPerfilamientoCell.1RecommendationLabel")
    private MobileElement recomendacion2;

    /*********************************************************************
     *RECOMENDACIONES ANDROID
     **********************************************************************/

    //Recomendaciones inversión
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_perfilamiento_recomendaciones_values")
    private MobileElement recomendaciones;


    /*********************************************************************
     *METODOS
     **********************************************************************/

    @Step
    public void validarIngresoPantalla() {
        AccionesGenericas.ingresoPantalla(this.resultado, "Resultado perfilamiento");
    }

    @Step
    public void validarElementos() {
        AccionesGenericas.validarElemento(this.resultado, "Resultado", 5);
        AccionesGenericas.validarElemento(this.labelTuPerfilInv, "Tu perfil de inversionista es", 5);
        AccionesGenericas.validarElemento(this.perfil, 5);
        AccionesGenericas.validarElemento(this.infoPerfil, 5);
        AccionesGenericas.validarElemento(this.recomendamosInvertir, "Te recomendamos invertir en :", 5);
        if (DriverContext.getDriver() instanceof IOSDriver) {
            AccionesGenericas.validarElemento(this.recomendacion1, 5);
            AccionesGenericas.validarElemento(this.recomendacion2, 5);
        } else {
            AccionesGenericas.validarElemento(this.recomendaciones, 5);
        }
        AccionesGenericas.validarElemento(this.infoVolverPerfilar, "Si no te sientes cómodo con el perfil propuesto, puedes volver a contestar este cuestionario.", 5);
        AccionesGenericas.validarElemento(this.btnComenzarInvertir, "COMENZAR A INVERTIR", 5);
    }

    @Step
    public void tapComenzarInvertir() {
        this.btnComenzarInvertir.click();
        PdfBciReports.addReport("Tap Comenzar Invertir", "Se hace tap al botón Comenzar a Invertir", EstadoPrueba.PASSED, false);
    }

    @Step
    public void tapFinalizar() {
        this.btnFinalizar.click();
        PdfBciReports.addReport("Tap botón Finalizar", "Se hace Tap al botón finalizar", EstadoPrueba.PASSED, false);
    }

    @Step
    public void tapLinkVolverAResponderCuestionario() {
        this.infoVolverPerfilar.click();
        PdfBciReports.addReport("Tap link volver a contestar cuestionaria", "Se hace tap a link en Volver a contestar cuestionario", EstadoPrueba.PASSED, false);
    }
}