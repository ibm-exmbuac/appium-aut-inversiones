package pages.Perfilamiento;


import constants.Constants;
import driver.DriverContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.qameta.allure.Step;
import org.openqa.selenium.support.PageFactory;
import reporter.EstadoPrueba;
import reporter.PdfBciReports;
import util.AccionesGenericas;

import java.util.List;

public class PerfilamientoPregunta1 {
    private AppiumDriver driver;

    public PerfilamientoPregunta1() {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver), this);
    }

    /*********************************************************************
     * OBJETOS
     **********************************************************************/

    //Título Conoce tu perfl de inversionista
    @iOSFindBy(accessibility = "Conoce tu perfil de inversionista")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/toolbar_title")
    private MobileElement tituloConocePerfil;

    //Botón cerrar
    @iOSFindBy(accessibility = "close")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/action_close")
    private MobileElement btnCerrar;

    //Info perfilamiento
    @iOSFindBy(accessibility = "El perfil de inversionista describe cómo eres a la hora de invertir. Dependiendo de cuánto riesgo estás dispuesto a asumir en una inversión te podemos ofrecer diversas alternativas. Conoce tu perfil, respondiendo estas preguntas:")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_disclaimer")
    private MobileElement infoPerfilamiento;

    //Pregunta
    @iOSFindBy(accessibility = "¿Cuál es tu conocimiento del mundo financiero?")
    @AndroidFindBy(xpath = "//*[contains(@text,\"¿Cuál es tu conocimiento del mundo financiero?\")]")
    private MobileElement pregunta;

    //Elige una opción
    @iOSFindBy(accessibility = "Elige una opción")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_labelSelectOption")
    private MobileElement labelEligeOpcion;

    //Stepper
    @iOSFindBy(accessibility = "Pregunta 1 de 5")
    @AndroidFindBy(xpath = "//android.view.ViewGroup[@index='0']/android.widget.TextSwitcher[@index='7']/android.widget.TextView[@index='0']")
    private MobileElement stepper;

    /*********************************************************************
     *RADIO BUTTONS Y ALTERNATIVAS IOS
     **********************************************************************/

    //Radio button respuesta 1
    @iOSFindBy(id = "AlternativeTableViewCell.127RadioButton")
    private MobileElement rdoRespuesta1;

    //Respuesta 1
    @iOSFindBy(accessibility = "AlternativeTableViewCell.127Label")
    private MobileElement respuesta1;

    //Radio button respuesta 2
    @iOSFindBy(accessibility = "AlternativeTableViewCell.128RadioButton")
    private MobileElement rdoRespuesta2;

    //Respuesta 2
    @iOSFindBy(accessibility = "AlternativeTableViewCell.128Label")
    private MobileElement respuesta2;

    //Radio button respuesta 3
    @iOSFindBy(accessibility = "AlternativeTableViewCell.129RadioButton")
    private MobileElement rdoRespuesta3;

    //Respuesta 3
    @iOSFindBy(accessibility = "AlternativeTableViewCell.129Label")
    private MobileElement respuesta3;

    //Radio button respuesta 4
    @iOSFindBy(accessibility = "AlternativeTableViewCell.130RadioButton")
    private MobileElement rdoRespuesta4;

    //Respuesta 4
    @iOSFindBy(accessibility = "AlternativeTableViewCell.130Label")
    private MobileElement respuesta4;

    /*********************************************************************
     *RADIO BUTTONS Y ALTERNATIVAS ANDROID
     **********************************************************************/

    //Radio buttons
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/radioButton_alternativa")
    private List<MobileElement> rdoRespuestas;

    //Respuestas
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_alternativa")
    private List<MobileElement> respuestas;

    /*********************************************************************
     *MODAL SALIR DEL PERFILAMIENTO
     **********************************************************************/
    //Icono
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/iv_error_icon")
    private MobileElement icono;

    //Título Estás seguro de salir?
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_error_message")
    private MobileElement tituloModal;

    //Info modal
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_error_message_description")
    private MobileElement infoModal;

    //Botón continuar
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/positive_button")
    private MobileElement btnContinuar;

    //Respuestas
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/negative_button")
    private MobileElement btnSalir;

    /*********************************************************************
     *METODOS
     **********************************************************************/

    @Step
    public void validarIngresoPantalla() {
        AccionesGenericas.ingresoPantalla(this.tituloConocePerfil, "Pregunta 1 Perfilamiento");
    }

    @Step
    public void validarElementosCabecera() {
        AccionesGenericas.validarElemento(this.tituloConocePerfil, "Conoce tu perfil de inversionista", 5);
        AccionesGenericas.validarElemento(this.btnCerrar, 5);
//        AccionesGenericas.validarElemento(this.infoPerfilamiento, "El perfil de inversionista describe cómo eres a la hora de invertir. Dependiendo de cuánto riesgo estás dispuesto a asumir en una inversión te podemos ofrecer diversas alternativas. Conoce tu perfil, respondiendo estas preguntas:", 5);
    }

    @Step
    public void validarElementosPreguntaRespuestas() {
        AccionesGenericas.validarElemento(this.pregunta, "¿Cuál es tu conocimiento del mundo financiero?", 5);
        AccionesGenericas.validarElemento(this.labelEligeOpcion, "Elige una opción", 5);
//        AccionesGenericas.validarElemento(this.stepper, "Pregunta 1 de 5", 5);
        if (DriverContext.getDriver() instanceof IOSDriver) {
//            AccionesGenericas.validarElemento(this.rdoRespuesta1, 5);
//            AccionesGenericas.validarElemento(this.rdoRespuesta2, 5);
//            AccionesGenericas.validarElemento(this.rdoRespuesta3, 5);
//            AccionesGenericas.validarElemento(this.rdoRespuesta4, 5);
            AccionesGenericas.validarElemento(this.respuesta1, "No tengo conocimientos.", 5);
            AccionesGenericas.validarElemento(this.respuesta2, "Entiendo lo que es, pero no sigo el mundo financiero.", 5);
            AccionesGenericas.validarElemento(this.respuesta3, "Tengo conocimientos, pero no soy experto en finanzas.", 5);
            AccionesGenericas.validarElemento(this.respuesta4, "Tengo conocimientos y me considero experto en finanzas.", 5);
        } else {
            AccionesGenericas.validarElemento(this.respuestas.get(0), "No tengo conocimientos.", 5);
            AccionesGenericas.validarElemento(this.rdoRespuestas.get(0), 5);
            AccionesGenericas.validarElemento(this.respuestas.get(1), "Entiendo lo que es, pero no sigo el mundo financiero.", 5);
            AccionesGenericas.validarElemento(this.rdoRespuestas.get(1), 5);
            AccionesGenericas.validarElemento(this.respuestas.get(2), "Tengo conocimientos, pero no soy experto en finanzas.", 5);
            AccionesGenericas.validarElemento(this.rdoRespuestas.get(2), 5);
            AccionesGenericas.validarElemento(this.respuestas.get(3), "Tengo conocimientos y me considero experto en finanzas.", 5);
            AccionesGenericas.validarElemento(this.rdoRespuestas.get(3), 5);
        }
    }

    @Step
    public void seleccionarRespuesta(String respuesta) {
        if (DriverContext.getDriver() instanceof IOSDriver) {
            switch (respuesta) {
                case "1":
                    PdfBciReports.addReport("Seleccionar Respuesta", "Se selecciona la respuesta " + respuesta1.getText(), EstadoPrueba.PASSED, false);
                    this.respuesta1.click();
                    break;
                case "2":
                    PdfBciReports.addReport("Seleccionar Respuesta", "Se selecciona la respuesta " + respuesta2.getText(), EstadoPrueba.PASSED, false);
                    this.respuesta2.click();
                    break;
                case "3":
                    PdfBciReports.addReport("Seleccionar Respuesta", "Se selecciona la respuesta " + respuesta3.getText(), EstadoPrueba.PASSED, false);
                    this.respuesta3.click();
                    break;
                case "4":
                    PdfBciReports.addReport("Seleccionar Respuesta", "Se selecciona la respuesta " + respuesta4.getText(), EstadoPrueba.PASSED, false);
                    this.respuesta4.click();
            }
        } else {
            PdfBciReports.addReport("Seleccionar Respuesta", "Se selecciona la respuesta " + respuestas.get(Integer.parseInt(respuesta) - 1).getText(), EstadoPrueba.PASSED, false);
            this.respuestas.get(Integer.parseInt(respuesta) - 1).click();
        }
    }

    @Step
    public void tapBotonSalirPerfilamiento() {
        this.btnCerrar.click();
        PdfBciReports.addReport("Tap botón salir", "Se hace tap al botón salir", EstadoPrueba.PASSED, false);
    }

    @Step
    public void validarModalSalir() {
        AccionesGenericas.validarElemento(this.icono, 10);
        AccionesGenericas.validarElemento(this.tituloModal, "¿Estás seguro de salir?", 5);
        AccionesGenericas.validarElemento(this.infoModal, "Los datos ingresados no serán guardados.", 5);
        AccionesGenericas.validarElemento(this.btnContinuar, "CONTINUAR", 5);
        AccionesGenericas.validarElemento(this.btnSalir, "SALIR", 5);
    }

    @Step
    public void tapBotonContinuarModal() {
        this.btnContinuar.click();
        PdfBciReports.addReport("Tap botón continuar Modal", "Se hace tap al botón continuar", EstadoPrueba.PASSED, false);
    }

    @Step
    public void tapBotonSalirModal() {
        this.btnSalir.click();
        PdfBciReports.addReport("Tap botón salir Modal", "Se hace tap al botón salir", EstadoPrueba.PASSED, false);
    }
}

