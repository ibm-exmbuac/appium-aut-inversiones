package pages.Perfilamiento;

import constants.Constants;
import driver.DriverContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.qameta.allure.Step;
import org.openqa.selenium.support.PageFactory;
import reporter.EstadoPrueba;
import reporter.PdfBciReports;
import util.AccionesGenericas;
import util.Gestures;

import java.util.List;

public class PerfilamientoPregunta3 {

    private AppiumDriver driver;

    public PerfilamientoPregunta3() {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver), this);
    }

    /*********************************************************************
     * OBJETOS
     **********************************************************************/

    //Título conoce tu perfil de inversionista
    @iOSFindBy(accessibility = "Conoce tu perfil de inversionista")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/toolbar_title")
    private MobileElement tituloConocePerfil;

    //Flecla volver atrás
    @iOSFindBy(accessibility = "left arrow")
    @AndroidFindBy(xpath = "//android.widget.LinearLayout[@index='2']/android.view.ViewGroup[@index='1']/android.widget.ImageButton[@index='0']")
    private MobileElement flechaAtras;

    //Botón cerrar
    @iOSFindBy(accessibility = "close")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/action_close")
    private MobileElement btnCerrar;

    //Pregunta
    @iOSFindBy(accessibility = "En el supuesto que inviertes $10.000.000 y que el resultado de tu inversión al cabo de un año puede tener 3 resultados posibles, ¿con qué resultado de inversión te sentirías más cómodo?")
    @AndroidFindBy(xpath = "//android.view.ViewGroup[@index='0']/android.widget.TextSwitcher[@index='4']/android.widget.TextView[@index='0']")
    private MobileElement pregunta;

    //Elige una opción
    @iOSFindBy(accessibility = "Elige una opción")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_labelSelectOption")
    private MobileElement labelEligeOpcion;

    //Stepper
    @iOSFindBy(accessibility = "Pregunta 3 de 5")
    @AndroidFindBy(xpath = "//android.view.ViewGroup[@index='0']/android.widget.TextSwitcher[@index='6']/android.widget.TextView[@index='0']")
    private MobileElement stepper;

    /*********************************************************************
     *RADIO BUTTONS, ALTERNATIVAS E IMÁGENES IOS
     **********************************************************************/

    //Radio button respuesta 1
    @iOSFindBy(accessibility = "AlternativeTableViewCell.137RadioButton")
    private MobileElement rdoRespuesta1;

    //Respuesta 1
    @iOSFindBy(accessibility = "AlternativeTableViewCell.137Label")
    private MobileElement respuesta1;

    //Imagen respuesta 1
    @iOSFindBy(accessibility = "AlternativeTableViewCell.137GraphImageView")
    private MobileElement imgRespuesta1;

    //Radio button respuesta 2
    @iOSFindBy(accessibility = "AlternativeTableViewCell.138RadioButton")
    private MobileElement rdoRespuesta2;

    //Respuesta 2
    @iOSFindBy(accessibility = "AlternativeTableViewCell.138Label")
    private MobileElement respuesta2;

    //Imagen respuesta 2
    @iOSFindBy(accessibility = "AlternativeTableViewCell.138GraphImageView")
    private MobileElement imgRespuesta2;

    //Radio button respuesta 3
    @iOSFindBy(accessibility = "AlternativeTableViewCell.139RadioButton")
    private MobileElement rdoRespuesta3;

    //Respuesta 3
    @iOSFindBy(accessibility = "AlternativeTableViewCell.139Label")
    private MobileElement respuesta3;

    //Imagen respuesta 3
    @iOSFindBy(accessibility = "AlternativeTableViewCell.139GraphImageView")
    private MobileElement imgRespuesta3;

    /*********************************************************************
     *RADIO BUTTONS, ALTERNATIVAS E IMÁGENES ANDROID
     **********************************************************************/

    //Radio buttons
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/radioButton_alternativa")
    private List<MobileElement> rdoRespuestas;

    //Respuestas
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_alternativa")
    private List<MobileElement> respuestas;

    //Imágenes
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/iv_alternativaImage")
    private List<MobileElement> imagenes;


    /*********************************************************************
     *METODOS
     **********************************************************************/

    @Step
    public void validarIngresoPantalla() {
        if (DriverContext.getDriver() instanceof IOSDriver) {
            AccionesGenericas.ingresoPantalla(this.respuesta1, "Pregunta 3 Perfilamiento");
        }else{
            AccionesGenericas.ingresoPantalla(this.pregunta, "Pregunta 3 Perfilamiento");
        }
    }

    @Step
    public void validarElementosCabecera() {
        AccionesGenericas.validarElemento(this.flechaAtras, 5);
        AccionesGenericas.validarElemento(this.tituloConocePerfil, "Conoce tu perfil de inversionista", 5);
        AccionesGenericas.validarElemento(this.btnCerrar, 5);
    }

    @Step
    public void validarElementosPreguntaRespuestas() {

        Gestures ges = new Gestures();
//        AccionesGenericas.validarElemento(this.pregunta, "En el supuesto que inviertes $10.000.000 y que el resultado de tu inversión al cabo de un año puede tener 3 resultados posibles, ¿con qué resultado de inversión te sentirías más cómodo?", 5);
        AccionesGenericas.validarElemento(this.labelEligeOpcion, "Elige una opción", 5);
        AccionesGenericas.validarElemento(this.stepper, "Pregunta 3 de 5", 5);
        if (DriverContext.getDriver() instanceof IOSDriver) {
//            AccionesGenericas.validarElemento(this.rdoRespuesta1, 5);
            AccionesGenericas.validarElemento(this.respuesta1, "Escenario hipotético A", 5);
            AccionesGenericas.validarElemento(this.imgRespuesta1, 5);
//            AccionesGenericas.validarElemento(this.rdoRespuesta2, 5);
            AccionesGenericas.validarElemento(this.respuesta2, "Escenario hipotético B", 5);
            AccionesGenericas.validarElemento(this.imgRespuesta2, 5);
            ges.scrollAbajo();
//            AccionesGenericas.validarElemento(this.rdoRespuesta3, 5);
            AccionesGenericas.validarElemento(this.respuesta3, "Escenario hipotético C", 5);
            AccionesGenericas.validarElemento(this.imgRespuesta3, 5);
            ges.scrollArriba();
        } else {
            AccionesGenericas.validarElemento(rdoRespuestas.get(0), 5);
            AccionesGenericas.validarElemento(respuestas.get(0), "Escenario hipotético A", 5);
            AccionesGenericas.validarElemento(imagenes.get(0), 5);
            AccionesGenericas.validarElemento(rdoRespuestas.get(1), 5);
            AccionesGenericas.validarElemento(respuestas.get(1), "Escenario hipotético B", 5);
            AccionesGenericas.validarElemento(imagenes.get(1), 5);
            ges.swipe(0.5, 0.2);
            AccionesGenericas.validarElemento(rdoRespuestas.get(1), 5);
            AccionesGenericas.validarElemento(respuestas.get(1), "Escenario hipotético C", 5);
            AccionesGenericas.validarElemento(imagenes.get(1), 5);
            ges.swipe(0.5, 0.8);
        }
    }

    @Step
    public void seleccionarRespuesta(String respuesta) {
        Gestures ges = new Gestures();
        if (DriverContext.getDriver() instanceof IOSDriver) {
            switch (respuesta) {
                case "1":
                    PdfBciReports.addReport("Seleccionar Respuesta", "Se selecciona la respuesta " + respuesta1.getText(), EstadoPrueba.PASSED, false);
                    this.respuesta1.click();
                    break;
                case "2":
                    PdfBciReports.addReport("Seleccionar Respuesta", "Se selecciona la respuesta " + respuesta2.getText(), EstadoPrueba.PASSED, false);
                    this.respuesta2.click();
                    break;
                case "3":
                    ges.scrollAbajo();
                    PdfBciReports.addReport("Seleccionar Respuesta", "Se selecciona la respuesta " + respuesta3.getText(), EstadoPrueba.PASSED, false);
                    this.respuesta3.click();
                    break;
            }
        } else {
            if (respuesta.equals("3")) {
                ges.scrollAbajo();
                PdfBciReports.addReport("Seleccionar Respuesta", "Se selecciona la respuesta " + respuestas.get(Integer.parseInt(respuesta) - 2), EstadoPrueba.PASSED, false);
                this.respuestas.get(Integer.parseInt(respuesta) - 2).click();
            } else {
                PdfBciReports.addReport("Seleccionar Respuesta", "Se selecciona la respuesta " + respuestas.get(Integer.parseInt(respuesta) - 1), EstadoPrueba.PASSED, false);
                this.respuestas.get(Integer.parseInt(respuesta) - 1).click();

            }

        }
    }

}
