package pages.Perfilamiento;

import constants.Constants;
import driver.DriverContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.qameta.allure.Step;
import org.openqa.selenium.support.PageFactory;
import reporter.EstadoPrueba;
import reporter.PdfBciReports;
import util.AccionesGenericas;

import java.util.List;

public class PerfilamientoPregunta4 {

    private AppiumDriver driver;

    public PerfilamientoPregunta4() {
        this.driver = DriverContext.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver), this);
    }

    /*********************************************************************
     * OBJETOS
     **********************************************************************/

    //Título conoce tu perfil de inversionista
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/toolbar_title")
    @iOSFindBy(accessibility = "Conoce tu perfil de inversionista")
    private MobileElement tituloConocePerfil;

    //Flecla volver atrás
    @iOSFindBy(accessibility = "left arrow")
    @AndroidFindBy(xpath = "//android.widget.LinearLayout[@index='2']/android.view.ViewGroup[@index='1']/android.widget.ImageButton[@index='0']")
    private MobileElement flechaAtras;

    //Botón cerrar
    @iOSFindBy(accessibility = "close")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/action_close")
    private MobileElement btnCerrar;

    //Pregunta
    @iOSFindBy(accessibility = "Si perdieras un 30% de tu inversión debido a una crisis económica mundial, ¿qué harías?")
    @AndroidFindBy(xpath = "//android.view.ViewGroup[@index='0']/android.widget.TextSwitcher[@index='4']/android.widget.TextView[@index='0']")
    private MobileElement pregunta;

    //Elige una opción
    @iOSFindBy(accessibility = "Elige una opción")
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_labelSelectOption")
    private MobileElement labelEligeOpcion;

    //Stepper
    @iOSFindBy(accessibility = "Pregunta 4 de 5")
    @AndroidFindBy(xpath = "//android.view.ViewGroup[@index='0']/android.widget.TextSwitcher[@index='6']/android.widget.TextView[@index='0']")
    private MobileElement stepper;

    /*********************************************************************
     *RADIO BUTTONS Y ALTERNATIVAS IOS
     **********************************************************************/

    //Radio button respuesta 1
    @iOSFindBy(accessibility = "AlternativeTableViewCell.140RadioButton")
    private MobileElement rdoRespuesta1;

    //Respuesta 1
    @iOSFindBy(accessibility = "AlternativeTableViewCell.140Label")
    private MobileElement respuesta1;

    //Radio button respuesta 2
    @iOSFindBy(accessibility = "AlternativeTableViewCell.141RadioButton")
    private MobileElement rdoRespuesta2;

    //Respuesta 2
    @iOSFindBy(accessibility = "AlternativeTableViewCell.141Label")
    private MobileElement respuesta2;

    //Radio button respuesta 3
    @iOSFindBy(accessibility = "AlternativeTableViewCell.142RadioButton")
    private MobileElement rdoRespuesta3;

    //Respuesta 3
    @iOSFindBy(accessibility = "AlternativeTableViewCell.142Label")
    private MobileElement respuesta3;

    //Radio button respuesta 4
    @iOSFindBy(accessibility = "AlternativeTableViewCell.143RadioButton")
    private MobileElement rdoRespuesta4;

    //Respuesta 4
    @iOSFindBy(accessibility = "AlternativeTableViewCell.143Label")
    private MobileElement respuesta4;

    /*********************************************************************
     *RADIO BUTTONS Y ALTERNATIVAS ANDROID
     **********************************************************************/

    //Radio buttons
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/radioButton_alternativa")
    private List<MobileElement> rdoRespuestas;

    //Respuestas
    @AndroidFindBy(id = Constants.BCI_APP_PACKAGE_ID + ":id/tv_alternativa")
    private List<MobileElement> respuestas;


    /*********************************************************************
     *METODOS
     **********************************************************************/

    @Step
    public void validarIngresoPantalla() {
        AccionesGenericas.ingresoPantalla(this.pregunta, "Pregunta 4 Perfilamiento");
    }

    @Step
    public void validarElementosCabecera() {
        AccionesGenericas.validarElemento(this.flechaAtras, 5);
        AccionesGenericas.validarElemento(this.tituloConocePerfil, "Conoce tu perfil de inversionista", 5);
        AccionesGenericas.validarElemento(this.btnCerrar, 5);
    }

    @Step
    public void validarElementosPreguntaRespuesta() {
        AccionesGenericas.validarElemento(this.pregunta, "Si perdieras un 30% de tu inversión debido a una crisis económica mundial, ¿qué harías?", 5);
        AccionesGenericas.validarElemento(this.labelEligeOpcion, "Elige una opción", 5);
        AccionesGenericas.validarElemento(this.stepper, "Pregunta 4 de 5", 5);
        if (DriverContext.getDriver() instanceof IOSDriver) {
//            AccionesGenericas.validarElemento(this.rdoRespuesta1, 5);
//            AccionesGenericas.validarElemento(this.rdoRespuesta2, 5);
//            AccionesGenericas.validarElemento(this.rdoRespuesta3, 5);
//            AccionesGenericas.validarElemento(this.rdoRespuesta4, 5);
            AccionesGenericas.validarElemento(this.respuesta1, "Invertiría más.", 5);
            AccionesGenericas.validarElemento(this.respuesta2, 5);
            AccionesGenericas.validarElemento(this.respuesta3, "Vendería parte de lo que tengo e invertiría ese monto en un producto de menor riesgo.", 5);
            AccionesGenericas.validarElemento(this.respuesta4, "Vendería todo lo que tengo y lo invertiría en un producto de menor riesgo.", 5);
        } else {
            AccionesGenericas.validarElemento(this.rdoRespuestas.get(0), 5);
            AccionesGenericas.validarElemento(this.rdoRespuestas.get(1), 5);
            AccionesGenericas.validarElemento(this.rdoRespuestas.get(2), 5);
            AccionesGenericas.validarElemento(this.rdoRespuestas.get(3), 5);
            AccionesGenericas.validarElemento(this.respuestas.get(0), "Invertiría más.", 5);
            AccionesGenericas.validarElemento(this.respuestas.get(1), "Me preocuparía, pero no harí\u00ADa ningún cambio.", 5);
            AccionesGenericas.validarElemento(this.respuestas.get(2), "Vendería parte de lo que tengo e invertiría ese monto en un producto de menor riesgo.", 5);
            AccionesGenericas.validarElemento(this.respuestas.get(3), "Vendería todo lo que tengo y lo invertiría en un producto de menor riesgo.", 5);
        }
    }

    @Step
    public void seleccionarRespuesta(String respuesta) {
        if (DriverContext.getDriver() instanceof IOSDriver) {
            switch (respuesta) {
                case "1":
                    PdfBciReports.addReport("Seleccionar Respuesta", "Se selecciona la respuesta " + respuesta1.getText(), EstadoPrueba.PASSED, false);
                    this.respuesta1.click();
                    break;
                case "2":
                    PdfBciReports.addReport("Seleccionar Respuesta", "Se selecciona la respuesta " + respuesta2.getText(), EstadoPrueba.PASSED, false);
                    this.respuesta2.click();
                    break;
                case "3":
                    PdfBciReports.addReport("Seleccionar Respuesta", "Se selecciona la respuesta " + respuesta3.getText(), EstadoPrueba.PASSED, false);
                    this.respuesta3.click();
                    break;
                case "4":
                    PdfBciReports.addReport("Seleccionar Respuesta", "Se selecciona la respuesta " + respuesta4.getText(), EstadoPrueba.PASSED, false);
                    this.respuesta4.click();
                    break;
            }
        } else {
            PdfBciReports.addReport("Seleccionar Respuesta", "Se selecciona la respuesta " + respuestas.get(Integer.parseInt(respuesta) - 1).getText(), EstadoPrueba.PASSED, false);
            this.respuestas.get(Integer.parseInt(respuesta) - 1).click();
        }
    }
}
