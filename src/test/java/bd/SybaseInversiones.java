package bd;

import dataSources.SyBase;
import io.qameta.allure.model.Status;
import org.testng.Assert;
import reporter.EstadoPrueba;
import reporter.PdfBciReports;
import reporter.Reports;

import java.sql.*;

public class SybaseInversiones extends SyBase {

    public static Connection getConnectionInv() {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection("jdbc:jtds:sybase://161.131.232.222:17000/gestglobinv", "tuxesyb", "zzsytux");
            PdfBciReports.addReport("Conexión Inversiones", "Se generó conexión a Sybase en ambiente de certificación", EstadoPrueba.PASSED, false);
        } catch (SQLException e) {
            Assert.fail("Error de conexion a SQL, se detiene prueba" + e.getMessage());
            e.printStackTrace();
        } finally {
            return connection;
        }
    }


    public boolean resetearPerfilInversionista(String rut) {
        String vRut = rut.replace(".", "");
        vRut = vRut.replace("-", "");
        vRut = vRut.substring(0, vRut.length() - 1);

        String query = "delete from TEST_CLIENTE where tst_rut_rutcliente = " + vRut;
        System.out.println(query);
        try {
            Connection con = getConnectionInv();
            Statement stmt = con.createStatement();
            boolean result;
            result = stmt.execute(query);
            Reports.addStep("Se realizó la siguiente consulta SQL en Sybase: " + query, false, Status.PASSED, false);
            if (!result) {
                Reports.addStep("[SybaseDB] Exito en la ejecucion de la query.", false, Status.PASSED, false);
                return true;
            } else {
                Reports.addStep("[SybaseDB] No hubo respuesta en la ejecucion de la query.", false, Status.FAILED, false);
                return false;
            }

        } catch (SQLException sqe) {
            System.out.println("Unexpected exception : " +
                    sqe.toString() + ", sqlstate = " +
                    sqe.getSQLState());
            Reports.addStep("[SybaseDB]Error: al consulta el registro en la Base de Datos Query: [" +
                    query + "] " + sqe.getMessage(), false, Status.FAILED, false);
            return false;
        } catch (NullPointerException javaNull) {
            System.out.println("Unexpected exception : " +
                    javaNull.toString());
            Reports.addStep("[SybaseDB]Error: al consulta el registro en la Base de Datos Query: [" +
                    query + "] " + javaNull.getMessage(), false, Status.FAILED, false);
            return false;
        }
    }

    public boolean resetearEnrolamiento(String rut) {
        String vRut = rut.replace(".", "");
        vRut = vRut.replace("-", "");
        vRut = vRut.substring(0, vRut.length() - 1);

        String query = "delete from ein_enrolamiento_inversion where ein_cli_rut = " + vRut;
        System.out.println(query);
        try {
            Connection con = getConnectionInv();
            Statement stmt = con.createStatement();
            boolean result;
            result = stmt.execute(query);
            PdfBciReports.addReport("Resetear Enrolamiento en Gestglobinv", "Se realizó la siguinete consulta SQL en Sybase: " + query, EstadoPrueba.PASSED, false);
            if (!result) {
                PdfBciReports.addReport("Resetear Enrolamiento en Gestglobinv", "La query se ejecutó exitosamente", EstadoPrueba.PASSED, false);
                return true;
            } else {
                PdfBciReports.addReport("Resetear Enrolamiento en Gestglobinv", "La query NO se ha ejecutado exitosamente", EstadoPrueba.FAILED, false);
                return false;
            }

        } catch (SQLException sqe) {
            System.out.println("Unexpected exception : " + sqe.toString() + ", sqlstate = " + sqe.getSQLState());
            PdfBciReports.addReport("Resetear Enrolamiento en Gestglobinv", "Error al consultar el registro en la BBDD; Query: [" + query + "] " + sqe.getMessage(), EstadoPrueba.FAILED, false);
            return false;
        } catch (NullPointerException javaNull) {
            System.out.println("Unexpected exception : " + javaNull.toString());
            PdfBciReports.addReport("Resetear Enrolamiento en Gestglobinv", "Error al consultar el registro en la BBDD; Query: [" + query + "] " + javaNull.getMessage(), EstadoPrueba.FAILED, false);
            return false;
        }
    }

    public void consultaEnrolamiento(String rut) {
        String vRut = rut.replace(".", "");
        vRut = vRut.replace("-", "");
        vRut = vRut.substring(0, vRut.length() - 1);

        String query = "select * from ein_enrolamiento_inversion where ein_cli_rut = " + vRut;
        System.out.println(query);
        try {

            Connection con = getConnectionInv();
            Statement stmt = con.createStatement();
            boolean result = stmt.execute(query);
            ResultSet resultado = stmt.executeQuery(query);

            PdfBciReports.addReport("Consulta Enrolamiento Gestglobinv", "Se realizó la siguiente consulta SQL en Sybase: " + query, EstadoPrueba.PASSED, false);
            if (result) {
                PdfBciReports.addReport("Consulta Enrolamiento en Gestglobinv", "La query se ejecutó exitosamente", EstadoPrueba.PASSED, false);
                if (!resultado.next()) {
                    PdfBciReports.addReport("Consulta Enrolamiento", "No se ha encontrado ningun registro del enrolamiento", EstadoPrueba.FAILED, true);
                } else {
                    do {
                        String rutRegistro = resultado.getString("ein_cli_rut");
                        String dvRegistro = resultado.getString("ein_cli_rut_dv");
                        PdfBciReports.addReport("Consulta Enrolamiento", "Se obtiene registro de enrolamiento para el siguiente rut: " + rutRegistro + "-" + dvRegistro, EstadoPrueba.PASSED, false);
                    } while (resultado.next());
                }
            } else {
                PdfBciReports.addReport("Consulta Enrolamiento en Gestglobinv", "La query NO se ha ejecutado exitosamente", EstadoPrueba.FAILED, false);
            }

        } catch (SQLException sqe) {
            System.out.println("Unexpected exception : " + sqe.toString() + ", sqlstate = " + sqe.getSQLState());
            PdfBciReports.addReport("Resetear Enrolamiento en Gestglobinv", "Error al consultar el registro en la BBDD; Query: [" + query + "] " + sqe.getMessage(), EstadoPrueba.FAILED, false);
        } catch (NullPointerException javaNull) {
            System.out.println("Unexpected exception : " + javaNull.toString());
            PdfBciReports.addReport("Resetear Enrolamiento en Gestglobinv", "Error al consultar el registro en la BBDD; Query: [" + query + "] " + javaNull.getMessage(), EstadoPrueba.FAILED, false);
        }
    }

}
