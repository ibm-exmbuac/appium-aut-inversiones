package util;

import driver.DriverContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSElement;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import io.qameta.allure.model.Status;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import reporter.EstadoPrueba;
import reporter.PdfBciReports;
import reporter.Reports;

import java.time.Duration;

public class Gestures {
    private AppiumDriver driver;
    private Dimension size;
    private TouchAction touchAction;

    public Gestures() {
        this.driver = DriverContext.getDriver();
        this.size = DriverContext.getDriver().manage().window().getSize();
        this.touchAction = new TouchAction(driver);
    }


    public void scrollAbajo() {
        Dimension size = driver.manage().window().getSize();
        int anchor, startPoint, endPoint;
        anchor = (int) (size.width * 0.080);
        startPoint = (int) (size.height * 0.8);
        endPoint = (int) (size.height * 0.4);
        touchAction.press(PointOption.point(anchor, startPoint)).waitAction(WaitOptions.waitOptions(Duration.ofMillis(500)))
                .moveTo(PointOption.point(anchor, endPoint)).release().perform();
        Reports.addStep("[Gestures]Se hace swipe hacia abajo", true, Status.PASSED, false);
    }

    public void scrollAbajoCorto() {
        Dimension size = driver.manage().window().getSize();
        int anchor, startPoint, endPoint;
        anchor = (int) (size.width * 0.090);
        startPoint = (int) (size.height * 0.6);
        endPoint = (int) (size.height * 0.4);
        touchAction.press(PointOption.point(anchor, startPoint)).waitAction(WaitOptions.waitOptions(Duration.ofMillis(500)))
                .moveTo(PointOption.point(anchor, endPoint)).release().perform();
        Reports.addStep("[Gestures]Se hace swipe hacia abajo", true, Status.PASSED, false);
    }

    public void scrollArriba() {
        Dimension size = driver.manage().window().getSize();
        int anchor, startPoint, endPoint;
        anchor = (int) (size.width * 0.080);
        endPoint = (int) (size.height * 0.9);
        startPoint = (int) (size.height * 0.5);
        touchAction.press(PointOption.point(anchor, startPoint)).waitAction(WaitOptions.waitOptions(Duration.ofMillis(500)))
                .moveTo(PointOption.point(anchor, endPoint)).release().perform();
        Reports.addStep("[Gestures]Se hace swipe hacia arriba", true, Status.PASSED, false);
    }

    public void tap(Integer x, Integer y, String elemento) {
        TouchAction touchAction = new TouchAction(driver);
        touchAction.tap(PointOption.point(x, y)).perform();
        Reports.addStep("[Gestures]Se hizo tap con las coordenadas x:" + x + " y:" + y +
                "Correspondientes al elemento:" + elemento, true, Status.PASSED, false);
    }

    public void scrollSelectorElementos(MobileElement elemento) {
        TouchAction touchAction = new TouchAction(driver);
        Point point = elemento.getLocation();
        int x = point.x + 5;
        int y = point.y + 5;
        touchAction.press(PointOption.point(x, y)).waitAction(WaitOptions.waitOptions(Duration.ofMillis(1000))).moveTo(PointOption.point(x, y - 120)).release().perform();
    }


    public void swipe(double comienzo, double fin) {
        Dimension size = driver.manage().window().getSize();
        int anchor, startPoint, endPoint;
        anchor = (int) (size.width * 0.080);
        endPoint = (int) (size.height * fin);
        startPoint = (int) (size.height * comienzo);
        touchAction.press(PointOption.point(anchor, startPoint)).waitAction(WaitOptions.waitOptions(Duration.ofMillis(700)))
                .moveTo(PointOption.point(anchor, endPoint)).release().perform();
    }

    public void encontrarObjeto(MobileElement elemento, MobileElement elemento2, String detalle) {
        boolean existe, existe2;
        int intento = 0;
        do {
            swipe(0.5, 0.3);
            existe = AccionesGenericas.existeElemento(elemento, 1);
            existe2 = AccionesGenericas.existeElemento(elemento2, 1);
            intento++;
            if (intento == 5) {
                PdfBciReports.addMobilReportImage("Encontrar Objeto", "No se visualiza " + detalle, EstadoPrueba.FAILED, true);
                PdfBciReports.closePDF();
            }
        } while (!existe || !existe2);
        PdfBciReports.addMobilReportImage("Encontrar Objeto", "Se visualiza " + detalle, EstadoPrueba.PASSED, false);
    }

    public void encontrarObjeto(MobileElement elemento, String detalle) {
        boolean existe;
        int intento = 0;
        do {
            existe = AccionesGenericas.existeElemento(elemento, 1);
            intento++;
            if(intento==10){
                PdfBciReports.addMobilReportImage("Encontrar Objeto", "No se visualiza "+ detalle, EstadoPrueba.FAILED, true);
                PdfBciReports.closePDF();
            }
            if (intento > 0) {
                swipe(0.5, 0.4);
            }
        }
        while (!existe);
        PdfBciReports.addMobilReportImage("Encontrar Objeto", "Se visualiza " + detalle, EstadoPrueba.PASSED, false);
    }

}
