package util;

import driver.DriverContext;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import reporter.EstadoPrueba;
import reporter.PdfBciReports;
import java.util.Calendar;

public class AccionesGenericas {


    /************************************************************
     *  Metodo que valida si el elemento ingresado por parametro*
     *  existe. Devuelve un boolean dependiendo si se encuentra *
     *  o no el elemento.                                       *
     ************************************************************/

    public static boolean existeElemento(MobileElement objeto, int segundos) {
        try {
            WebDriverWait wait = new WebDriverWait(DriverContext.getDriver(), (long) segundos);
            wait.until(ExpectedConditions.visibilityOf(objeto));
            return true;
        } catch (Exception var3) {
            return false;
        }
    }


    /************************************************************
     * Metodo que valida si el elemento ingresado por parametro *
     * existe. La prueba falla si no encuentra el elemento.     *
     ************************************************************/

    public static void validarElemento(MobileElement elemento, int segundos) {
        try {
            boolean existe = existeElemento(elemento, segundos);
            if (existe) {
                PdfBciReports.addReport("Elemento encontrado", "Se encuentra el elemento " + elemento, EstadoPrueba.PASSED, false);
                System.out.println("Se encuentra el elemnto" + elemento);
            } else {
                PdfBciReports.addMobilReportImage("Elemento NO encontrado", "No se ha podido encontrar el elemento " + elemento, EstadoPrueba.FAILED, false);
                System.out.println("No se encuentra el elemento" + elemento);
                PdfBciReports.closePDF();
            }
        } catch (Exception var3) {
            PdfBciReports.addReport("Validación de objeto", "Error en el metodo ‘validarElemento’, motivo: " + var3.getMessage(), EstadoPrueba.FAILED, false);
            PdfBciReports.closePDF();
        }
    }


    /**********************************************************
     *  Metodo que hace una comparativa entre el texto        *
     *  que se despliega en la APP contra el texto que        *
     *  se espera que se muestre. La prueba falla si          *
     *  los textos no coinciden                               *
     **********************************************************/

    public static void validarElemento(MobileElement elemento, String textoAValidar, int segundos) {
        try {
            boolean existe = existeElemento(elemento, segundos);
            if (existe) {
                PdfBciReports.addReport("Elemento encontrado", "Se encuentra el elemento " + elemento, EstadoPrueba.PASSED, false);
                System.out.println("Se encuentra el elemnto" + elemento);
                if (elemento.getText().trim().equals(textoAValidar.trim())) {
                    PdfBciReports.addReport("Validación texto", "Se visualiza el texto esperado: " + textoAValidar, EstadoPrueba.PASSED, false);
                } else {
                    PdfBciReports.addReport("Validación texto", "NO se visualiza el texto esperado: " + textoAValidar + " se visualiza el texto: " + elemento.getText(), EstadoPrueba.FAILED, false);
                    PdfBciReports.closePDF();
                }
            } else {
                PdfBciReports.addMobilReportImage("Elemento NO encontrado", "No se ha podido encontrar el elemento " + elemento, EstadoPrueba.FAILED, false);
                System.out.println("No se encuentra el elemento" + elemento);
                PdfBciReports.closePDF();
            }
        } catch (Exception var3) {
            PdfBciReports.addReport("Validación de objeto", "Error en el metodo 'validarElemento', motivo: " + var3.getMessage(), EstadoPrueba.FAILED, false);
            PdfBciReports.closePDF();
        }
    }


    /***********************************************************
     * Metodo que espera una cantidad determinada de segundos  *
     ***********************************************************/

    public static void esperar(int segundos) throws InterruptedException {
        long sec = segundos * 1000;
        try {
            Thread.sleep(sec);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    /***************************************************
     * Metodo que valida si se ha cargado la pantalla. *
     ***************************************************/

    public static void ingresoPantalla(MobileElement objeto, String tituloPantalla) {
        boolean ingreso = existeElemento(objeto, 60);
        if (ingreso) {
            PdfBciReports.addMobilReportImage("Ingreso a Pantalla", "Se ingresa correctamente a la pantalla " + tituloPantalla, EstadoPrueba.PASSED, false);
        } else {
            PdfBciReports.addMobilReportImage("Ingreso a Pantalla", "No ingresa a la pantalla esperada: " + tituloPantalla, EstadoPrueba.FAILED, false);
            PdfBciReports.closePDF();
        }
    }


    /***************************************************
     * Metodo que genera un dia mayor a 8 a contar de  *
     * de la fecha actual, y que no es un día habil    *
     ***************************************************/

    public static int generadorDiaNoHabil() {
        Calendar calendar = Calendar.getInstance();
        int dias = 8;
        calendar.add(Calendar.DAY_OF_YEAR, dias);
        while (calendar.get(Calendar.DAY_OF_WEEK) < 7) {
            calendar.add(Calendar.DAY_OF_YEAR, 1);
            dias++;
        }
        return dias;
    }


}
