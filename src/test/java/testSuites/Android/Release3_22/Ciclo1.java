package testSuites.Android.Release3_22;

import bd.SybaseInversiones;
import constants.OS;
import constants.Tabs;
import driver.DriverContext;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import reporter.PdfBciReports;
import reporter.Reports;
import testClasses.DAP.BannerADAP.CPA00001DesplegarBannerDAP;
import testClasses.DAP.OperacionExitosa.CPA00002OperacionExitosaTomaDAPPlazoRenovable;
import testClasses.DAP.OperacionExitosa.CPA00003OperacionExitosaTomaDAPPlazoFijo;
import testClasses.DAP.ResultadoSimulacion.CPA00014EmailCorreoVacioCargadoPorDefecto;
import testClasses.DAP.ResultadoSimulacion.CPA0007ResultadoDAPAjusteDiaHabil;
import testClasses.DAP.Simular.CPA00012DesplegarModalAlTomarDAPFueraDeHorario;
import testClasses.FFMM.Actualizar.CPA00010IncorporarDashboarddeInversionesConPerfilDeInversionistaEnApp;
import testClasses.FFMM.Actualizar.CPA00011IncorporarDashboardDeInversionesSinPerfilDeInversionistaEnApp;
import testClasses.FFMM.Asesorar.CPA0005InvertirEnFFMMDesdeLaAsesoria;
import testClasses.FFMM.Asesorar.CPA0015InvertirEnFFMMDesdeLaAsesoriaParaClienteEnroladoYNoPerfiladoMVP2;
import testClasses.FFMM.Asesorar.CPA0025InvertirEnCarteraAsesoradaDesdeLaAsesoria;
import testClasses.FFMM.Asesorar.CPA0027AsesoriaDeClienteNoEnroladoYPerfilado;
import testClasses.FFMM.CarterasDestacadas.CPA003FinalizaFlujoInvertirDeUnaCarteraDestacada;
import testClasses.FFMM.Enrolamiento.*;
import testClasses.FFMM.Invertir.CPA00003InvertirEnAppFFMMFlujoTres;
import testClasses.FFMM.Invertir.CPA00004InvertirFueraDeHorarioApp;
import testClasses.FFMM.Invertir.CPA00011FirmarCGFENApp;
import testClasses.FFMM.PerfilarCliente.*;
import util.AccionesGenericas;

import static ALM.UploadEvidenceALM.setTestInstanceID;
import static ALM.UploadEvidenceALM.uploadALM;
import static constants.Tabs.Inversiones;
import static testbase.TestBase.loginRapido;
import static testbase.TestBase.tabSwitcher;

public class Ciclo1 {

    @BeforeMethod
    public void setUp() {
        PdfBciReports.createPDF();
       DriverContext.setUp(OS.ANDROID, "ZY223KFX37", "Android", false, "app-qa(3.23.0)");
        //DriverContext.setUp(OS.ANDROID,"M4VDU18301000367","Huawei 10 lite","4727","app-qa-inversiones-v3-22",true);
    }

    @AfterMethod
    public void tearDown() {
        uploadALM();
        Reports.clean();
        DriverContext.quitDriver();
    }

    //**************************************
    //          ENROLAMIENTO
    //**************************************

    @Test
    public void CPA0001VerificarQueSeMuestreElBotonDeFirmaDeContratos() {
        setTestInstanceID("35035", "almInversiones.properties");
        loginRapido("12773650-2", "inve019");
        tabSwitcher(Tabs.Inversiones);
        CPA0001VerificarQueSeMuestreElBotonDeFirmaDeContratos cpa0001VerificarQueSeMuestreElBotonDeFirmaDeContratos = new CPA0001VerificarQueSeMuestreElBotonDeFirmaDeContratos();
        cpa0001VerificarQueSeMuestreElBotonDeFirmaDeContratos.CPA0001VerificarQueSeMuestreElBotonDeFirmaDeContratos();
        PdfBciReports.closePDF();
    }

    @Test
    public void CPA0002VerificarLaPrecargaDeLosDatosPersonalesDelEnrolamiento() {
        setTestInstanceID("35036", "almInversiones.properties");
        loginRapido("12773650-2", "inve019");
        tabSwitcher(Tabs.Inversiones);
        CPA0002VerificarLaPrecargaDeLosDatosPersonalesDelEnrolamiento cpa0002VerificarLaPrecargaDeLosDatosPersonalesDelEnrolamiento = new CPA0002VerificarLaPrecargaDeLosDatosPersonalesDelEnrolamiento();
        cpa0002VerificarLaPrecargaDeLosDatosPersonalesDelEnrolamiento.CPA0002VerificarLaPrecargaDeLosDatosPersonalesDelEnrolamiento();
        PdfBciReports.closePDF();
    }

    @Test
    public void CPA0004VerificarClienteConPaisDeResidenciaDistintoAChile() {
        setTestInstanceID("35038", "almInversiones.properties");
        loginRapido("12773650-2", "inve019");
        tabSwitcher(Tabs.Inversiones);
        CPA0004VerificarClienteConPaisDeResidenciaDistintoAChile cpa0004VerificarClienteConPaisDeResidenciaDistintoAChile = new CPA0004VerificarClienteConPaisDeResidenciaDistintoAChile();
        cpa0004VerificarClienteConPaisDeResidenciaDistintoAChile.CPA0004VerificarClienteConPaisDeResidenciaDistintoAChile();
        PdfBciReports.closePDF();
    }

    @Test
    public void CPA0005VerificarClienteConDobleNacionalidad() {
        setTestInstanceID("35039", "almInversiones.properties");
        loginRapido("12773650-2", "inve019");
        tabSwitcher(Tabs.Inversiones);
        CPA0005VerificarClienteConDobleNacionalidad cpa0005VerificarClienteConDobleNacionalidad = new CPA0005VerificarClienteConDobleNacionalidad();
        cpa0005VerificarClienteConDobleNacionalidad.CPA0005VerificarClienteConDobleNacionalidad();
        PdfBciReports.closePDF();
    }

    @Test
    public void CPA0007ValidarQueSePuedeSalirDelFlujoDeEnrolamientoEnLaVentanaDeDatosPersonalesDesdeLaX() {
        setTestInstanceID("35040", "almInversiones.properties");
        loginRapido("12773650-2", "inve019");
        tabSwitcher(Tabs.Inversiones);
        CPA0007ValidarQueSePuedeSalirDelFlujoDeEnrolamientoEnLaVentanaDeDatosPersonalesDesdeLaX cpa0007ValidarQueSePuedeSalirDelFlujoDeEnrolamientoEnLaVentanaDeDatosPersonalesDesdeLaX = new CPA0007ValidarQueSePuedeSalirDelFlujoDeEnrolamientoEnLaVentanaDeDatosPersonalesDesdeLaX();
        cpa0007ValidarQueSePuedeSalirDelFlujoDeEnrolamientoEnLaVentanaDeDatosPersonalesDesdeLaX.CPA0007ValidarQueSePuedeSalirDelFlujoDeEnrolamientoEnLaVentanaDeDatosPersonalesDesdeLaX();
        PdfBciReports.closePDF();
    }

    @Test
    public void CPA0008ValidarQueSePuedeContinuarEnLaVentanaDeDatosPersonalesLuegoDeDarALaX() {
        setTestInstanceID("35041", "almInversiones.properties");
        loginRapido("12773650-2", "inve019");
        tabSwitcher(Tabs.Inversiones);
        CPA0008ValidarQueSePuedeContinuarEnLaVentanaDeDatosPersonalesLuegoDeDarALaX cpa0008ValidarQueSePuedeContinuarEnLaVentanaDeDatosPersonalesLuegoDeDarALaX = new CPA0008ValidarQueSePuedeContinuarEnLaVentanaDeDatosPersonalesLuegoDeDarALaX();
        cpa0008ValidarQueSePuedeContinuarEnLaVentanaDeDatosPersonalesLuegoDeDarALaX.CPA0008ValidarQueSePuedeContinuarEnLaVentanaDeDatosPersonalesLuegoDeDarALaX();
        PdfBciReports.closePDF();
    }

    @Test
    public void CPA0011VerificarLasValidacionesDelCampoEstadoCivil() {
        setTestInstanceID("35042", "almInversiones.properties");
        loginRapido("12773650-2", "inve019");
        tabSwitcher(Tabs.Inversiones);
        CPA0011VerificarLasValidacionesDelCampoEstadoCivil cpa0011VerificarLasValidacionesDelCampoEstadoCivil = new CPA0011VerificarLasValidacionesDelCampoEstadoCivil();
        cpa0011VerificarLasValidacionesDelCampoEstadoCivil.CPA0011VerificarLasValidacionesDelCampoEstadoCivil();
        PdfBciReports.closePDF();
    }

    @Test
    public void CPA0012VerificarLaPrecargaDeLosDatosDeContactoDelEnrolamiento() {
        setTestInstanceID("35043", "almInversiones.properties");
        loginRapido("12773650-2", "inve019");
        tabSwitcher(Tabs.Inversiones);
        CPA0012VerificarLaPrecargaDeLosDatosDeContactoDelEnrolamiento cpa0012VerificarLaPrecargaDeLosDatosDeContactoDelEnrolamiento = new CPA0012VerificarLaPrecargaDeLosDatosDeContactoDelEnrolamiento();
        cpa0012VerificarLaPrecargaDeLosDatosDeContactoDelEnrolamiento.CPA0012VerificarLaPrecargaDeLosDatosDeContactoDelEnrolamiento();
        PdfBciReports.closePDF();
    }


    @Test
    public void CPA0013VerificarLasValidacionesDeLosCamposDeDatosDeContacto() {
        setTestInstanceID("35044", "almInversiones.properties");
        loginRapido("12773650-2", "inve019");
        tabSwitcher(Tabs.Inversiones);
        CPA0013VerificarLasValidacionesDeLosCamposDeDatosDeContacto cpa0013VerificarLasValidacionesDeLosCamposDeDatosDeContacto = new CPA0013VerificarLasValidacionesDeLosCamposDeDatosDeContacto();
        cpa0013VerificarLasValidacionesDeLosCamposDeDatosDeContacto.CPA0013VerificarLasValidacionesDeLosCamposDeDatosDeContacto();
        PdfBciReports.closePDF();
    }

    @Test
    public void CPA0014ValidarQuesePuedeSalirDelFlujoDeEnrolamientoEnLaVentanaDeDatosDeContactoDesdaLaX() {
        setTestInstanceID("35045", "almInversiones.properties");
        loginRapido("12773650-2", "inve019");
        tabSwitcher(Tabs.Inversiones);
        CPA0014ValidarQuesePuedeSalirDelFlujoDeEnrolamientoEnLaVentanaDeDatosDeContactoDesdaLaX cpa0014ValidarQuesePuedeSalirDelFlujoDeEnrolamientoEnLaVentanaDeDatosDeContactoDesdaLaX = new CPA0014ValidarQuesePuedeSalirDelFlujoDeEnrolamientoEnLaVentanaDeDatosDeContactoDesdaLaX();
        cpa0014ValidarQuesePuedeSalirDelFlujoDeEnrolamientoEnLaVentanaDeDatosDeContactoDesdaLaX.CPA0014ValidarQuesePuedeSalirDelFlujoDeEnrolamientoEnLaVentanaDeDatosDeContactoDesdaLaX();
        PdfBciReports.closePDF();
    }

    @Test
    public void CPA0015ValidarQueSePuedeContinuarEnLaVentanaDeDatosDeContactoLuegoDeDarALaX() {
        setTestInstanceID("35046", "almInversiones.properties");
        loginRapido("12773650-2", "inve019");
        tabSwitcher(Tabs.Inversiones);
        CPA0015ValidarQueSePuedeContinuarEnLaVentanaDeDatosDeContactoLuegoDeDarALaX cpa0015ValidarQueSePuedeContinuarEnLaVentanaDeDatosDeContactoLuegoDeDarALaX = new CPA0015ValidarQueSePuedeContinuarEnLaVentanaDeDatosDeContactoLuegoDeDarALaX();
        cpa0015ValidarQueSePuedeContinuarEnLaVentanaDeDatosDeContactoLuegoDeDarALaX.CPA0015ValidarQueSePuedeContinuarEnLaVentanaDeDatosDeContactoLuegoDeDarALaX();
        PdfBciReports.closePDF();
    }

    @Test
    public void CPA0016ValidarQueSePuedeVolverAtrasYAdelanteDesdeLosDatosDeContacto() {
        setTestInstanceID("35047", "almInversiones.properties");
        loginRapido("12773650-2", "inve019");
        tabSwitcher(Tabs.Inversiones);
        CPA0016ValidarQueSePuedeVolverAtrasYAdelanteDesdeLosDatosDeContacto cpa0016ValidarQueSePuedeVolverAtrasYAdelanteDesdeLosDatosDeContacto = new CPA0016ValidarQueSePuedeVolverAtrasYAdelanteDesdeLosDatosDeContacto();
        cpa0016ValidarQueSePuedeVolverAtrasYAdelanteDesdeLosDatosDeContacto.CPA0016ValidarQueSePuedeVolverAtrasYAdelanteDesdeLosDatosDeContacto();
        PdfBciReports.closePDF();
    }

    @Test
    public void CPA0017VerificarLaPrecargaDeLosDatosLabolaresDelEnrolamiento() {
        setTestInstanceID("35048", "almInversiones.properties");
        loginRapido("12773650-2", "inve019");
        tabSwitcher(Tabs.Inversiones);
        CPA0017VerificarLaPrecargaDeLosDatosLabolaresDelEnrolamiento cpa0017VerificarLaPrecargaDeLosDatosLabolaresDelEnrolamiento = new CPA0017VerificarLaPrecargaDeLosDatosLabolaresDelEnrolamiento();
        cpa0017VerificarLaPrecargaDeLosDatosLabolaresDelEnrolamiento.CPA0017VerificarLaPrecargaDeLosDatosLabolaresDelEnrolamiento();
        PdfBciReports.closePDF();
    }

    @Test
    public void CPA0018VerificarOpcionesEmpleadoYEmpresarioEnLosDatosLabolares() {
        setTestInstanceID("35049", "almInversiones.properties");
        loginRapido("12773650-2", "inve019");
        tabSwitcher(Tabs.Inversiones);
        CPA0018VerificarOpcionesEmpleadoYEmpresarioEnLosDatosLabolares cpa0018VerificarOpcionesEmpleadoYEmpresarioEnLosDatosLabolares = new CPA0018VerificarOpcionesEmpleadoYEmpresarioEnLosDatosLabolares();
        cpa0018VerificarOpcionesEmpleadoYEmpresarioEnLosDatosLabolares.CPA0018VerificarOpcionesEmpleadoYEmpresarioEnLosDatosLabolares();
        PdfBciReports.closePDF();
    }

    @Test
    public void CPA0019VerificarLasOpcionesRentistaEIndependienteEnLosDatosLaborales() {
        setTestInstanceID("35050", "almInversiones.properties");
        loginRapido("12773650-2", "inve019");
        tabSwitcher(Tabs.Inversiones);
        CPA0019VerificarLasOpcionesRentistaEIndependienteEnLosDatosLaborales cpa0019VerificarLasOpcionesRentistaEIndependienteEnLosDatosLaborales = new CPA0019VerificarLasOpcionesRentistaEIndependienteEnLosDatosLaborales();
        cpa0019VerificarLasOpcionesRentistaEIndependienteEnLosDatosLaborales.CPA0019VerificarLasOpcionesRentistaEIndependienteEnLosDatosLaborales();
        PdfBciReports.closePDF();
    }

    @Test
    public void CPA0020VerificarLaOpcionJubiladoEnLosDatosLaborales() {
        setTestInstanceID("35051", "almInversiones.properties");
        loginRapido("12773650-2", "inve019");
        tabSwitcher(Tabs.Inversiones);
        CPA0020VerificarLaOpcionJubiladoEnLosDatosLaborales cpa0020VerificarLaOpcionJubiladoEnLosDatosLaborales = new CPA0020VerificarLaOpcionJubiladoEnLosDatosLaborales();
        cpa0020VerificarLaOpcionJubiladoEnLosDatosLaborales.CPA0020VerificarLaOpcionJubiladoEnLosDatosLaborales();
        PdfBciReports.closePDF();
    }

    @Test
    public void CPA0021ValidarQueSePuedeContinuarEnLaVentanaDeDatosLaboralesLuegoDeDarALaX() {
        setTestInstanceID("35052", "almInversiones.properties");
        loginRapido("12773650-2", "inve019");
        tabSwitcher(Tabs.Inversiones);
        CPA0021ValidarQueSePuedeContinuarEnLaVentanaDeDatosLaboralesLuegoDeDarALaX cpa0021ValidarQueSePuedeContinuarEnLaVentanaDeDatosLaboralesLuegoDeDarALaX = new CPA0021ValidarQueSePuedeContinuarEnLaVentanaDeDatosLaboralesLuegoDeDarALaX();
        cpa0021ValidarQueSePuedeContinuarEnLaVentanaDeDatosLaboralesLuegoDeDarALaX.CPA0021ValidarQueSePuedeContinuarEnLaVentanaDeDatosLaboralesLuegoDeDarALaX();
        PdfBciReports.closePDF();
    }

    @Test
    public void CPA0022ValidarQueSePuedeSalirDelFlujoDeEnrolamientoEnLaVentanaDeDatosLaboralesDesdeLaX() {
        setTestInstanceID("35053", "almInversiones.properties");
        loginRapido("12773650-2", "inve019");
        tabSwitcher(Tabs.Inversiones);
        CPA0022ValidarQueSePuedeSalirDelFlujoDeEnrolamientoEnLaVentanaDeDatosLaboralesDesdeLaX cpa0022ValidarQueSePuedeSalirDelFlujoDeEnrolamientoEnLaVentanaDeDatosLaboralesDesdeLaX = new CPA0022ValidarQueSePuedeSalirDelFlujoDeEnrolamientoEnLaVentanaDeDatosLaboralesDesdeLaX();
        cpa0022ValidarQueSePuedeSalirDelFlujoDeEnrolamientoEnLaVentanaDeDatosLaboralesDesdeLaX.CPA0022ValidarQueSePuedeSalirDelFlujoDeEnrolamientoEnLaVentanaDeDatosLaboralesDesdeLaX();
        PdfBciReports.closePDF();
    }

    @Test
    public void CPA0023ValidarQueSePuedeVolverAtrasYAdelanteDesdeLosDatosLabolares() {
        setTestInstanceID("35054", "almInversiones.properties");
        loginRapido("12773650-2", "inve019");
        tabSwitcher(Tabs.Inversiones);
        CPA0023ValidarQueSePuedeVolverAtrasYAdelanteDesdeLosDatosLabolares cpa0023ValidarQueSePuedeVolverAtrasYAdelanteDesdeLosDatosLabolares = new CPA0023ValidarQueSePuedeVolverAtrasYAdelanteDesdeLosDatosLabolares();
        cpa0023ValidarQueSePuedeVolverAtrasYAdelanteDesdeLosDatosLabolares.CPA0023ValidarQueSePuedeVolverAtrasYAdelanteDesdeLosDatosLabolares();
        PdfBciReports.closePDF();
    }

    @Test
    public void CPA0024VerificarLaPrecargaDeLosDatosDePatrimonioDelEnrolamiento() {
        setTestInstanceID("35055", "almInversiones.properties");
        loginRapido("12773650-2", "inve019");
        tabSwitcher(Tabs.Inversiones);
        CPA0024VerificarLaPrecargaDeLosDatosDePatrimonioDelEnrolamiento cpa0024VerificarLaPrecargaDeLosDatosDePatrimonioDelEnrolamiento = new CPA0024VerificarLaPrecargaDeLosDatosDePatrimonioDelEnrolamiento();
        cpa0024VerificarLaPrecargaDeLosDatosDePatrimonioDelEnrolamiento.CPA0024VerificarLaPrecargaDeLosDatosDePatrimonioDelEnrolamiento();
        PdfBciReports.closePDF();
    }

    @Test
    public void CPA0025ValidarQueSePuedeContinuarEnLaVentanaDeDatosIngresosYPatrimoniosLuegoDeDarALaX() {
        setTestInstanceID("35056", "almInversiones.properties");
        loginRapido("12773650-2", "inve019");
        tabSwitcher(Tabs.Inversiones);
        CPA0025ValidarQueSePuedeContinuarEnLaVentanaDeDatosIngresosYPatrimoniosLuegoDeDarALaX cpa0025ValidarQueSePuedeContinuarEnLaVentanaDeDatosIngresosYPatrimoniosLuegoDeDarALaX = new CPA0025ValidarQueSePuedeContinuarEnLaVentanaDeDatosIngresosYPatrimoniosLuegoDeDarALaX();
        cpa0025ValidarQueSePuedeContinuarEnLaVentanaDeDatosIngresosYPatrimoniosLuegoDeDarALaX.CPA0025ValidarQueSePuedeContinuarEnLaVentanaDeDatosIngresosYPatrimoniosLuegoDeDarALaX();
        PdfBciReports.closePDF();
    }

    @Test
    public void CPA0026ValidarQueSePuedeSalirDelFlujoDeEnrolamientoEnLaVentanaDeDatosIngresosYPatrimonioDesdeLaX() {
        setTestInstanceID("35057", "almInversiones.properties");
        loginRapido("12773650-2", "inve019");
        tabSwitcher(Tabs.Inversiones);
        CPA0026ValidarQueSePuedeSalirDelFlujoDeEnrolamientoEnLaVentanaDeDatosIngresosYPatrimonioDesdeLaX cpa0026ValidarQueSePuedeSalirDelFlujoDeEnrolamientoEnLaVentanaDeDatosIngresosYPatrimonioDesdeLaX = new CPA0026ValidarQueSePuedeSalirDelFlujoDeEnrolamientoEnLaVentanaDeDatosIngresosYPatrimonioDesdeLaX();
        cpa0026ValidarQueSePuedeSalirDelFlujoDeEnrolamientoEnLaVentanaDeDatosIngresosYPatrimonioDesdeLaX.CPA0026ValidarQueSePuedeSalirDelFlujoDeEnrolamientoEnLaVentanaDeDatosIngresosYPatrimonioDesdeLaX();
        PdfBciReports.closePDF();
    }

    @Test
    public void CPA0027ValidarQueSePuedeVolverAtrasYAdelanteDesdeLosDatosIngresosYPatrimonio() throws InterruptedException {
        setTestInstanceID("35058", "almInversiones.properties");
        loginRapido("12773650-2", "inve019");
        tabSwitcher(Tabs.Inversiones);
        CPA0027ValidarQueSePuedeVolverAtrasYAdelanteDesdeLosDatosIngresosYPatrimonio cpa0027ValidarQueSePuedeVolverAtrasYAdelanteDesdeLosDatosIngresosYPatrimonio = new CPA0027ValidarQueSePuedeVolverAtrasYAdelanteDesdeLosDatosIngresosYPatrimonio();
        cpa0027ValidarQueSePuedeVolverAtrasYAdelanteDesdeLosDatosIngresosYPatrimonio.CPA0027ValidarQueSePuedeVolverAtrasYAdelanteDesdeLosDatosIngresosYPatrimonio();
        PdfBciReports.closePDF();
    }

    @Test
    public void CPA0028ValidarQueLosCamposDeLaPaginaCincoNoVienenPrecargados() {
        setTestInstanceID("35059", "almInversiones.properties");
        loginRapido("12773650-2", "inve019");
        tabSwitcher(Tabs.Inversiones);
        CPA0028ValidarQueLosCamposDeLaPaginaCincoNoVienenPrecargados cpa0028ValidarQueLosCamposDeLaPaginaCincoNoVienenPrecargados = new CPA0028ValidarQueLosCamposDeLaPaginaCincoNoVienenPrecargados();
        cpa0028ValidarQueLosCamposDeLaPaginaCincoNoVienenPrecargados.CPA0028ValidarQueLosCamposDeLaPaginaCincoNoVienenPrecargados();
        PdfBciReports.closePDF();
    }

    @Test
    public void CPA0029VerificarComportamientoDeClienteUSPerson() {
        setTestInstanceID("35060", "almInversiones.properties");
        loginRapido("12773650-2", "inve019");
        tabSwitcher(Tabs.Inversiones);
        CPA0029VerificarComportamientoDeClienteUSPerson cpa0029VerificarComportamientoDeClienteUSPerson = new CPA0029VerificarComportamientoDeClienteUSPerson();
        cpa0029VerificarComportamientoDeClienteUSPerson.CPA0029VerificarComportamientoDeClienteUSPerson();
        PdfBciReports.closePDF();
    }

    @Test
    public void CPA0030VerificarComportamientoDelFlujoAlConfirmarSISOYUSPERSON() {
        setTestInstanceID("35061", "almInversiones.properties");
        loginRapido("12773650-2", "inve019");
        tabSwitcher(Tabs.Inversiones);
        CPA0030VerificarComportamientoDelFlujoAlConfirmarSISOYUSPERSON cpa0030VerificarComportamientoDelFlujoAlConfirmarSISOYUSPERSON = new CPA0030VerificarComportamientoDelFlujoAlConfirmarSISOYUSPERSON();
        cpa0030VerificarComportamientoDelFlujoAlConfirmarSISOYUSPERSON.CPA0030VerificarComportamientoDelFlujoAlConfirmarSISOYUSPERSON();
        PdfBciReports.closePDF();
    }

    @Test
    public void CPA0031VerificarComportamientoDeClienteConVinculoConPEP() {
        setTestInstanceID("35062", "almInversiones.properties");
        loginRapido("12773650-2", "inve019");
        tabSwitcher(Tabs.Inversiones);
        CPA0031VerificarComportamientoDeClienteConVinculoConPEP cpa0031VerificarComportamientoDeClienteConVinculoConPEP = new CPA0031VerificarComportamientoDeClienteConVinculoConPEP();
        cpa0031VerificarComportamientoDeClienteConVinculoConPEP.CPA0031VerificarComportamientoDeClienteConVinculoConPEP();
        PdfBciReports.closePDF();
    }

    @Test
    public void CPA0032VerificarComportamientoDeClienteQueDeclaraImpuestosEnOtroPais() {
        setTestInstanceID("35063", "almInversiones.properties");
        loginRapido("12773650-2", "inve019");
        tabSwitcher(Tabs.Inversiones);
        CPA0032VerificarComportamientoDeClienteQueDeclaraImpuestosEnOtroPais cpa0032VerificarComportamientoDeClienteQueDeclaraImpuestosEnOtroPais = new CPA0032VerificarComportamientoDeClienteQueDeclaraImpuestosEnOtroPais();
        cpa0032VerificarComportamientoDeClienteQueDeclaraImpuestosEnOtroPais.CPA0032VerificarComportamientoDeClienteQueDeclaraImpuestosEnOtroPais();
        PdfBciReports.closePDF();
    }

    @Test
    public void CPA0033ValidarEnvioCorrectoDeDatosDelClienteEnrolado() {
        setTestInstanceID("35064", "almInversiones.properties");
        SybaseInversiones sybaseInversiones = new SybaseInversiones();
        sybaseInversiones.resetearEnrolamiento("12773650-2");

        loginRapido("12773650-2", "inve019");
        tabSwitcher(Tabs.Inversiones);

        CPA0033ValidarEnvioCorrectoDeDatosDelClienteEnrolado cpa0033ValidarEnvioCorrectoDeDatosDelClienteEnrolado = new CPA0033ValidarEnvioCorrectoDeDatosDelClienteEnrolado();
        cpa0033ValidarEnvioCorrectoDeDatosDelClienteEnrolado.CPA0033ValidarEnvioCorrectoDeDatosDelClienteEnrolado();

        sybaseInversiones.consultaEnrolamiento("12773650-2");

        PdfBciReports.closePDF();
    }

    @Test
    public void CPA0036ValidarQueSePuedeVolverAtrasYAdelanteDesdeLosDatosDeSituacionTributaria() {
        setTestInstanceID("35065", "almInversiones.properties");
        loginRapido("13012431-3", "inve019");
        tabSwitcher(Tabs.Inversiones);
        CPA0036ValidarQueSePuedeVolverAtrasYAdelanteDesdeLosDatosDeSituacionTributaria cpa0036ValidarQueSePuedeVolverAtrasYAdelanteDesdeLosDatosDeSituacionTributaria = new CPA0036ValidarQueSePuedeVolverAtrasYAdelanteDesdeLosDatosDeSituacionTributaria();
        cpa0036ValidarQueSePuedeVolverAtrasYAdelanteDesdeLosDatosDeSituacionTributaria.CPA0036ValidarQueSePuedeVolverAtrasYAdelanteDesdeLosDatosDeSituacionTributaria();
        PdfBciReports.closePDF();
    }

    @Test
    public void CPA0037ValidarQueSePuedeContinuarEnLaVentanaDeDatosDeSituacionTributariaLuegoDeDarALaX() {
        setTestInstanceID("35066", "almInversiones.properties");
        loginRapido("13012431-3", "inve019");
        tabSwitcher(Tabs.Inversiones);
        CPA0037ValidarQueSePuedeContinuarEnLaVentanaDeDatosDeSituacionTributariaLuegoDeDarALaX cpa0037ValidarQueSePuedeContinuarEnLaVentanaDeDatosDeSituacionTributariaLuegoDeDarALaX = new CPA0037ValidarQueSePuedeContinuarEnLaVentanaDeDatosDeSituacionTributariaLuegoDeDarALaX();
        cpa0037ValidarQueSePuedeContinuarEnLaVentanaDeDatosDeSituacionTributariaLuegoDeDarALaX.CPA0037ValidarQueSePuedeContinuarEnLaVentanaDeDatosDeSituacionTributariaLuegoDeDarALaX();
        PdfBciReports.closePDF();
    }

    @Test
    public void CPA0038ValidarQueSePuedeSalirDelFlujoDeEnrolamientoEnLaVentanaDeDatosDeSituacionTributariaDesdeLaX() {
        setTestInstanceID("35067", "almInversiones.properties");
        loginRapido("13012431-3", "inve019");
        tabSwitcher(Tabs.Inversiones);
        CPA0038ValidarQueSePuedeSalirDelFlujoDeEnrolamientoEnLaVentanaDeDatosDeSituacionTributariaDesdeLaX cpa0038ValidarQueSePuedeSalirDelFlujoDeEnrolamientoEnLaVentanaDeDatosDeSituacionTributariaDesdeLaX = new CPA0038ValidarQueSePuedeSalirDelFlujoDeEnrolamientoEnLaVentanaDeDatosDeSituacionTributariaDesdeLaX();
        cpa0038ValidarQueSePuedeSalirDelFlujoDeEnrolamientoEnLaVentanaDeDatosDeSituacionTributariaDesdeLaX.CPA0038ValidarQueSePuedeSalirDelFlujoDeEnrolamientoEnLaVentanaDeDatosDeSituacionTributariaDesdeLaX();
        PdfBciReports.closePDF();
    }

    @Test
    public void CPA0039ValidarQueSeListanTodosLosContratosConLaInformacionDelCliente() throws InterruptedException {
        setTestInstanceID("35068", "almInversiones.properties");
        SybaseInversiones sybaseInversiones = new SybaseInversiones();
        sybaseInversiones.resetearEnrolamiento("13342568-3");

        loginRapido("13342568-3", "inve019");
        tabSwitcher(Tabs.Inversiones);

        CPA0039ValidarQueSeListanTodosLosContratosConLaInformacionDelCliente cpa0039ValidarQueSeListanTodosLosContratosConLaInformacionDelCliente = new CPA0039ValidarQueSeListanTodosLosContratosConLaInformacionDelCliente();
        cpa0039ValidarQueSeListanTodosLosContratosConLaInformacionDelCliente.CPA0039ValidarQueSeListanTodosLosContratosConLaInformacionDelCliente();
        AccionesGenericas.esperar(30);
        sybaseInversiones.consultaEnrolamiento("13342568-3");

        PdfBciReports.closePDF();
    }

    @Test
    public void CPA0040ValidarQueSePuedeContinuarALaFirmaDeContratosLuegoDeRevisarLosMismos() throws InterruptedException {
        setTestInstanceID("35112", "almInversiones.properties");
        SybaseInversiones sybaseInversiones = new SybaseInversiones();
        sybaseInversiones.resetearEnrolamiento("12773650-2");

        loginRapido("12773650-2", "inve019");
        tabSwitcher(Tabs.Inversiones);
        CPA0040ValidarQueSePuedeContinuarALaFirmaDeContratosLuegoDeRevisarLosMismos cpa0040ValidarQueSePuedeContinuarALaFirmaDeContratosLuegoDeRevisarLosMismos = new CPA0040ValidarQueSePuedeContinuarALaFirmaDeContratosLuegoDeRevisarLosMismos();
        cpa0040ValidarQueSePuedeContinuarALaFirmaDeContratosLuegoDeRevisarLosMismos.CPA0040ValidarQueSePuedeContinuarALaFirmaDeContratosLuegoDeRevisarLosMismos();
        sybaseInversiones.consultaEnrolamiento("12773650-2");

        PdfBciReports.closePDF();
    }

    @Test
    public void CPA0041ValidarQueSePuedeContinuarEnLaVentanaDeDocumentosLuegoDeDarALaX(){
        setTestInstanceID("35069", "almInversiones.properties");
        loginRapido("12773650-2", "inve019");
        tabSwitcher(Tabs.Inversiones);
        CPA0041ValidarQueSePuedeContinuarEnLaVentanaDeDocumentosLuegoDeDarALaX cpa0041ValidarQueSePuedeContinuarEnLaVentanaDeDocumentosLuegoDeDarALaX = new CPA0041ValidarQueSePuedeContinuarEnLaVentanaDeDocumentosLuegoDeDarALaX();
        cpa0041ValidarQueSePuedeContinuarEnLaVentanaDeDocumentosLuegoDeDarALaX.CPA0041ValidarQueSePuedeContinuarEnLaVentanaDeDocumentosLuegoDeDarALaX();
        PdfBciReports.closePDF();
    }

    @Test
    public void CPA0042ValidarQueSePuedeSalirDelFlujoDeEnrolamientoEnLaVentanaDeDocumentosDesdeLaX() {
        setTestInstanceID("35072", "almInversiones.properties");
        loginRapido("12773650-2", "inve019");
        tabSwitcher(Tabs.Inversiones);
        CPA0042ValidarQueSePuedeSalirDelFlujoDeEnrolamientoEnLaVentanaDeDocumentosDesdeLaX cpa0042ValidarQueSePuedeSalirDelFlujoDeEnrolamientoEnLaVentanaDeDocumentosDesdeLaX = new CPA0042ValidarQueSePuedeSalirDelFlujoDeEnrolamientoEnLaVentanaDeDocumentosDesdeLaX();
        cpa0042ValidarQueSePuedeSalirDelFlujoDeEnrolamientoEnLaVentanaDeDocumentosDesdeLaX.CPA0042ValidarQueSePuedeSalirDelFlujoDeEnrolamientoEnLaVentanaDeDocumentosDesdeLaX();
        PdfBciReports.closePDF();
    }

    @Test
    public void CPA0043ValidarClientesNoPuedeAccederAlEnrolamiento() {
        setTestInstanceID("35073", "almInversiones.properties");
        loginRapido("10042323-5", "111222");
        tabSwitcher(Tabs.Inversiones);
        CPA0043ValidarClientesNoPuedeAccederAlEnrolamiento cpa0043ValidarClientesNoPuedeAccederAlEnrolamiento = new CPA0043ValidarClientesNoPuedeAccederAlEnrolamiento();
        cpa0043ValidarClientesNoPuedeAccederAlEnrolamiento.CPA0043ValidarClientesNoPuedeAccederAlEnrolamiento();
        PdfBciReports.closePDF();
    }

    @Test
    public void CPA0045ValidarQueSePuedeRechazarLaFirmaDeContratos() throws InterruptedException {
        SybaseInversiones sybaseInversiones = new SybaseInversiones();
        sybaseInversiones.resetearEnrolamiento("12773650-2");

        loginRapido("12773650-2", "inve019");
        tabSwitcher(Tabs.Inversiones);

        CPA0045ValidarQueSePuedeRechazarLaFirmaDeContratos cpa0045ValidarQueSePuedeRechazarLaFirmaDeContratos = new CPA0045ValidarQueSePuedeRechazarLaFirmaDeContratos();
        cpa0045ValidarQueSePuedeRechazarLaFirmaDeContratos.CPA0045ValidarQueSePuedeRechazarLaFirmaDeContratos();

        sybaseInversiones.consultaEnrolamiento("12773650-2");

        PdfBciReports.closePDF();
    }


    //**************************************
    //          FFMM
    //**************************************

    @Test
    public void CPA00010IncorporarDashboarddeInversionesConPerfilDeInversionistaEnApp() {
        setTestInstanceID("35075", "almInversiones.properties");
        loginRapido("13560049-0", "111222");
        tabSwitcher(Tabs.Inversiones);
        CPA00010IncorporarDashboarddeInversionesConPerfilDeInversionistaEnApp cpa00010IncorporarDashboarddeInversionesConPerfilDeInversionistaEnApp = new CPA00010IncorporarDashboarddeInversionesConPerfilDeInversionistaEnApp();
        cpa00010IncorporarDashboarddeInversionesConPerfilDeInversionistaEnApp.CPA00010IncorporarDashboarddeInversionesConPerfilDeInversionistaEnApp();
        PdfBciReports.closePDF();
    }


    @Test
    public void CPA00011IncorporarDashboardDeInversionesSinPerfilDeInversionistaEnApp() {
        setTestInstanceID("35074", "almInversiones.properties");
        SybaseInversiones sybaseInversiones = new SybaseInversiones();
        sybaseInversiones.resetearPerfilInversionista("13560049-0");

        loginRapido("13560049-0", "111222");
        tabSwitcher(Tabs.Inversiones);
        CPA00011IncorporarDashboardDeInversionesSinPerfilDeInversionistaEnApp cpa00011IncorporarDashboardDeInversionesSinPerfilDeInversionistaEnApp = new CPA00011IncorporarDashboardDeInversionesSinPerfilDeInversionistaEnApp();
        cpa00011IncorporarDashboardDeInversionesSinPerfilDeInversionistaEnApp.CPA00011IncorporarDashboardDeInversionesSinPerfilDeInversionistaEnApp();
        PdfBciReports.closePDF();
    }

    @Test
    public void CPA0005InvertirEnFFMMDesdeLaAsesoria() {
        setTestInstanceID("35076", "almInversiones.properties");
        loginRapido("135600490", "111222");
        tabSwitcher(Tabs.Inversiones);
        CPA0005InvertirEnFFMMDesdeLaAsesoria cpa0005InvertirEnFFMMDesdeLaAsesoria = new CPA0005InvertirEnFFMMDesdeLaAsesoria();
        cpa0005InvertirEnFFMMDesdeLaAsesoria.CPA0005InvertirEnFFMMDesdeLaAsesoria();
        PdfBciReports.closePDF();
    }

    @Test
    public void CPA0015InvertirEnFFMMDesdeLaAsesoriaParaClienteEnroladoYNoPerfiladoMVP2() {
        setTestInstanceID("35864", "almInversiones.properties");
        SybaseInversiones sybaseInversiones = new SybaseInversiones();
        sybaseInversiones.resetearPerfilInversionista("135600490");
        loginRapido("135600490", "111222");
        tabSwitcher(Tabs.Inversiones);
        CPA0015InvertirEnFFMMDesdeLaAsesoriaParaClienteEnroladoYNoPerfiladoMVP2 cpa0015InvertirEnFFMMDesdeLaAsesoriaParaClienteEnroladoYNoPerfiladoMVP2 = new CPA0015InvertirEnFFMMDesdeLaAsesoriaParaClienteEnroladoYNoPerfiladoMVP2();
        cpa0015InvertirEnFFMMDesdeLaAsesoriaParaClienteEnroladoYNoPerfiladoMVP2.CPA0015InvertirEnFFMMDesdeLaAsesoriaParaClienteEnroladoYNoPerfiladoMVP2();
        PdfBciReports.closePDF();
    }

    @Test
    public void CPA0025InvertirEnCarteraAsesoradaDesdeLaAsesoria() throws InterruptedException {
        setTestInstanceID("35078", "almInversiones.properties");
        loginRapido("135600490", "111222");
        tabSwitcher(Tabs.Inversiones);
        CPA0025InvertirEnCarteraAsesoradaDesdeLaAsesoria cpa0025InvertirEnCarteraAsesoradaDesdeLaAsesoria = new CPA0025InvertirEnCarteraAsesoradaDesdeLaAsesoria();
        cpa0025InvertirEnCarteraAsesoradaDesdeLaAsesoria.CPA0025InvertirEnCarteraAsesoradaDesdeLaAsesoria();
        PdfBciReports.closePDF();
    }

    @Test
    public void CPA0027AsesoriaDeClienteNoEnroladoYPerfilado() {
        setTestInstanceID("35879", "almInversiones.properties");

        loginRapido("10042323-5", "111222");
        tabSwitcher(Tabs.Inversiones);
        CPA0027AsesoriaDeClienteNoEnroladoYPerfilado cpa0027AsesoriaDeClienteNoEnroladoYPerfilado = new CPA0027AsesoriaDeClienteNoEnroladoYPerfilado();
        cpa0027AsesoriaDeClienteNoEnroladoYPerfilado.CPA0027AsesoriaDeClienteNoEnroladoYPerfilado();
        PdfBciReports.closePDF();
    }

    @Test
    public void CPA003FinalizaFlujoInvertirDeUnaCarteraDestacada() {
        setTestInstanceID("35080", "almInversiones.properties");
        loginRapido("17360130-1", "226872");
        CPA003FinalizaFlujoInvertirDeUnaCarteraDestacada cpa003FinalizaFlujoInvertirDeUnaCarteraDestacada = new CPA003FinalizaFlujoInvertirDeUnaCarteraDestacada();
        cpa003FinalizaFlujoInvertirDeUnaCarteraDestacada.CPA003FinalizaFlujoInvertirDeUnaCarteraDestacada();
        PdfBciReports.closePDF();
    }

    @Test
    public void CPA00003InvertirEnAppFFMMFlujoTres() throws Exception {
        setTestInstanceID("35081", "almInversiones.properties");
        loginRapido("13560049-0", "111222");
        tabSwitcher(Tabs.Inversiones);
        CPA00003InvertirEnAppFFMMFlujoTres cpa00003InvertirEnAppFFMMFlujoTres = new CPA00003InvertirEnAppFFMMFlujoTres();
        cpa00003InvertirEnAppFFMMFlujoTres.CPA00001InvertirAppFFMMFlujoTres();
        PdfBciReports.closePDF();
    }

    @Test
    public void CPA00004InvertirFueraDeHorarioApp() throws Exception {
        setTestInstanceID("35082", "almInversiones.properties");
        loginRapido("13560049-0", "111222");
        tabSwitcher(Tabs.Inversiones);
        CPA00004InvertirFueraDeHorarioApp cpa00004InvertirFueraDeHorarioApp = new CPA00004InvertirFueraDeHorarioApp();
        cpa00004InvertirFueraDeHorarioApp.CPA00004InvertirFueraDeHorarioApp();
        PdfBciReports.closePDF();
    }

    @Test
    public void CPA00011FirmarCGFENApp() throws Exception {
        setTestInstanceID("35083", "almInversiones.properties");
        loginRapido("14794564-7", "inve019");
        tabSwitcher(Tabs.Inversiones);
        CPA00011FirmarCGFENApp cpa00011FirmarCGFENApp = new CPA00011FirmarCGFENApp();
        cpa00011FirmarCGFENApp.CPA00011FirmarCGFENApp();
        PdfBciReports.closePDF();
    }

    @Test
    public void CPA00001PerfilarAClienteEnFFMM() throws Exception {
        setTestInstanceID("35084", "almInversiones.properties");
        SybaseInversiones sybaseInversiones = new SybaseInversiones();
        sybaseInversiones.resetearPerfilInversionista("13560049-0");

        loginRapido("135600490", "111222");
        tabSwitcher(Tabs.Inversiones);

        CPA00001PerfilarAClienteEnFFMM cpa00001PerfilarAClienteEnFFMM = new CPA00001PerfilarAClienteEnFFMM();
        cpa00001PerfilarAClienteEnFFMM.CPA00001PerfilarAClienteEnFFMM();
        PdfBciReports.closePDF();
    }

    @Test
    public void CPA00004DesplegarModalCuandoClienteNoSeEncuentraEnroladoEnApp() throws Exception {
        setTestInstanceID("35085", "almInversiones.properties");
        loginRapido("10042323-5", "111222");
        tabSwitcher(Tabs.Inversiones);
        CPA00004DesplegarModalCuandoClienteNoSeEncuentraEnroladoEnApp cpa00004DesplegarModalCuandoClienteNoSeEncuentraEnroladoEnApp = new CPA00004DesplegarModalCuandoClienteNoSeEncuentraEnroladoEnApp();
        cpa00004DesplegarModalCuandoClienteNoSeEncuentraEnroladoEnApp.CPA00004DesplegarModalCuandoClienteNoSeEncuentraEnroladoEnApp();
        PdfBciReports.closePDF();
    }

    @Test
    public void CPA00005DesplegarModalCuandoClienteNoPoseePerfilDeInversionistaEnApp() throws InterruptedException {
        setTestInstanceID("35872", "almInversiones.properties");
        SybaseInversiones sybaseInversiones = new SybaseInversiones();
        sybaseInversiones.resetearPerfilInversionista("13560049-0");
        loginRapido("13560049-0", "111222");
        tabSwitcher(Tabs.Inversiones);
        CPA00005DesplegarModalCuandoClienteNoPoseePerfilDeInversionistaEnApp cpa00005DesplegarModalCuandoClienteNoPoseePerfilDeInversionistaEnApp = new CPA00005DesplegarModalCuandoClienteNoPoseePerfilDeInversionistaEnApp();
        cpa00005DesplegarModalCuandoClienteNoPoseePerfilDeInversionistaEnApp.CPA00005DesplegarModalCuandoClienteNoPoseePerfilDeInversionistaEnApp();
        PdfBciReports.closePDF();
    }

    @Test
    public void CPA00006DesplegarModalCuandoClientePoseePerfilDeInversionistaMenorAPerfilDeFondoEnApp() throws InterruptedException {
        setTestInstanceID("35873", "almInversiones.properties");
        loginRapido("13560049-0", "111222");
        tabSwitcher(Tabs.Inversiones);

        CPA00006DesplegarModalCuandoClientePoseePerfilDeInversionistaMenorAPerfilDeFondoEnApp cpa00006DesplegarModalCuandoClientePoseePerfilDeInversionistaMenorAPerfilDeFondoEnApp = new CPA00006DesplegarModalCuandoClientePoseePerfilDeInversionistaMenorAPerfilDeFondoEnApp();
        cpa00006DesplegarModalCuandoClientePoseePerfilDeInversionistaMenorAPerfilDeFondoEnApp.CPA00006DesplegarModalCuandoClientePoseePerfilDeInversionistaMenorAPerfilDeFondoEnApp();
        PdfBciReports.closePDF();
    }

    @Test
    public void CPA00007DesplegarModalCuandoClienteSeEncuentreEnroladoPeroBloqueadoParaInvertirEnApp() throws InterruptedException {
        setTestInstanceID("35088", "almInversiones.properties");
        loginRapido("16729046-9", "789123");
        tabSwitcher(Tabs.Inversiones);
        CPA00007DesplegarModalCuandoClienteSeEncuentreEnroladoPeroBloqueadoParaInvertirEnApp cpa00007DesplegarModalCuandoClienteSeEncuentreEnroladoPeroBloqueadoParaInvertirEnApp = new CPA00007DesplegarModalCuandoClienteSeEncuentreEnroladoPeroBloqueadoParaInvertirEnApp();
        cpa00007DesplegarModalCuandoClienteSeEncuentreEnroladoPeroBloqueadoParaInvertirEnApp.CPA00007DesplegarModalCuandoClienteSeEncuentreEnroladoPeroBloqueadoParaInvertirEnApp();
        PdfBciReports.closePDF();
    }

    @Test
    public void CPA00008DesplegarModalCUnadoClienteNoPoseeSaldoDisponibleParaInvertirEnApp() throws InterruptedException {
        setTestInstanceID("35875", "almInversiones.properties");
        loginRapido("24609469-1", "226872");
        tabSwitcher(Tabs.Inversiones);
        CPA00008DesplegarModalCUnadoClienteNoPoseeSaldoDisponibleParaInvertirEnApp cpa00008DesplegarModalCUnadoClienteNoPoseeSaldoDisponibleParaInvertirEnApp = new CPA00008DesplegarModalCUnadoClienteNoPoseeSaldoDisponibleParaInvertirEnApp();
        cpa00008DesplegarModalCUnadoClienteNoPoseeSaldoDisponibleParaInvertirEnApp.CPA00008DesplegarModalCUnadoClienteNoPoseeSaldoDisponibleParaInvertirEnApp();
        PdfBciReports.closePDF();
    }


    //**************************************
    //          DAP
    //**************************************

    @Test
    public void CPA00001DesplegarBannerDAP() {
        setTestInstanceID("35090", "almInversiones.properties");
        loginRapido("17087834-5", "789123");
        CPA00001DesplegarBannerDAP cpa00001DesplegarBannerDAP = new CPA00001DesplegarBannerDAP();
        cpa00001DesplegarBannerDAP.CPA0001DesplegarBannerDAP();
        PdfBciReports.closePDF();
    }

    @Test
    public void CPA00002OperacionExitosaTomaDAPPlazoRenovable() throws Throwable {
        setTestInstanceID("35091", "almInversiones.properties");
        loginRapido("17546784K", "226872");
        tabSwitcher(Inversiones);
        CPA00002OperacionExitosaTomaDAPPlazoRenovable cpa00002OperacionExitosaTomaDAPPlazoRenovable = new CPA00002OperacionExitosaTomaDAPPlazoRenovable();
        cpa00002OperacionExitosaTomaDAPPlazoRenovable.CPA00002OperacionExitosaTomaDAPPlazoRenovable();
        PdfBciReports.closePDF();
    }

    @Test
    public void CPA00003OperacionExitosaTomaDAPPlazoFijo() {
        setTestInstanceID("35092", "almInversiones.properties");
        loginRapido("17546784K", "226872");
        tabSwitcher(Inversiones);
        CPA00003OperacionExitosaTomaDAPPlazoFijo cpa00003OperacionExitosaTomaDAPPlazoFijo = new CPA00003OperacionExitosaTomaDAPPlazoFijo();
        cpa00003OperacionExitosaTomaDAPPlazoFijo.CPA00003OperacionExitosaTomaDAPPlazoFijo();
        PdfBciReports.closePDF();
    }

    @Test
    public void CPA0007ResultadoDAPAjusteDiaHabil() {
        setTestInstanceID("35093", "almInversiones.properties");
        loginRapido("17546784K", "226872");
        tabSwitcher(Inversiones);
        CPA0007ResultadoDAPAjusteDiaHabil cpa0007ResultadoDAPAjusteDiaHabil = new CPA0007ResultadoDAPAjusteDiaHabil();
        cpa0007ResultadoDAPAjusteDiaHabil.CPA0007ResultadoDAPAjusteDiaHabil();
        PdfBciReports.closePDF();
    }

    @Test
    public void CPA00014EmailCorreoVacioCargadoPorDefecto() {
        setTestInstanceID("35877", "almInversiones.properties");
        loginRapido("17546784K", "226872");
        tabSwitcher(Inversiones);
        CPA00014EmailCorreoVacioCargadoPorDefecto cpa00014EmailCorreoVacioCargadoPorDefecto = new CPA00014EmailCorreoVacioCargadoPorDefecto();
        cpa00014EmailCorreoVacioCargadoPorDefecto.CPA00014EmailCorreoVacioCargadoPorDefecto();
        PdfBciReports.closePDF();
    }

    @Test
    public void CPA00012DesplegarModalAlTomarDAPFueraDeHorario() {
        setTestInstanceID("35095", "almInversiones.properties");
        loginRapido("135600490", "111222");
        tabSwitcher(Inversiones);
        CPA00012DesplegarModalAlTomarDAPFueraDeHorario cpa00012DesplegarModalAlTomarDAPFueraDeHorario = new CPA00012DesplegarModalAlTomarDAPFueraDeHorario();
        cpa00012DesplegarModalAlTomarDAPFueraDeHorario.CPA00012DesplegarModalAlTomarDAPFueraDeHorario();
        PdfBciReports.closePDF();
    }

}
