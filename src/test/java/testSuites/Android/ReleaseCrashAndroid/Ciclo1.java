package testSuites.Android.ReleaseCrashAndroid;

import bd.SybaseInversiones;
import constants.OS;
import constants.Tabs;
import driver.DriverContext;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import reporter.PdfBciReports;
import reporter.Reports;
import testClasses.DAP.OperacionExitosa.CPA00002OperacionExitosaTomaDAPPlazoRenovable;
import testClasses.DAP.OperacionExitosa.CPA00003OperacionExitosaTomaDAPPlazoFijo;
import testClasses.FFMM.Asesorar.CPA0005InvertirEnFFMMDesdeLaAsesoria;
import testClasses.FFMM.Asesorar.CPA0015InvertirEnFFMMDesdeLaAsesoriaParaClienteEnroladoYNoPerfiladoMVP2;
import testClasses.FFMM.Enrolamiento.CPA0045ValidarQueSePuedeRechazarLaFirmaDeContratos;
import testClasses.FFMM.Invertir.CPA00003InvertirEnAppFFMMFlujoTres;
import util.AccionesGenericas;

import static ALM.UploadEvidenceALM.setTestInstanceID;
import static ALM.UploadEvidenceALM.uploadALM;
import static constants.Tabs.Inversiones;
import static testbase.TestBase.loginRapido;
import static testbase.TestBase.tabSwitcher;

public class Ciclo1 {

    @BeforeMethod
    public void setUp() {
        PdfBciReports.createPDF();
        DriverContext.setUp(OS.ANDROID, "KPSDU18411000345", "Android", false, "app-qa");
    }

    @AfterMethod
    public void tearDown() {
        uploadALM();
        Reports.clean();
        DriverContext.quitDriver();
    }

    @Test
    public void CPA00002OperacionExitosaTomaDAPPlazoRenovable() throws Throwable {
        setTestInstanceID("35663", "almInversiones.properties");
        loginRapido("17546784K", "226872");
        tabSwitcher(Inversiones);
        CPA00002OperacionExitosaTomaDAPPlazoRenovable cpa00002OperacionExitosaTomaDAPPlazoRenovable = new CPA00002OperacionExitosaTomaDAPPlazoRenovable();
        cpa00002OperacionExitosaTomaDAPPlazoRenovable.CPA00002OperacionExitosaTomaDAPPlazoRenovable();
        PdfBciReports.closePDF();
    }

    @Test
    public void CPA00003OperacionExitosaTomaDAPPlazoFijo() {
        setTestInstanceID("35664", "almInversiones.properties");
        loginRapido("17546784K", "226872");
        tabSwitcher(Inversiones);
        CPA00003OperacionExitosaTomaDAPPlazoFijo cpa00003OperacionExitosaTomaDAPPlazoFijo = new CPA00003OperacionExitosaTomaDAPPlazoFijo();
        cpa00003OperacionExitosaTomaDAPPlazoFijo.CPA00003OperacionExitosaTomaDAPPlazoFijo();
        PdfBciReports.closePDF();
    }

    @Test
    public void CPA0005InvertirEnFFMMDesdeLaAsesoria() {
        setTestInstanceID("35693", "almInversiones.properties");
        loginRapido("135600490", "111222");
        tabSwitcher(Tabs.Inversiones);
        CPA0005InvertirEnFFMMDesdeLaAsesoria cpa0005InvertirEnFFMMDesdeLaAsesoria = new CPA0005InvertirEnFFMMDesdeLaAsesoria();
        cpa0005InvertirEnFFMMDesdeLaAsesoria.CPA0005InvertirEnFFMMDesdeLaAsesoria();
        PdfBciReports.closePDF();
    }

    @Test
    public void CPA0015InvertirEnFFMMDesdeLaAsesoriaParaClienteEnroladoYNoPerfiladoMVP2() {
        setTestInstanceID("35694", "almInversiones.properties");
        SybaseInversiones sybaseInversiones = new SybaseInversiones();
        sybaseInversiones.resetearPerfilInversionista("135600490");
        loginRapido("135600490", "111222");
        tabSwitcher(Tabs.Inversiones);
        CPA0015InvertirEnFFMMDesdeLaAsesoriaParaClienteEnroladoYNoPerfiladoMVP2 cpa0015InvertirEnFFMMDesdeLaAsesoriaParaClienteEnroladoYNoPerfiladoMVP2 = new CPA0015InvertirEnFFMMDesdeLaAsesoriaParaClienteEnroladoYNoPerfiladoMVP2();
        cpa0015InvertirEnFFMMDesdeLaAsesoriaParaClienteEnroladoYNoPerfiladoMVP2.CPA0015InvertirEnFFMMDesdeLaAsesoriaParaClienteEnroladoYNoPerfiladoMVP2();
        PdfBciReports.closePDF();
    }

    @Test
    public void CPA00003InvertirEnAppFFMMFlujoTres() throws Exception {
        setTestInstanceID("35748", "almInversiones.properties");
        loginRapido("13560049-0", "111222");
        tabSwitcher(Tabs.Inversiones);
        CPA00003InvertirEnAppFFMMFlujoTres cpa00003InvertirEnAppFFMMFlujoTres = new CPA00003InvertirEnAppFFMMFlujoTres();
        cpa00003InvertirEnAppFFMMFlujoTres.CPA00001InvertirAppFFMMFlujoTres();
        PdfBciReports.closePDF();
    }


    @Test
    public void CPA0045ValidarQueSePuedeRechazarLaFirmaDeContratos() throws InterruptedException {
        setTestInstanceID("35745", "almInversiones.properties");
        SybaseInversiones sybaseInversiones = new SybaseInversiones();
        sybaseInversiones.resetearEnrolamiento("13342568-3");

        loginRapido("13342568-3", "inve019");
        tabSwitcher(Tabs.Inversiones);

        CPA0045ValidarQueSePuedeRechazarLaFirmaDeContratos cpa0045ValidarQueSePuedeRechazarLaFirmaDeContratos = new CPA0045ValidarQueSePuedeRechazarLaFirmaDeContratos();
        cpa0045ValidarQueSePuedeRechazarLaFirmaDeContratos.CPA0045ValidarQueSePuedeRechazarLaFirmaDeContratos();
        AccionesGenericas.esperar(20);
        sybaseInversiones.consultaEnrolamiento("13342568-3");

        PdfBciReports.closePDF();
    }
}
