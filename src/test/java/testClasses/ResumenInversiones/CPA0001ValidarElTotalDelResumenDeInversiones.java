package testClasses.ResumenInversiones;

import org.testng.annotations.Test;
import pages.Dashboard.DashboardInversiones;

public class CPA0001ValidarElTotalDelResumenDeInversiones {

    @Test
    public void CPA0001ValidarElTotalDelResumenDeInversiones(){

        /********************************
         * DASHBOARD
         ********************************/
        DashboardInversiones dashboardInversiones = new DashboardInversiones();
        dashboardInversiones.validarIngresoPantalla();
        dashboardInversiones.validarElementosResumenInversiones();
        dashboardInversiones.validarElementosAlternativasInversion();
        dashboardInversiones.validarElementosAsesoria();
        dashboardInversiones.validarElementosPerfilamiento("ConPerfil");
        dashboardInversiones.tapPerfilamiento();
    }
}
