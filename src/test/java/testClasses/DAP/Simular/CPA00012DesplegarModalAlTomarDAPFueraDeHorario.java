package testClasses.DAP.Simular;

import org.testng.annotations.Test;
import pages.Dashboard.DashboardInversiones;
import pages.Modales.DAPFueraHorario;

public class CPA00012DesplegarModalAlTomarDAPFueraDeHorario {
    @Test
    public void CPA00012DesplegarModalAlTomarDAPFueraDeHorario() {


        /********************************
         * DASHBOARD
         ********************************/
        DashboardInversiones dashboardInversiones = new DashboardInversiones();
        dashboardInversiones.validarIngresoPantalla();
        dashboardInversiones.validarElementosPerfilamiento("ConPerfil");
        dashboardInversiones.validarElementosAlternativasInversion();
        dashboardInversiones.validarElementosAsesoria();
        dashboardInversiones.tapDAP();

        /********************************
         * MODAL DAP FUERA HORARIO
         ********************************/

        DAPFueraHorario dapFueraHorario = new DAPFueraHorario();
        dapFueraHorario.validarElementos();
        dapFueraHorario.validarIngresoPantalla();
        dapFueraHorario.tapBotonEntendido();

        /********************************
         * DASHBOARD
         ********************************/
        dashboardInversiones.validarIngresoPantalla();
        dashboardInversiones.validarElementosPerfilamiento("ConPerfil");
        dashboardInversiones.validarElementosAlternativasInversion();
        dashboardInversiones.validarElementosAsesoria();
    }
}
