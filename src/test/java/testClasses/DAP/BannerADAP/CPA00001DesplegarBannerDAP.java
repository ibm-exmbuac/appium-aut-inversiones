package testClasses.DAP.BannerADAP;

import org.testng.annotations.Test;
import pages.DAP.CampaniaDAP;
import pages.DAP.IngresoMonedaMontoDAP;
import testbase.TestBase;

public class CPA00001DesplegarBannerDAP extends TestBase {

    @Test
    public void CPA0001DesplegarBannerDAP() {

        /********************************
         * BANNER DAP
         ********************************/
        CampaniaDAP campaniaDAP = new CampaniaDAP();
        campaniaDAP.validarDespliegueCampania();
        campaniaDAP.tapBtnSimular();

        /********************************
         * MONEDA Y MONTO DAP
         ********************************/
        IngresoMonedaMontoDAP ingresoMonedaMontoDAP = new IngresoMonedaMontoDAP();
        ingresoMonedaMontoDAP.validaIngresoPantalla();
    }
}
