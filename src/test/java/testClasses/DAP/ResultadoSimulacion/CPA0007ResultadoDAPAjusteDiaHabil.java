package testClasses.DAP.ResultadoSimulacion;

import org.testng.annotations.Test;
import pages.DAP.*;
import pages.Dashboard.DashboardInversiones;
import testbase.TestBase;

public class CPA0007ResultadoDAPAjusteDiaHabil extends TestBase {

    @Test
    public void CPA0007ResultadoDAPAjusteDiaHabil() {


        /********************************
         * DASHBOARD
         ********************************/
        DashboardInversiones dashboardInversiones = new DashboardInversiones();
        dashboardInversiones.validarIngresoPantalla();
        dashboardInversiones.validarElementosPerfilamiento("ConPerfil");
        dashboardInversiones.validarElementosAlternativasInversion();
        dashboardInversiones.validarElementosAsesoria();
        dashboardInversiones.tapDAP();


        /******++************************
         * MONEDA Y MONTO DAP
         ********************************/

        IngresoMonedaMontoDAP ingresoMonedaMontoDAP = new IngresoMonedaMontoDAP();
        ingresoMonedaMontoDAP.validaIngresoPantalla();
        ingresoMonedaMontoDAP.validaElementos();
        ingresoMonedaMontoDAP.ingresoMonto("5000");
        ingresoMonedaMontoDAP.tapContinuar();


        /******++************************
         * PLAZO Y TIPO DAP
         ********************************/

        IngresoPlazoTipoDAP ingresoPlazoTipoDAP = new IngresoPlazoTipoDAP();
        ingresoPlazoTipoDAP.validaElementos();
        ingresoPlazoTipoDAP.ingresoPlazoDiaNoHabil();
        ingresoPlazoTipoDAP.seleccionarTipoDAP("Renovable");
        ingresoPlazoTipoDAP.tapSimular();

        /******++************************
         * RESULTADO SIMULACIÓN DETALLE
         ********************************/

        ResultadoSimulacionDetalle resultadoSimulacionDetalle = new ResultadoSimulacionDetalle();
        resultadoSimulacionDetalle.validaIngresoPantalla();
        resultadoSimulacionDetalle.validaElementos();
        resultadoSimulacionDetalle.validarAjusteDiaHabil();

        /******++************************
         * RESULTADO SIMULACIÓN CUENTAS
         ********************************/

        ResultadoSimulacionCuentas resultadoSimulacionCuentas = new ResultadoSimulacionCuentas();
        resultadoSimulacionCuentas.validarElementos();

        /******++************************
         * RESULTADO SIMULACIÓN COMPROBANTE
         ********************************/

        ResultadoSimulacionComprobante resultadoSimulacionComprobante = new ResultadoSimulacionComprobante();
        resultadoSimulacionComprobante.validarElementos();
        resultadoSimulacionComprobante.ingresarCorreo("test@test.cl");

        /******++************************
         * RESULTADO SIMULACIÓN TÉRMINOS Y CONDICIONES
         ********************************/

        ResultadoSimulacionTYC resultadoSimulacionTYC = new ResultadoSimulacionTYC();
        resultadoSimulacionTYC.validaElementos();

        /******++************************
         * RESULTADO SIMULACIÓN INVERTIR / VOLVER A SIMULAR
         ********************************/

        ResultadoSimulacionInvertirSimular resultadoSimulacionInvertirSimular = new ResultadoSimulacionInvertirSimular();
        resultadoSimulacionInvertirSimular.validarElementos();
    }
}
