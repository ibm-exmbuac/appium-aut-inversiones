package testClasses.DAP.ResultadoSimulacion;


import org.testng.annotations.Test;
import pages.DAP.*;
import pages.Dashboard.DashboardInversiones;
import testbase.TestBase;

public class CPA00014EmailCorreoVacioCargadoPorDefecto extends TestBase {


    @Test
    public void CPA00014EmailCorreoVacioCargadoPorDefecto() {

        /********************************
         * DASHBOARD
         ********************************/
        DashboardInversiones dashboardInversiones = new DashboardInversiones();
        dashboardInversiones.validarIngresoPantalla();
        dashboardInversiones.validarElementosPerfilamiento("ConPerfil");
        dashboardInversiones.validarElementosAlternativasInversion();
        dashboardInversiones.validarElementosAsesoria();
        dashboardInversiones.tapDAP();

        /******++************************
         * MONEDA Y MONTO DAP
         ********************************/

        IngresoMonedaMontoDAP ingresoMonedaMontoDAP = new IngresoMonedaMontoDAP();
        ingresoMonedaMontoDAP.validaIngresoPantalla();
        ingresoMonedaMontoDAP.validaElementos();
        ingresoMonedaMontoDAP.ingresoMonto("10000");
        ingresoMonedaMontoDAP.tapContinuar();

        /******++************************
         * PLAZO Y TIPO DAP
         ********************************/

        IngresoPlazoTipoDAP ingresoPlazoTipoDAP = new IngresoPlazoTipoDAP();
        ingresoPlazoTipoDAP.validaElementos();
        ingresoPlazoTipoDAP.ingresoPlazo("13");
        ingresoPlazoTipoDAP.seleccionarTipoDAP("Renovable");
        ingresoPlazoTipoDAP.tapSimular();

        /******++************************
         * RESULTADO SIMULACIÓN DETALLE
         ********************************/

        ResultadoSimulacionDetalle resultadoSimulacionDetalle = new ResultadoSimulacionDetalle();
        resultadoSimulacionDetalle.validaIngresoPantalla();
        resultadoSimulacionDetalle.validaElementos();

        /******++************************
         * RESULTADO SIMULACIÓN CUENTAS
         ********************************/

        ResultadoSimulacionCuentas resultadoSimulacionCuentas = new ResultadoSimulacionCuentas();
        resultadoSimulacionCuentas.validarElementos();

        /******++************************
         * RESULTADO SIMULACIÓN COMPROBANTE
         ********************************/

        ResultadoSimulacionComprobante resultadoSimulacionComprobante = new ResultadoSimulacionComprobante();
        resultadoSimulacionComprobante.validarElementos();
        resultadoSimulacionComprobante.validarCorreoVacio();
        resultadoSimulacionComprobante.ingresarCorreo("test@test.cl");

        /******++************************
         * RESULTADO SIMULACIÓN TÉRMINOS Y CONDICIONES
         ********************************/

        ResultadoSimulacionTYC resultadoSimulacionTYC = new ResultadoSimulacionTYC();
        resultadoSimulacionTYC.validaElementos();

        /******++************************
         * RESULTADO SIMULACIÓN INVERTIR / VOLVER A SIMULAR
         ********************************/

        ResultadoSimulacionInvertirSimular resultadoSimulacionInvertirSimular = new ResultadoSimulacionInvertirSimular();
        resultadoSimulacionInvertirSimular.validarElementos();
    }
}
