package testClasses.DAP.ResultadoSimulacion;

import org.testng.annotations.Test;
import pages.DAP.*;
import pages.Dashboard.DashboardInversiones;
import testbase.TestBase;
import util.Gestures;

import static constants.Tabs.Inversiones;

public class CPA00006ResultadoSimulacionTerminosYCondiciones extends TestBase {

    @Test
    public void CPA00006ResultadoSimulacionTerminosYCondiciones(boolean smokeTest) {

        Gestures ges = new Gestures();

        /********************************
         * LOGIN
         ********************************/
        loginRapido("135600490", "111222");
        tabSwitcher(Inversiones);

        /********************************
         * DASHBOARD
         ********************************/
        DashboardInversiones dashboardInversiones = new DashboardInversiones();
        dashboardInversiones.validarIngresoPantalla();
        dashboardInversiones.validarElementosPerfilamiento("ConPerfil");
        dashboardInversiones.validarElementosAlternativasInversion();
        dashboardInversiones.validarElementosAsesoria();
        dashboardInversiones.tapDAP();

        /******++************************
         * MONEDA Y MONTO DAP
         ********************************/

        IngresoMonedaMontoDAP ingresoMonedaMontoDAP = new IngresoMonedaMontoDAP();
        ingresoMonedaMontoDAP.validaIngresoPantalla();
        ingresoMonedaMontoDAP.validaElementos();
        ingresoMonedaMontoDAP.ingresoMonto("10000");
        ingresoMonedaMontoDAP.tapContinuar();


        /******++************************
         * PLAZO Y TIPO DAP
         ********************************/

        IngresoPlazoTipoDAP ingresoPlazoTipoDAP = new IngresoPlazoTipoDAP();
        ingresoPlazoTipoDAP.validaElementos();
        ingresoPlazoTipoDAP.ingresoPlazo("13");
        ingresoPlazoTipoDAP.tapSimular();

        /******++************************
         * RESULTADO SIMULACIÓN DETALLE
         ********************************/

        ResultadoSimulacionDetalle resultadoSimulacionDetalle = new ResultadoSimulacionDetalle();
        resultadoSimulacionDetalle.validaIngresoPantalla();
        resultadoSimulacionDetalle.validaElementos();
        ges.scrollAbajoCorto();

        /******++************************
         * RESULTADO SIMULACIÓN CUENTAS
         ********************************/

        ResultadoSimulacionCuentas resultadoSimulacionCuentas = new ResultadoSimulacionCuentas();
        resultadoSimulacionCuentas.validarElementos();
        ges.scrollAbajo();

        /******++************************
         * RESULTADO SIMULACIÓN COMPROBANTE
         ********************************/

        ResultadoSimulacionComprobante resultadoSimulacionComprobante = new ResultadoSimulacionComprobante();
        resultadoSimulacionComprobante.validarElementos();
        resultadoSimulacionComprobante.ingresarCorreo("test@test.cl");
        ges.scrollAbajo();

        /******++************************
         * RESULTADO SIMULACIÓN TÉRMINOS Y CONDICIONES
         ********************************/

        ResultadoSimulacionTYC resultadoSimulacionTYC = new ResultadoSimulacionTYC();
        resultadoSimulacionTYC.validaElementos();
        resultadoSimulacionTYC.validaTerminosYCondiciones();

        /******++************************
         * RESULTADO SIMULACIÓN INVERTIR / VOLVER A SIMULAR
         ********************************/

        ResultadoSimulacionInvertirSimular resultadoSimulacionInvertirSimular = new ResultadoSimulacionInvertirSimular();
        resultadoSimulacionInvertirSimular.validarElementos();
    }
}
