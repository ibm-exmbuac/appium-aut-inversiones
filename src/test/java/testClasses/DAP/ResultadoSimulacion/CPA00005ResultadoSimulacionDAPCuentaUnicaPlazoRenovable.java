package testClasses.DAP.ResultadoSimulacion;

import org.testng.annotations.Test;
import pages.DAP.*;
import pages.Dashboard.DashboardInversiones;
import util.Gestures;

import static constants.Tabs.Inversiones;
import static testbase.TestBase.loginRapido;
import static testbase.TestBase.tabSwitcher;

public class CPA00005ResultadoSimulacionDAPCuentaUnicaPlazoRenovable {

    @Test
    public void CPA00005ResultadoSimulacionDAPCuentaUnicaPlazoRenovable(boolean smokeTest) {

        Gestures ges = new Gestures();

        /********************************
         * LOGIN
         ********************************/
        loginRapido("135600490", "111222");
        tabSwitcher(Inversiones);

        /********************************
         * DASHBOARD
         ********************************/
        DashboardInversiones dashboardInversiones = new DashboardInversiones();
        dashboardInversiones.validarIngresoPantalla();
        dashboardInversiones.validarElementosPerfilamiento("ConPerfil");
        dashboardInversiones.validarElementosAlternativasInversion();
        dashboardInversiones.validarElementosAsesoria();
        dashboardInversiones.tapDAP();

        /******++************************
         * MONEDA Y MONTO DAP
         ********************************/

        IngresoMonedaMontoDAP ingresoMonedaMontoDAP = new IngresoMonedaMontoDAP();
        ingresoMonedaMontoDAP.validaIngresoPantalla();
        ingresoMonedaMontoDAP.validaElementos();
        ingresoMonedaMontoDAP.ingresoMonto("10000");
        ingresoMonedaMontoDAP.tapContinuar();


        /******++************************
         * PLAZO Y TIPO DAP
         ********************************/

        IngresoPlazoTipoDAP ingresoPlazoTipoDAP = new IngresoPlazoTipoDAP();
        ingresoPlazoTipoDAP.validaElementos();
        ingresoPlazoTipoDAP.ingresoPlazo("13");
        ingresoPlazoTipoDAP.seleccionarTipoDAP("Renovable");
        ingresoPlazoTipoDAP.tapSimular();

        /******++************************
         * RESULTADO SIMULACIÓN DETALLE
         ********************************/

        ResultadoSimulacionDetalle resultadoSimulacionDetalle = new ResultadoSimulacionDetalle();
        resultadoSimulacionDetalle.validaIngresoPantalla();
        resultadoSimulacionDetalle.validaElementos();
        ges.scrollAbajo();

        /******++************************
         * RESULTADO SIMULACIÓN CUENTAS
         ********************************/

        ResultadoSimulacionCuentas resultadoSimulacionCuentas = new ResultadoSimulacionCuentas();
        resultadoSimulacionCuentas.validarElementos();
        ges.scrollAbajo();

        /******++************************
         * RESULTADO SIMULACIÓN COMPROBANTE
         ********************************/

        ResultadoSimulacionComprobante resultadoSimulacionComprobante = new ResultadoSimulacionComprobante();
        resultadoSimulacionComprobante.validarElementos();
        resultadoSimulacionComprobante.ingresarCorreo("test@test.cl");
        ges.scrollAbajo();

        /******++************************
         * RESULTADO SIMULACIÓN TÉRMINOS Y CONDICIONES
         ********************************/

        ResultadoSimulacionTYC resultadoSimulacionTYC = new ResultadoSimulacionTYC();
        resultadoSimulacionTYC.validaElementos();

        /******++************************
         * RESULTADO SIMULACIÓN INVERTIR / VOLVER A SIMULAR
         ********************************/

        ResultadoSimulacionInvertirSimular resultadoSimulacionInvertirSimular = new ResultadoSimulacionInvertirSimular();
        resultadoSimulacionInvertirSimular.validarElementos();
    }
}
