package testClasses.DAP.OperacionExitosa;

import org.testng.annotations.Test;
import pages.DAP.*;
import pages.Dashboard.DashboardInversiones;


public class CPA00002OperacionExitosaTomaDAPPlazoRenovable {

    @Test
    public void CPA00002OperacionExitosaTomaDAPPlazoRenovable() throws Throwable {

        /********************************
         * DASHBOARD
         ********************************/
        DashboardInversiones dashboardInversiones = new DashboardInversiones();
        dashboardInversiones.validarIngresoPantalla();
        dashboardInversiones.validarElementosPerfilamiento("ConPerfil");
        dashboardInversiones.validarElementosAlternativasInversion();
        dashboardInversiones.validarElementosAsesoria();
        dashboardInversiones.tapDAP();

        /********************************
         * MONEDA Y MONTO DAP
         ********************************/

        IngresoMonedaMontoDAP ingresoMonedaMontoDAP = new IngresoMonedaMontoDAP();
        ingresoMonedaMontoDAP.validaIngresoPantalla();
        ingresoMonedaMontoDAP.validaElementos();
        ingresoMonedaMontoDAP.ingresoMonto("10000");
        ingresoMonedaMontoDAP.tapContinuar();


        /********************************
         * PLAZO Y TIPO DAP
         ********************************/
        IngresoPlazoTipoDAP ingresoPlazoTipoDAP = new IngresoPlazoTipoDAP();
        ingresoPlazoTipoDAP.validaElementos();
        ingresoPlazoTipoDAP.ingresoPlazo("34");
        ingresoPlazoTipoDAP.seleccionarTipoDAP("Renovable");
        ingresoPlazoTipoDAP.tapSimular();

        /******++************************
         * RESULTADO SIMULACIÓN DETALLE
         ********************************/

        ResultadoSimulacionDetalle resultadoSimulacionDetalle = new ResultadoSimulacionDetalle();
        resultadoSimulacionDetalle.validaIngresoPantalla();
        resultadoSimulacionDetalle.validaElementos();

        /******++************************
         * RESULTADO SIMULACIÓN CUENTAS
         ********************************/

        ResultadoSimulacionCuentas resultadoSimulacionCuentas = new ResultadoSimulacionCuentas();
        resultadoSimulacionCuentas.validarElementos();

        /******++************************
         * RESULTADO SIMULACIÓN COMPROBANTE
         ********************************/

        ResultadoSimulacionComprobante resultadoSimulacionComprobante = new ResultadoSimulacionComprobante();
        resultadoSimulacionComprobante.validarElementos();
        resultadoSimulacionComprobante.ingresarCorreo("test@test.cl");

        /******++************************
         * RESULTADO SIMULACIÓN TÉRMINOS Y CONDICIONES
         ********************************/

        ResultadoSimulacionTYC resultadoSimulacionTYC = new ResultadoSimulacionTYC();
        resultadoSimulacionTYC.validaElementos();

        /******++************************
         * RESULTADO SIMULACIÓN INVERTIR / VOLVER A SIMULAR
         ********************************/
        ResultadoSimulacionInvertirSimular resultadoSimulacionInvertirSimular = new ResultadoSimulacionInvertirSimular();
        resultadoSimulacionInvertirSimular.validarElementos();
        resultadoSimulacionInvertirSimular.tapInvertir();

        /******++************************
         * OPERACIÓN EXITOSA
         ********************************/
        OperacionExitosa operacionExitosa = new OperacionExitosa();
        operacionExitosa.validarIngresoPantalla();
        operacionExitosa.validarElementos();
//        operacionExitosa.validarTipoDeposito("Renovable");
        operacionExitosa.tapFinalizar();

    }
}