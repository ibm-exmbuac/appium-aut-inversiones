package testClasses.FFMM.Enrolamiento;

import driver.DriverContext;
import io.appium.java_client.ios.IOSDriver;
import org.testng.annotations.Test;
import pages.Dashboard.DashboardInversiones;
import pages.Enrolamiento.DatosPersonales;

public class CPA0005VerificarClienteConDobleNacionalidad {

    @Test
    public void CPA0005VerificarClienteConDobleNacionalidad() {

        /********************************
         * DASHBOARD
         ********************************/
        DashboardInversiones dashboardInversiones = new DashboardInversiones();
        dashboardInversiones.validarIngresoPantalla();
        dashboardInversiones.validarElementosEnrolamiento();
        dashboardInversiones.validarElementosPerfilamiento("ConPerfil");
        dashboardInversiones.validarElementosAlternativasInversion();
        dashboardInversiones.validarElementosAsesoria();
        dashboardInversiones.tapEnrolamiento();

        /********************************
         * DATOS PERSONALES
         ********************************/
        DatosPersonales datosPersonales = new DatosPersonales();
        datosPersonales.validarIngresoPantalla();
        datosPersonales.validarElementosSuperiores();
        datosPersonales.validarSelectorNacionalidad();
        datosPersonales.validarRdoPaisResidencia();
        if (DriverContext.getDriver() instanceof IOSDriver){
            datosPersonales.validarRdoDobleNacionalidad();
            datosPersonales.seleccionarRdoDobleNacionalidad("Si");
            datosPersonales.validarSelectorDobleNacionalidad();
            datosPersonales.validarSelectorEstCivil();
            datosPersonales.validarBotonContinuar();
        }else{
            datosPersonales.validarSelectorPaisResidencia();
            datosPersonales.validarRdoDobleNacionalidad();
            datosPersonales.seleccionarRdoDobleNacionalidad("Si");
            datosPersonales.validarSelectorDobleNacionalidad();
            datosPersonales.validarInputIDDobleNac();
            datosPersonales.validarSelectorEstCivil();
            datosPersonales.validarBotonContinuar();
        }

    }
}
