package testClasses.FFMM.Enrolamiento;

import org.testng.annotations.Test;
import pages.Dashboard.DashboardInversiones;

public class CPA0001VerificarQueSeMuestreElBotonDeFirmaDeContratos {

    @Test
    public void CPA0001VerificarQueSeMuestreElBotonDeFirmaDeContratos() {

        /********************************
         * DASHBOARD
         ********************************/
        DashboardInversiones dashboardInversiones = new DashboardInversiones();
        dashboardInversiones.validarIngresoPantalla();
        dashboardInversiones.validarElementosEnrolamiento();
        dashboardInversiones.validarElementosPerfilamiento("ConPerfil");
        dashboardInversiones.validarElementosAlternativasInversion();
        dashboardInversiones.validarElementosAsesoria();

    }
}
