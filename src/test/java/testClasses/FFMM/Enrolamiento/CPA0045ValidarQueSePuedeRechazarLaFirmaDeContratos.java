package testClasses.FFMM.Enrolamiento;

import org.testng.annotations.Test;
import pages.BciPass.ContratosFFMMEnrolamiento;
import pages.Dashboard.DashboardInversiones;
import pages.Enrolamiento.*;
import pages.Modales.ModalSalirEnrolamiento;
import util.AccionesGenericas;

public class CPA0045ValidarQueSePuedeRechazarLaFirmaDeContratos {

    @Test
    public void CPA0045ValidarQueSePuedeRechazarLaFirmaDeContratos() throws InterruptedException {

        /********************************
         * DASHBOARD
         ********************************/
        DashboardInversiones dashboardInversiones = new DashboardInversiones();
        dashboardInversiones.validarIngresoPantalla();
        dashboardInversiones.validarElementosEnrolamiento();
        dashboardInversiones.validarElementosPerfilamiento("ConPerfil");
        dashboardInversiones.validarElementosAlternativasInversion();
        dashboardInversiones.validarElementosAsesoria();
        dashboardInversiones.tapEnrolamiento();

        /********************************
         * DATOS PERSONALES
         ********************************/
        DatosPersonales datosPersonales = new DatosPersonales();
        datosPersonales.validarIngresoPantalla();
        datosPersonales.validarElementosSuperiores();
        datosPersonales.validarSelectorNacionalidad();
        datosPersonales.validarRdoPaisResidencia();
        datosPersonales.validarSelectorPaisResidencia();
        datosPersonales.validarRdoDobleNacionalidad();
        datosPersonales.validarSelectorDobleNacionalidad();
        datosPersonales.validarInputIDDobleNac();
        datosPersonales.validarSelectorEstCivil();
        datosPersonales.validarBotonContinuar();
        datosPersonales.tapBotonContinuar();

        /********************************
         * DATOS CONTACTO
         ********************************/
        DatosContacto datosContacto = new DatosContacto();
        datosContacto.validarIngresoPantalla();
        datosContacto.validarElementosSuperiores();
        datosContacto.validarInputDireccion();
        datosContacto.validarSelectorRegion();
        datosContacto.validarSelectorComuna();
        datosContacto.validarInputTelefono();
        datosContacto.ingresarTelefono("912345678");
        datosContacto.validarInputEmail();
        datosContacto.ingresarEmail("test@test.cl");
        datosContacto.validarElementosInferiores();
        datosContacto.tapBotonContinuar();

        /********************************
         * DATOS LABORALES
         ********************************/
        DatosLaborales datosLaborales = new DatosLaborales();
        datosLaborales.validarIngresoPantalla();
        datosLaborales.validarElementosSuperiores();
        datosLaborales.validarSelectorActividad();
        datosLaborales.seleccionarActividad("Jubilado(a)");
        datosLaborales.validarOpcionesActividades();
        datosLaborales.tapBotonContinuar();

        /********************************
         * INGRESOS PATRIMONIO
         ********************************/
        IngresosPatrimonios ingresosPatrimonios = new IngresosPatrimonios();
        ingresosPatrimonios.validarIngresoPantalla();
        ingresosPatrimonios.validarElemtosSuperiores();
        ingresosPatrimonios.validarOrigenFondos();
        ingresosPatrimonios.seleccionarOrigenFondos("Herencia");
        ingresosPatrimonios.validarSelectorPaisOrigenFF();
        ingresosPatrimonios.seleccionarPaisOrigenFF("CHILE");
        ingresosPatrimonios.validarSelectorIngresosMensuales();
        ingresosPatrimonios.validarSelectorIngresosOcasionales();
        ingresosPatrimonios.validarSelectorPatriminioFinanciero();
        ingresosPatrimonios.validarSelectorPatriminioNoFinanciero();
        ingresosPatrimonios.validarBotonContinuar();
        ingresosPatrimonios.tapBotonContinuar();

        /********************************
         * SITUACION TRIBUTARIA
         ********************************/
        SituacionTributaria situacionTributaria = new SituacionTributaria();
        situacionTributaria.validarIngresoPantalla();
        situacionTributaria.validarElementosSuperiores();
        situacionTributaria.validarRdosUSPerson();
        situacionTributaria.seleccionarUSPerson("No");
        situacionTributaria.validarRdosFondosUSPerson();
        situacionTributaria.seleccionarFondosUSPerson("No");
        situacionTributaria.validarRdosPEP();
        situacionTributaria.seleccionarPEP("No");
        situacionTributaria.validarSwitchImpuestos();
        situacionTributaria.validarTYC();
        situacionTributaria.checkTYC();
        situacionTributaria.validarBtnContinuar();
        situacionTributaria.tapBtnContinuar();

        /********************************
         * CONTRATOS
         ********************************/
        Contratos contratos = new Contratos();
        contratos.validarIngresoPantalla();
        contratos.validarElementosSuperiores();
        contratos.validarListadoContratos();
        contratos.validarTYC();
        contratos.checkTYC();
        contratos.validarBtnContinuar();
        contratos.tapBotonContinuar();

        /********************************
         * BCIPASS FIRMA CONTRATOS
         ********************************/
        AccionesGenericas.esperar(10);
        ContratosFFMMEnrolamiento contratosFFMMEnrolamiento = new ContratosFFMMEnrolamiento();
        contratosFFMMEnrolamiento.validarIngresoPantalla();
        contratosFFMMEnrolamiento.validarElemetos();
        contratosFFMMEnrolamiento.ingresarPass("789123");
        contratosFFMMEnrolamiento.tapBtnRechazar();
        contratosFFMMEnrolamiento.validarModal();
        contratosFFMMEnrolamiento.tapSiModal();

        /********************************
         * CONTRATOS
         ********************************/
        AccionesGenericas.esperar(10);
        contratos.validarIngresoPantalla();
        contratos.validarElementosSuperiores();
        contratos.validarListadoContratos();
        contratos.validarTYC();
        contratos.checkTYC();
        contratos.validarBtnContinuar();
        contratos.tapBotonCerrar();

        /********************************
         * MODAL SALIR ENROLAMIENTO
         ********************************/
        ModalSalirEnrolamiento modalSalirEnrolamiento = new ModalSalirEnrolamiento();
        modalSalirEnrolamiento.validarIngresoModal();
        modalSalirEnrolamiento.validarElementosModal();
        modalSalirEnrolamiento.tapBotonSalir();

        /********************************
         * DASHBOARD
         ********************************/
        dashboardInversiones.validarIngresoPantalla();
        dashboardInversiones.validarElementosPerfilamiento("ConPerfil");
        dashboardInversiones.validarElementosAlternativasInversion();
        dashboardInversiones.validarElementosAsesoria();
    }
}
