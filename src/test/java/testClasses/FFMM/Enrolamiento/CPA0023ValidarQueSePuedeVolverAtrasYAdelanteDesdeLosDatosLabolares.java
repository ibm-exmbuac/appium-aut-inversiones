package testClasses.FFMM.Enrolamiento;

import driver.DriverContext;
import io.appium.java_client.ios.IOSDriver;
import org.testng.annotations.Test;
import pages.Dashboard.DashboardInversiones;
import pages.Enrolamiento.DatosContacto;
import pages.Enrolamiento.DatosLaborales;
import pages.Enrolamiento.DatosPersonales;
import util.Gestures;

public class CPA0023ValidarQueSePuedeVolverAtrasYAdelanteDesdeLosDatosLabolares {

    @Test
    public void CPA0023ValidarQueSePuedeVolverAtrasYAdelanteDesdeLosDatosLabolares() {

        Gestures gestures = new Gestures();

        /********************************
         * DASHBOARD
         ********************************/
        DashboardInversiones dashboardInversiones = new DashboardInversiones();
        dashboardInversiones.validarIngresoPantalla();
        dashboardInversiones.validarElementosEnrolamiento();
        dashboardInversiones.validarElementosPerfilamiento("ConPerfil");
        dashboardInversiones.validarElementosAlternativasInversion();
        dashboardInversiones.validarElementosAsesoria();
        dashboardInversiones.tapEnrolamiento();

        /********************************
         * DATOS PERSONALES
         ********************************/
        DatosPersonales datosPersonales = new DatosPersonales();
        datosPersonales.validarIngresoPantalla();
        datosPersonales.validarElementosSuperiores();
        datosPersonales.validarSelectorNacionalidad();
        datosPersonales.validarRdoPaisResidencia();
        datosPersonales.seleccionarRdoPaisResidencia("OtroPais");
        datosPersonales.validarSelectorPaisResidencia();
        datosPersonales.validarRdoDobleNacionalidad();
        if (DriverContext.getDriver() instanceof IOSDriver) {
            datosPersonales.validarSelectorEstCivil();
            datosPersonales.validarBotonContinuar();
        }else{
            datosPersonales.validarSelectorDobleNacionalidad();
            datosPersonales.validarInputIDDobleNac();
            datosPersonales.validarSelectorEstCivil();
            datosPersonales.validarBotonContinuar();
        }
        datosPersonales.tapBotonContinuar();

        /********************************
         * DATOS CONTACTO
         ********************************/
        DatosContacto datosContacto = new DatosContacto();
        datosContacto.validarIngresoPantalla();
        datosContacto.validarElementosSuperiores();
        datosContacto.validarInputDireccion();
        datosContacto.validarSelectorRegion();
        datosContacto.validarSelectorComuna();
        datosContacto.validarInputTelefono();
        datosContacto.ingresarTelefono("912345678");
        datosContacto.validarInputEmail();
        datosContacto.ingresarEmail("test@test.cl");
        datosContacto.validarElementosInferiores();
        datosContacto.tapBotonContinuar();

        /********************************
         * DATOS LABORALES
         ********************************/
        DatosLaborales datosLaborales = new DatosLaborales();
        datosLaborales.validarIngresoPantalla();
        datosLaborales.validarElementosSuperiores();
        datosLaborales.validarSelectorActividad();
        datosLaborales.validarOpcionesActividades();
        datosLaborales.tapFlechaVolverAtras();

        /********************************
         * DATOS CONTACTO
         ********************************/
        if (DriverContext.getDriver() instanceof IOSDriver) {
            gestures.swipe(0.4, 0.9);
            gestures.swipe(0.4, 0.9);
            gestures.swipe(0.4, 0.9);
        }
        datosContacto.validarIngresoPantalla();
        datosContacto.validarElementosSuperiores();
        datosContacto.validarInputDireccion();
        datosContacto.validarSelectorRegion();
        datosContacto.validarSelectorComuna();
        datosContacto.validarInputTelefono();
        datosContacto.validarInputEmail();
        datosContacto.validarElementosInferiores();
        datosContacto.tapBotonContinuar();

        /********************************
         * DATOS LABORALES
         ********************************/
        datosLaborales.validarIngresoPantalla();
        datosLaborales.validarElementosSuperiores();
        datosLaborales.validarSelectorActividad();
        datosLaborales.validarOpcionesActividades();

    }
}
