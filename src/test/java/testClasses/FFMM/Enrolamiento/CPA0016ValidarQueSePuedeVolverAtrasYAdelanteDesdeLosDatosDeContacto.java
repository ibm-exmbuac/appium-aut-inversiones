package testClasses.FFMM.Enrolamiento;

import driver.DriverContext;
import io.appium.java_client.ios.IOSDriver;
import org.testng.annotations.Test;
import pages.Dashboard.DashboardInversiones;
import pages.Enrolamiento.DatosContacto;
import pages.Enrolamiento.DatosPersonales;
import util.Gestures;

public class CPA0016ValidarQueSePuedeVolverAtrasYAdelanteDesdeLosDatosDeContacto {

    @Test
    public void CPA0016ValidarQueSePuedeVolverAtrasYAdelanteDesdeLosDatosDeContacto() {

        Gestures gestures = new Gestures();

        /********************************
         * DASHBOARD
         ********************************/
        DashboardInversiones dashboardInversiones = new DashboardInversiones();
        dashboardInversiones.validarIngresoPantalla();
        dashboardInversiones.validarElementosEnrolamiento();
        dashboardInversiones.validarElementosPerfilamiento("ConPerfil");
        dashboardInversiones.validarElementosAlternativasInversion();
        dashboardInversiones.validarElementosAsesoria();
        dashboardInversiones.tapEnrolamiento();

        /********************************
         * DATOS PERSONALES
         ********************************/
        DatosPersonales datosPersonales = new DatosPersonales();
        datosPersonales.validarIngresoPantalla();
        datosPersonales.validarElementosSuperiores();
        datosPersonales.validarSelectorNacionalidad();
        datosPersonales.validarRdoPaisResidencia();
        datosPersonales.seleccionarRdoPaisResidencia("OtroPais");
        datosPersonales.validarSelectorPaisResidencia();
        datosPersonales.validarRdoDobleNacionalidad();
        if (DriverContext.getDriver() instanceof IOSDriver) {
            datosPersonales.validarSelectorEstCivil();
            datosPersonales.validarBotonContinuar();
        }else{
            datosPersonales.validarSelectorDobleNacionalidad();
            datosPersonales.validarInputIDDobleNac();
            datosPersonales.validarSelectorEstCivil();
            datosPersonales.validarBotonContinuar();
        }
        datosPersonales.tapBotonContinuar();

        /********************************
         * DATOS CONTACTO
         ********************************/
        DatosContacto datosContacto = new DatosContacto();
        datosContacto.validarIngresoPantalla();
        datosContacto.validarElementosSuperiores();
        datosContacto.validarInputDireccion();
        datosContacto.validarSelectorRegion();
        datosContacto.validarSelectorComuna();
        datosContacto.validarInputTelefono();
        datosContacto.validarInputEmail();
        datosContacto.validarElementosInferiores();
        datosContacto.tapFlechaAtras();

        /********************************
         * DATOS PERSONALES
         ********************************/
        if (DriverContext.getDriver() instanceof IOSDriver) {
            gestures.swipe(0.4, 0.9);
        }
        datosPersonales.validarIngresoPantalla();
        datosPersonales.validarElementosSuperiores();
        datosPersonales.validarSelectorNacionalidad();
        datosPersonales.validarRdoPaisResidencia();
        datosPersonales.seleccionarRdoPaisResidencia("OtroPais");
        datosPersonales.validarSelectorPaisResidencia();
        datosPersonales.validarRdoDobleNacionalidad();
        if (DriverContext.getDriver() instanceof IOSDriver) {
            datosPersonales.validarSelectorEstCivil();
            datosPersonales.validarBotonContinuar();
        }else{
            datosPersonales.validarSelectorDobleNacionalidad();
            datosPersonales.validarInputIDDobleNac();
            datosPersonales.validarSelectorEstCivil();
            datosPersonales.validarBotonContinuar();
        }
        datosPersonales.tapBotonContinuar();

        /********************************
         * DATOS CONTACTO
         ********************************/
        if (DriverContext.getDriver() instanceof IOSDriver) {
            gestures.swipe(0.4, 0.9);
        }
        datosContacto.validarIngresoPantalla();
        datosContacto.validarElementosSuperiores();
        datosContacto.validarInputDireccion();
        datosContacto.validarSelectorRegion();
        datosContacto.validarSelectorComuna();
        datosContacto.validarInputTelefono();
        datosContacto.validarInputEmail();
        datosContacto.validarElementosInferiores();
    }
}
