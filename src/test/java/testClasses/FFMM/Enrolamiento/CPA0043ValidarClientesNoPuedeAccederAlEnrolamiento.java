package testClasses.FFMM.Enrolamiento;

import org.testng.annotations.Test;
import pages.Dashboard.DashboardInversiones;

public class CPA0043ValidarClientesNoPuedeAccederAlEnrolamiento {

    @Test
    public void CPA0043ValidarClientesNoPuedeAccederAlEnrolamiento() {

        /********************************
         * DASHBOARD
         ********************************/
        DashboardInversiones dashboardInversiones = new DashboardInversiones();
        dashboardInversiones.validarIngresoPantalla();
        dashboardInversiones.validarQueNoSeMuestreAccesoAEnrolamiento();
        dashboardInversiones.validarElementosPerfilamiento("ConPerfil");
        dashboardInversiones.validarElementosAlternativasInversion();
        dashboardInversiones.validarElementosAsesoria();
    }
}
