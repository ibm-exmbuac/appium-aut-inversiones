package testClasses.FFMM.Enrolamiento;

import driver.DriverContext;
import io.appium.java_client.ios.IOSDriver;
import org.testng.annotations.Test;
import pages.Dashboard.DashboardInversiones;
import pages.Enrolamiento.DatosPersonales;

public class CPA0002VerificarLaPrecargaDeLosDatosPersonalesDelEnrolamiento {

    @Test
    public void CPA0002VerificarLaPrecargaDeLosDatosPersonalesDelEnrolamiento() {

        /********************************
         * DASHBOARD
         ********************************/
        DashboardInversiones dashboardInversiones = new DashboardInversiones();
        dashboardInversiones.validarIngresoPantalla();
        dashboardInversiones.validarElementosEnrolamiento();
        dashboardInversiones.validarElementosPerfilamiento("ConPerfil");
        dashboardInversiones.validarElementosAlternativasInversion();
        dashboardInversiones.validarElementosAsesoria();
        dashboardInversiones.tapEnrolamiento();

        /********************************
         * DATOS PERSONALES
         ********************************/
        DatosPersonales datosPersonales = new DatosPersonales();
        datosPersonales.validarIngresoPantalla();
        datosPersonales.validarElementosSuperiores();
        datosPersonales.validarSelectorNacionalidad();
        if (DriverContext.getDriver() instanceof IOSDriver){
            datosPersonales.validarRdoPaisResidencia();
            datosPersonales.validarRdoDobleNacionalidad();
            datosPersonales.validarSelectorEstCivil();
            datosPersonales.validarBotonContinuar();
        }else{
            datosPersonales.validarRdoPaisResidencia();
            datosPersonales.validarSelectorPaisResidencia();
            datosPersonales.validarRdoDobleNacionalidad();
            datosPersonales.validarSelectorDobleNacionalidad();
            datosPersonales.validarInputIDDobleNac();
            datosPersonales.validarSelectorEstCivil();
            datosPersonales.validarBotonContinuar();
        }

    }
}
