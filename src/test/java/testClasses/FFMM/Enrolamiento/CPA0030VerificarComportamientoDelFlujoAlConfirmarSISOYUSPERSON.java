package testClasses.FFMM.Enrolamiento;

import driver.DriverContext;
import io.appium.java_client.ios.IOSDriver;
import org.testng.annotations.Test;
import pages.Dashboard.DashboardInversiones;
import pages.Enrolamiento.*;

public class CPA0030VerificarComportamientoDelFlujoAlConfirmarSISOYUSPERSON {

    @Test
    public void CPA0030VerificarComportamientoDelFlujoAlConfirmarSISOYUSPERSON() {

        /********************************
         * DASHBOARD
         ********************************/
        DashboardInversiones dashboardInversiones = new DashboardInversiones();
        dashboardInversiones.validarIngresoPantalla();
        dashboardInversiones.validarElementosEnrolamiento();
        dashboardInversiones.validarElementosPerfilamiento("ConPerfil");
        dashboardInversiones.validarElementosAlternativasInversion();
        dashboardInversiones.validarElementosAsesoria();
        dashboardInversiones.tapEnrolamiento();

        /********************************
         * DATOS PERSONALES
         ********************************/
        DatosPersonales datosPersonales = new DatosPersonales();
        datosPersonales.validarIngresoPantalla();
        datosPersonales.validarElementosSuperiores();
        datosPersonales.validarSelectorNacionalidad();
        datosPersonales.validarRdoPaisResidencia();
        datosPersonales.seleccionarRdoPaisResidencia("OtroPais");
        datosPersonales.validarSelectorPaisResidencia();
        datosPersonales.validarRdoDobleNacionalidad();
        if (DriverContext.getDriver() instanceof IOSDriver) {
            datosPersonales.validarSelectorEstCivil();
            datosPersonales.validarBotonContinuar();
        }else{
            datosPersonales.validarSelectorDobleNacionalidad();
            datosPersonales.validarInputIDDobleNac();
            datosPersonales.validarSelectorEstCivil();
            datosPersonales.validarBotonContinuar();
        }
        datosPersonales.tapBotonContinuar();

        /********************************
         * DATOS CONTACTO
         ********************************/
        DatosContacto datosContacto = new DatosContacto();
        datosContacto.validarIngresoPantalla();
        datosContacto.validarElementosSuperiores();
        datosContacto.validarInputDireccion();
        datosContacto.validarSelectorRegion();
        datosContacto.validarSelectorComuna();
        datosContacto.validarInputTelefono();
        datosContacto.ingresarTelefono("912345678");
        datosContacto.validarInputEmail();
        datosContacto.ingresarEmail("test@test.cl");
        datosContacto.validarElementosInferiores();
        datosContacto.tapBotonContinuar();

        /********************************
         * DATOS LABORALES
         ********************************/
        DatosLaborales datosLaborales = new DatosLaborales();
        datosLaborales.validarIngresoPantalla();
        datosLaborales.validarElementosSuperiores();
        datosLaborales.validarSelectorActividad();
        datosLaborales.seleccionarActividad("Jubilado(a)");
        datosLaborales.validarOpcionesActividades();
        datosLaborales.tapBotonContinuar();

        /********************************
         * INGRESOS PATRIMONIO
         ********************************/
        IngresosPatrimonios ingresosPatrimonios = new IngresosPatrimonios();
        ingresosPatrimonios.validarIngresoPantalla();
        ingresosPatrimonios.validarElemtosSuperiores();
        ingresosPatrimonios.validarOrigenFondos();
        ingresosPatrimonios.seleccionarOrigenFondos("Herencia");
        ingresosPatrimonios.validarSelectorPaisOrigenFF();
        ingresosPatrimonios.seleccionarPaisOrigenFF("CHILE");
        ingresosPatrimonios.validarSelectorIngresosMensuales();
        ingresosPatrimonios.validarSelectorIngresosOcasionales();
        ingresosPatrimonios.validarSelectorPatriminioFinanciero();
        ingresosPatrimonios.validarSelectorPatriminioNoFinanciero();
        ingresosPatrimonios.validarBotonContinuar();
        ingresosPatrimonios.tapBotonContinuar();

        /********************************
         * SITUACION TRIBUTARIA
         ********************************/
        SituacionTributaria situacionTributaria = new SituacionTributaria();
        situacionTributaria.validarIngresoPantalla();
        situacionTributaria.validarElementosSuperiores();
        situacionTributaria.validarRdosUSPerson();
        situacionTributaria.seleccionarUSPerson("Si");
        situacionTributaria.validarModalUSPerson();
        situacionTributaria.tapBotonSiModal();

        /********************************
         * DASHBOARD
         ********************************/
        dashboardInversiones.validarIngresoPantalla();
        dashboardInversiones.validarElementosEnrolamiento();
        dashboardInversiones.validarElementosPerfilamiento("ConPerfil");
        dashboardInversiones.validarElementosAlternativasInversion();
        dashboardInversiones.validarElementosAsesoria();
    }
}
