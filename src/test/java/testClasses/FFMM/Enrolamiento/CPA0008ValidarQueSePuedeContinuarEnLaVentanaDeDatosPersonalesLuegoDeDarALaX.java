package testClasses.FFMM.Enrolamiento;

import driver.DriverContext;
import io.appium.java_client.ios.IOSDriver;
import org.testng.annotations.Test;
import pages.Dashboard.DashboardInversiones;
import pages.Enrolamiento.DatosPersonales;
import pages.Modales.ModalSalirEnrolamiento;

public class CPA0008ValidarQueSePuedeContinuarEnLaVentanaDeDatosPersonalesLuegoDeDarALaX {

    @Test
    public void CPA0008ValidarQueSePuedeContinuarEnLaVentanaDeDatosPersonalesLuegoDeDarALaX() {

        /********************************
         * DASHBOARD
         ********************************/
        DashboardInversiones dashboardInversiones = new DashboardInversiones();
        dashboardInversiones.validarIngresoPantalla();
        dashboardInversiones.validarElementosEnrolamiento();
        dashboardInversiones.validarElementosPerfilamiento("ConPerfil");
        dashboardInversiones.validarElementosAlternativasInversion();
        dashboardInversiones.validarElementosAsesoria();
        dashboardInversiones.tapEnrolamiento();

        /********************************
         * DATOS PERSONALES
         ********************************/
        DatosPersonales datosPersonales = new DatosPersonales();
        datosPersonales.validarIngresoPantalla();
        datosPersonales.validarElementosSuperiores();
        datosPersonales.validarSelectorNacionalidad();
        datosPersonales.validarRdoPaisResidencia();
        datosPersonales.seleccionarRdoPaisResidencia("OtroPais");
        datosPersonales.validarSelectorPaisResidencia();
        datosPersonales.validarRdoDobleNacionalidad();
        if (DriverContext.getDriver() instanceof IOSDriver) {
            datosPersonales.validarSelectorEstCivil();
            datosPersonales.validarBotonContinuar();
        }else{
            datosPersonales.validarSelectorDobleNacionalidad();
            datosPersonales.validarInputIDDobleNac();
            datosPersonales.validarSelectorEstCivil();
            datosPersonales.validarBotonContinuar();
        }
        datosPersonales.tapBotonCerrar();

        /********************************
         * MODAL SALIR FLUJO ENROLAMIENTO
         ********************************/
        ModalSalirEnrolamiento modalSalirEnrolamiento = new ModalSalirEnrolamiento();
        modalSalirEnrolamiento.validarIngresoModal();
        modalSalirEnrolamiento.validarElementosModal();
        modalSalirEnrolamiento.tapBotonContinuar("Datos Personales");


    }
}
