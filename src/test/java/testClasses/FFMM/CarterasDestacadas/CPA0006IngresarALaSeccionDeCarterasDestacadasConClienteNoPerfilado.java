package testClasses.FFMM.CarterasDestacadas;

import org.testng.annotations.Test;
import pages.FFMM.CampaniaFFMM;
import pages.FFMM.CarterasAutogestionadas;
import testbase.TestBase;
import util.Gestures;

public class CPA0006IngresarALaSeccionDeCarterasDestacadasConClienteNoPerfilado extends TestBase {

    @Test
    public void CPA0006IngresarALaSeccionDeCarterasDestacadasConClienteNoPerfilado() {

        Gestures ges = new Gestures();

        /********************************
         * LOGIN
         ********************************/

        loginRapido("135600490", "111222");

        /********************************
         * CAMPAÑA FFMM
         ********************************/
        ges.scrollAbajoCorto();
        CampaniaFFMM campaniaFFMM = new CampaniaFFMM();
        campaniaFFMM.validarDespliegueCampania();
        campaniaFFMM.tapBtnSimular();

        /********************************
         * CARTERAS DESTACADAS
         ********************************/
        CarterasAutogestionadas carterasAutogestionadas = new CarterasAutogestionadas();
        carterasAutogestionadas.validarIngresoPantalla();
        carterasAutogestionadas.validarElementos("Campania");
        carterasAutogestionadas.validarCarteras();

    }
}
