package testClasses.FFMM.CarterasDestacadas;

import org.testng.annotations.Test;
import pages.FFMM.*;
import testbase.TestBase;
import util.Gestures;

public class CPA003FinalizaFlujoInvertirDeUnaCarteraDestacada extends TestBase {


    @Test
    public void CPA003FinalizaFlujoInvertirDeUnaCarteraDestacada() {

        Gestures ges = new Gestures();

        /********************************
         * CAMPAÑA FFMM
         ********************************/
        ges.scrollAbajoCorto();
        CampaniaFFMM campaniaFFMM = new CampaniaFFMM();
        campaniaFFMM.validarDespliegueCampania();
        campaniaFFMM.tapBtnSimular();

        /********************************
         * CARTERAS DESTACADAS
         ********************************/
        CarterasAutogestionadas carterasAutogestionadas = new CarterasAutogestionadas();
        carterasAutogestionadas.validarIngresoPantalla();
        carterasAutogestionadas.validarElementos("Campania");
        carterasAutogestionadas.validarCarteras();
        carterasAutogestionadas.seleccionarCartera("Gestión Ahorro Corto Plazo");

        /********************************
         * DETALLE CARTERA
         ********************************/
        DetalleCartera detalleCartera = new DetalleCartera();
        detalleCartera.validarIngresoPantalla();
        detalleCartera.validarElementosPerfil();
        detalleCartera.validarElementosCartera();

        /********************************
         * DETALLE CARTERA ESTADISTICA RENTABILIDAD NOMINAL
         ********************************/
        DetalleCarteraEstadisticasRentabilidad detalleCarteraEstadisticasRentabilidad = new DetalleCarteraEstadisticasRentabilidad();
        detalleCarteraEstadisticasRentabilidad.validarElementos();

        /********************************
         * DETALLE CARTERA VALOR CUOTA
         ********************************/
        DetalleCarteraValorCuota detalleCarteraValorCuota = new DetalleCarteraValorCuota();
        detalleCarteraValorCuota.validarElementos();

        /********************************
         * DETALLE CARTERA DOCS ANEXOS
         ********************************/
        DetalleCarteraDocsAnexos detalleCarteraDocsAnexos = new DetalleCarteraDocsAnexos();
        detalleCarteraDocsAnexos.validarElementos();

        /********************************
         * DETALLE CARTERA INVERTIR
         ********************************/
        DetalleCarteraInvertir detalleCarteraInvertir = new DetalleCarteraInvertir();
        detalleCarteraInvertir.validarElementos();
        detalleCarteraInvertir.tapIrAInvertir();

        /********************************
         * INVERTIR EN CARTERAS AUTOGESTIONADAS
         ********************************/
        InvertirEnCarterasAutogestionadas invertirEnCarterasAutogestionadas = new InvertirEnCarterasAutogestionadas();
        invertirEnCarterasAutogestionadas.validarIngresoPantalla();
        invertirEnCarterasAutogestionadas.tapBotonCerrar();

    }
}
