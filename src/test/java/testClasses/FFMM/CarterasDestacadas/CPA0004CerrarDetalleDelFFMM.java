package testClasses.FFMM.CarterasDestacadas;

import org.testng.annotations.Test;
import pages.FFMM.*;
import testbase.TestBase;
import util.Gestures;

public class CPA0004CerrarDetalleDelFFMM extends TestBase {


    @Test
    public void CPA0004CerrarDetalleDelFFMM() {

        Gestures ges = new Gestures();

        /********************************
         * LOGIN
         ********************************/

        loginRapido("135600490", "111222");

        /********************************
         * CAMPAÑA FFMM
         ********************************/
        ges.scrollAbajoCorto();
        CampaniaFFMM campaniaFFMM = new CampaniaFFMM();
        campaniaFFMM.validarDespliegueCampania();
        campaniaFFMM.tapBtnSimular();

        /********************************
         * CARTERAS DESTACADAS
         ********************************/
        CarterasAutogestionadas carterasAutogestionadas = new CarterasAutogestionadas();
        carterasAutogestionadas.validarIngresoPantalla();
        carterasAutogestionadas.validarElementos("Campania");
        carterasAutogestionadas.validarCarteras();
        carterasAutogestionadas.seleccionarCartera("Gestión Ahorro Corto Plazo");

        /********************************
         * DETALLE CARTERA
         ********************************/
        DetalleCartera detalleCartera = new DetalleCartera();
        detalleCartera.validarIngresoPantalla();
        detalleCartera.validarElementosCartera();
        ges.scrollAbajoCorto();

        /********************************
         * DETALLE CARTERA ESTADISTICA RENTABILIDAD NOMINAL
         ********************************/
        DetalleCarteraEstadisticasRentabilidad detalleCarteraEstadisticasRentabilidad = new DetalleCarteraEstadisticasRentabilidad();
        detalleCarteraEstadisticasRentabilidad.validarElementos();
        ges.scrollAbajoCorto();

        /********************************
         * DETALLE CARTERA VALOR CUOTA
         ********************************/
        DetalleCarteraValorCuota detalleCarteraValorCuota = new DetalleCarteraValorCuota();
        detalleCarteraValorCuota.validarElementos();
        ges.scrollAbajoCorto();

        /********************************
         * DETALLE CARTERA DOCS ANEXOS
         ********************************/
        DetalleCarteraDocsAnexos detalleCarteraDocsAnexos = new DetalleCarteraDocsAnexos();
        detalleCarteraDocsAnexos.validarElementos();

        /********************************
         * DETALLE CARTERA INVERTIR
         ********************************/
        DetalleCarteraInvertir detalleCarteraInvertir = new DetalleCarteraInvertir();
        detalleCarteraInvertir.validarElementos();

        /********************************
         * CERRAR DETALLE CARTERA
         ********************************/
        detalleCartera.cerrarDetalleFFFMM();
    }
}
