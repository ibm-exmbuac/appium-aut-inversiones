package testClasses.FFMM.CarterasDestacadas;

import org.testng.annotations.Test;
import pages.FFMM.CampaniaFFMM;
import pages.FFMM.CarterasAutogestionadas;
import testbase.TestBase;
import util.Gestures;

public class CPA0001IngresarALaSeccionDeCarterasDestacadas extends TestBase {

    @Test
    public void CPA0001IngresarALaSeccionDeCarterasDestacadas() throws Exception {

        Gestures ges = new Gestures();

        /********************************
         * LOGIN
         ********************************/

        loginRapido("17360130-1", "226872");

        /********************************
         * CAMPAÑA FFMM
         ********************************/
        ges.scrollAbajoCorto();
        CampaniaFFMM campaniaFFMM = new CampaniaFFMM();
        campaniaFFMM.validarDespliegueCampania();
        campaniaFFMM.tapBtnSimular();

        /********************************
         * CARTERAS DESTACADAS
         ********************************/
        CarterasAutogestionadas carterasAutogestionadas = new CarterasAutogestionadas();
        carterasAutogestionadas.validarIngresoPantalla();
        carterasAutogestionadas.validarElementos("Campania");
        carterasAutogestionadas.validarCarteras();
    }

}
