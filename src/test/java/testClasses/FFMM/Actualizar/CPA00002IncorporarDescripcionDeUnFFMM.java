package testClasses.FFMM.Actualizar;

import org.testng.annotations.Test;
import pages.Dashboard.DashboardInversiones;
import pages.FFMM.*;
import testbase.TestBase;
import util.Gestures;

import static constants.Tabs.Inversiones;

public class CPA00002IncorporarDescripcionDeUnFFMM extends TestBase {

    @Test
    public void CPA00002IncorporarDescripcionDeUnFFMM(boolean smokeTest) {

        Gestures ges = new Gestures();

        /********************************
         * LOGIN
         ********************************/
        loginRapido("17546784K", "226872");
        tabSwitcher(Inversiones);

        /******++************************
         * DASHBOARD
         ********************************/
        DashboardInversiones dashboardInversiones = new DashboardInversiones();
        dashboardInversiones.validarIngresoPantalla();
        dashboardInversiones.validarElementosPerfilamiento("ConPerfil");
        dashboardInversiones.validarElementosAlternativasInversion();
        dashboardInversiones.validarElementosAsesoria();
        dashboardInversiones.tapFFMM();

        /********************************
         * CARTERAS AUTOGESTIONADAS
         ********************************/
        CarterasAutogestionadas carterasAutogestionadas = new CarterasAutogestionadas();
        carterasAutogestionadas.validarIngresoPantalla();
        carterasAutogestionadas.validarElementos("Dashboard");
        carterasAutogestionadas.validarCarteras();
        carterasAutogestionadas.seleccionarCartera("Gestión Ahorro Corto Plazo");

        /********************************
         * DETALLE CARTERA
         ********************************/
        DetalleCartera detalleCartera = new DetalleCartera();
        detalleCartera.validarIngresoPantalla();
        detalleCartera.validarElementosPerfil();
        detalleCartera.validarElementosCartera();
        ges.scrollAbajoCorto();

        /********************************
         * DETALLE CARTERA ESTADISTICAS RENTABILIDAD
         ********************************/
        DetalleCarteraEstadisticasRentabilidad detalleCarteraEstadisticasRentabilidad = new DetalleCarteraEstadisticasRentabilidad();
        detalleCarteraEstadisticasRentabilidad.validarElementos();
        ges.scrollAbajoCorto();

        /********************************
         * DETALLE CARTERA VALOR CUOTA
         ********************************/
        DetalleCarteraValorCuota detalleCarteraValorCuota = new DetalleCarteraValorCuota();
        detalleCarteraValorCuota.validarElementos();
        ges.scrollAbajoCorto();


        /********************************
         * DETALLE CARTERA DOCS ANEXOS
         ********************************/
        DetalleCarteraDocsAnexos detalleCarteraDocsAnexos = new DetalleCarteraDocsAnexos();
        detalleCarteraDocsAnexos.validarElementos();

        /********************************
         * VOLVER ATRÁS
         ********************************/
        detalleCartera.tapFlechaAtras();

    }
}
