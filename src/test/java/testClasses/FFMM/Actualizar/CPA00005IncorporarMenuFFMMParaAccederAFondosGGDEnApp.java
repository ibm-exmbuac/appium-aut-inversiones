package testClasses.FFMM.Actualizar;

import constants.Tabs;
import org.testng.annotations.Test;
import pages.Dashboard.DashboardInversiones;
import pages.FFMM.*;
import testbase.TestBase;
import util.Gestures;

public class CPA00005IncorporarMenuFFMMParaAccederAFondosGGDEnApp extends TestBase {

    @Test
    public void CPA00005IncorporarMenuFFMMParaAccederAFondosGGDEnApp(boolean smokeTest) throws InterruptedException {

        Gestures ges = new Gestures();

        /********************************
         * LOGIN
         ********************************/

        loginRapido("13560049-0", "111222");
        tabSwitcher(Tabs.Inversiones);

        /********************************
         * DASHBOARD
         ********************************/
        DashboardInversiones dashboardInversiones = new DashboardInversiones();
        dashboardInversiones.validarIngresoPantalla();
        dashboardInversiones.validarElementosPerfilamiento("ConPerfil");
        dashboardInversiones.validarElementosAlternativasInversion();
        dashboardInversiones.validarElementosAsesoria();
        dashboardInversiones.tapFFMM();

        /********************************
         * CARTERAS AUTOGESTIONADAS
         ********************************/
        CarterasAutogestionadas carterasAutogestionadas = new CarterasAutogestionadas();
        carterasAutogestionadas.validarIngresoPantalla();
        carterasAutogestionadas.validarElementos("Dashboard");
        carterasAutogestionadas.validarCarteras();
        carterasAutogestionadas.seleccionarCartera("Gestión Ahorro Corto Plazo");

        /********************************
         * DETALLE CARTERA
         ********************************/
        DetalleCartera detalleCartera = new DetalleCartera();
        detalleCartera.validarIngresoPantalla();
        detalleCartera.validarElementosPerfil();
        detalleCartera.validarElementosCartera();

        /********************************
         * DETALLE CARTERA ESTADISTICAS RENTABILIDAD
         ********************************/
        DetalleCarteraEstadisticasRentabilidad detalleCarteraEstadisticasRentabilidad = new DetalleCarteraEstadisticasRentabilidad();
        detalleCarteraEstadisticasRentabilidad.validarElementos();

        /********************************
         * DETALLE CARTERA VALOR CUOTA
         ********************************/
        DetalleCarteraValorCuota detalleCarteraValorCuota = new DetalleCarteraValorCuota();
        detalleCarteraValorCuota.validarElementos();

        /********************************
         * DETALLE CARTERA DOCS ANEXOS
         * ******************************/
        DetalleCarteraDocsAnexos detalleCarteraDocsAnexos = new DetalleCarteraDocsAnexos();
        detalleCarteraDocsAnexos.validarElementos();

        /********************************
         * DETALLE CARTERA INVERTIR
         * ******************************/
        DetalleCarteraInvertir detalleCarteraInvertir = new DetalleCarteraInvertir();
        detalleCarteraInvertir.validarElementos();
        detalleCarteraInvertir.tapIrAInvertir();

        /********************************
         * INVERTIR EN CARTERAS AUTOGESTIONADAS
         * ******************************/
        InvertirEnCarterasAutogestionadas invertirEnCarterasAutogestionadas = new InvertirEnCarterasAutogestionadas();
        invertirEnCarterasAutogestionadas.validarIngresoPantalla();
        invertirEnCarterasAutogestionadas.validarElementos();
        invertirEnCarterasAutogestionadas.ingresarMonto("5000");
        invertirEnCarterasAutogestionadas.tapContinuar();

        /********************************
         * CONFIRMAR INVERSIÓN DETALLE
         * ******************************/
        ConfirmarInversionDetalle confirmarInversionDetalle = new ConfirmarInversionDetalle();
        confirmarInversionDetalle.validarIngresoPantalla();
        confirmarInversionDetalle.validarElementos();

        /********************************
         * CONFIRMAR INVERSIÓN COMPROBANTE
         * ******************************/
        ConfirmarInversionComprobante confirmarInversionComprobante = new ConfirmarInversionComprobante();
        confirmarInversionComprobante.validarElementos();
        confirmarInversionComprobante.ingresarCorreo("test@test.cl");

        /********************************
         * CONFIRMAR INVERSIÓN DOCS ANEXOS
         * ******************************/
        ConfirmarInversionDocsAnexos confirmarInversionDocsAnexos = new ConfirmarInversionDocsAnexos();
        confirmarInversionDocsAnexos.validarElementos();

        /********************************
         * CONFIRMAR INVERSIÓN TYC
         * ******************************/
        ConfirmarInversionTYC confirmarInversionTYC = new ConfirmarInversionTYC();
        confirmarInversionTYC.checkCGF();
        confirmarInversionTYC.tapInvertir();

        /********************************
         * SOLICITUD EXITOSA
         * ******************************/
        SolicitudExitosa solicitudExitosa = new SolicitudExitosa();
        solicitudExitosa.validarIngresoPantalla();
        solicitudExitosa.validarElementos("FueraHorario");
        solicitudExitosa.tapFinalizar();
    }
}
