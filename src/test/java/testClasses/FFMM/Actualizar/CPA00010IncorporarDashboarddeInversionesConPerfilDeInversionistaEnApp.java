package testClasses.FFMM.Actualizar;

import org.testng.annotations.Test;
import pages.Dashboard.DashboardInversiones;
import testbase.TestBase;

public class CPA00010IncorporarDashboarddeInversionesConPerfilDeInversionistaEnApp extends TestBase {

    @Test
    public void CPA00010IncorporarDashboarddeInversionesConPerfilDeInversionistaEnApp() {

        /********************************
         * DASHBOARD
         ********************************/
        DashboardInversiones dashboardInversiones = new DashboardInversiones();
        dashboardInversiones.validarIngresoPantalla();
        dashboardInversiones.validarElementosPerfilamiento("ConPerfil");
        dashboardInversiones.validarElementosAlternativasInversion();
        dashboardInversiones.validarElementosAsesoria();
    }
}

