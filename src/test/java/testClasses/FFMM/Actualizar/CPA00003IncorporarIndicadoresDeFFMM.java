package testClasses.FFMM.Actualizar;

import constants.Tabs;
import org.testng.annotations.Test;
import pages.Dashboard.DashboardInversiones;
import pages.FFMM.CarterasAutogestionadas;
import pages.FFMM.DetalleCartera;
import pages.FFMM.DetalleCarteraEstadisticasRentabilidad;
import pages.FFMM.DetalleCarteraValorCuota;
import testbase.TestBase;
import util.Gestures;

public class CPA00003IncorporarIndicadoresDeFFMM extends TestBase {

    @Test
    public void CPA00003IncorporarIndicadoresDeFFMM(boolean smokeTest) throws InterruptedException {

        Gestures ges = new Gestures();

        /********************************
         * LOGIN
         ********************************/
        loginRapido("135600490", "111222");
        tabSwitcher(Tabs.Inversiones);

        /********************************
         * DASHBOARD
         ********************************/
        DashboardInversiones dashboardInversiones = new DashboardInversiones();
        dashboardInversiones.validarIngresoPantalla();
        dashboardInversiones.validarElementosPerfilamiento("ConPerfil");
        dashboardInversiones.validarElementosAlternativasInversion();
        dashboardInversiones.validarElementosAsesoria();
        dashboardInversiones.tapFFMM();

        /********************************
         * CARTERAS AUTOGESTIONADAS
         ********************************/
        CarterasAutogestionadas carterasAutogestionadas = new CarterasAutogestionadas();
        carterasAutogestionadas.validarIngresoPantalla();
        carterasAutogestionadas.validarElementos("Dashboard");
        carterasAutogestionadas.validarCarteras();
        carterasAutogestionadas.seleccionarCartera("Gestión Ahorro Corto Plazo");

        /********************************
         * DETALLE CARTERA
         ********************************/
        DetalleCartera detalleCartera = new DetalleCartera();
        detalleCartera.validarIngresoPantalla();
        detalleCartera.validarElementosPerfil();
        detalleCartera.validarElementosCartera();
        ges.scrollAbajoCorto();

        /********************************
         * DETALLE CARTERA ESTADISTICAS RENTABILIDAD
         ********************************/
        DetalleCarteraEstadisticasRentabilidad detalleCarteraEstadisticasRentabilidad = new DetalleCarteraEstadisticasRentabilidad();
        detalleCarteraEstadisticasRentabilidad.validarElementos();
        detalleCarteraEstadisticasRentabilidad.validarRentabilidadAnual();
        detalleCarteraEstadisticasRentabilidad.validarAcumuladoAnio();
        ges.scrollAbajoCorto();

        /********************************
         * DETALLE CARTERA VALOR CUOTA
         ********************************/
        DetalleCarteraValorCuota detalleCarteraValorCuota = new DetalleCarteraValorCuota();
        detalleCarteraValorCuota.validarElementos();
        detalleCarteraValorCuota.validarValorCuota();

        /********************************
         * VOLVER ATRÁS
         ********************************/
        detalleCartera.tapFlechaAtras();

    }
}