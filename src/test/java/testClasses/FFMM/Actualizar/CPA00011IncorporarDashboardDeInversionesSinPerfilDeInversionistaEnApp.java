package testClasses.FFMM.Actualizar;

import org.testng.annotations.Test;
import pages.Dashboard.DashboardInversiones;
import testbase.TestBase;

public class CPA00011IncorporarDashboardDeInversionesSinPerfilDeInversionistaEnApp extends TestBase {


    @Test
    public void CPA00011IncorporarDashboardDeInversionesSinPerfilDeInversionistaEnApp() {

        /********************************
         * DASHBOARD
         ********************************/
        DashboardInversiones dashboardInversiones = new DashboardInversiones();
        dashboardInversiones.validarIngresoPantalla();
        dashboardInversiones.validarElementosPerfilamiento("SinPerfil");
        dashboardInversiones.validarElementosAlternativasInversion();
        dashboardInversiones.validarElementosAsesoria();
    }
}
