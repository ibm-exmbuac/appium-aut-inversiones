package testClasses.FFMM.Asesorar;

import constants.Tabs;
import org.testng.annotations.Test;
import pages.Asesoria.*;
import pages.Dashboard.DashboardInversiones;
import pages.FFMM.InvertirEnCarterasAutogestionadas;
import pages.Perfilamiento.PerfilamientoPregunta1;
import pages.Perfilamiento.PerfilamientoPregunta2;
import testbase.TestBase;
import util.Gestures;

public class CPA0023ContinuarEnLaPantallaDeConfirmacionDeLaInversionEnFFMM extends TestBase {

    @Test
    public void CPA0023ContinuarEnLaPantallaDeConfirmacionDeLaInversionEnFFMM() {

        Gestures ges = new Gestures();

        /********************************
         * LOGIN
         ********************************/
        tabSwitcher(Tabs.Inversiones);
        loginRapido("135600490", "111222");

        /********************************
         * DASHBOARD
         ********************************/
        DashboardInversiones dashboardInversiones = new DashboardInversiones();
        dashboardInversiones.validarIngresoPantalla();
        dashboardInversiones.validarElementosPerfilamiento("ConPerfil");
        dashboardInversiones.validarElementosAlternativasInversion();
        dashboardInversiones.validarElementosAsesoria();
        dashboardInversiones.tapAsesoria();

        /********************************
         * ASESORÍA PREGUNTA 1
         ********************************/
        AsesorarPregunta1 asesorarPregunta1 = new AsesorarPregunta1();
        asesorarPregunta1.validarIngresoPantalla();
        asesorarPregunta1.validarElementos();
        asesorarPregunta1.seleccionarRespuesta("1");

        /********************************
         * PERFILAMIENTO PREGUNTA 1
         ********************************/
        PerfilamientoPregunta1 perfilamientoPregunta1 = new PerfilamientoPregunta1();
        perfilamientoPregunta1.validarIngresoPantalla();
        perfilamientoPregunta1.validarElementosPreguntaRespuestas();
        perfilamientoPregunta1.seleccionarRespuesta("3");

        /********************************
         * PERFILAMIENTO PREGUNTA 2
         ********************************/
        PerfilamientoPregunta2 perfilamientoPregunta2 = new PerfilamientoPregunta2();
        perfilamientoPregunta2.validarIngresoPantalla();
        perfilamientoPregunta2.validarElementosPreguntaRespuestas();
        perfilamientoPregunta2.seleccionarRespuesta("2");

        /********************************
         * ASESORÍA PREGUNTA 2
         ********************************/
        AsesorarPregunta2 asesorarPregunta2 = new AsesorarPregunta2();
        asesorarPregunta2.checkIngresoPantalla();
        asesorarPregunta2.validaElementos();
        asesorarPregunta2.ingresoMonto("5000");
        asesorarPregunta2.tapBotonContinuar();

        /********************************
         * ASESORÍA PREGUNTA 3
         ********************************/
        AsesorarPregunta3 asesorarPregunta3 = new AsesorarPregunta3();
        asesorarPregunta3.validarIngresoPantalla();
        asesorarPregunta3.validaElementos();
        asesorarPregunta3.seleccionarRespuesta("3");

        /********************************
         * RESULTADO ASESORÍA DETALLE CARTERA
         ********************************/
        ResultadoAsesoriaDetalleCartera resultadoAsesoriaDetalleCartera = new ResultadoAsesoriaDetalleCartera();
        resultadoAsesoriaDetalleCartera.validarIngresoPantalla();
        resultadoAsesoriaDetalleCartera.validarElementos();

        /********************************
         * RESULTADO ASESORÍA ESTADÍSTICAS
         ********************************/
        ResultadoAsesoriaEstadisticaRentabilidad resultadoAsesoriaEstadisticaRentabilidad = new ResultadoAsesoriaEstadisticaRentabilidad();
        resultadoAsesoriaEstadisticaRentabilidad.validarElementos();

        /********************************
         * RESULTADO ASESORÍA VALOR CUOTA
         ********************************/
        ResultadoAsesoriaValorCuota resultadoAsesoriaValorCuota = new ResultadoAsesoriaValorCuota();
        resultadoAsesoriaValorCuota.validarElementos();

        /********************************
         * RESULTADO ASESORÍA DOCS ANEXOS
         ********************************/
        ResultadoAsesoriaDocsAnexos resultadoAsesoriaDocsAnexos = new ResultadoAsesoriaDocsAnexos();
        resultadoAsesoriaDocsAnexos.validarElementos();

        /********************************
         * RESULTADO ASESORÍA OTRAS CARTERAS DISCLAIMER
         ********************************/
        ResultadoAsesoriaOtrasCarterasDisclaimerInvertir resultadoAsesoriaOtrasCarterasDisclaimerInvertir = new ResultadoAsesoriaOtrasCarterasDisclaimerInvertir();
        resultadoAsesoriaOtrasCarterasDisclaimerInvertir.validarElementos();
        resultadoAsesoriaOtrasCarterasDisclaimerInvertir.tapIrAInvertir();

        /********************************
         * INVERTIR EN CARTERAS AUTOGESTIONADAS
         ********************************/
        InvertirEnCarterasAutogestionadas invertirEnCarterasAutogestionadas = new InvertirEnCarterasAutogestionadas();
        invertirEnCarterasAutogestionadas.validarIngresoPantalla();
        invertirEnCarterasAutogestionadas.tapContinuar();
    }
}
