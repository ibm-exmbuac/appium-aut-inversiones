package testClasses.FFMM.Asesorar;

import constants.Tabs;
import org.testng.annotations.Test;
import pages.Asesoria.*;
import pages.Dashboard.DashboardInversiones;
import pages.Modales.ModalNoEnrolado;
import testbase.TestBase;
import util.Gestures;

public class CPA0027AsesoriaDeClienteNoEnroladoYPerfilado extends TestBase {

    @Test
    public void CPA0027AsesoriaDeClienteNoEnroladoYPerfilado() {


        /********************************
         * DASHBOARD
         ********************************/
        DashboardInversiones dashboardInversiones = new DashboardInversiones();
        dashboardInversiones.validarIngresoPantalla();
        dashboardInversiones.validarElementosPerfilamiento("ConPerfil");
        dashboardInversiones.validarElementosAlternativasInversion();
        dashboardInversiones.validarElementosAsesoria();
        dashboardInversiones.tapAsesoria();

        /********************************
         * ASESORÍA PREGUNTA 1
         ********************************/
        AsesorarPregunta1 asesorarPregunta1 = new AsesorarPregunta1();
        asesorarPregunta1.validarIngresoPantalla();
        asesorarPregunta1.validarElementos();
        asesorarPregunta1.seleccionarRespuesta("1");

        /********************************
         * ASESORÍA PREGUNTA 2
         ********************************/
        AsesorarPregunta2 asesorarPregunta2 = new AsesorarPregunta2();
        asesorarPregunta2.checkIngresoPantalla();
        asesorarPregunta2.validaElementos();
        asesorarPregunta2.ingresoMonto("12000");
        asesorarPregunta2.tapBotonContinuar();

        /********************************
         * ASESORÍA PREGUNTA 3
         ********************************/
        AsesorarPregunta3 asesorarPregunta3 = new AsesorarPregunta3();
        asesorarPregunta3.validarIngresoPantalla();
        asesorarPregunta3.validaElementos();
        asesorarPregunta3.seleccionarRespuesta("3");

        /********************************
         * RESULTADO ASESORÍA DETALLE CARTERA
         ********************************/
        ResultadoAsesoriaDetalleCartera resultadoAsesoriaDetalleCartera = new ResultadoAsesoriaDetalleCartera();
        resultadoAsesoriaDetalleCartera.validarIngresoPantalla();
        resultadoAsesoriaDetalleCartera.validarElementos();

        /********************************
         * RESULTADO ASESORÍA ESTADÍSTICAS
         ********************************/
        ResultadoAsesoriaEstadisticaRentabilidad resultadoAsesoriaEstadisticaRentabilidad = new ResultadoAsesoriaEstadisticaRentabilidad();
        resultadoAsesoriaEstadisticaRentabilidad.validarElementos();

        /********************************
         * RESULTADO ASESORÍA VALOR CUOTA
         ********************************/
        ResultadoAsesoriaValorCuota resultadoAsesoriaValorCuota = new ResultadoAsesoriaValorCuota();
        resultadoAsesoriaValorCuota.tapValorCuota();
//        resultadoAsesoriaValorCuota.validarElementos();

        /********************************
         * RESULTADO ASESORÍA DOCS ANEXOS
         ********************************/
        ResultadoAsesoriaDocsAnexos resultadoAsesoriaDocsAnexos = new ResultadoAsesoriaDocsAnexos();
        resultadoAsesoriaDocsAnexos.tapDocsAnexos();
//        resultadoAsesoriaDocsAnexos.validarElementos();

        /********************************
         * RESULTADO ASESORÍA OTRAS CARTERAS DISCLAIMER
         ********************************/
        ResultadoAsesoriaOtrasCarterasDisclaimerInvertir resultadoAsesoriaOtrasCarterasDisclaimerInvertir = new ResultadoAsesoriaOtrasCarterasDisclaimerInvertir();
//        resultadoAsesoriaOtrasCarterasDisclaimerInvertir.validarElementos();
        resultadoAsesoriaOtrasCarterasDisclaimerInvertir.tapIrAInvertir();

        /********************************
         * MODAL NO ENROLADO
         ********************************/
        ModalNoEnrolado modalNoEnrolado = new ModalNoEnrolado();
        modalNoEnrolado.tapInvertir();
        modalNoEnrolado.validarDespliegueModal();
        modalNoEnrolado.validarElementos();
        modalNoEnrolado.tapEnOtroMomento();

        /********************************
         * RESULTADO ASESORÍA
         ********************************/
        resultadoAsesoriaDetalleCartera.validarIngresoPantalla();
    }
}
