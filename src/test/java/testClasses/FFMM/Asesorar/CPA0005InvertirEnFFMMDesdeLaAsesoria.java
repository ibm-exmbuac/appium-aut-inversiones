package testClasses.FFMM.Asesorar;

import org.testng.annotations.Test;
import pages.Asesoria.*;
import pages.Dashboard.DashboardInversiones;
import pages.FFMM.*;
import testbase.TestBase;

public class CPA0005InvertirEnFFMMDesdeLaAsesoria extends TestBase {

    @Test
    public void CPA0005InvertirEnFFMMDesdeLaAsesoria() {


        /********************************
         * DASHBOARD
         ********************************/
        DashboardInversiones dashboardInversiones = new DashboardInversiones();
        dashboardInversiones.validarIngresoPantalla();
        dashboardInversiones.validarElementosPerfilamiento("ConPerfil");
        dashboardInversiones.validarElementosAlternativasInversion();
        dashboardInversiones.validarElementosAsesoria();
        dashboardInversiones.tapAsesoria();

        /********************************
         * ASESORÍA PREGUNTA 1
         ********************************/
        AsesorarPregunta1 asesorarPregunta1 = new AsesorarPregunta1();
        asesorarPregunta1.validarIngresoPantalla();
        asesorarPregunta1.validarElementos();
        asesorarPregunta1.seleccionarRespuesta("3");

        /********************************
         * ASESORÍA PREGUNTA 2
         ********************************/
        AsesorarPregunta2 asesorarPregunta2 = new AsesorarPregunta2();
        asesorarPregunta2.checkIngresoPantalla();
        asesorarPregunta2.validaElementos();
        asesorarPregunta2.ingresoMonto("12000000");
        asesorarPregunta2.tapBotonContinuar();

        /********************************
         * ASESORÍA PREGUNTA 3
         ********************************/
        AsesorarPregunta3 asesorarPregunta3 = new AsesorarPregunta3();
        asesorarPregunta3.validarIngresoPantalla();
        asesorarPregunta3.validaElementos();
        asesorarPregunta3.seleccionarRespuesta("1");

        /********************************
         * RESULTADO ASESORÍA DETALLE CARTERA
         ********************************/
        ResultadoAsesoriaDetalleCartera resultadoAsesoriaDetalleCartera = new ResultadoAsesoriaDetalleCartera();
        resultadoAsesoriaDetalleCartera.validarIngresoPantalla();
        resultadoAsesoriaDetalleCartera.validarElementos();

        /********************************
         * RESULTADO ASESORÍA ESTADÍSTICAS
         ********************************/
        ResultadoAsesoriaEstadisticaRentabilidad resultadoAsesoriaEstadisticaRentabilidad = new ResultadoAsesoriaEstadisticaRentabilidad();
        resultadoAsesoriaEstadisticaRentabilidad.validarElementos();

        /********************************
         * RESULTADO ASESORÍA VALOR CUOTA
         ********************************/
        ResultadoAsesoriaValorCuota resultadoAsesoriaValorCuota = new ResultadoAsesoriaValorCuota();
        resultadoAsesoriaValorCuota.tapValorCuota();
        resultadoAsesoriaValorCuota.validarElementos();

        /********************************
         * RESULTADO ASESORÍA DOCS ANEXOS
         ********************************/
        ResultadoAsesoriaDocsAnexos resultadoAsesoriaDocsAnexos = new ResultadoAsesoriaDocsAnexos();
        resultadoAsesoriaDocsAnexos.tapDocsAnexos();
        resultadoAsesoriaDocsAnexos.validarElementos();

        /********************************
         * RESULTADO ASESORÍA OTRAS CARTERAS DISCLAIMER
         ********************************/
        ResultadoAsesoriaOtrasCarterasDisclaimerInvertir resultadoAsesoriaOtrasCarterasDisclaimerInvertir = new ResultadoAsesoriaOtrasCarterasDisclaimerInvertir();
        resultadoAsesoriaOtrasCarterasDisclaimerInvertir.validarElementos();
        resultadoAsesoriaOtrasCarterasDisclaimerInvertir.tapIrAInvertir();

        /********************************
         * INVERTIR EN CARTERAS AUTOGESTIONADAS
         * ******************************/
        InvertirEnCarterasAutogestionadas invertirEnCarterasAutogestionadas = new InvertirEnCarterasAutogestionadas();
        invertirEnCarterasAutogestionadas.validarIngresoPantalla();
        invertirEnCarterasAutogestionadas.validarElementos();
        invertirEnCarterasAutogestionadas.ingresarMonto("5000");
        invertirEnCarterasAutogestionadas.tapContinuar();

        /********************************
         * CONFIRMAR INVERSIÓN DETALLE
         * ******************************/
        ConfirmarInversionDetalle confirmarInversionDetalle = new ConfirmarInversionDetalle();
        confirmarInversionDetalle.validarIngresoPantalla();
        confirmarInversionDetalle.validarElementos();

        /********************************
         * CONFIRMAR INVERSIÓN COMPROBANTE
         * ******************************/
        ConfirmarInversionComprobante confirmarInversionComprobante = new ConfirmarInversionComprobante();
        confirmarInversionComprobante.validarElementos();
        confirmarInversionComprobante.ingresarCorreo("test@test.cl");

        /********************************
         * CONFIRMAR INVERSIÓN DOCS ANEXOS
         * ******************************/
        ConfirmarInversionDocsAnexos confirmarInversionDocsAnexos = new ConfirmarInversionDocsAnexos();
        confirmarInversionDocsAnexos.validarElementos();

        /********************************
         * CONFIRMAR INVERSIÓN TYC
         * ******************************/
        ConfirmarInversionTYC confirmarInversionTYC = new ConfirmarInversionTYC();
        confirmarInversionTYC.validaElementos();
        confirmarInversionTYC.checkCGF();
        confirmarInversionTYC.tapInvertir();

        /********************************
         * SOLICITUD EXITOSA
         * ******************************/
        SolicitudExitosa solicitudExitosa = new SolicitudExitosa();
        solicitudExitosa.validarIngresoPantalla();
        solicitudExitosa.validarElementos("FueraHorario");
        solicitudExitosa.tapFinalizar();
    }
}
