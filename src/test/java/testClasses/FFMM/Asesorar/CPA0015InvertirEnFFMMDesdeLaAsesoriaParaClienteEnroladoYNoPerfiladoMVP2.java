package testClasses.FFMM.Asesorar;

import org.testng.annotations.Test;
import pages.Asesoria.*;
import pages.Dashboard.DashboardInversiones;
import pages.FFMM.*;
import testbase.TestBase;
import util.Gestures;

public class CPA0015InvertirEnFFMMDesdeLaAsesoriaParaClienteEnroladoYNoPerfiladoMVP2 extends TestBase {

    @Test
    public void CPA0015InvertirEnFFMMDesdeLaAsesoriaParaClienteEnroladoYNoPerfiladoMVP2() {


        /********************************
         * DASHBOARD
         ********************************/
        DashboardInversiones dashboardInversiones = new DashboardInversiones();
        dashboardInversiones.validarIngresoPantalla();
        dashboardInversiones.validarElementosPerfilamiento("SinPerfil");
        dashboardInversiones.validarElementosAlternativasInversion();
        dashboardInversiones.validarElementosAsesoria();
        dashboardInversiones.tapAsesoria();

        /********************************
         * ASESORÍA PREGUNTA 1
         ********************************/
        AsesorarPregunta1 asesorarPregunta1 = new AsesorarPregunta1();
        asesorarPregunta1.validarIngresoPantalla();
        asesorarPregunta1.validarElementos();
        asesorarPregunta1.seleccionarRespuesta("3");

        /********************************
         * ASESORÍA PREGUNTA 2
         ********************************/
        AsesorarPerfilarPregunta1 asesorarPerfilarPregunta1 = new AsesorarPerfilarPregunta1();
        asesorarPerfilarPregunta1.validarIngresoPantalla();
        asesorarPerfilarPregunta1.validarElementosPreguntaRespuestas();
        asesorarPerfilarPregunta1.seleccionarRespuesta("1");

        /********************************
         * ASESORÍA PREGUNTA 3
         ********************************/
        AsesorarPerfilarPregunta2 asesorarPerfilarPregunta2 = new AsesorarPerfilarPregunta2();
        asesorarPerfilarPregunta2.validarIngresoPantalla();
        asesorarPerfilarPregunta2.validarElementosPreguntaRespuestas();
        asesorarPerfilarPregunta2.seleccionarRespuesta("1");
        /********************************
         * ASESORÍA PREGUNTA 4
         ********************************/
        AsesorarPregunta2 asesorarPregunta2 = new AsesorarPregunta2();
        asesorarPregunta2.checkIngresoPantalla();
        asesorarPregunta2.validaElementos();
        asesorarPregunta2.ingresoMonto("1200000");
        asesorarPregunta2.tapBotonContinuar();

        /********************************
         * ASESORÍA PREGUNTA 5
         ********************************/
        AsesorarPerfilarPregunta3 asesorarPerfilarPregunta3 = new AsesorarPerfilarPregunta3();
        asesorarPerfilarPregunta3.validarIngresoPantalla();
        asesorarPerfilarPregunta3.validarElementosPreguntaRespuestas();
        asesorarPerfilarPregunta3.seleccionarRespuesta("2");

        /********************************
         * ASESORÍA PREGUNTA 6
         ********************************/
        AsesorarPerfilarPregunta4 asesorarPerfilarPregunta4 = new AsesorarPerfilarPregunta4();
        asesorarPerfilarPregunta4.validarIngresoPantalla();
        asesorarPerfilarPregunta4.validarElementosPreguntaRespuesta();
        asesorarPerfilarPregunta4.seleccionarRespuesta("1");

        /********************************
         * ASESORÍA PREGUNTA 7
         ********************************/
        AsesorarPregunta3 asesorarPregunta3 = new AsesorarPregunta3();
        asesorarPregunta3.validarIngresoPantalla();
        asesorarPregunta3.validaElementos();
        asesorarPregunta3.seleccionarRespuesta("2");

        /********************************
         * ASESORÍA PREGUNTA 8
         ********************************/
        AsesorarPerfilarPregunta5 asesorarPerfilarPregunta5 = new AsesorarPerfilarPregunta5();
        asesorarPerfilarPregunta5.validarIngresoPantalla();
        asesorarPerfilarPregunta5.validarElementosPreguntaRespuestas();
        asesorarPerfilarPregunta5.seleccionarRespuesta("1");

        /********************************
         * RESULTADO ASESORÍA DETALLE CARTERA
         ********************************/
        ResultadoAsesoriaDetalleCartera resultadoAsesoriaDetalleCartera = new ResultadoAsesoriaDetalleCartera();
        resultadoAsesoriaDetalleCartera.validarIngresoPantalla();
        resultadoAsesoriaDetalleCartera.validarElementos();

        /********************************
         * RESULTADO ASESORÍA ESTADÍSTICAS
         ********************************/
        ResultadoAsesoriaEstadisticaRentabilidad resultadoAsesoriaEstadisticaRentabilidad = new ResultadoAsesoriaEstadisticaRentabilidad();
        resultadoAsesoriaEstadisticaRentabilidad.validarElementos();

        /********************************
         * RESULTADO ASESORÍA VALOR CUOTA
         ********************************/
        ResultadoAsesoriaValorCuota resultadoAsesoriaValorCuota = new ResultadoAsesoriaValorCuota();
        resultadoAsesoriaValorCuota.tapValorCuota();
        resultadoAsesoriaValorCuota.validarElementos();

        /********************************
         * RESULTADO ASESORÍA DOCS ANEXOS
         ********************************/
        ResultadoAsesoriaDocsAnexos resultadoAsesoriaDocsAnexos = new ResultadoAsesoriaDocsAnexos();
        resultadoAsesoriaDocsAnexos.tapDocsAnexos();
        resultadoAsesoriaDocsAnexos.validarElementos();

        /********************************
         * RESULTADO ASESORÍA OTRAS CARTERAS DISCLAIMER
         ********************************/
        ResultadoAsesoriaOtrasCarterasDisclaimerInvertir resultadoAsesoriaOtrasCarterasDisclaimerInvertir = new ResultadoAsesoriaOtrasCarterasDisclaimerInvertir();
        resultadoAsesoriaOtrasCarterasDisclaimerInvertir.validarElementos();
        resultadoAsesoriaOtrasCarterasDisclaimerInvertir.tapIrAInvertir();

        /********************************
         * INVERTIR EN CARTERAS AUTOGESTIONADAS
         * ******************************/
        InvertirEnCarterasAutogestionadas invertirEnCarterasAutogestionadas = new InvertirEnCarterasAutogestionadas();
        invertirEnCarterasAutogestionadas.validarIngresoPantalla();
        invertirEnCarterasAutogestionadas.validarElementos();
        invertirEnCarterasAutogestionadas.ingresarMonto("5000");
        invertirEnCarterasAutogestionadas.tapContinuar();

        /********************************
         * CONFIRMAR INVERSIÓN DETALLE
         * ******************************/
        ConfirmarInversionDetalle confirmarInversionDetalle = new ConfirmarInversionDetalle();
        confirmarInversionDetalle.validarIngresoPantalla();
        confirmarInversionDetalle.validarElementos();

        /********************************
         * CONFIRMAR INVERSIÓN COMPROBANTE
         * ******************************/
        ConfirmarInversionComprobante confirmarInversionComprobante = new ConfirmarInversionComprobante();
        confirmarInversionComprobante.validarElementos();
        confirmarInversionComprobante.ingresarCorreo("test@test.cl");

        /********************************
         * CONFIRMAR INVERSIÓN DOCS ANEXOS
         * ******************************/
        ConfirmarInversionDocsAnexos confirmarInversionDocsAnexos = new ConfirmarInversionDocsAnexos();
        confirmarInversionDocsAnexos.validarElementos();

        /********************************
         * CONFIRMAR INVERSIÓN TYC
         * ******************************/
        ConfirmarInversionTYC confirmarInversionTYC = new ConfirmarInversionTYC();
        confirmarInversionTYC.validaElementos();
        confirmarInversionTYC.checkCGF();
        confirmarInversionTYC.tapInvertir();

        /********************************
         * SOLICITUD EXITOSA
         * ******************************/
        SolicitudExitosa solicitudExitosa = new SolicitudExitosa();
        solicitudExitosa.validarIngresoPantalla();
        solicitudExitosa.validarElementos("FueraHorario");
        solicitudExitosa.tapFinalizar();
    }
}
