package testClasses.FFMM.PerfilarCliente;

import org.testng.annotations.Test;
import pages.Dashboard.DashboardInversiones;
import pages.FFMM.*;
import pages.Modales.ModalSinPerfil;
import testbase.TestBase;

public class CPA00005DesplegarModalCuandoClienteNoPoseePerfilDeInversionistaEnApp extends TestBase {

    @Test
    public void CPA00005DesplegarModalCuandoClienteNoPoseePerfilDeInversionistaEnApp() throws InterruptedException {


        /********************************
         * DASHBOARD
         ********************************/
        DashboardInversiones dashboardInversiones = new DashboardInversiones();
        dashboardInversiones.validarIngresoPantalla();
        dashboardInversiones.validarElementosPerfilamiento("SinPerfil");
        dashboardInversiones.validarElementosAlternativasInversion();
        dashboardInversiones.validarElementosAsesoria();
        dashboardInversiones.tapFFMM();

        /********************************
         * CARTERAS AUTOGESTIONADAS
         ********************************/
        CarterasAutogestionadas carterasAutogestionadas = new CarterasAutogestionadas();
        carterasAutogestionadas.validarIngresoPantalla();
        carterasAutogestionadas.validarElementos("Dashboard");
        carterasAutogestionadas.validarCarteras();
        carterasAutogestionadas.seleccionarCartera("Gestión Ahorro Corto Plazo");

        /********************************
         * DETALLE CARTERA
         ********************************/
        DetalleCartera detalleCartera = new DetalleCartera();
        detalleCartera.validarIngresoPantalla();
        detalleCartera.validarElementosCartera();

        /********************************
         * DETALLE CARTERA ESTADISTICAS RENTABILIDAD
         ********************************/
        DetalleCarteraEstadisticasRentabilidad detalleCarteraEstadisticasRentabilidad = new DetalleCarteraEstadisticasRentabilidad();
        detalleCarteraEstadisticasRentabilidad.validarElementos();
        detalleCarteraEstadisticasRentabilidad.validarRentabilidadAnual();
        detalleCarteraEstadisticasRentabilidad.validarAcumuladoAnio();

        /********************************
         * DETALLE CARTERA VALOR CUOTA
         ********************************/
        DetalleCarteraValorCuota detalleCarteraValorCuota = new DetalleCarteraValorCuota();
        detalleCarteraValorCuota.validarElementos();
        detalleCarteraValorCuota.validarValorCuota();

        /********************************
         * DETALLE CARTERA DOCS ANEXOS
         * ******************************/
        DetalleCarteraDocsAnexos detalleCarteraDocsAnexos = new DetalleCarteraDocsAnexos();
        detalleCarteraDocsAnexos.validarElementos();

        /********************************
         * DETALLE CARTERA INVERTIR
         * ******************************/
        DetalleCarteraInvertir detalleCarteraInvertir = new DetalleCarteraInvertir();
        detalleCarteraInvertir.validarElementos();

        /********************************
         * MODAL CLIENTE NO PERFILADO
         * ******************************/
        ModalSinPerfil modalSinPerfil = new ModalSinPerfil();
        modalSinPerfil.tapBotonInvertir();
        modalSinPerfil.validarDespliegueModal();
        modalSinPerfil.validarElementosModal();
    }
}
