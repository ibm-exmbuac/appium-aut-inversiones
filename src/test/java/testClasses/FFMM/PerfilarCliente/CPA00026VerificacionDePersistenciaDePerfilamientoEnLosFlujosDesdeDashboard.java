package testClasses.FFMM.PerfilarCliente;

import constants.Tabs;
import org.testng.annotations.Test;
import pages.Dashboard.DashboardInversiones;
import pages.FFMM.*;
import pages.Perfilamiento.*;
import testbase.TestBase;
import util.Gestures;

public class CPA00026VerificacionDePersistenciaDePerfilamientoEnLosFlujosDesdeDashboard extends TestBase {

    @Test
    public void CPA00026VerificacionDePersistenciaDePerfilamientoEnLosFlujosDesdeDashboard() throws InterruptedException {

        Gestures ges = new Gestures();

        /********************************
         * LOGIN
         ********************************/

        loginRapido("17360130-1", "226872");
        tabSwitcher(Tabs.Inversiones);

        /********************************
         * DASHBOARD
         ********************************/
        DashboardInversiones dashboardInversiones = new DashboardInversiones();
        dashboardInversiones.validarIngresoPantalla();
        dashboardInversiones.validarElementosPerfilamiento("SinPerfil");
        dashboardInversiones.validarElementosAlternativasInversion();
        dashboardInversiones.validarElementosAsesoria();
        dashboardInversiones.tapPerfilamiento();

        /********************************
         * PERFILAMIETO PREGUNTA 1
         ********************************/
        PerfilamientoPregunta1 perfilamientoPregunta1 = new PerfilamientoPregunta1();
        perfilamientoPregunta1.validarIngresoPantalla();
        perfilamientoPregunta1.validarElementosCabecera();
        perfilamientoPregunta1.validarElementosPreguntaRespuestas();
        perfilamientoPregunta1.seleccionarRespuesta("1");

        /********************************
         * PERFILAMIETO PREGUNTA 2
         ********************************/
        PerfilamientoPregunta2 perfilamientoPregunta2 = new PerfilamientoPregunta2();
        perfilamientoPregunta2.validarIngresoPantalla();
        perfilamientoPregunta2.validarElementosCabecera();
        perfilamientoPregunta2.validarElementosPreguntaRespuestas();
        perfilamientoPregunta2.seleccionarRespuesta("1");

        /********************************
         * PERFILAMIETO PREGUNTA 3
         ********************************/
        PerfilamientoPregunta3 perfilamientoPregunta3 = new PerfilamientoPregunta3();
        perfilamientoPregunta3.validarIngresoPantalla();
        perfilamientoPregunta3.validarElementosCabecera();
        perfilamientoPregunta3.validarElementosPreguntaRespuestas();
        perfilamientoPregunta3.seleccionarRespuesta("1");

        /********************************
         * PERFILAMIETO PREGUNTA 4
         ********************************/
        PerfilamientoPregunta4 perfilamientoPregunta4 = new PerfilamientoPregunta4();
        perfilamientoPregunta4.validarIngresoPantalla();
        perfilamientoPregunta4.validarElementosCabecera();
        perfilamientoPregunta4.validarElementosPreguntaRespuesta();
        perfilamientoPregunta4.seleccionarRespuesta("1");

        /********************************
         * PERFILAMIETO PREGUNTA 5
         ********************************/
        PerfilamientoPregunta5 perfilamientoPregunta5 = new PerfilamientoPregunta5();
        perfilamientoPregunta5.validarIngresoPantalla();
        perfilamientoPregunta5.validarElementosCabecera();
        perfilamientoPregunta5.validarElementosPreguntaRespuestas();
        perfilamientoPregunta5.seleccionarRespuesta("1");

        /********************************
         * RESULTADO PERFILAMIENTO
         ********************************/
        ResultadoPerfilamiento resultadoPerfilamiento = new ResultadoPerfilamiento();
        resultadoPerfilamiento.validarIngresoPantalla();
        resultadoPerfilamiento.validarElementos();
        resultadoPerfilamiento.tapComenzarInvertir();

        /********************************
         * CARTERAS AUTOGESTIONADAS
         ********************************/
        CarterasAutogestionadas carterasAutogestionadas = new CarterasAutogestionadas();
        carterasAutogestionadas.validarIngresoPantalla();
        carterasAutogestionadas.validarElementos("Dashboard");
        carterasAutogestionadas.validarCarteras();
        carterasAutogestionadas.seleccionarCartera("Gestión Ahorro Corto Plazo");

        /********************************
         * DETALLE CARTERA
         ********************************/
        DetalleCartera detalleCartera = new DetalleCartera();
        detalleCartera.validarIngresoPantalla();
        detalleCartera.validarElementosPerfil();
        detalleCartera.validarElementosCartera();
        ges.scrollAbajoCorto();

        /********************************
         * DETALLE CARTERA ESTADISTICAS RENTABILIDAD
         ********************************/
        DetalleCarteraEstadisticasRentabilidad detalleCarteraEstadisticasRentabilidad = new DetalleCarteraEstadisticasRentabilidad();
        detalleCarteraEstadisticasRentabilidad.validarElementos();
        detalleCarteraEstadisticasRentabilidad.validarRentabilidadAnual();
        detalleCarteraEstadisticasRentabilidad.validarAcumuladoAnio();
        ges.scrollAbajoCorto();

        /********************************
         * DETALLE CARTERA VALOR CUOTA
         ********************************/
        DetalleCarteraValorCuota detalleCarteraValorCuota = new DetalleCarteraValorCuota();
        detalleCarteraValorCuota.validarElementos();
        detalleCarteraValorCuota.validarValorCuota();
        ges.scrollAbajoCorto();

        /********************************
         * DETALLE CARTERA DOCS ANEXOS
         * ******************************/
        DetalleCarteraDocsAnexos detalleCarteraDocsAnexos = new DetalleCarteraDocsAnexos();
        detalleCarteraDocsAnexos.validarElementos();

        /********************************
         * DETALLE CARTERA INVERTIR
         * ******************************/
        DetalleCarteraInvertir detalleCarteraInvertir = new DetalleCarteraInvertir();
        detalleCarteraInvertir.validarElementos();
        detalleCarteraInvertir.tapIrAInvertir();

        /********************************
         * INVERTIR EN CARTERAS AUTOGESTIONADAS
         * ******************************/
        InvertirEnCarterasAutogestionadas invertirEnCarterasAutogestionadas = new InvertirEnCarterasAutogestionadas();
        invertirEnCarterasAutogestionadas.validarIngresoPantalla();
        invertirEnCarterasAutogestionadas.validarElementos();
        invertirEnCarterasAutogestionadas.ingresarMonto("5000");
        invertirEnCarterasAutogestionadas.tapContinuar();

        /********************************
         * CONFIRMAR INVERSIÓN DETALLE
         * ******************************/
        ConfirmarInversionDetalle confirmarInversionDetalle = new ConfirmarInversionDetalle();
        confirmarInversionDetalle.validarIngresoPantalla();
        confirmarInversionDetalle.validarElementos();

        /********************************
         * CONFIRMAR INVERSIÓN COMPROBANTE
         * ******************************/
        ConfirmarInversionComprobante confirmarInversionComprobante = new ConfirmarInversionComprobante();
        confirmarInversionComprobante.validarElementos();
        confirmarInversionComprobante.ingresarCorreo("test@test.cl");
        ges.scrollAbajoCorto();

        /********************************
         * CONFIRMAR INVERSIÓN DOCS ANEXOS
         * ******************************/
        ConfirmarInversionDocsAnexos confirmarInversionDocsAnexos = new ConfirmarInversionDocsAnexos();
        confirmarInversionDocsAnexos.validarElementos();

        /********************************
         * CONFIRMAR INVERSIÓN TYC
         * ******************************/
        ConfirmarInversionTYC confirmarInversionTYC = new ConfirmarInversionTYC();
        confirmarInversionTYC.checkCGF();
        confirmarInversionTYC.tapInvertir();

        /********************************
         * SOLICITUD EXITOSA
         * ******************************/
        SolicitudExitosa solicitudExitosa = new SolicitudExitosa();
        solicitudExitosa.validarIngresoPantalla();
        solicitudExitosa.validarElementos("FueraHorario");
        solicitudExitosa.tapFinalizar();

        /********************************
         * DASHBOARD
         ********************************/
        dashboardInversiones.validarIngresoPantalla();
        dashboardInversiones.validarElementosPerfilamiento("ConPerfil");
        dashboardInversiones.validarElementosAlternativasInversion();
        dashboardInversiones.validarElementosAsesoria();
        dashboardInversiones.tapVolverAPerfilar();

        /********************************
         * PERFILAMIETO PREGUNTA 1
         ********************************/
        perfilamientoPregunta1.validarIngresoPantalla();
        perfilamientoPregunta1.validarElementosCabecera();
        perfilamientoPregunta1.validarElementosPreguntaRespuestas();
        perfilamientoPregunta1.tapBotonSalirPerfilamiento();
        perfilamientoPregunta1.validarModalSalir();
        perfilamientoPregunta1.tapBotonSalirModal();


    }


}
