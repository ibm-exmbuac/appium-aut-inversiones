package testClasses.FFMM.PerfilarCliente;

import org.testng.annotations.Test;
import pages.Dashboard.DashboardInversiones;
import pages.Perfilamiento.*;
import testbase.TestBase;

public class CPA00001PerfilarAClienteEnFFMM extends TestBase {

    @Test
    public void CPA00001PerfilarAClienteEnFFMM() throws Exception {

        /********************************
         * DASHBOARD
         ********************************/
        DashboardInversiones dashboardInversiones = new DashboardInversiones();
        dashboardInversiones.validarIngresoPantalla();
        dashboardInversiones.validarElementosPerfilamiento("SinPerfil");
        dashboardInversiones.validarElementosAlternativasInversion();
        dashboardInversiones.validarElementosAsesoria();
        dashboardInversiones.tapPerfilamiento();

        /********************************
         * PERFILAMIETO PREGUNTA 1
         ********************************/
        PerfilamientoPregunta1 perfilamientoPregunta1 = new PerfilamientoPregunta1();
        perfilamientoPregunta1.validarIngresoPantalla();
        perfilamientoPregunta1.validarElementosCabecera();
        perfilamientoPregunta1.validarElementosPreguntaRespuestas();
        perfilamientoPregunta1.seleccionarRespuesta("1");

        /********************************
         * PERFILAMIETO PREGUNTA 2
         ********************************/
        PerfilamientoPregunta2 perfilamientoPregunta2 = new PerfilamientoPregunta2();
        perfilamientoPregunta2.validarIngresoPantalla();
        perfilamientoPregunta2.validarElementosCabecera();
        perfilamientoPregunta2.validarElementosPreguntaRespuestas();
        perfilamientoPregunta2.seleccionarRespuesta("1");

        /********************************
         * PERFILAMIETO PREGUNTA 3
         ********************************/
        PerfilamientoPregunta3 perfilamientoPregunta3 = new PerfilamientoPregunta3();
        perfilamientoPregunta3.validarIngresoPantalla();
        perfilamientoPregunta3.validarElementosCabecera();
        perfilamientoPregunta3.validarElementosPreguntaRespuestas();
        perfilamientoPregunta3.seleccionarRespuesta("1");

        /********************************
         * PERFILAMIETO PREGUNTA 4
         ********************************/
        PerfilamientoPregunta4 perfilamientoPregunta4 = new PerfilamientoPregunta4();
        perfilamientoPregunta4.validarIngresoPantalla();
        perfilamientoPregunta4.validarElementosCabecera();
        perfilamientoPregunta4.validarElementosPreguntaRespuesta();
        perfilamientoPregunta4.seleccionarRespuesta("1");

        /********************************
         * PERFILAMIETO PREGUNTA 5
         ********************************/
        PerfilamientoPregunta5 perfilamientoPregunta5 = new PerfilamientoPregunta5();
        perfilamientoPregunta5.validarIngresoPantalla();
        perfilamientoPregunta5.validarElementosCabecera();
        perfilamientoPregunta5.validarElementosPreguntaRespuestas();
        perfilamientoPregunta5.seleccionarRespuesta("1");

        /********************************
         * RESULTADO PERFILAMIENTO
         ********************************/
        ResultadoPerfilamiento resultadoPerfilamiento = new ResultadoPerfilamiento();
        resultadoPerfilamiento.validarIngresoPantalla();
//        resultadoPerfilamiento.validarElementos();
//        resultadoPerfilamiento.tapFinalizar();
    }
}
