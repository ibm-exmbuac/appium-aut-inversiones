package testClasses.FFMM.PerfilarCliente;


import constants.Tabs;
import org.testng.annotations.Test;
import pages.Dashboard.DashboardInversiones;
import pages.FFMM.*;
import pages.Modales.ModalSinPerfil;
import pages.Perfilamiento.*;
import testbase.TestBase;
import util.Gestures;

public class CPA00025VerificacionDePersistenciaDePerfiamientoEnLosFlujosDesdeFFMM extends TestBase {

    @Test
    public void CPA00025VerificacionDePersistenciaDePerfiamientoEnLosFlujosDesdeFFMM() throws Exception {

        Gestures ges = new Gestures();

        /********************************
         * LOGIN
         ********************************/

        loginRapido("13560049-0", "111222");
        tabSwitcher(Tabs.Inversiones);

        /********************************
         * DASHBOARD
         ********************************/
        DashboardInversiones dashboardInversiones = new DashboardInversiones();
        dashboardInversiones.validarIngresoPantalla();
        dashboardInversiones.validarElementosPerfilamiento("SinPeril");
        dashboardInversiones.validarElementosAlternativasInversion();
        dashboardInversiones.validarElementosAsesoria();
        dashboardInversiones.tapFFMM();

        /********************************
         * CARTERAS AUTOGESTIONADAS
         ********************************/
        CarterasAutogestionadas carterasAutogestionadas = new CarterasAutogestionadas();
        carterasAutogestionadas.validarIngresoPantalla();
        carterasAutogestionadas.validarElementos("Dashboard");
        carterasAutogestionadas.validarCarteras();
        carterasAutogestionadas.seleccionarCartera("Gestión Ahorro Corto Plazo");

        /********************************
         * DETALLE CARTERA
         ********************************/
        DetalleCartera detalleCartera = new DetalleCartera();
        detalleCartera.validarIngresoPantalla();
        detalleCartera.validarElementosCartera();
        ges.scrollAbajoCorto();

        /********************************
         * DETALLE CARTERA ESTADISTICAS RENTABILIDAD
         ********************************/
        DetalleCarteraEstadisticasRentabilidad detalleCarteraEstadisticasRentabilidad = new DetalleCarteraEstadisticasRentabilidad();
        detalleCarteraEstadisticasRentabilidad.validarElementos();
        detalleCarteraEstadisticasRentabilidad.validarRentabilidadAnual();
        detalleCarteraEstadisticasRentabilidad.validarAcumuladoAnio();
        ges.scrollAbajoCorto();

        /********************************
         * DETALLE CARTERA VALOR CUOTA
         ********************************/
        DetalleCarteraValorCuota detalleCarteraValorCuota = new DetalleCarteraValorCuota();
        detalleCarteraValorCuota.validarElementos();
        detalleCarteraValorCuota.validarValorCuota();
        ges.scrollAbajoCorto();

        /********************************
         * DETALLE CARTERA DOCS ANEXOS
         * ******************************/
        DetalleCarteraDocsAnexos detalleCarteraDocsAnexos = new DetalleCarteraDocsAnexos();
        detalleCarteraDocsAnexos.validarElementos();

        /********************************
         * DETALLE CARTERA INVERTIR
         * ******************************/
        DetalleCarteraInvertir detalleCarteraInvertir = new DetalleCarteraInvertir();
        detalleCarteraInvertir.validarElementos();

        /********************************
         * MODAL SIN PERFIL
         * ******************************/
        ModalSinPerfil modalSinPerfil = new ModalSinPerfil();
        modalSinPerfil.tapBotonInvertir();
        modalSinPerfil.validarDespliegueModal();
        modalSinPerfil.validarElementosModal();
        modalSinPerfil.tapBotonEnOtroMomento();
        modalSinPerfil.tapBotonInvertir();
        modalSinPerfil.tapBotonConocerMiPerfil();

        /********************************
         * PERFILAMIETO PREGUNTA 1
         ********************************/
        PerfilamientoPregunta1 perfilamientoPregunta1 = new PerfilamientoPregunta1();
        perfilamientoPregunta1.validarIngresoPantalla();
        perfilamientoPregunta1.validarElementosPreguntaRespuestas();
        perfilamientoPregunta1.seleccionarRespuesta("1");

        /********************************
         * PERFILAMIETO PREGUNTA 2
         ********************************/
        PerfilamientoPregunta2 perfilamientoPregunta2 = new PerfilamientoPregunta2();
        perfilamientoPregunta2.validarIngresoPantalla();
        perfilamientoPregunta2.validarElementosCabecera();
        perfilamientoPregunta2.validarElementosPreguntaRespuestas();
        perfilamientoPregunta2.seleccionarRespuesta("1");

        /********************************
         * PERFILAMIETO PREGUNTA 3
         ********************************/
        PerfilamientoPregunta3 perfilamientoPregunta3 = new PerfilamientoPregunta3();
        perfilamientoPregunta3.validarIngresoPantalla();
        perfilamientoPregunta3.validarElementosCabecera();
        perfilamientoPregunta3.validarElementosPreguntaRespuestas();
        perfilamientoPregunta3.seleccionarRespuesta("1");

        /********************************
         * PERFILAMIETO PREGUNTA 4
         ********************************/
        PerfilamientoPregunta4 perfilamientoPregunta4 = new PerfilamientoPregunta4();
        perfilamientoPregunta4.validarIngresoPantalla();
        perfilamientoPregunta4.validarElementosCabecera();
        perfilamientoPregunta4.validarElementosPreguntaRespuesta();
        perfilamientoPregunta4.seleccionarRespuesta("1");

        /********************************
         * PERFILAMIETO PREGUNTA 5
         ********************************/
        PerfilamientoPregunta5 perfilamientoPregunta5 = new PerfilamientoPregunta5();
        perfilamientoPregunta5.validarIngresoPantalla();
        perfilamientoPregunta5.validarElementosCabecera();
        perfilamientoPregunta5.validarElementosPreguntaRespuestas();
        perfilamientoPregunta5.seleccionarRespuesta("1");

        /********************************
         * RESULTADO PERFILAMIENTO
         ********************************/
        ResultadoPerfilamiento resultadoPerfilamiento = new ResultadoPerfilamiento();
        resultadoPerfilamiento.validarIngresoPantalla();
        resultadoPerfilamiento.validarElementos();
        resultadoPerfilamiento.tapLinkVolverAResponderCuestionario();

        /********************************
         * PERFILAMIETO PREGUNTA 1
         ********************************/
        perfilamientoPregunta1.validarIngresoPantalla();
        perfilamientoPregunta1.validarElementosPreguntaRespuestas();
        perfilamientoPregunta1.tapBotonSalirPerfilamiento();
        perfilamientoPregunta1.validarModalSalir();
        perfilamientoPregunta1.tapBotonSalirModal();

        /********************************
         * DETALLE CARTERA
         ********************************/
        detalleCartera.validarIngresoPantalla();
        detalleCartera.validarElementosPerfil();
        detalleCartera.validarElementosCartera();

        /********************************
         * DETALLE CARTERA ESTADISTICAS RENTABILIDAD
         ********************************/
        detalleCarteraEstadisticasRentabilidad.validarElementos();
        detalleCarteraEstadisticasRentabilidad.validarRentabilidadAnual();
        detalleCarteraEstadisticasRentabilidad.validarAcumuladoAnio();

        /********************************
         * DETALLE CARTERA VALOR CUOTA
         ********************************/
        detalleCarteraValorCuota.validarElementos();
        detalleCarteraValorCuota.validarValorCuota();

        /********************************
         * DETALLE CARTERA DOCS ANEXOS
         * ******************************/
        detalleCarteraDocsAnexos.validarElementos();

        /********************************
         * DETALLE CARTERA INVERTIR
         * ******************************/
        detalleCarteraInvertir.validarElementos();
    }
}
