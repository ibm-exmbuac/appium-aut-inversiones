package testClasses.FFMM.Invertir;

import org.testng.annotations.Test;
import pages.Dashboard.DashboardInversiones;
import pages.FFMM.*;
import testbase.TestBase;

public class CPA00004InvertirFueraDeHorarioApp extends TestBase {

    @Test
    public void CPA00004InvertirFueraDeHorarioApp() throws Exception {


        /********************************
         * DASHBOARD
         ********************************/
        DashboardInversiones dashboardInversiones = new DashboardInversiones();
        dashboardInversiones.validarIngresoPantalla();
        dashboardInversiones.validarElementosPerfilamiento("ConPerfil");
        dashboardInversiones.validarElementosAlternativasInversion();
        dashboardInversiones.validarElementosAsesoria();
        dashboardInversiones.tapFFMM();

        /********************************
         * CARTERAS AUTOGESTIONADAS
         ********************************/
        CarterasAutogestionadas carterasAutogestionadas = new CarterasAutogestionadas();
        carterasAutogestionadas.validarIngresoPantalla();
        carterasAutogestionadas.validarElementos("Dashboard");
        carterasAutogestionadas.validarCarteras();
        carterasAutogestionadas.seleccionarCartera("Gestión Ahorro Corto Plazo");

        /********************************
         * DETALLE CARTERA
         ********************************/
        DetalleCartera detalleCartera = new DetalleCartera();
        detalleCartera.validarIngresoPantalla();
        detalleCartera.validarElementosPerfil();
        detalleCartera.validarElementosCartera();

        /********************************
         * DETALLE CARTERA ESTADISTICAS RENTABILIDAD
         ********************************/
        DetalleCarteraEstadisticasRentabilidad detalleCarteraEstadisticasRentabilidad = new DetalleCarteraEstadisticasRentabilidad();
        detalleCarteraEstadisticasRentabilidad.validarElementos();
        detalleCarteraEstadisticasRentabilidad.validarRentabilidadAnual();
        detalleCarteraEstadisticasRentabilidad.validarAcumuladoAnio();

        /********************************
         * DETALLE CARTERA VALOR CUOTA
         ********************************/
        DetalleCarteraValorCuota detalleCarteraValorCuota = new DetalleCarteraValorCuota();
        detalleCarteraValorCuota.validarElementos();
        detalleCarteraValorCuota.validarValorCuota();

        /********************************
         * DETALLE CARTERA DOCS ANEXOS
         * ******************************/
        DetalleCarteraDocsAnexos detalleCarteraDocsAnexos = new DetalleCarteraDocsAnexos();
        detalleCarteraDocsAnexos.validarElementos();

        /********************************
         * DETALLE CARTERA INVERTIR
         * ******************************/
        DetalleCarteraInvertir detalleCarteraInvertir = new DetalleCarteraInvertir();
        detalleCarteraInvertir.validarElementos();
        detalleCarteraInvertir.tapIrAInvertir();

        /********************************
         * INVERTIR EN CARTERAS AUTOGESTIONADAS
         * ******************************/
        InvertirEnCarterasAutogestionadas invertirEnCarterasAutogestionadas = new InvertirEnCarterasAutogestionadas();
        invertirEnCarterasAutogestionadas.validarIngresoPantalla();
        invertirEnCarterasAutogestionadas.validarElementos();
        invertirEnCarterasAutogestionadas.ingresarMonto("5000");
        invertirEnCarterasAutogestionadas.tapContinuar();

        /********************************
         * CONFIRMAR INVERSIÓN DETALLE
         * ******************************/
        ConfirmarInversionDetalle confirmarInversionDetalle = new ConfirmarInversionDetalle();
        confirmarInversionDetalle.validarIngresoPantalla();
        confirmarInversionDetalle.validarElementos();

        /********************************
         * CONFIRMAR INVERSIÓN COMPROBANTE
         * ******************************/
        ConfirmarInversionComprobante confirmarInversionComprobante = new ConfirmarInversionComprobante();
        confirmarInversionComprobante.validarElementos();
        confirmarInversionComprobante.ingresarCorreo("test@test.cl");

        /********************************
         * CONFIRMAR INVERSIÓN DOCS ANEXOS
         * ******************************/
        ConfirmarInversionDocsAnexos confirmarInversionDocsAnexos = new ConfirmarInversionDocsAnexos();
        confirmarInversionDocsAnexos.validarElementos();

        /********************************
         * CONFIRMAR INVERSIÓN TYC
         * ******************************/
        ConfirmarInversionTYC confirmarInversionTYC = new ConfirmarInversionTYC();
        confirmarInversionTYC.validaElementos();
        confirmarInversionTYC.checkCGF();
        confirmarInversionTYC.tapInvertir();

        /********************************
         * SOLICITUD EXITOSA
         * ******************************/
        SolicitudExitosa solicitudExitosa = new SolicitudExitosa();
        solicitudExitosa.validarIngresoPantalla();
        solicitudExitosa.validarElementos("FueraHorario");
        solicitudExitosa.tapFinalizar();
    }
}